﻿using System;
using System.Collections.Generic;
using System.Data;

namespace TN_User
{
    public class SessionUser
    {
        static string _ClientUser = "thuytientien";
        static string _ClientPass = "thuytientien2020";
        private static List<User_Role_Info> _TableRole = new List<User_Role_Info>();

        private static User_Info _UserLogin;
        private static DateTime _DateWork = DateTime.Now;

        public static DateTime DateWork
        {
            get
            {
                return _DateWork;
            }

            set
            {
                _DateWork = value;
            }
        }
        public static User_Info UserLogin
        {
            get
            {
                return _UserLogin;
            }
        }

        public static string ClientUser
        {
            get
            {
                return _ClientUser;
            }

            set
            {
                _ClientUser = value;
            }
        }
        public static string ClientPass
        {
            get
            {
                return _ClientPass;
            }

            set
            {
                _ClientPass = value;
            }
        }
        public static List<User_Role_Info> TableRole
        {
            get
            {
                return _TableRole;
            }

            set
            {
                _TableRole = value;
            }
        }

        public SessionUser()
        {
            _UserLogin = new User_Info();
            _TableRole = new List<User_Role_Info>();
        }
        public SessionUser(string UserKey)
        {
            _UserLogin = new User_Info(UserKey);
            _TableRole = Role_Data.ListRole_UserOfPartner(_UserLogin.PartnerNumber, _UserLogin.UserKey).CopyToList<User_Role_Info>();
        }
    }
}
