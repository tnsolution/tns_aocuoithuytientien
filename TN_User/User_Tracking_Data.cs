﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN_Connection;

namespace TN_User
{
    public class User_Tracking_Data
    {
        public static string InsertTracking(string UserKey, string[] TrackingData)
        {
            string zResult = "";
            string zSQL = @"INSERT INTO dbo.SYS_User_Tracking " +
                "(UserKey , BrowserType, BrowserName,BrowserVersion ,BrowserPlatform ,IsMobileDevice,UserAddress,UserAgent,FailedPassword) " +
                "VALUES " +
                "(@UserKey, @BrowserType, @BrowserName, @BrowserVersion, @BrowserPlatform, @IsMobileDevice, @UserAddress, @UserAgent,@FailedPassword)";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(UserKey);
                zCommand.Parameters.Add("@BrowserType", SqlDbType.NVarChar).Value = TrackingData[0];
                zCommand.Parameters.Add("@BrowserName", SqlDbType.NVarChar).Value = TrackingData[1];
                zCommand.Parameters.Add("@BrowserVersion", SqlDbType.NVarChar).Value = TrackingData[2];
                zCommand.Parameters.Add("@BrowserPlatform", SqlDbType.NVarChar).Value = TrackingData[3];
                zCommand.Parameters.Add("@IsMobileDevice", SqlDbType.Int).Value = int.Parse(TrackingData[4]);
                zCommand.Parameters.Add("@UserAddress", SqlDbType.NVarChar).Value = TrackingData[5];
                zCommand.Parameters.Add("@UserAgent", SqlDbType.NVarChar).Value = TrackingData[6];
                zCommand.Parameters.Add("@FailedPassword", SqlDbType.Int).Value = int.Parse(TrackingData[7]);
                zResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zResult = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;

        }

        public static DataTable History(string UserKey, int AmountTop)
        {
            DataTable zResult = new DataTable();

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();

                //------------------------------------------------------------------
                string zSQL = @"SELECT TOP " + AmountTop.ToString() + " * FROM SYS_User_Tracking A  " +
                            " WHERE UserKey = @UserKey " +
                            " ORDER BY DateLogin DESC";
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(UserKey);
                SqlDataAdapter zListMenu = new SqlDataAdapter(zCommand);
                zListMenu.Fill(zResult);

                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zMeassage = ex.ToString();
            }

            return zResult;
        }
    }
}
