﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN_Connection;

namespace TN_User
{
    public class Role_Info
    {

        #region [ Field Name ]
        private string _RoleKey = "";
        private string _RoleName = "";
        private string _RoleID = "";
        private string _RoleURL = "";
        private string _Description = "";
        private string _Module = "";
        private int _Category = 0;
        private int _Rank = 0;
        private int _Slug = 0;
        private int _RecordStatus = 0;
        private DateTime? _CreatedOn = null;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime? _ModifiedOn = null;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Role_Info()
        {
            Guid zNewID = Guid.NewGuid();
            _RoleKey = zNewID.ToString();
        }
        public Role_Info(string Key)
        {
            if (Key.Length != 36)
                return;
            string zSQL = "SELECT * FROM [dbo].[SYS_Role] WHERE RoleKey = @RoleKey AND RecordStatus != 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = new Guid(Key);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _RoleKey = zReader["RoleKey"].ToString();
                    _RoleName = zReader["RoleName"].ToString();
                    _RoleID = zReader["RoleID"].ToString();
                    _RoleURL = zReader["RoleURL"].ToString();
                    _Description = zReader["Description"].ToString();
                    _Module = zReader["Module"].ToString();
                    _Category = int.Parse(zReader["Category"].ToString());
                    _Rank = int.Parse(zReader["Rank"].ToString());
                    _Slug = int.Parse(zReader["Slug"].ToString());
                    _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Properties ]
        public string RoleKey
        {
            get { return _RoleKey; }
            set { _RoleKey = value; }
        }
        public string RoleName
        {
            get { return _RoleName; }
            set { _RoleName = value; }
        }
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public string RoleURL
        {
            get { return _RoleURL; }
            set { _RoleURL = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string Module
        {
            get { return _Module; }
            set { _Module = value; }
        }
        public int Category
        {
            get { return _Category; }
            set { _Category = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime? CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime? ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create_ServerKey()
        {
            _Message = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO [dbo].[SYS_Role] (" +
                         " RoleName ,RoleID ,RoleURL ,Description ,Module ,Category ,Rank ,Slug ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) " +
                         " VALUES ( " +
                         " @RoleName ,@RoleID ,@RoleURL ,@Description ,@Module ,@Category ,@Rank ,@Slug ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RoleName", SqlDbType.NVarChar).Value = _RoleName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zCommand.Parameters.Add("@RoleURL", SqlDbType.NVarChar).Value = _RoleURL;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = _Module;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = _Category;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = 1;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                if (zResult == "1")
                {
                    _Message = "201 Created";
                    _RecordStatus = 1;
                }
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            _Message = "";
            //---------- String SQL Access Database ---------------
            string zSQL = " INSERT INTO [dbo].[SYS_Role] (" +
                          " RoleKey ,RoleName ,RoleID ,RoleURL ,Description ,Module ,Category ,Rank ,Slug ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) " +
                          " VALUES ( " +
                          " @RoleKey ,@RoleName ,@RoleID ,@RoleURL ,@Description ,@Module ,@Category ,@Rank ,@Slug ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = new Guid(_RoleKey);
                zCommand.Parameters.Add("@RoleName", SqlDbType.NVarChar).Value = _RoleName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zCommand.Parameters.Add("@RoleURL", SqlDbType.NVarChar).Value = _RoleURL;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = _Module;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = _Category;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = 1;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                if (zResult == "1")
                {
                    _Message = "201 Created";
                    _RecordStatus = 1;
                }

            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save(bool MakeKeyAtClient)
        {
            if (_RecordStatus == 0)
            {
                if (MakeKeyAtClient)
                    return Create_ClientKey();
                else
                    return Create_ServerKey();
            }
            else
            {
                return Update();
            }

        }
        public string Update()
        {
            string zSQL = "UPDATE [dbo].[SYS_Role] SET "
                        + " RoleName = @RoleName,"
                        + " RoleID = @RoleID,"
                        + " RoleURL = @RoleURL,"
                        + " Description = @Description,"
                        + " Module = @Module,"
                        + " Category = @Category,"
                        + " Rank = @Rank,"
                        + " Slug = @Slug,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE RoleKey = @RoleKey";
            string zResult = "";
            _Message = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = new Guid(_RoleKey);
                zCommand.Parameters.Add("@RoleName", SqlDbType.NVarChar).Value = _RoleName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zCommand.Parameters.Add("@RoleURL", SqlDbType.NVarChar).Value = _RoleURL;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = _Module;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = _Category;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                if (zResult == "1")
                    _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE [dbo].[SYS_Role] Set RecordStatus = 99 WHERE RoleKey = @RoleKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = new Guid(_RoleKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                if (zResult == "1")
                    _Message = "200 OK";
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            _Message = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM [dbo].[SYS_Role] WHERE RoleKey = @RoleKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = new Guid(_RoleKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                if (zResult == "1")
                    _Message = "200 OK";
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
