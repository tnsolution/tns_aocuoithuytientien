﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN_Connection;

namespace TN_User
{
    public class Role_Data
    {
        #region [ Standard Global ]
        public static DataTable List_Global(int PageNumber, int PageSize)
        {
            return List_Global(PageNumber, PageSize, "");
        }
        public static DataTable List_Global(int PageNumber, int PageSize, string OrderBy)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM dbo.SYS_Role WHERE RecordStatus != 99";
            if (OrderBy.Trim().Length > 0)
            {
                zSQL += " ORDER BY " + OrderBy;
            }
            else
            {
                zSQL += " ORDER BY RoleName ";
            }

            zSQL += " OFFSET @PageSize * (@PageNumber - 1) ROWS " +
                    " FETCH NEXT @PageSize ROWS ONLY OPTION(RECOMPILE)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = PageSize;
                zCommand.Parameters.Add("@PageNumber", SqlDbType.Int).Value = PageNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string Result = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Global(int PageNumber, int PageSize, string OrderBy, out string Error)
        {
            DataTable zTable = new DataTable();
            Error = "";
            string zSQL = @"SELECT * FROM dbo.SYS_Role WHERE RecordStatus != 99";
            if (OrderBy.Trim().Length > 0)
            {
                zSQL += " ORDER BY " + OrderBy;
            }
            else
            {
                zSQL += " ORDER BY RoleName ";
            }

            zSQL += " OFFSET @PageSize * (@PageNumber - 1) ROWS " +
                    " FETCH NEXT @PageSize ROWS ONLY OPTION(RECOMPILE)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = PageSize;
                zCommand.Parameters.Add("@PageNumber", SqlDbType.Int).Value = PageNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                Error = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Global(int PageNumber, int PageSize, string OrderBy, string SearchContent)
        {
            DataTable zTable = new DataTable();
            string zSQL = "";

            zSQL = @"SELECT * FROM dbo.SYS_Role WHERE RecordStatus != 99 " +
                   " AND (RoleName LIKE @SearchContent OR RoleURL LIKE @SearchContent OR Module LIKE @SearchContent) ";

            if (OrderBy.Trim().Length > 0)
            {
                zSQL += " ORDER BY " + OrderBy;
            }
            else
            {
                zSQL += " ORDER BY RoleName ";
            }

            zSQL += " OFFSET @PageSize * (@PageNumber - 1) ROWS " +
                    " FETCH NEXT @PageSize ROWS ONLY OPTION(RECOMPILE)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SearchContent", SqlDbType.NVarChar).Value = "%" + SearchContent + "%";
                zCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = PageSize;
                zCommand.Parameters.Add("@PageNumber", SqlDbType.Int).Value = PageNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string Result = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Global(int PageNumber, int PageSize, string OrderBy, string SearchContent, out string Error)
        {
            DataTable zTable = new DataTable();
            string zSQL = "";
            Error = "";
            zSQL = @"SELECT * FROM dbo.SYS_Role WHERE RecordStatus != 99 " +
                   " AND (RoleName LIKE @SearchContent OR RoleURL LIKE @SearchContent OR Module LIKE @SearchContent) ";

            if (OrderBy.Trim().Length > 0)
            {
                zSQL += " ORDER BY " + OrderBy;
            }
            else
            {
                zSQL += " ORDER BY RoleName ";
            }

            zSQL += " OFFSET @PageSize * (@PageNumber - 1) ROWS " +
                    " FETCH NEXT @PageSize ROWS ONLY OPTION(RECOMPILE)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SearchContent", SqlDbType.NVarChar).Value = "%" + SearchContent + "%";
                zCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = PageSize;
                zCommand.Parameters.Add("@PageNumber", SqlDbType.Int).Value = PageNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                Error = ex.ToString();
            }
            return zTable;
        }
        public static int Count_Global_Record()
        {
            string zMessage = "";
            int zQuantity = 0;
            string zSQL = "";

            zSQL = "SELECT Count(*) FROM dbo.SYS_Role WHERE RecordStatus != 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                string zResult = zCommand.ExecuteScalar().ToString();
                int.TryParse(zResult, out zQuantity);
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zMessage = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }

            return zQuantity;
        }
        public static int Count_Global_Record(string SearchContent)
        {
            int zQuantity = 0;
            string zMessage = "";
            DataTable zTable = new DataTable();
            string zSQL = "";

            zSQL = @"SELECT Count(*) FROM dbo.SYS_Role WHERE RecordStatus != 99 " +
                   " AND (RoleName LIKE @SearchContent OR RoleURL LIKE @SearchContent OR Module LIKE @SearchContent) ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SearchContent", SqlDbType.NVarChar).Value = "%" + SearchContent + "%";
                string zResult = zCommand.ExecuteScalar().ToString();
                int.TryParse(zResult, out zQuantity);
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zMessage = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }

            return zQuantity;
        }
        #endregion

        #region [ Config Global ]
        public static DataTable List(string PartnerNumber)
        {

            DataTable zTable = new DataTable();
            string zSQL = @" SELECT * FROM SYS_Role_Partner A " +
                        " LEFT JOIN dbo.SYS_Role B ON A.RoleKey = B.RoleKey" +
                        " WHERE A.PartnerNumber = @PartnerNumber ORDER BY RoleURL";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                SqlDataAdapter zUsers = new SqlDataAdapter(zCommand);
                zUsers.Fill(zTable);
                //---- Close Connect SQL ----
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }

            return zTable;
        }
        public static string InsertRoleToPartner(string PartnerNumber, string RoleKey)
        {

            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_Role_Partner "
                        + " VALUES (@RoleKey ,@PartnerNumber ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = new Guid(RoleKey);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zResult = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static string RemoveRoleOfPartner(string PartnerNumber, string RoleKey)
        {

            //---------- String SQL Access Database ---------------
            string zSQL = "SYS_Role_RemoveRoleOfPartner";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = new Guid(RoleKey);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zResult = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static string SetDefaultForSupport(string PartnerNumber, string UserKey)
        {

            string zSQL = "[dbo].[SYS_Role_SetRoleForSuppor]";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(UserKey);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zResult = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion

        public static DataTable ListRole_UserOfPartner(string PartnerNumber, string UserKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "[dbo].[SYS_Role_ListRole_UserOfPartner]";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(UserKey);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                SqlDataAdapter zData = new SqlDataAdapter(zCommand);
                zData.Fill(zTable);
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zResult = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zTable;
        }
        public static int Find_User(string UserKey, string RoleKey)
        {
            int zResult = 0;
            string zSQL = @"SELECT Count(*) FROM SYS_User_Role WHERE UserKey = @UserKey AND RoleKey = @RoleKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zCommand.Parameters.Add("@RoleKey", SqlDbType.NVarChar).Value = RoleKey;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zResult;
        }
    }
}
