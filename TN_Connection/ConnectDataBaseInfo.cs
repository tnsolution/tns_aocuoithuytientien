﻿namespace TN_Connection
{
    public class ConnectDataBaseInfo
    {
        private string _DataSource = @"";
        private string _DataBase = "";
        private string _User = "";
        private string _Password = "";

        private string _AttachDbFilename = "";
        private string _ConnectionString = "";

        private bool _IsConnectLocal = false;

        public ConnectDataBaseInfo()
        {

        }
        public string DataSource
        {
            get
            {
                return _DataSource;
            }
            set
            {
                _DataSource = value;
            }
        }
        public string DataBase
        {
            get
            {
                return _DataBase;
            }
            set
            {
                _DataBase = value;
            }
        }
        public string UserName
        {
            get
            {
                return _User;
            }
            set
            {
                _User = value;
            }
        }
        public string Password
        {
            get
            {
                return _Password;
            }
            set
            {
                _Password = value;
            }
        }
        public string AttachDbFilename
        {
            get
            {
                return _AttachDbFilename;
            }
            set
            {
                _AttachDbFilename = value;
            }
        }
        public bool IsConnectLocal
        {
            get
            {
                return _IsConnectLocal;
            }
            set
            {
                _IsConnectLocal = value;
            }
        }
        public string ConnectionString
        {
            get
            {
                if (!_IsConnectLocal)
                {
                    _ConnectionString = "Data Source = " + _DataSource + ";DataBase= " + _DataBase + ";User=" + _User + ";Password= " + _Password;
                }
                else
                {
                    _ConnectionString = "Data Source = " + _DataSource + ";AttachDbFilename= " + _AttachDbFilename + ";User Instance =true;Integrated Security= SSPI";
                }
                return _ConnectionString;
            }
            set
            {
                _ConnectionString = value;
            }
        }
    }
}
