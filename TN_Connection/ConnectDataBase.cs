﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace TN_Connection
{
    public class ConnectDataBase
    {
        private string _Message = "";
        private static SqlConnection _SQLConnect;
        private static string _ConnectionString = @"Data Source=103.77.166.6;Initial Catalog=TN_ERP_V01;Persist Security Info=True;User ID=mng;Password=123456Aa@";
        public ConnectDataBase()
        {
            _SQLConnect = new SqlConnection();
        }
        public ConnectDataBase(string StrConnect)
        {
            try
            {
                _SQLConnect = new SqlConnection();
                _SQLConnect.ConnectionString = StrConnect;
                _SQLConnect.Open();
                _ConnectionString = StrConnect;
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                _SQLConnect.Close();
            }

        }
        public void CloseConnect()
        {
            _SQLConnect.Close();
        }
        public string Message
        {
            get
            {
                return _Message;
            }
        }
        public static string ConnectionString
        {
            set
            {
                _ConnectionString = value;
            }
            get
            {
                return _ConnectionString;
            }

        }
        public static bool StillConnect
        {
            get
            {
                if (_SQLConnect == null || _SQLConnect.State == ConnectionState.Closed)
                    return false;
                else
                    return true;
            }
        }
    }
}
