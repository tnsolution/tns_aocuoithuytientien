﻿namespace TN_WinApp
{
    partial class Frm_FollowPrint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_FollowPrint));
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.rptViewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.TitleForm = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // rptViewer
            // 
            this.rptViewer.ActiveViewIndex = -1;
            this.rptViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rptViewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.rptViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptViewer.Location = new System.Drawing.Point(0, 42);
            this.rptViewer.Name = "rptViewer";
            this.rptViewer.ShowGroupTreeButton = false;
            this.rptViewer.ShowLogo = false;
            this.rptViewer.ShowParameterPanelButton = false;
            this.rptViewer.ShowTextSearchButton = false;
            this.rptViewer.Size = new System.Drawing.Size(933, 512);
            this.rptViewer.TabIndex = 168;
            this.rptViewer.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // TitleForm
            // 
            this.TitleForm.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnClose});
            this.TitleForm.Dock = System.Windows.Forms.DockStyle.Top;
            this.TitleForm.Location = new System.Drawing.Point(0, 0);
            this.TitleForm.Name = "TitleForm";
            this.TitleForm.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.TitleForm.Size = new System.Drawing.Size(933, 42);
            this.TitleForm.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.TitleForm.TabIndex = 167;
            this.TitleForm.Values.Description = "";
            this.TitleForm.Values.Heading = "";
            this.TitleForm.Values.Image = null;
            // 
            // Frm_FollowPrint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 554);
            this.Controls.Add(this.rptViewer);
            this.Controls.Add(this.TitleForm);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_FollowPrint";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer rptViewer;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader TitleForm;
    }
}