﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN_Connection;

namespace TN_User
{
    public class User_Info
    {
        #region [ Field Name ]
        private string _UserKey = "";
        private string _UserAPI = "";
        private string _UserName = "";
        private string _Password = "";
        private string _PIN = "000000";
        private string _Url = "";
        private string _Description = "";
        private string _GroupName = "";
        private int _BusinessKey = 0;
        private string _PartnerNumber = "";
        private bool _Activate;
        private DateTime _ExpireDate;
        private DateTime _LastLoginDate;
        private int _FailedPasswordCount = 0;

        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private int _Slug = 0;
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]

        public string UserKey
        {
            get { return _UserKey; }
            set { _UserKey = value; }
        }

        public string API
        {
            get { return _UserAPI; }
            set { _UserAPI = value; }
        }
        public string Name
        {
            get { return _UserName; }
            set { _UserName = value; }
        }
        public string Password
        {
            set
            {
                string zPassword = value;
                _Password = MyCryptography.HashPass(zPassword);
            }
            get { return _Password; }
        }
        public string PIN
        {
            get { return _PIN; }
            set { _PIN = value; }
        }
        public string Url
        {
            get { return _Url; }
            set { _Url = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string GroupName
        {
            get { return _GroupName; }
            set { _GroupName = value; }
        }
        public int BusinessKey
        {
            get { return _BusinessKey; }
            set { _BusinessKey = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public bool Activate
        {
            get { return _Activate; }
            set { _Activate = value; }
        }
        public DateTime ExpireDate
        {
            get { return _ExpireDate; }
            set { _ExpireDate = value; }
        }
        public DateTime LastLoginDate
        {
            get { return _LastLoginDate; }
            set { _LastLoginDate = value; }
        }
        public int FailedPasswordCount
        {
            get { return _FailedPasswordCount; }
            set { _FailedPasswordCount = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else
                    return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #endregion

        #region [ Constructor Get Information ]
        public User_Info()
        {
            Guid zNewID = Guid.NewGuid();
            _UserKey = zNewID.ToString();

        }
        public User_Info(string Key)
        {
            if (Key.Length != 36)
                return;
            string zSQL = "SELECT * FROM SYS_User WHERE UserKey = @UserKey AND RecordStatus != 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(Key);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _UserKey = zReader["UserKey"].ToString();
                    _UserAPI = zReader["UserAPI"].ToString().Trim();
                    _UserName = zReader["UserName"].ToString().Trim();
                    _Password = zReader["Password"].ToString().Trim();
                    _PIN = zReader["PIN"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    _GroupName = zReader["GroupName"].ToString().Trim();
                    _BusinessKey = (int)zReader["BusinessKey"];
                    _PartnerNumber = zReader["PartnerNumber"].ToString().Trim();
                    _Activate = (bool)(zReader["Activate"]);
                    if (zReader["ExpireDate"] != DBNull.Value)
                        _ExpireDate = (DateTime)zReader["ExpireDate"];
                    if (zReader["LastLoginDate"] != DBNull.Value)
                        _LastLoginDate = (DateTime)zReader["LastLoginDate"];
                    _FailedPasswordCount = int.Parse(zReader["FailedPasswordCount"].ToString());
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    _Slug = int.Parse(zReader["Slug"].ToString());
                    _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }

                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public User_Info(string Partner, string Key)
        {
            if (Key.Length != 36)
                return;
            string zSQL = "SELECT * FROM SYS_User WHERE PartnerNumber = @PartnerNumber AND UserKey = @UserKey AND RecordStatus != 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(Key);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(Partner);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _UserKey = zReader["UserKey"].ToString();
                    _UserAPI = zReader["UserAPI"].ToString().Trim();
                    _UserName = zReader["UserName"].ToString().Trim();
                    _Password = zReader["Password"].ToString().Trim();
                    _PIN = zReader["PIN"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    _GroupName = zReader["GroupName"].ToString().Trim();
                    _BusinessKey = (int)zReader["BusinessKey"];
                    _PartnerNumber = zReader["PartnerNumber"].ToString().Trim();
                    _Activate = (bool)(zReader["Activate"]);
                    if (zReader["ExpireDate"] != DBNull.Value)
                        _ExpireDate = (DateTime)zReader["ExpireDate"];
                    if (zReader["LastLoginDate"] != DBNull.Value)
                        _LastLoginDate = (DateTime)zReader["LastLoginDate"];
                    _FailedPasswordCount = int.Parse(zReader["FailedPasswordCount"].ToString());
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    _Slug = int.Parse(zReader["Slug"].ToString());
                    _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }

                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public void Get_UserName_Info(string User_Name)
        {
            string zSQL = "SELECT * FROM SYS_User WHERE UserName = @UserName AND RecordStatus != 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = User_Name;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _UserKey = zReader["UserKey"].ToString();
                    _UserAPI = zReader["UserAPI"].ToString().Trim();
                    _UserName = zReader["UserName"].ToString().Trim();
                    _Password = zReader["Password"].ToString().Trim();
                    _PIN = zReader["PIN"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    _GroupName = zReader["GroupName"].ToString().Trim();
                    _BusinessKey = (int)zReader["BusinessKey"];
                    _PartnerNumber = zReader["PartnerNumber"].ToString().Trim();
                    _Activate = (bool)(zReader["Activate"]);
                    if (zReader["ExpireDate"] != DBNull.Value)
                        _ExpireDate = (DateTime)zReader["ExpireDate"];
                    if (zReader["LastLoginDate"] != DBNull.Value)
                        _LastLoginDate = (DateTime)zReader["LastLoginDate"];
                    _FailedPasswordCount = int.Parse(zReader["FailedPasswordCount"].ToString());
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    _Slug = int.Parse(zReader["Slug"].ToString());
                    _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public void Get_UserAPI_Info(string User_API)
        {
            string zSQL = "SELECT * FROM SYS_User WHERE UserAPI = @UserAPI AND RecordStatus != 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserAPI", SqlDbType.NVarChar).Value = User_API;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _UserKey = zReader["UserKey"].ToString();
                    _UserAPI = zReader["UserAPI"].ToString().Trim();
                    _UserName = zReader["UserName"].ToString().Trim();
                    _Password = zReader["Password"].ToString().Trim();
                    _PIN = zReader["PIN"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    _GroupName = zReader["GroupName"].ToString().Trim();
                    _BusinessKey = (int)zReader["BusinessKey"];
                    _PartnerNumber = zReader["PartnerNumber"].ToString().Trim();
                    _Activate = (bool)(zReader["Activate"]);
                    if (zReader["ExpireDate"] != DBNull.Value)
                        _ExpireDate = (DateTime)zReader["ExpireDate"];
                    if (zReader["LastLoginDate"] != DBNull.Value)
                        _LastLoginDate = (DateTime)zReader["LastLoginDate"];
                    _FailedPasswordCount = int.Parse(zReader["FailedPasswordCount"].ToString());
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    _Slug = int.Parse(zReader["Slug"].ToString());
                    _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public void Get_User_Info(string EmployeeKey)
        {
            if(EmployeeKey.Trim().Length != 36)
            {
                return;
            }
            string zSQL = "SELECT * FROM SYS_User WHERE EmployeeKey = @EmployeeKey AND RecordStatus != 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (EmployeeKey.Trim().Length == 36)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = new Guid(EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;

                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _UserKey = zReader["UserKey"].ToString();
                    _UserAPI = zReader["UserAPI"].ToString().Trim();
                    _UserName = zReader["UserName"].ToString().Trim();
                    _Password = zReader["Password"].ToString().Trim();
                    _PIN = zReader["PIN"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    _GroupName = zReader["GroupName"].ToString().Trim();
                    _BusinessKey = (int)zReader["BusinessKey"];
                    _PartnerNumber = zReader["PartnerNumber"].ToString().Trim();
                    _Activate = (bool)(zReader["Activate"]);
                    if (zReader["ExpireDate"] != DBNull.Value)
                        _ExpireDate = (DateTime)zReader["ExpireDate"];
                    if (zReader["LastLoginDate"] != DBNull.Value)
                        _LastLoginDate = (DateTime)zReader["LastLoginDate"];
                    _FailedPasswordCount = int.Parse(zReader["FailedPasswordCount"].ToString());
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    _Slug = int.Parse(zReader["Slug"].ToString());
                    _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        

        #region [ Constructor Update Information ]
        public string Create_ServerKey()
        {
            string zSQL = @" INSERT INTO dbo.SYS_User " +  
                        " (UserName ,Password,Activate,ExpireDate ,PartnerNumber , CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) " + 
                        " VALUES " + 
                        " (@UserName ,@Password ,@Activate ,@ExpireDate ,@PartnerNumber , @CreatedBy ,@CreatedName,@ModifiedBy ,@ModifiedName) ";

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                _UserKey = Guid.NewGuid().ToString();

                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = _UserName.Trim();
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = _Password.Trim();
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = _Activate;
                if (_ExpireDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = _ExpireDate;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PartnerNumber.Trim());
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                if (zResult == "1")
                {
                    _Message = "201 Created";
                    _RecordStatus = 1;
                }
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            string zSQL = @" INSERT INTO dbo.SYS_User " +
                        " (UserKey,UserName ,Password,Activate,ExpireDate ,PartnerNumber , CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) " +
                        " VALUES " +
                        " (@UserKey,@UserName ,@Password ,@Activate ,@ExpireDate ,@PartnerNumber , @CreatedBy ,@CreatedName,@ModifiedBy ,@ModifiedName) ";

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(_UserKey);
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = _UserName.Trim();
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = _Password.Trim();
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = _Activate;
                if (_ExpireDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = _ExpireDate;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PartnerNumber.Trim());
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zResult = zCommand.ExecuteNonQuery().ToString();
                if (zResult == "1")
                {
                    _Message = "201 Created";
                    _RecordStatus = 1;
                }
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = @"UPDATE SYS_User SET " + 
                        " UserName = @UserName, " +
                        " PIN = @PIN," +
                        " Description = @Description," +
                        " Activate = @Activate," +
                        " ExpireDate = @ExpireDate," +
                        " FailedPasswordCount = @FailedPasswordCount," +
                        " EmployeeKey = @EmployeeKey," +
                        " EmployeeID = @EmployeeID," +
                        " EmployeeName = @EmployeeName," +
                        " ModifiedOn = GETDATE()," +
                        " ModifiedBy = @ModifiedBy," +
                        " ModifiedName = @ModifiedName" +
                        " WHERE UserKey = @UserKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_UserKey);
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = _UserName.Trim();
                zCommand.Parameters.Add("@PIN", SqlDbType.NVarChar).Value = _PIN.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = _Activate;
                if (_ExpireDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = _ExpireDate;
                zCommand.Parameters.Add("@FailedPasswordCount", SqlDbType.Int).Value = _FailedPasswordCount;

                if (_EmployeeKey.Length > 0)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey.Trim());
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();

                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.ExecuteNonQuery();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save(bool MakeKeyAtClient)
        {
            if (_RecordStatus == 0)
            {
                if (MakeKeyAtClient)
                    return Create_ClientKey();
                else
                    return Create_ServerKey();
            }
            else
            {
                return Update();
            }

        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE SYS_User SET
                        RecordStatus = 99, 
                        ModifiedOn = GETDATE(), 
                        ModifiedBy = @ModifiedBy, 
                        ModifiedName = @ModifiedName 
                        WHERE UserKey = @UserKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(_UserKey);
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                if (zResult == "1")
                    _Message = "200 OK";
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SYS_User WHERE UserKey = @UserKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(_UserKey);
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();

                if (zResult == "1")
                    _Message = "200 OK";
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion

        #region [ Function ]
        public string ChangePass(string PasswordOld, string PasswordNew)
        {
            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE SYS_User SET Password = @PasswordNew WHERE UserKey = @UserKey AND Password = @PasswordOld";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(_UserKey);
                zCommand.Parameters.Add("@PasswordOld", SqlDbType.NVarChar).Value = MyCryptography.HashPass(PasswordOld);
                zCommand.Parameters.Add("@PasswordNew", SqlDbType.NVarChar).Value = MyCryptography.HashPass(PasswordNew);
                zResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close(); ;
            }

            return zResult;

        }
        public string ResetPass(string PasswordNew)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE SYS_User SET Password = @Password WHERE UserKey = @UserKey ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(_UserKey);
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = MyCryptography.HashPass(PasswordNew);
                zResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close(); ;
            }

            return zResult;

        }
        public string ChangePIN()
        {

            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE SYS_User SET PIN = @PIN WHERE UserKey = @UserKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(_UserKey);
                zCommand.Parameters.Add("@PIN", SqlDbType.NVarChar).Value = _PIN;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close(); ;
            }
            return zResult;

        }
        public string UpdateFailedPass()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE SYS_User SET  FailedPasswordCount = FailedPasswordCount + 1 WHERE UserKey = @UserKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(_UserKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();

            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateDateLogin()
        {

            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE SYS_User SET LastLoginDate = getdate() WHERE UserKey = @UserKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(_UserKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string SetActivate()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE SYS_User SET Activate = 1^Activate,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedOn = GetDate()"
                        + " WHERE UserKey = @UserKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_UserKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zResult = Err.ToString();
            }
            finally
            {
                zConnect.Close(); ;
            }
            return zResult;
        }
   
        #endregion
    }
}
