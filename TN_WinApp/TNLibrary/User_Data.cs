﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN_Connection;
using TN_Tools;

namespace TN_User
{
    public class User_Data
    {
        #region [ Securiry ]
        public static string[] CheckUser(string UserName, string Pass)
        {

            string[] zResult = new string[2];
            User_Info zUserLogin = new User_Info();
            zUserLogin.Get_UserName_Info(UserName);

            if (zUserLogin.UserKey.Trim().Length == 0)
            {
                zResult[0] = "ERR";
                zResult[1] = "CheckUser_Error01";
                return zResult;//"Don't have this UserName";
            }
            string zPass = MyCryptography.HashPass(Pass);
            if (zUserLogin.Password != zPass)
            {
                zUserLogin.UpdateFailedPass();
                zResult[0] = "ERR";
                zResult[1] = "CheckUser_Error02";
                return zResult;// "Wrong Password";
            }

            if (!zUserLogin.Activate)
            {
                zResult[0] = "ERR";
                zResult[1] = "CheckUser_Error03";
                return zResult;//"Don't Activate"
            }

            if (zUserLogin.ExpireDate < DateTime.Now)
            {
                zResult[0] = "ERR";
                zResult[1] = "CheckUser_Error04";
                return zResult;//"Expire On"
            }
            zResult[0] = "OK";
            zResult[1] = zUserLogin.UserKey;
            zUserLogin.UpdateDateLogin();
            return zResult;
        }
        public static string[] CheckUser(string UserName, string Pass, string[] TrackingUser)
        {

            string[] zResult = new string[2];
            User_Info zUserLogin = new User_Info();
            zUserLogin.Get_UserName_Info(UserName);

            if (zUserLogin.UserKey.Trim().Length == 0)
            {
                zResult[0] = "ERR";
                zResult[1] = "CheckUser_Error01";
                return zResult;//"Don't have this UserName";
            }
            string zPass = MyCryptography.HashPass(Pass);
            if (zUserLogin.Password != zPass)
            {
                TrackingUser[7] = "1";
                User_Tracking_Data.InsertTracking(zUserLogin.UserKey, TrackingUser);
                zUserLogin.UpdateFailedPass();
                zResult[0] = "ERR";
                zResult[1] = "CheckUser_Error02";
                return zResult;// "Wrong Password";
            }

            if (!zUserLogin.Activate)
            {
                zResult[0] = "ERR";
                zResult[1] = "CheckUser_Error03";
                return zResult;//"Don't Activate"
            }

            if (zUserLogin.ExpireDate < DateTime.Now)
            {
                zResult[0] = "ERR";
                zResult[1] = "CheckUser_Error04";
                return zResult;//"Expire On"
            }
            zResult[0] = "OK";
            zResult[1] = zUserLogin.UserKey;
            User_Tracking_Data.InsertTracking(zUserLogin.UserKey, TrackingUser);
            zUserLogin.UpdateDateLogin();
            return zResult;
        }
        public static bool CheckUserName(string UserName)
        {
            bool zIsHave = false;
            string zSQL = @"SELECT Count(*) FROM SYS_User WHERE UserName = @UserName ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = UserName;
                int zQuantity = 0;
                string zResult = zCommand.ExecuteScalar().ToString();
                if (int.TryParse(zResult, out zQuantity))
                {
                    if (zQuantity > 0)
                    {
                        zIsHave = true;
                    }
                }
                zCommand.Dispose();
                //---- Close Connect SQL ----
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }

            return zIsHave;
        }
        #endregion

        #region [ Standard PartnerNumber ]
        public static DataTable List(string PartnerNumber, int PageNumber, int PageSize)
        {
            return List(PartnerNumber, PageNumber, PageSize, "");
        }
        public static DataTable List(string PartnerNumber, int PageNumber, int PageSize, string OrderBy)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM dbo.SYS_User WHERE PartnerNumber = @PartnerNumber AND RecordStatus != 99";
            if (OrderBy.Trim().Length > 0)
            {
                zSQL += " ORDER BY " + OrderBy;
            }
            else
            {
                zSQL += " ORDER BY UserName ";
            }

            zSQL += " OFFSET @PageSize * (@PageNumber - 1) ROWS " +
                    " FETCH NEXT @PageSize ROWS ONLY OPTION(RECOMPILE)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                zCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = PageSize;
                zCommand.Parameters.Add("@PageNumber", SqlDbType.Int).Value = PageNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string Result = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(string PartnerNumber, int PageNumber, int PageSize, string OrderBy, out string Error)
        {
            DataTable zTable = new DataTable();
            Error = "";
            string zSQL = @"SELECT * FROM dbo.SYS_User WHERE PartnerNumber = @PartnerNumber AND RecordStatus != 99";
            if (OrderBy.Trim().Length > 0)
            {
                zSQL += " ORDER BY " + OrderBy;
            }
            else
            {
                zSQL += " ORDER BY UserName ";
            }

            zSQL += " OFFSET @PageSize * (@PageNumber - 1) ROWS " +
                    " FETCH NEXT @PageSize ROWS ONLY OPTION(RECOMPILE)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                zCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = PageSize;
                zCommand.Parameters.Add("@PageNumber", SqlDbType.Int).Value = PageNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                Error = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(string PartnerNumber, int PageNumber, int PageSize, string OrderBy, string SearchContent)
        {
            DataTable zTable = new DataTable();
            string zSQL = "";

            zSQL = @"SELECT * FROM dbo.SYS_User WHERE PartnerNumber = @PartnerNumber AND RecordStatus != 99 " +
                   " AND (UserName LIKE @SearchContent OR GroupName LIKE @SearchContent OR EmployeeName LIKE @SearchContent) ";

            if (OrderBy.Trim().Length > 0)
            {
                zSQL += " ORDER BY " + OrderBy;
            }
            else
            {
                zSQL += " ORDER BY UserName ";
            }

            zSQL += " OFFSET @PageSize * (@PageNumber - 1) ROWS " +
                    " FETCH NEXT @PageSize ROWS ONLY OPTION(RECOMPILE)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                zCommand.Parameters.Add("@SearchContent", SqlDbType.NVarChar).Value = "%" + SearchContent + "%";
                zCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = PageSize;
                zCommand.Parameters.Add("@PageNumber", SqlDbType.Int).Value = PageNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string Result = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(string PartnerNumber, int PageNumber, int PageSize, string OrderBy, string SearchContent, out string Error)
        {
            DataTable zTable = new DataTable();
            string zSQL = "";
            Error = "";
            zSQL = @"SELECT * FROM dbo.SYS_User WHERE PartnerNumber = @PartnerNumber AND RecordStatus != 99 " +
                   " AND (UserName LIKE @SearchContent OR GroupName LIKE @SearchContent OR EmployeeName LIKE @SearchContent) ";

            if (OrderBy.Trim().Length > 0)
            {
                zSQL += " ORDER BY " + OrderBy;
            }
            else
            {
                zSQL += " ORDER BY UserName ";
            }

            zSQL += " OFFSET @PageSize * (@PageNumber - 1) ROWS " +
                    " FETCH NEXT @PageSize ROWS ONLY OPTION(RECOMPILE)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                zCommand.Parameters.Add("@SearchContent", SqlDbType.NVarChar).Value = "%" + SearchContent + "%";
                zCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = PageSize;
                zCommand.Parameters.Add("@PageNumber", SqlDbType.Int).Value = PageNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                Error = ex.ToString();
            }
            return zTable;
        }
        public static int Count_Record(string PartnerNumber)
        {
            string zMessage = "";
            int zQuantity = 0;
            string zSQL = "";

            zSQL = "SELECT Count(*) FROM dbo.SYS_User WHERE PartnerNumber = @PartnerNumber AND RecordStatus != 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                string zResult = zCommand.ExecuteScalar().ToString();
                int.TryParse(zResult, out zQuantity);
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zMessage = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }

            return zQuantity;
        }
        public static int Count_Record(string PartnerNumber, string SearchContent)
        {
            int zQuantity = 0;
            string zMessage = "";
            DataTable zTable = new DataTable();
            string zSQL = "";

            zSQL = @"SELECT Count(*) FROM dbo.SYS_User WHERE PartnerNumber = @PartnerNumber AND RecordStatus != 99 " +
                   " AND (UserName LIKE @SearchContent OR GroupName LIKE @SearchContent OR EmployeeName LIKE @SearchContent) ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                zCommand.Parameters.Add("@SearchContent", SqlDbType.NVarChar).Value = "%" + SearchContent + "%";
                string zResult = zCommand.ExecuteScalar().ToString();
                int.TryParse(zResult, out zQuantity);
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zMessage = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }

            return zQuantity;
        }
        #endregion

        public static string[] CheckEmployee(string PartnerNumber, string EmployeeID)
        {
            string[] zResult = new string[3];
            DataTable zTable = new DataTable();
            string zError = "";
            string zSQL = @"SELECT EmployeeKey,LastName + ' ' + FirstName AS FullName FROM dbo.HRM_Employee WHERE EmployeeID = @EmployeeID AND PartnerNumber = @PartnerNumber AND RecordStatus != 99";
            zResult[0] = "ERROR";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = EmployeeID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    zResult[0] = "OK";
                    zResult[1] = zReader["EmployeeKey"].ToString();
                    zResult[2] = zReader["FullName"].ToString().Trim();
                }
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zError = ex.ToString();
                zResult[2] = zError;
            }
            return zResult;
        }
    }  
}