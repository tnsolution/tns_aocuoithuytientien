﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN_Connection;

namespace TN_User
{
    public class User_Login_Info
    {
        #region [ Field Name ]
        private string _UserKey;
        private string _UserAPI = "";
        private string _UserName = "";
        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private int _BusinessKey = 0;
        private string _PartnerNumber = "";
        private string _PartnerName = "";
        private string _Language = "VN";
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public User_Login_Info()
        {
        }
   
        public User_Login_Info(string UserName)
        {
            string zSQL = @" SELECT * FROM SYS_User A"
                         + " INNER JOIN SYS_Partner B ON A.PartnerNumber = B.PartnerNumber"
                         + " WHERE A.UserName  = @UserName AND A.RecordStatus != 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = UserName;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _UserKey = zReader["UserKey"].ToString();
                    _UserAPI = zReader["UserAPI"].ToString();
                    _UserName = zReader["UserName"].ToString();
                    _BusinessKey = (int)zReader["BusinessKey"];
                    _PartnerNumber = zReader["PartnerNumber"].ToString().Trim();
                    _PartnerName = zReader["PartnerName"].ToString().Trim();

                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString();
                    _EmployeeName = zReader["EmployeeName"].ToString();
                  
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]
        public string Key
        {
            get { return _UserKey; }
            set { _UserKey = value; }
        }
        public string API
        {
            get { return _UserAPI; }
            set { _UserAPI = value; }
        }
        public string Name
        {
            get { return _UserName; }
            set { _UserName = value; }
        }
        public int BusinessKey
        {
            get { return _BusinessKey; }
            set { _BusinessKey = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public string PartnerName
        {
            get { return _PartnerName; }
            set { _PartnerName = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public string Language
        {
            get { return _Language; }
            set { _Language = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string EmployeeID
        {
            get
            {
                return _EmployeeID;
            }

            set
            {
                _EmployeeID = value;
            }
        }
        #endregion

        public string ChangePass(string PasswordOld, string PasswordNew)
        {
            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE SYS_User SET Password = @PasswordNew WHERE (UserKey = @UserKey AND Password = @PasswordOld)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(_UserKey);
                zCommand.Parameters.Add("@PasswordOld", SqlDbType.NVarChar).Value = MyCryptography.HashPass(PasswordOld);
                zCommand.Parameters.Add("@PasswordNew", SqlDbType.NVarChar).Value = MyCryptography.HashPass(PasswordNew);
                zResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close(); ;
            }

            return zResult;

        }
    }
}
