﻿using System;

namespace TN_User
{
    public class SessionUser
    {
        private static User_Info _UserLogin;
        private static DateTime _DateWork = DateTime.Now;

        public static DateTime DateWork
        {
            get
            {
                return _DateWork;
            }

            set
            {
                _DateWork = value;
            }
        }
        public static User_Info UserLogin
        {
            get
            {
                return _UserLogin;
            }
        }

        public SessionUser()
        {
            _UserLogin = new User_Info();
        }
        public SessionUser(string UserKey)
        {
            _UserLogin = new User_Info(UserKey);
        }
    }
}
