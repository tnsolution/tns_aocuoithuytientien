﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace TN_User
{
    public class User_Role_Info
    {
        #region [ Field Name ]
        private string _UserKey ="";
        private string _RoleKey ="";
        private string _RoleID ="";
        private string _RoleURL="";
        private string _RoleName="";
        private bool _RoleRead;
        private bool _RoleEdit;
        private bool _RoleAdd;
        private bool _RoleDel;
        private bool _RoleApprove;
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public User_Role_Info()
        {

        }
        public User_Role_Info(string UserKey, string RoleURL)
        {
            string zSQL = " SELECT A.* FROM SYS_User_Role A "
                        + " INNER JOIN SYS_Role B ON A.RoleKey = B.RoleKey"
                        + " WHERE A.UserKey = @UserKey AND B.RoleURL = @RoleURL";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(UserKey);
                zCommand.Parameters.Add("@RoleURL", SqlDbType.NVarChar).Value = RoleURL;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _UserKey = zReader["UserKey"].ToString();
                    _RoleKey = zReader["RoleKey"].ToString();
                    _RoleRead = (bool)zReader["RoleRead"];
                    _RoleEdit = (bool)zReader["RoleEdit"];
                    _RoleAdd = (bool)zReader["RoleAdd"];
                    _RoleDel = (bool)zReader["RoleDel"];
                    _RoleApprove = (bool)zReader["RoleApprove"];
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally { zConnect.Close(); }
        }     
        #endregion

        #region [ Properties ]
        public string UserKey
        {
            get { return _UserKey; }
            set { _UserKey = value; }
        }
        public string RoleKey
        {
            get { return _RoleKey; }
            set { _RoleKey = value; }
        }
        public bool RoleRead
        {
            get { return _RoleRead; }
            set { _RoleRead = value; }
        }
        public bool RoleEdit
        {
            get { return _RoleEdit; }
            set { _RoleEdit = value; }
        }
        public bool RoleAdd
        {
            get { return _RoleAdd; }
            set { _RoleAdd = value; }
        }
        public bool RoleDel
        {
            get { return _RoleDel; }
            set { _RoleDel = value; }
        }
        public bool RoleApprove
        {
            get { return _RoleApprove; }
            set { _RoleApprove = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else
                    return "";
            }
        }
        public string RoleID
        {
            get
            {
                return _RoleID;
            }

            set
            {
                _RoleID = value;
            }
        }
        public string RoleURL
        {
            get
            {
                return _RoleURL;
            }

            set
            {
                _RoleURL = value;
            }
        }
        public string RoleName
        {
            get
            {
                return _RoleName;
            }

            set
            {
                _RoleName = value;
            }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_User_Role ("
                        + " UserKey,RoleKey,RoleRead ,RoleEdit ,RoleAdd ,RoleDel ,RoleApprove ) "
                        + " VALUES ( "
                        + " @UserKey,@RoleKey,@RoleRead ,@RoleEdit ,@RoleAdd ,@RoleDel ,@RoleApprove ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(_UserKey);
                zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = new Guid(_RoleKey);               
                zCommand.Parameters.Add("@RoleRead", SqlDbType.Bit).Value = _RoleRead;
                zCommand.Parameters.Add("@RoleEdit", SqlDbType.Bit).Value = _RoleEdit;
                zCommand.Parameters.Add("@RoleAdd", SqlDbType.Bit).Value = _RoleAdd;
                zCommand.Parameters.Add("@RoleDel", SqlDbType.Bit).Value = _RoleDel;
                zCommand.Parameters.Add("@RoleApprove", SqlDbType.Bit).Value = _RoleApprove;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();

            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE SYS_User_Role SET "
                        + " RoleRead = @RoleRead,"
                        + " RoleEdit = @RoleEdit,"
                        + " RoleAdd = @RoleAdd,"
                        + " RoleDel = @RoleDel,"
                        + " RoleApprove = @RoleApprove"
                       + " WHERE UserKey = @UserKey AND RoleKey = @RoleKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(_UserKey);
                zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = new Guid(_RoleKey);
                zCommand.Parameters.Add("@RoleRead", SqlDbType.Bit).Value = _RoleRead;
                zCommand.Parameters.Add("@RoleEdit", SqlDbType.Bit).Value = _RoleEdit;
                zCommand.Parameters.Add("@RoleAdd", SqlDbType.Bit).Value = _RoleAdd;
                zCommand.Parameters.Add("@RoleDel", SqlDbType.Bit).Value = _RoleDel;
                zCommand.Parameters.Add("@RoleApprove", SqlDbType.Bit).Value = _RoleApprove;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Save()
        {
            string zResult;
            if (_UserKey.Trim().Length == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SYS_User_Role WHERE UserKey = @UserKey AND RoleKey = @RoleKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(UserKey);
                zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = new Guid(RoleKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion

        #region [ Function ]
        public string SetRoleRead()
        {
            string zSQL = "UPDATE SYS_User_Role SET "
                       + " RoleRead = CASE WHEN RoleRead = 1 THEN 0 ELSE 1 END "
                       + " WHERE UserKey = @UserKey AND RoleKey = @RoleKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(_UserKey);
                zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = new Guid(_RoleKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string SetRoleEdit()
        {
            string zSQL = "UPDATE SYS_User_Role SET "
                       + " RoleEdit = CASE WHEN RoleEdit = 1 THEN 0 ELSE 1 END "
                       + " WHERE UserKey = @UserKey AND RoleKey = @RoleKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(_UserKey);
                zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = new Guid(_RoleKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string SetRoleAdd()
        {
            string zSQL = "UPDATE SYS_User_Role SET "
                       + " RoleAdd = CASE WHEN RoleAdd = 1 THEN 0 ELSE 1 END "
                       + " WHERE UserKey = @UserKey AND RoleKey = @RoleKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(_UserKey);
                zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = new Guid(_RoleKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string SetRoleDel()
        {
            string zSQL = "UPDATE SYS_User_Role SET "
                       + " RoleDel = CASE WHEN RoleDel = 1 THEN 0 ELSE 1 END "
                       + " WHERE UserKey = @UserKey AND RoleKey = @RoleKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(_UserKey);
                zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = new Guid(_RoleKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string SetRoleApprove()
        {
            string zSQL = "UPDATE SYS_User_Role SET "
                       + " RoleApprove = CASE WHEN RoleApprove = 1 THEN 0 ELSE 1 END "
                       + " WHERE UserKey = @UserKey AND RoleKey = @RoleKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(_UserKey);
                zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = new Guid(_RoleKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string SetRoleAll()
        {
            string zSQL = "UPDATE SYS_User_Role SET "
                       + " RoleRead = 1, RoleEdit = 1, RoleAdd = 1,RoleDel = 1 ,RoleApprove = 1"
                       + " WHERE UserKey = @UserKey AND RoleKey = @RoleKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(_UserKey);
                zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = new Guid(_RoleKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
