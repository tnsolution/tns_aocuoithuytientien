﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using TN_Connection;
using TN_Tools;
using TN_User;

namespace TN_WinApp.IVT
{
    public partial class Frm_ServiceList : Form
    {
        private string _FormUrl = "/IVT/Frm_ServiceList";
        public string FormUrl
        {
            get
            {
                return _FormUrl;
            }

            set
            {
                _FormUrl = value;
            }
        }

        public Frm_ServiceList()
        {
            InitializeComponent();

            TitleForm.MouseDown += Frm_MouseDown;
            TitleForm.MouseMove += Frm_MouseMove;
            TitleForm.MouseUp += Frm_MouseUp;

            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;

            LVData.ItemActivate += LVData_ItemActivate;
            LVData.KeyDown += LVData_KeyDown;

            btn_Search.Click += BtnSearch_Click;
            btnAddNew.Click += BtnAddNew_Click;

            //format Form
            this.Text = "Danh sách dịch vụ";
            this.WindowState = FormWindowState.Maximized;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.FormBorderStyle = FormBorderStyle.None;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.Load += Frm_ServiceList_Load;
        }

        private void Frm_ServiceList_Load(object sender, EventArgs e)
        {
            Get_Auth();
            if (_RoleForm.RoleRead)
            {
                LoadCombobox();
                DesignLayout(LVData);
                LoadListView();
            }
        }

        #region [Event]
        private void BtnSearch_Click(object sender, EventArgs e)
        {
            LoadListView();
        }
        private void BtnAddNew_Click(object sender, EventArgs e)
        {
            Frm_ServiceEdit Frm = new Frm_ServiceEdit();
            Frm.ServiceKey = "";
            Frm.ShowDialog();
            LoadListView();
        }
        #endregion

        #region [Process]
        private void LoadCombobox()
        {
            string zCategory = @"SELECT CategoryKey, CategoryNameVN FROM[dbo].[PDT_Service_Category] WHERE RecordStatus<> 99 AND PartnerNumber = '" + SessionUser.UserLogin.PartnerNumber + "'";

            LoadDataToToolbox.KryptonComboBox(cboCategory, zCategory, "--Chọn--");
        }
        private string Delete(string Key)
        {
            Frm_ServiceEdit.Service_Info zInfo = new Frm_ServiceEdit.Service_Info();
            zInfo.ServiceKey = Key;
            zInfo.Delete();
            return zInfo.Message;
        }
        #endregion

        #region [ListView]
        private void DesignLayout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã dịch vụ";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên dịch vụ";
            colHead.Width = 350;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Đơn vị";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Giá thuê";
            colHead.Width = 130;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tình trạng";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ghi chú";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            TNPaintControl.DrawLVStyle(ref LVData);
        }
        private void LoadListView()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable zTable = Access_Data.List(SessionUser.UserLogin.PartnerNumber, txtSearch.Text.Trim(), cboCategory.SelectedValue.ToInt());
            LV.Items.Clear();
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow zRow = zTable.Rows[i];
                lvi.Tag = zRow["ServiceKey"].ToString();
                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["ServiceID"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["ServiceName"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["StandardUnitName"].ToString();
                lvi.SubItems.Add(lvsi);

                double zStandardCost = zRow["StandardCost"].ToDouble();
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zStandardCost.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["StatusName"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["Description"].ToString();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;

        }
        private void LVData_ItemActivate(object sender, EventArgs e)
        {
            if (LVData.SelectedItems.Count > 0)
            {
                Frm_ServiceEdit Frm = new Frm_ServiceEdit();
                Frm.ServiceKey = LVData.SelectedItems[0].Tag.ToString();
                Frm.ShowDialog();
                LoadListView();
            }
        }
        private void LVData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (_RoleForm.RoleDel)
                {
                    DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dlr == DialogResult.Yes && LVData.SelectedItems.Count > 0)
                    {
                        string Key = LVData.SelectedItems[0].Tag.ToString();
                        string zResult = Delete(Key);

                        if (zResult.Substring(0, 3) == "200" ||
                            zResult.Substring(0, 3) == "201")
                        {
                            MessageBox.Show("Đã xóa thành công !");
                            LoadListView();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Bạn không có quyền xóa !");
                }
            }
        }
        #endregion

        #region [Dùng kéo rê form]
        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;

        private void Frm_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }
        private void Frm_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }
        private void Frm_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        #region [Auth]
        User_Role_Info _RoleForm = new User_Role_Info();
        private void Get_Auth()
        {
            List<User_Role_Info> zListRole = SessionUser.TableRole;
            if (zListRole.Count > 0)
            {
                try
                {
                    _RoleForm = zListRole.SingleOrDefault(r => r.RoleURL == FormUrl);
                    if (_RoleForm == null)
                    {
                        _RoleForm = new User_Role_Info();
                        MessageBox.Show("Form này chưa được thêm quyền, vui lòng liên hệ IT !.");
                        this.Close();
                    }
                    else
                    {
                        if (!_RoleForm.RoleRead)
                        {
                            MessageBox.Show("Bạn không có quyền xem, vui lòng liên hệ IT !.");
                            this.Close();
                        }
                        if (!_RoleForm.RoleEdit)
                        {

                        }
                        if (!_RoleForm.RoleAdd)
                        {

                        }
                        if (!_RoleForm.RoleDel)
                        {

                        }
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Bạn chưa được phân quyền, vui lòng liên hệ IT !.");
            }
        }
        #endregion       

        #region[Access_Data]
        public class Access_Data
        {
            public static DataTable List(string PartnerNumber, string Name, int CategoryKey)
            {
                DataTable zTable = new DataTable();
                string zSQL = @"
SELECT * FROM [dbo].[PDT_Service] 
WHERE RecordStatus <> 99  
AND PartnerNumber= @PartnerNumber ";
                if (Name.Trim().Length > 0)
                {
                    zSQL += " AND (ServiceID LIKE @Name OR ServiceName LIKE @Name)";
                }
                if (CategoryKey != 0)
                {
                    zSQL += " AND CategoryKey = @CategoryKey";
                }
                zSQL += " ORDER BY CreatedOn DESC";
                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%"; ;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
        }
        #endregion]
    }
}
