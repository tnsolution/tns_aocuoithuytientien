﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using TN_Connection;
using TN_Tools;
using TN_User;

namespace TN_WinApp.IVT
{
    public partial class Frm_ProductEdit : Form
    {
        private string _FormUrl = "/IVT/Frm_ProductEdit";
        private string _ProductKey = "";

        public string ProductKey
        {
            get
            {
                return _ProductKey;
            }

            set
            {
                _ProductKey = value;
            }
        }
        public string FormUrl
        {
            get
            {
                return _FormUrl;
            }

            set
            {
                _FormUrl = value;
            }
        }

        public Frm_ProductEdit()
        {
            InitializeComponent();
            TitleForm.MouseDown += Frm_MouseDown;
            TitleForm.MouseMove += Frm_MouseMove;
            TitleForm.MouseUp += Frm_MouseUp;

            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;

            LVPicture.Click += LVPicture_Click;

            btnNewOrder.Click += BtnNewOrder_Click;
            btnSave.Click += BtnSave_Click;
            btnDelete.Click += BtnDelete_Click;
            btnUploadImage.Click += BtnUploadImage_Click;

            txtProductPrice.KeyPress += TextBoxNumber_ProductKeyPress;
            txtRentCost.KeyPress += TextBoxNumber_ProductKeyPress;
            txtStandardCost.KeyPress += TextBoxNumber_ProductKeyPress;
            txtSafetyInStock.KeyPress += TextBoxNumber_ProductKeyPress;

            txtStandardCost.Leave += (o, e) => { txtStandardCost.Text = txtStandardCost.Text.FormatMoneyFromString(); };
            txtRentCost.Leave += (o, e) => { txtRentCost.Text = txtRentCost.Text.FormatMoneyFromString(); };
            txtProductPrice.Leave += (o, e) => { txtProductPrice.Text = txtProductPrice.Text.FormatMoneyFromString(); };

            this.Text = "Chi tiết sản phẩm";
            this.StartPosition = FormStartPosition.CenterScreen;
            this.FormBorderStyle = FormBorderStyle.None;
            this.ControlBox = false;
            this.DoubleBuffered = true;

            tabControl1.TabPages.Remove(tabPage2);
        }

        private void Frm_ProductEdit_Load(object sender, EventArgs e)
        {
            Get_Auth();
            if (_RoleForm.RoleRead)
            {
                LoadCombobox();
                DesignLayout(LVPicture);
                LoadData();
            }
        }

        #region [ListView]
        private void DesignLayout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Hình";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Đường dẫn";
            colHead.Width = 350;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            TNPaintControl.DrawLVStyle(ref LVPicture);
        }
        private void LoadListView()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVPicture;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable zTable = Access_Data.ListPhoto(ProductKey);
            LV.Items.Clear();
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow zRow = zTable.Rows[i];
                lvi.Tag = zRow["PhotoKey"].ToString();
                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["ProductName"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["PhotoPath"].ToString();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;
        }
        private void LVPicture_Click(object sender, EventArgs e)
        {
            if (LVPicture.SelectedItems.Count > 0)
            {
                //string url = "https://drive.google.com/file/d/14OOTm_iGjTwkBV6-4pZalP5ECGQ-K1hG"; //LVPicture.SelectedItems[0].SubItems[2].Text;
                //Image img = GetPicture(url);
                //PicturePreview.Image = img;
            }
        }
        #endregion

        #region[Event]
        private void TextBoxNumber_ProductKeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void BtnUploadImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog zOdialog = new OpenFileDialog();
            zOdialog.Multiselect = true;
            zOdialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            if (zOdialog.ShowDialog() == DialogResult.OK)
            {
                UploadImage(zOdialog.FileNames);
                LoadListView();
            }
        }
        private void BtnNewOrder_Click(object sender, EventArgs e)
        {
            ClearForm();
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            string zResult = "";
            string zMessage = "";
            zMessage = CheckBeforSave();
            if (zMessage.Length == 0)
            {
                zResult = Save();
                if (zResult.Substring(0, 3) == "200" || zResult.Substring(0, 3) == "201")
                {
                    MessageBox.Show("Cập nhật sản phẩm thành công!");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Lỗi.Vui lòng liên hệ IT.Chi tiết lỗi mã : \n " + zResult);
                }
            }
            else
            {
                MessageBox.Show(zMessage);
            }
        }
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            if (ProductKey != "")
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    Product_FED_Info zInfo = new Product_FED_Info(ProductKey);
                    zInfo.Delete();
                    ProductProperty_FED_Info zPro = new ProductProperty_FED_Info();
                    zPro.GetProduct_Info(zInfo.ProductKey);
                    zPro.Delete();
                    if (zInfo.Message.Substring(0, 3) == "200")
                    {
                        MessageBox.Show("Xóa thành công!");
                        ClearForm();
                    }
                    else
                    {
                        MessageBox.Show("Lỗi.Vui lòng liên hệ IT.Chi tiết lỗi mã: \n " + zInfo.Message);
                    }

                }
            }
        }
        #endregion

        #region[Process]
        private void LoadCombobox()
        {
            string zColor = @"SELECT ColorKey,ColorNameVN FROM [dbo].[PDT_Product_Color]
            WHERE RecordStatus<> 99 AND PartnerNumber = '" + SessionUser.UserLogin.PartnerNumber + "' ORDER BY ColorNameVN ASC";

            string zSize = @"SELECT SizeKey,SizeNameVN FROM[dbo].[PDT_Product_Size]
            WHERE RecordStatus<> 99 AND PartnerNumber = '" + SessionUser.UserLogin.PartnerNumber + "' ORDER BY SizeNameVN ASC";

            string zCategory = @"SELECT CategoryKey,CategoryNameVN FROM [dbo].[PDT_Product_Category]
            WHERE RecordStatus<> 99 AND PartnerNumber = '" + SessionUser.UserLogin.PartnerNumber + "' ORDER BY CategoryNameVN ASC";

            string zStatus = @"SELECT StatusKey,StatusNameVN FROM [dbo].[PDT_Product_Status]
            WHERE RecordStatus<> 99 AND PartnerNumber = '" + SessionUser.UserLogin.PartnerNumber + "' ORDER BY StatusNameVN ASC";

            string zUnit = @"SELECT UnitKey,UnitNameVN FROM [dbo].[PDT_Product_Unit]
            WHERE RecordStatus<> 99 AND PartnerNumber = '" + SessionUser.UserLogin.PartnerNumber + "' ORDER BY UnitNameVN ASC";

            LoadDataToToolbox.KryptonComboBox(cboColor, zColor, "--Chọn--");
            LoadDataToToolbox.KryptonComboBox(cboSize, zSize, "--Chọn--");
            LoadDataToToolbox.KryptonComboBox(cboCategory, zCategory, "--Chọn--");
            LoadDataToToolbox.KryptonComboBox(cboStatus, zStatus, "--Chọn--");
            LoadDataToToolbox.KryptonComboBox(cboUnit, zUnit, "--Chọn--");
        }

        //Tải hình lên google drive
        //Chưa test trường hợp nếu drive hết dung lượng !.
        private void UploadImage(string[] ListFile)
        {
            //string zCred = Application.StartupPath + @"\credentials_thuytien.json";
            //GoogleAPI.GoogleApplicationName = "aocuoithuytien";
            //GoogleAPI.GoogleCredentialFilePath = zCred;

            //string Status;
            //string FolderID = GoogleAPI.GoogleNewFolder(txtName.Text.Trim(), out Status);
            //if (Status == "OK")
            //{
            //    ListFile = GoogleAPI.GoogleUpload(ListFile, FolderID, out Status);
            //    if (Status == "OK")
            //    {
            //        InsertImage(ListFile);
            //        MessageBox.Show("Đã tải hình thành công !.");
            //    }
            //    else
            //    {
            //        MessageBox.Show("Không thể tải hình vui lòng kiểm tra dung lượng Google Drive hoặc liên hệ IT !.");
            //    }
            //}
            //else
            //{
            //    MessageBox.Show("Không thể tạo thư mục trên Google Drive !.");
            //}
        }
        private void InsertImage(string[] ListFile)
        {
            foreach (string s in ListFile)
            {
                ProductPhoto_FED_Info zInfo = new ProductPhoto_FED_Info();
                zInfo.PartnerNumber = SessionUser.UserLogin.PartnerNumber;
                zInfo.ProductKey = ProductKey;
                zInfo.PhotoPath = s;
                zInfo.Create_ClientKey();
            }
        }

        private void FTPUpload(string[] ListFile)
        {
            //FTPWinSCP FtpWinSCP = new FTPWinSCP("45.119.80.12", "ftpTNS", "ftpTNS");
            //string ServerPath = "/WinApp/Wedding/Images/";
            //foreach (string file in ListFile)
            //{
            //    FtpWinSCP.Upload(file, ServerPath);
            //}
        }

        private string CheckBeforSave()
        {
            string zResult = "";
            if (txtID.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng điền mã sản phẩm! \n";
            }

            if (ProductKey == "")
            {
                bool Count = Access_Data.CheckProductID(SessionUser.UserLogin.PartnerNumber, txtID.Text.Trim());
                if (!Count)
                {
                    zResult += "Vui lòng chọn mã sản phẩm khác! \n";
                }
            }
            if (txtName.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng điền tên sản phẩm! \n";
            }

            if (cboCategory.SelectedValue.ToInt() == 0)
            {
                zResult += "Vui lòng chọn danh mục sản phẩm! \n";
            }

            if (cboStatus.SelectedValue.ToInt() == 0)
            {
                zResult += "Vui lòng chọn tình trạng! \n";
            }

            return zResult;
        }
        private string Save()
        {
            string zMesage = "";

            #region [Product_FED_Info]
            Product_FED_Info zInfo;
            if (ProductKey == "")
            {
                zInfo = new Product_FED_Info();
            }
            else
            {
                zInfo = new Product_FED_Info(ProductKey);
            }
            zInfo.ProductID = txtID.Text.Trim().ToUpper();
            zInfo.ProductName = txtName.Text.Trim();
            zInfo.Description = txtDescription.Text.Trim();
            zInfo.CategoryKey = cboCategory.SelectedValue.ToInt();
            zInfo.CategoryName = cboCategory.Text;
            zInfo.StatusKey = cboStatus.SelectedValue.ToInt();
            zInfo.StatusName = cboStatus.Text;
            zInfo.StandardUnitKey = cboUnit.SelectedValue.ToInt();
            zInfo.StandardUnitName = cboUnit.Text;

            if (txtSafetyInStock.Text != string.Empty)
            {
                float SafetyInStock = float.Parse(txtSafetyInStock.Text);
                zInfo.SafetyInStock = SafetyInStock;
            }
            if (txtStandardCost.Text != string.Empty)
            {
                double StandardCost = double.Parse(txtStandardCost.Text, CultureInfo.GetCultureInfo("vi-VN"));
                zInfo.StandardCost = StandardCost;
            }

            if (txtProductPrice.Text != string.Empty)
            {
                double ProductPrice = double.Parse(txtProductPrice.Text, CultureInfo.GetCultureInfo("vi-VN"));
                zInfo.ProductPrice = ProductPrice;
            }

            if (txtRentCost.Text != string.Empty)
            {
                double RentCost = double.Parse(txtRentCost.Text, CultureInfo.GetCultureInfo("vi-VN"));
                zInfo.RentCost = RentCost;
            }

            zInfo.PartnerNumber = SessionUser.UserLogin.PartnerNumber;

            if (ProductKey == "")
            {
                zInfo.Create_ClientKey();
            }
            else
            {
                zInfo.Update();
            }

            //-------------------------------------------------------
            if (zInfo.Code != "200" &&
                zInfo.Code != "201")
            {
                return zInfo.Message;
            }
            #endregion

            #region [ProductProperty_FED_Info]
            ProductKey = zInfo.ProductKey;

            ProductProperty_FED_Info zPro = new ProductProperty_FED_Info();
            zPro.GetProduct_Info(ProductKey);
            zPro.ProductID = txtID.Text.Trim().ToUpper();
            zPro.ProductName = txtName.Text.Trim();
            zPro.ProductKey = ProductKey;
            zPro.PartnerNumber = SessionUser.UserLogin.PartnerNumber;
            if (cboColor.SelectedValue != null)
            {
                zPro.ColorKey = cboColor.SelectedValue.ToInt();
                zPro.ColorName = cboColor.Text;
            }
            if (cboSize.SelectedValue != null)
            {
                zPro.SizeKey = cboSize.SelectedValue.ToInt();
                zPro.SizeName = cboSize.Text;
            }
            if (zPro.Code == "404")
            {
                zPro.Create_ClientKey();
            }
            else
            {
                zPro.Update();
            }

            //-------------------------------------------------------
            if (zPro.Code != "200" &&
                zPro.Code != "201")
            {
                zMesage = zPro.Message;
            }
            else
            {
                zMesage = "200";
            }
            #endregion

            return zMesage;
        }
        private void ClearForm()
        {
            ProductKey = "";
            txtID.Text = "";
            txtName.Text = "";
            cboCategory.SelectedValue = 0;
            cboStatus.SelectedValue = 0;
            txtDescription.Text = "";
            txtStandardCost.Text = "0";
            txtRentCost.Text = "0";
            txtProductPrice.Text = "0";
            txtSafetyInStock.Text = "0";
            cboColor.SelectedValue = 0;
            cboSize.SelectedValue = 0;
            cboUnit.SelectedValue = 0;
        }
        private void LoadData()
        {
            Product_FED_Info zInfo;
            if (ProductKey == "")
            {
                zInfo = new Product_FED_Info();
            }
            else
            {
                zInfo = new Product_FED_Info(ProductKey);
            }

            txtID.Text = zInfo.ProductID;
            txtName.Text = zInfo.ProductName;
            txtDescription.Text = zInfo.Description;
            txtStandardCost.Text = zInfo.StandardCost.ToString("n0", CultureInfo.GetCultureInfo("vi-VN"));
            txtRentCost.Text = zInfo.RentCost.ToString("n0", CultureInfo.GetCultureInfo("vi-VN"));
            txtProductPrice.Text = zInfo.ProductPrice.ToString("n0", CultureInfo.GetCultureInfo("vi-VN"));
            txtSafetyInStock.Text = zInfo.SafetyInStock.ToString();
            cboCategory.SelectedValue = zInfo.CategoryKey;
            cboStatus.SelectedValue = zInfo.StatusKey;
            cboUnit.SelectedValue = zInfo.StandardUnitKey;

            ProductProperty_FED_Info zPro = new ProductProperty_FED_Info();
            zPro.GetProduct_Info(zInfo.ProductKey);
            cboColor.SelectedValue = zPro.ColorKey;
            cboSize.SelectedValue = zPro.SizeKey;

            LoadListView();
        }

        #endregion

        #region [Dùng kéo rê form]
        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;



        private void Frm_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }
        private void Frm_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }
        private void Frm_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        #region [Auth]
        User_Role_Info _RoleForm = new User_Role_Info();
        private void Get_Auth()
        {
            List<User_Role_Info> zListRole = SessionUser.TableRole;
            if (zListRole.Count > 0)
            {
                try
                {
                    _RoleForm = zListRole.SingleOrDefault(r => r.RoleURL == FormUrl);
                    if (_RoleForm == null)
                    {
                        _RoleForm = new User_Role_Info();
                        MessageBox.Show("Form này chưa được thêm quyền, vui lòng liên hệ IT !.");
                        this.Close();
                    }
                    else
                    {
                        if (!_RoleForm.RoleRead)
                        {
                            MessageBox.Show("Bạn không có quyền xem, vui lòng liên hệ IT !.");
                            this.Close();
                        }
                        if (!_RoleForm.RoleEdit)
                        {
                            btnSave.Visible = false;
                        }
                        if (!_RoleForm.RoleAdd)
                        {
                            btnSave.Visible = false;
                        }
                        if (!_RoleForm.RoleDel)
                        {
                            btnDelete.Visible = false;
                        }
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Bạn chưa được phân quyền, vui lòng liên hệ IT !.");
            }
        }
        #endregion       

        #region[Access Info -Data]
        public class Product_FED_Info
        {

            #region [ Field Name ]
            private string _ProductKey = "";
            private string _ProductID = "";
            private string _ProductName = "";
            private string _ProductNumber = "";
            private string _ProductSerial = "";
            private string _ProductModel = "";
            private double _StandardCost = 0;
            private double _RentCost = 0;
            private double _ProductPrice = 0;
            private int _StandardUnitKey = 0;
            private string _StandardUnitName = "";
            private DateTime? _DiscontinuedDate = null;
            private float _SafetyInStock;
            private string _BarCode = "";
            private string _QRCode = "";
            private string _Style = "";
            private string _Class = "";
            private string _ProductLine = "";
            private int _CategoryKey = 0;
            private string _CategoryName = "";
            private int _ProductModeKey = 0;
            private string _PhotoPath = "";
            private int _StatusKey = 0;
            private string _StatusName = "";
            private string _Description = "";
            private string _PartnerNumber = "";
            private int _RecordStatus = 0;
            private DateTime? _CreatedOn = null;
            private string _CreatedBy = "";
            private string _CreatedName = "";
            private DateTime? _ModifiedOn = null;
            private string _ModifiedBy = "";
            private string _ModifiedName = "";
            private string _Message = "";
            #endregion

            #region [ Constructor Get Information ]
            public Product_FED_Info()
            {
                Guid zNewID = Guid.NewGuid();
                _ProductKey = zNewID.ToString();
            }
            public Product_FED_Info(string ProductKey)
            {
                string zSQL = "SELECT * FROM [dbo].[PDT_Product_FED] WHERE ProductKey = @ProductKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(ProductKey);
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _ProductKey = zReader["ProductKey"].ToString();
                        _ProductID = zReader["ProductID"].ToString();
                        _ProductName = zReader["ProductName"].ToString();
                        _ProductNumber = zReader["ProductNumber"].ToString();
                        _ProductSerial = zReader["ProductSerial"].ToString();
                        _ProductModel = zReader["ProductModel"].ToString();
                        _StandardCost = double.Parse(zReader["StandardCost"].ToString());
                        _RentCost = double.Parse(zReader["RentCost"].ToString());
                        _ProductPrice = double.Parse(zReader["ProductPrice"].ToString());
                        _StandardUnitKey = int.Parse(zReader["StandardUnitKey"].ToString());
                        _StandardUnitName = zReader["StandardUnitName"].ToString();
                        if (zReader["DiscontinuedDate"] != DBNull.Value)
                        {
                            _DiscontinuedDate = (DateTime)zReader["DiscontinuedDate"];
                        }

                        _SafetyInStock = float.Parse(zReader["SafetyInStock"].ToString());
                        _BarCode = zReader["BarCode"].ToString();
                        _QRCode = zReader["QRCode"].ToString();
                        _Style = zReader["Style"].ToString();
                        _Class = zReader["Class"].ToString();
                        _ProductLine = zReader["ProductLine"].ToString();
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                        _CategoryName = zReader["CategoryName"].ToString();
                        _ProductModeKey = int.Parse(zReader["ProductModeKey"].ToString());
                        _PhotoPath = zReader["PhotoPath"].ToString();
                        _StatusKey = int.Parse(zReader["StatusKey"].ToString());
                        _StatusName = zReader["StatusName"].ToString();
                        _Description = zReader["Description"].ToString();
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }

            #endregion

            #region [ Properties ]
            public string ProductKey
            {
                get { return _ProductKey; }
                set { _ProductKey = value; }
            }
            public string ProductID
            {
                get { return _ProductID; }
                set { _ProductID = value; }
            }
            public string ProductName
            {
                get { return _ProductName; }
                set { _ProductName = value; }
            }
            public string ProductNumber
            {
                get { return _ProductNumber; }
                set { _ProductNumber = value; }
            }
            public string ProductSerial
            {
                get { return _ProductSerial; }
                set { _ProductSerial = value; }
            }
            public string ProductModel
            {
                get { return _ProductModel; }
                set { _ProductModel = value; }
            }
            public double StandardCost
            {
                get { return _StandardCost; }
                set { _StandardCost = value; }
            }
            public double RentCost
            {
                get { return _RentCost; }
                set { _RentCost = value; }
            }
            public double ProductPrice
            {
                get { return _ProductPrice; }
                set { _ProductPrice = value; }
            }
            public int StandardUnitKey
            {
                get { return _StandardUnitKey; }
                set { _StandardUnitKey = value; }
            }
            public string StandardUnitName
            {
                get { return _StandardUnitName; }
                set { _StandardUnitName = value; }
            }
            public DateTime? DiscontinuedDate
            {
                get { return _DiscontinuedDate; }
                set { _DiscontinuedDate = value; }
            }
            public float SafetyInStock
            {
                get { return _SafetyInStock; }
                set { _SafetyInStock = value; }
            }
            public string BarCode
            {
                get { return _BarCode; }
                set { _BarCode = value; }
            }
            public string QRCode
            {
                get { return _QRCode; }
                set { _QRCode = value; }
            }
            public string Style
            {
                get { return _Style; }
                set { _Style = value; }
            }
            public string Class
            {
                get { return _Class; }
                set { _Class = value; }
            }
            public string ProductLine
            {
                get { return _ProductLine; }
                set { _ProductLine = value; }
            }
            public int CategoryKey
            {
                get { return _CategoryKey; }
                set { _CategoryKey = value; }
            }
            public string CategoryName
            {
                get { return _CategoryName; }
                set { _CategoryName = value; }
            }
            public int ProductModeKey
            {
                get { return _ProductModeKey; }
                set { _ProductModeKey = value; }
            }
            public string PhotoPath
            {
                get { return _PhotoPath; }
                set { _PhotoPath = value; }
            }
            public int StatusKey
            {
                get { return _StatusKey; }
                set { _StatusKey = value; }
            }
            public string StatusName
            {
                get { return _StatusName; }
                set { _StatusName = value; }
            }
            public string Description
            {
                get { return _Description; }
                set { _Description = value; }
            }
            public string PartnerNumber
            {
                get { return _PartnerNumber; }
                set { _PartnerNumber = value; }
            }
            public int RecordStatus
            {
                get { return _RecordStatus; }
                set { _RecordStatus = value; }
            }
            public DateTime? CreatedOn
            {
                get { return _CreatedOn; }
                set { _CreatedOn = value; }
            }
            public string CreatedBy
            {
                get { return _CreatedBy; }
                set { _CreatedBy = value; }
            }
            public string CreatedName
            {
                get { return _CreatedName; }
                set { _CreatedName = value; }
            }
            public DateTime? ModifiedOn
            {
                get { return _ModifiedOn; }
                set { _ModifiedOn = value; }
            }
            public string ModifiedBy
            {
                get { return _ModifiedBy; }
                set { _ModifiedBy = value; }
            }
            public string ModifiedName
            {
                get { return _ModifiedName; }
                set { _ModifiedName = value; }
            }
            public string Code
            {
                get
                {
                    if (_Message.Length >= 3)
                    {
                        return _Message.Substring(0, 3);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            public string Message
            {
                get { return _Message; }
                set { _Message = value; }
            }
            #endregion

            #region [ Constructor Update Information ]

            public string Create_ServerKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[PDT_Product_FED] ("
            + " ProductID ,ProductName ,ProductNumber ,ProductSerial ,ProductModel ,StandardCost ,RentCost ,ProductPrice ,StandardUnitKey ,StandardUnitName ,DiscontinuedDate ,SafetyInStock ,BarCode ,QRCode ,Style ,Class ,ProductLine ,CategoryKey ,CategoryName ,ProductModeKey ,PhotoPath ,StatusKey ,StatusName ,Description ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@ProductID ,@ProductName ,@ProductNumber ,@ProductSerial ,@ProductModel ,@StandardCost ,@RentCost ,@ProductPrice ,@StandardUnitKey ,@StandardUnitName ,@DiscontinuedDate ,@SafetyInStock ,@BarCode ,@QRCode ,@Style ,@Class ,@ProductLine ,@CategoryKey ,@CategoryName ,@ProductModeKey ,@PhotoPath ,@StatusKey ,@StatusName ,@Description ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID;
                    zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = _ProductName;
                    zCommand.Parameters.Add("@ProductNumber", SqlDbType.NVarChar).Value = _ProductNumber;
                    zCommand.Parameters.Add("@ProductSerial", SqlDbType.NVarChar).Value = _ProductSerial;
                    zCommand.Parameters.Add("@ProductModel", SqlDbType.NVarChar).Value = _ProductModel;
                    zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = _StandardCost;
                    zCommand.Parameters.Add("@RentCost", SqlDbType.Money).Value = _RentCost;
                    zCommand.Parameters.Add("@ProductPrice", SqlDbType.Money).Value = _ProductPrice;
                    zCommand.Parameters.Add("@StandardUnitKey", SqlDbType.Int).Value = _StandardUnitKey;
                    zCommand.Parameters.Add("@StandardUnitName", SqlDbType.NVarChar).Value = _StandardUnitName;
                    if (_DiscontinuedDate == null)
                    {
                        zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = _DiscontinuedDate;
                    }

                    zCommand.Parameters.Add("@SafetyInStock", SqlDbType.Float).Value = _SafetyInStock;
                    zCommand.Parameters.Add("@BarCode", SqlDbType.NVarChar).Value = _BarCode;
                    zCommand.Parameters.Add("@QRCode", SqlDbType.NVarChar).Value = _QRCode;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@ProductLine", SqlDbType.NChar).Value = _ProductLine;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@ProductModeKey", SqlDbType.Int).Value = _ProductModeKey;
                    zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = _PhotoPath;
                    zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = _StatusKey;
                    zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = _StatusName;
                    zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Create_ClientKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[PDT_Product_FED] ("
            + " ProductKey ,ProductID ,ProductName ,ProductNumber ,ProductSerial ,ProductModel ,StandardCost ,RentCost ,ProductPrice ,StandardUnitKey ,StandardUnitName ,DiscontinuedDate ,SafetyInStock ,BarCode ,QRCode ,Style ,Class ,ProductLine ,CategoryKey ,CategoryName ,ProductModeKey ,PhotoPath ,StatusKey ,StatusName ,Description ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@ProductKey ,@ProductID ,@ProductName ,@ProductNumber ,@ProductSerial ,@ProductModel ,@StandardCost ,@RentCost ,@ProductPrice ,@StandardUnitKey ,@StandardUnitName ,@DiscontinuedDate ,@SafetyInStock ,@BarCode ,@QRCode ,@Style ,@Class ,@ProductLine ,@CategoryKey ,@CategoryName ,@ProductModeKey ,@PhotoPath ,@StatusKey ,@StatusName ,@Description ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ProductKey);
                    zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID;
                    zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = _ProductName;
                    zCommand.Parameters.Add("@ProductNumber", SqlDbType.NVarChar).Value = _ProductNumber;
                    zCommand.Parameters.Add("@ProductSerial", SqlDbType.NVarChar).Value = _ProductSerial;
                    zCommand.Parameters.Add("@ProductModel", SqlDbType.NVarChar).Value = _ProductModel;
                    zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = _StandardCost;
                    zCommand.Parameters.Add("@RentCost", SqlDbType.Money).Value = _RentCost;
                    zCommand.Parameters.Add("@ProductPrice", SqlDbType.Money).Value = _ProductPrice;
                    zCommand.Parameters.Add("@StandardUnitKey", SqlDbType.Int).Value = _StandardUnitKey;
                    zCommand.Parameters.Add("@StandardUnitName", SqlDbType.NVarChar).Value = _StandardUnitName;
                    if (_DiscontinuedDate == null)
                    {
                        zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = _DiscontinuedDate;
                    }

                    zCommand.Parameters.Add("@SafetyInStock", SqlDbType.Float).Value = _SafetyInStock;
                    zCommand.Parameters.Add("@BarCode", SqlDbType.NVarChar).Value = _BarCode;
                    zCommand.Parameters.Add("@QRCode", SqlDbType.NVarChar).Value = _QRCode;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@ProductLine", SqlDbType.NChar).Value = _ProductLine;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@ProductModeKey", SqlDbType.Int).Value = _ProductModeKey;
                    zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = _PhotoPath;
                    zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = _StatusKey;
                    zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = _StatusName;
                    zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Update()
            {
                string zSQL = "UPDATE [dbo].[PDT_Product_FED] SET "
                            + " ProductID = @ProductID,"
                            + " ProductName = @ProductName,"
                            + " ProductNumber = @ProductNumber,"
                            + " ProductSerial = @ProductSerial,"
                            + " ProductModel = @ProductModel,"
                            + " StandardCost = @StandardCost,"
                            + " RentCost = @RentCost,"
                            + " ProductPrice = @ProductPrice,"
                            + " StandardUnitKey = @StandardUnitKey,"
                            + " StandardUnitName = @StandardUnitName,"
                            + " DiscontinuedDate = @DiscontinuedDate,"
                            + " SafetyInStock = @SafetyInStock,"
                            + " BarCode = @BarCode,"
                            + " QRCode = @QRCode,"
                            + " Style = @Style,"
                            + " Class = @Class,"
                            + " ProductLine = @ProductLine,"
                            + " CategoryKey = @CategoryKey,"
                            + " CategoryName = @CategoryName,"
                            + " ProductModeKey = @ProductModeKey,"
                            + " PhotoPath = @PhotoPath,"
                            + " StatusKey = @StatusKey,"
                            + " StatusName = @StatusName,"
                            + " Description = @Description,"
                            + " PartnerNumber = @PartnerNumber,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedOn = GetDate(),"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName"
                           + " WHERE ProductKey = @ProductKey";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ProductKey);
                    zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID;
                    zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = _ProductName;
                    zCommand.Parameters.Add("@ProductNumber", SqlDbType.NVarChar).Value = _ProductNumber;
                    zCommand.Parameters.Add("@ProductSerial", SqlDbType.NVarChar).Value = _ProductSerial;
                    zCommand.Parameters.Add("@ProductModel", SqlDbType.NVarChar).Value = _ProductModel;
                    zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = _StandardCost;
                    zCommand.Parameters.Add("@RentCost", SqlDbType.Money).Value = _RentCost;
                    zCommand.Parameters.Add("@ProductPrice", SqlDbType.Money).Value = _ProductPrice;
                    zCommand.Parameters.Add("@StandardUnitKey", SqlDbType.Int).Value = _StandardUnitKey;
                    zCommand.Parameters.Add("@StandardUnitName", SqlDbType.NVarChar).Value = _StandardUnitName;
                    if (_DiscontinuedDate == null)
                    {
                        zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = _DiscontinuedDate;
                    }

                    zCommand.Parameters.Add("@SafetyInStock", SqlDbType.Float).Value = _SafetyInStock;
                    zCommand.Parameters.Add("@BarCode", SqlDbType.NVarChar).Value = _BarCode;
                    zCommand.Parameters.Add("@QRCode", SqlDbType.NVarChar).Value = _QRCode;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@ProductLine", SqlDbType.NChar).Value = _ProductLine;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@ProductModeKey", SqlDbType.Int).Value = _ProductModeKey;
                    zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = _PhotoPath;
                    zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = _StatusKey;
                    zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = _StatusName;
                    zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Delete()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[PDT_Product_FED] Set RecordStatus = 99 WHERE ProductKey = @ProductKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ProductKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[PDT_Product_FED] WHERE ProductKey = @ProductKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ProductKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            #endregion
        }
        public class Access_Data
        {
            #region[Standard]
            public static bool CheckProductID(string PartnerNumber, string ProductID)
            {
                bool zResult = false;  //  Khong co
                DataTable zTable = new DataTable();
                string zSQL = @"SELECT Count(*) AS Amount FROM [dbo].[PDT_Product_FED] WHERE PartnerNumber = @PartnerNumber AND ProductID = @ProductID AND RecordStatus != 99";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = ProductID;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string Result = ex.ToString();
                }
                if (zTable.Rows.Count > 0)
                {
                    int zAmount = 0;
                    DataRow zRow = zTable.Rows[0];
                    zAmount = int.Parse(zRow["Amount"].ToString());
                    if (zAmount == 0)
                    {
                        zResult = true;
                    }
                    else
                    {
                        zResult = false;
                    }
                }
                return zResult;
            }
            #endregion

            public static DataTable ListPhoto(string ProductKey)
            {
                DataTable zTable = new DataTable();
                string zSQL = "SELECT * FROM [dbo].[PDT_ProductPhoto_FED] WHERE ProductKey = @ProductKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(ProductKey);
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
        }

        public class ProductProperty_FED_Info
        {

            #region [ Field Name ]
            private string _PropertyKey = "";
            private string _ProductKey = "";
            private string _ProductID = "";
            private string _ProductName = "";
            private int _SizeKey = 0;
            private string _SizeName = "";
            private int _ColorKey = 0;
            private string _ColorName = "";
            private double _ProductPrice = 0;
            private string _Description = "";
            private string _PartnerNumber = "";
            private int _RecordStatus = 0;
            private DateTime? _CreatedOn = null;
            private string _CreatedBy = "";
            private string _CreatedName = "";
            private DateTime? _ModifiedOn = null;
            private string _ModifiedBy = "";
            private string _ModifiedName = "";
            private string _Message = "";
            #endregion

            #region [ Constructor Get Information ]
            public ProductProperty_FED_Info()
            {
                Guid zNewID = Guid.NewGuid();
                _PropertyKey = zNewID.ToString();
            }
            public ProductProperty_FED_Info(string PropertyKey)
            {
                string zSQL = "SELECT * FROM [dbo].[PDT_Product_Property_FED] WHERE PropertyKey = @PropertyKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@PropertyKey", SqlDbType.UniqueIdentifier).Value = new Guid(PropertyKey);
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _PropertyKey = zReader["PropertyKey"].ToString();
                        _ProductKey = zReader["ProductKey"].ToString();
                        _ProductID = zReader["ProductID"].ToString();
                        _ProductName = zReader["ProductName"].ToString();
                        if (zReader["SizeKey"] != DBNull.Value)
                        {
                            _SizeKey = int.Parse(zReader["SizeKey"].ToString());
                        }

                        _SizeName = zReader["SizeName"].ToString();
                        if (zReader["ColorKey"] != DBNull.Value)
                        {
                            _ColorKey = int.Parse(zReader["ColorKey"].ToString());
                        }

                        _ColorName = zReader["ColorName"].ToString();
                        if (zReader["ProductPrice"] != DBNull.Value)
                        {
                            _ProductPrice = double.Parse(zReader["ProductPrice"].ToString());
                        }

                        _Description = zReader["Description"].ToString();
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        if (zReader["RecordStatus"] != DBNull.Value)
                        {
                            _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        }

                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }
            public void GetProduct_Info(string ProductKey)
            {
                string zSQL = "SELECT * FROM [dbo].[PDT_Product_Property_FED] WHERE ProductKey = @ProductKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(ProductKey);
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _PropertyKey = zReader["PropertyKey"].ToString();
                        _ProductKey = zReader["ProductKey"].ToString();
                        _ProductID = zReader["ProductID"].ToString();
                        _ProductName = zReader["ProductName"].ToString();
                        if (zReader["SizeKey"] != DBNull.Value)
                        {
                            _SizeKey = int.Parse(zReader["SizeKey"].ToString());
                        }
                        _SizeName = zReader["SizeName"].ToString();
                        if (zReader["ColorKey"] != DBNull.Value)
                        {
                            _ColorKey = int.Parse(zReader["ColorKey"].ToString());
                        }
                        _ColorName = zReader["ColorName"].ToString();
                        _ProductPrice = double.Parse(zReader["ProductPrice"].ToString());
                        _Description = zReader["Description"].ToString();
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }
            #endregion

            #region [ Properties ]
            public string PropertyKey
            {
                get { return _PropertyKey; }
                set { _PropertyKey = value; }
            }
            public string ProductKey
            {
                get { return _ProductKey; }
                set { _ProductKey = value; }
            }
            public string ProductID
            {
                get { return _ProductID; }
                set { _ProductID = value; }
            }
            public string ProductName
            {
                get { return _ProductName; }
                set { _ProductName = value; }
            }
            public int SizeKey
            {
                get { return _SizeKey; }
                set { _SizeKey = value; }
            }
            public string SizeName
            {
                get { return _SizeName; }
                set { _SizeName = value; }
            }
            public int ColorKey
            {
                get { return _ColorKey; }
                set { _ColorKey = value; }
            }
            public string ColorName
            {
                get { return _ColorName; }
                set { _ColorName = value; }
            }
            public double ProductPrice
            {
                get { return _ProductPrice; }
                set { _ProductPrice = value; }
            }
            public string Description
            {
                get { return _Description; }
                set { _Description = value; }
            }
            public string PartnerNumber
            {
                get { return _PartnerNumber; }
                set { _PartnerNumber = value; }
            }
            public int RecordStatus
            {
                get { return _RecordStatus; }
                set { _RecordStatus = value; }
            }
            public DateTime? CreatedOn
            {
                get { return _CreatedOn; }
                set { _CreatedOn = value; }
            }
            public string CreatedBy
            {
                get { return _CreatedBy; }
                set { _CreatedBy = value; }
            }
            public string CreatedName
            {
                get { return _CreatedName; }
                set { _CreatedName = value; }
            }
            public DateTime? ModifiedOn
            {
                get { return _ModifiedOn; }
                set { _ModifiedOn = value; }
            }
            public string ModifiedBy
            {
                get { return _ModifiedBy; }
                set { _ModifiedBy = value; }
            }
            public string ModifiedName
            {
                get { return _ModifiedName; }
                set { _ModifiedName = value; }
            }
            public string Code
            {
                get
                {
                    if (_Message.Length >= 3)
                    {
                        return _Message.Substring(0, 3);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            public string Message
            {
                get { return _Message; }
                set { _Message = value; }
            }
            #endregion

            #region [ Constructor Update Information ]

            public string Create_ServerKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[PDT_Product_Property_FED] ("
            + " ProductKey ,ProductID ,ProductName ,SizeKey ,SizeName ,ColorKey ,ColorName ,ProductPrice ,Description ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@ProductKey ,@ProductID ,@ProductName ,@SizeKey ,@SizeName ,@ColorKey ,@ColorName ,@ProductPrice ,@Description ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ProductKey);
                    zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID;
                    zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = _ProductName;
                    zCommand.Parameters.Add("@SizeKey", SqlDbType.Char).Value = _SizeKey;
                    zCommand.Parameters.Add("@SizeName", SqlDbType.NChar).Value = _SizeName;
                    zCommand.Parameters.Add("@ColorKey", SqlDbType.Char).Value = _ColorKey;
                    zCommand.Parameters.Add("@ColorName", SqlDbType.NVarChar).Value = _ColorName;
                    zCommand.Parameters.Add("@ProductPrice", SqlDbType.Money).Value = _ProductPrice;
                    zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Create_ClientKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[PDT_Product_Property_FED] ("
            + " PropertyKey ,ProductKey ,ProductID ,ProductName ,SizeKey ,SizeName ,ColorKey ,ColorName ,ProductPrice ,Description ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@PropertyKey ,@ProductKey ,@ProductID ,@ProductName ,@SizeKey ,@SizeName ,@ColorKey ,@ColorName ,@ProductPrice ,@Description ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@PropertyKey", SqlDbType.UniqueIdentifier).Value = new Guid(_PropertyKey);
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ProductKey);
                    zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID;
                    zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = _ProductName;
                    zCommand.Parameters.Add("@SizeKey", SqlDbType.Char).Value = _SizeKey;
                    zCommand.Parameters.Add("@SizeName", SqlDbType.NChar).Value = _SizeName;
                    zCommand.Parameters.Add("@ColorKey", SqlDbType.Char).Value = _ColorKey;
                    zCommand.Parameters.Add("@ColorName", SqlDbType.NVarChar).Value = _ColorName;
                    zCommand.Parameters.Add("@ProductPrice", SqlDbType.Money).Value = _ProductPrice;
                    zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Update()
            {
                string zSQL = "UPDATE [dbo].[PDT_Product_Property_FED] SET "
                            + " ProductKey = @ProductKey,"
                            + " ProductID = @ProductID,"
                            + " ProductName = @ProductName,"
                            + " SizeKey = @SizeKey,"
                            + " SizeName = @SizeName,"
                            + " ColorKey = @ColorKey,"
                            + " ColorName = @ColorName,"
                            + " ProductPrice = @ProductPrice,"
                            + " Description = @Description,"
                            + " PartnerNumber = @PartnerNumber,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedOn = GetDate(),"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName"
                           + " WHERE PropertyKey = @PropertyKey";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@PropertyKey", SqlDbType.UniqueIdentifier).Value = new Guid(_PropertyKey);
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ProductKey);
                    zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID;
                    zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = _ProductName;
                    zCommand.Parameters.Add("@SizeKey", SqlDbType.Int).Value = _SizeKey;
                    zCommand.Parameters.Add("@SizeName", SqlDbType.NChar).Value = _SizeName;
                    zCommand.Parameters.Add("@ColorKey", SqlDbType.Int).Value = _ColorKey;
                    zCommand.Parameters.Add("@ColorName", SqlDbType.NVarChar).Value = _ColorName;
                    zCommand.Parameters.Add("@ProductPrice", SqlDbType.Money).Value = _ProductPrice;
                    zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Delete()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[PDT_Product_Property_FED] Set RecordStatus = 99 WHERE PropertyKey = @PropertyKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PropertyKey", SqlDbType.UniqueIdentifier).Value = new Guid(_PropertyKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[PDT_Product_Property_FED] WHERE PropertyKey = @PropertyKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PropertyKey", SqlDbType.UniqueIdentifier).Value = new Guid(_PropertyKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            #endregion
        }
        public class ProductPhoto_FED_Info
        {

            #region [ Field Name ]
            private string _PhotoKey = "";
            private string _ProductKey = "";
            private string _ProductID = "";
            private string _ProductName = "";
            private string _PhotoPath = "";
            private string _PartnerNumber = "";
            private int _RecordStatus = 0;
            private DateTime? _CreatedOn = null;
            private string _CreatedBy = "";
            private string _CreatedName = "";
            private DateTime? _ModifiedOn = null;
            private string _ModifiedBy = "";
            private string _ModifiedName = "";
            private string _Message = "";
            #endregion

            #region [ Constructor Get Information ]
            public ProductPhoto_FED_Info()
            {
                Guid zNewID = Guid.NewGuid();
                _PhotoKey = zNewID.ToString();
            }
            public ProductPhoto_FED_Info(string PhotoKey)
            {
                string zSQL = "SELECT * FROM [dbo].[PDT_ProductPhoto_FED] WHERE PhotoKey = @PhotoKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@PhotoKey", SqlDbType.UniqueIdentifier).Value = new Guid(PhotoKey);
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _PhotoKey = zReader["PhotoKey"].ToString();
                        _ProductKey = zReader["ProductKey"].ToString();
                        _ProductID = zReader["ProductID"].ToString();
                        _ProductName = zReader["ProductName"].ToString();
                        _PhotoPath = zReader["PhotoPath"].ToString();
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }

            #endregion

            #region [ Properties ]
            public string PhotoKey
            {
                get { return _PhotoKey; }
                set { _PhotoKey = value; }
            }
            public string ProductKey
            {
                get { return _ProductKey; }
                set { _ProductKey = value; }
            }
            public string ProductID
            {
                get { return _ProductID; }
                set { _ProductID = value; }
            }
            public string ProductName
            {
                get { return _ProductName; }
                set { _ProductName = value; }
            }
            public string PhotoPath
            {
                get { return _PhotoPath; }
                set { _PhotoPath = value; }
            }
            public string PartnerNumber
            {
                get { return _PartnerNumber; }
                set { _PartnerNumber = value; }
            }
            public int RecordStatus
            {
                get { return _RecordStatus; }
                set { _RecordStatus = value; }
            }
            public DateTime? CreatedOn
            {
                get { return _CreatedOn; }
                set { _CreatedOn = value; }
            }
            public string CreatedBy
            {
                get { return _CreatedBy; }
                set { _CreatedBy = value; }
            }
            public string CreatedName
            {
                get { return _CreatedName; }
                set { _CreatedName = value; }
            }
            public DateTime? ModifiedOn
            {
                get { return _ModifiedOn; }
                set { _ModifiedOn = value; }
            }
            public string ModifiedBy
            {
                get { return _ModifiedBy; }
                set { _ModifiedBy = value; }
            }
            public string ModifiedName
            {
                get { return _ModifiedName; }
                set { _ModifiedName = value; }
            }
            public string Code
            {
                get
                {
                    if (_Message.Length >= 3)
                    {
                        return _Message.Substring(0, 3);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            public string Message
            {
                get { return _Message; }
                set { _Message = value; }
            }
            #endregion

            #region [ Constructor Update Information ]

            public string Create_ServerKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[PDT_ProductPhoto_FED] ("
            + " ProductKey ,ProductID ,ProductName ,PhotoPath ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@ProductKey ,@ProductID ,@ProductName ,@PhotoPath ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ProductKey);
                    zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID;
                    zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = _ProductName;
                    zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = _PhotoPath;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Create_ClientKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[PDT_ProductPhoto_FED] ("
            + " PhotoKey ,ProductKey ,ProductID ,ProductName ,PhotoPath ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@PhotoKey ,@ProductKey ,@ProductID ,@ProductName ,@PhotoPath ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@PhotoKey", SqlDbType.UniqueIdentifier).Value = new Guid(_PhotoKey);
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ProductKey);
                    zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID;
                    zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = _ProductName;
                    zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = _PhotoPath;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Update()
            {
                string zSQL = "UPDATE [dbo].[PDT_ProductPhoto_FED] SET "
                            + " ProductKey = @ProductKey,"
                            + " ProductID = @ProductID,"
                            + " ProductName = @ProductName,"
                            + " PhotoPath = @PhotoPath,"
                            + " PartnerNumber = @PartnerNumber,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedOn = GetDate(),"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName"
                           + " WHERE PhotoKey = @PhotoKey";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@PhotoKey", SqlDbType.UniqueIdentifier).Value = new Guid(_PhotoKey);
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ProductKey);
                    zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID;
                    zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = _ProductName;
                    zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = _PhotoPath;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Delete()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[PDT_ProductPhoto_FED] Set RecordStatus = 99 WHERE PhotoKey = @PhotoKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PhotoKey", SqlDbType.UniqueIdentifier).Value = new Guid(_PhotoKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[PDT_ProductPhoto_FED] WHERE PhotoKey = @PhotoKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PhotoKey", SqlDbType.UniqueIdentifier).Value = new Guid(_PhotoKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            #endregion
        }
        #endregion
    }
}
