﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using TN_Connection;
using TN_Tools;
using TN_User;

namespace TN_WinApp.IVT
{
    public partial class Frm_ComboList : Form
    {
        private string _FormUrl = "/IVT/Frm_ComboList";
        public string FormUrl
        {
            get
            {
                return _FormUrl;
            }

            set
            {
                _FormUrl = value;
            }
        }

        public Frm_ComboList()
        {
            InitializeComponent();

            TitleForm.MouseDown += Frm_MouseDown;
            TitleForm.MouseMove += Frm_MouseMove;
            TitleForm.MouseUp += Frm_MouseUp;

            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;

            LVData.ItemActivate += LVData_ItemActivate;
            LVData.KeyDown += LVData_KeyDown;

            btnAddNew.Click += BtnAddNew_Click;
            btnSearch.Click += BtnSearch_Click;

            this.Text = "Các gói sản phẩm";
            this.ShowInTaskbar = true;
            this.WindowState = FormWindowState.Maximized;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.FormBorderStyle = FormBorderStyle.None;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.Load += Frm_ComboList_Load;
        }

        private void Frm_ComboList_Load(object sender, EventArgs e)
        {
            Get_Auth();
            if (_RoleForm.RoleRead)
            {
                DateTime zFromDate = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0);
                DateTime zToDate = zFromDate.AddMonths(12).AddDays(-1);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

                dteFromDate.Value = zFromDate;
                dteToDate.Value = zToDate;

                DesignLayout(LVData);
                LoadListView();
            }
        }

        #region [Event]
        private void BtnAddNew_Click(object sender, EventArgs e)
        {
            Frm_ComboEdit Frm = new Frm_ComboEdit();
            Frm.ComboKey = "";
            Frm.ShowDialog();
            if (Frm.ComboKey != string.Empty)
            {
                LoadListView();
            }
        }
        private void BtnSearch_Click(object sender, EventArgs e)
        {
            LoadListView();
        }

        #endregion

        #region [Process]
        private string Delete(string Key)
        {
            Frm_ComboEdit.Combo_FED_Info zInfo = new Frm_ComboEdit.Combo_FED_Info();
            zInfo.ComboKey = Key;
            zInfo.Delete();
            zInfo.Delete_Detail();
            return zInfo.Message;
        }
        #endregion

        #region [ListView]
        private void DesignLayout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên gói";
            colHead.Width = 350;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Giá gói";
            colHead.Width = 130;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ghi chú";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            TNPaintControl.DrawLVStyle(ref LVData);
        }
        private void LoadListView()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable zTable = Access_Data.List(SessionUser.UserLogin.PartnerNumber, txtSearch.Text.Trim(), dteFromDate.Value, dteToDate.Value);
            LV.Items.Clear();
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow zRow = zTable.Rows[i];
                lvi.Tag = zRow["ComboKey"].ToString();
                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["ComboID"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["ComboName"].ToString();
                lvi.SubItems.Add(lvsi);

                double ComboCost = zRow["ComboCost"].ToDouble();
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = ComboCost.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["Description"].ToString();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;

        }

        private void LVData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes && LVData.SelectedItems.Count > 0)
                {
                    string Key = LVData.SelectedItems[0].Tag.ToString();
                    string zResult = Delete(Key);

                    if (zResult.Substring(0, 3) == "200" ||
                        zResult.Substring(0, 3) == "201")
                    {
                        MessageBox.Show("Đã xóa thành công !");
                        LoadListView();
                    }
                }
            }
        }

        private void LVData_ItemActivate(object sender, EventArgs e)
        {
            if (LVData.SelectedItems.Count > 0)
            {
                Frm_ComboEdit Frm = new Frm_ComboEdit();
                Frm.ComboKey = LVData.SelectedItems[0].Tag.ToString();
                Frm.ShowDialog();
                LoadListView();
            }
        }
        #endregion

        #region [Dùng kéo rê form]
        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;



        private void Frm_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }
        private void Frm_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }
        private void Frm_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        #region [Auth]
        User_Role_Info _RoleForm = new User_Role_Info();
        private void Get_Auth()
        {
            List<User_Role_Info> zListRole = SessionUser.TableRole;
            if (zListRole.Count > 0)
            {
                try
                {
                    _RoleForm = zListRole.SingleOrDefault(r => r.RoleURL == FormUrl);
                    if (_RoleForm == null)
                    {
                        _RoleForm = new User_Role_Info();
                        MessageBox.Show("Form này chưa được thêm quyền, vui lòng liên hệ IT !.");
                        this.Close();
                    }
                    else
                    {
                        if (!_RoleForm.RoleRead)
                        {
                            MessageBox.Show("Bạn không có quyền xem, vui lòng liên hệ IT !.");
                            this.Close();
                        }
                        if (!_RoleForm.RoleEdit)
                        {

                        }
                        if (!_RoleForm.RoleAdd)
                        {

                        }
                        if (!_RoleForm.RoleDel)
                        {

                        }
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Bạn chưa được phân quyền, vui lòng liên hệ IT !.");
            }
        }
        #endregion       

        #region[Access_Data]
        public class Access_Data
        {
            public static DataTable List(string PartnerNumber, string Name, DateTime FromDate, DateTime ToDate)
            {
                DataTable zTable = new DataTable();
                string zSQL = @"
SELECT * 
FROM [dbo].[PDT_Combo_FED] 
WHERE RecordStatus <> 99  
AND PartnerNumber = @PartnerNumber ";

                if (Name.Trim().Length > 0)
                {
                    zSQL += " AND (ComboID LIKE @Name OR ComboName LIKE @Name)";
                }

                if (FromDate != null && ToDate != null)
                {
                    zSQL += " AND FromDate >= @FromDate AND ToDate <= @ToDate";
                }

                zSQL += " ORDER BY CreatedOn DESC";
                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
            public static DataTable List(string PartnerNumber, string OrderBy, int PageNumber, int PageSize)
            {
                DataTable zTable = new DataTable();
                string zSQL = "SELECT  * FROM [dbo].[PDT_Combo_FED] WHERE PartnerNumber = @PartnerNumber AND RecordStatus != 99 ";
                if (OrderBy.Trim().Length > 0)
                {
                    zSQL += " ORDER BY " + OrderBy;
                }

                zSQL += " OFFSET @PageSize * (@PageNumber - 1) ROWS " +
                      " FETCH NEXT @PageSize ROWS ONLY OPTION(RECOMPILE)";
                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = PageSize;
                    zCommand.Parameters.Add("@PageNumber", SqlDbType.Int).Value = PageNumber;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
            public static int Total_Record(string PartnerNumber)
            {
                int zQuantity = 0;
                string zSQL = "SELECT  Count(*) FROM [dbo].[PDT_Combo_FED] WHERE PartnerNumber = @PartnerNumber AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    string zResult = zCommand.ExecuteScalar().ToString();
                    int.TryParse(zResult, out zQuantity);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zQuantity;
            }
            public static DataTable List_Search(string Content, string PartnerNumber, string OrderBy, int PageNumber, int PageSize)
            {
                DataTable zTable = new DataTable();
                string zSQL = "SELECT  * FROM [PDT_Combo_FED] WHERE PartnerNumber = @PartnerNumber AND RecordStatus != 99 ";
                if (Content.Length > 1)
                {
                    zSQL += "AND (ComboID LIKE @Content OR ComboName LIKE @Content)";
                }
                else
                {
                    zSQL += " AND Name LIKE @Content ";
                }
                if (OrderBy.Trim().Length > 0)
                {
                    zSQL += " ORDER BY " + OrderBy;
                }

                zSQL += " OFFSET @PageSize * (@PageNumber - 1) ROWS " +
                      " FETCH NEXT @PageSize ROWS ONLY OPTION(RECOMPILE)";
                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    if (Content.Length > 1)
                    {
                        zCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = "%" + Content + "%";
                    }
                    else
                    {
                        zCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = Content + "%";
                    }

                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = PageSize;
                    zCommand.Parameters.Add("@PageNumber", SqlDbType.Int).Value = PageNumber;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
            public static int Total_Record(string Content, string PartnerNumber)
            {
                int zQuantity = 0;
                string zSQL = "SELECT  Count(*) FROM [dbo].[PDT_Combo_FED] WHERE PartnerNumber = @PartnerNumber AND RecordStatus != 99 ";
                if (Content.Length > 1)
                {
                    zSQL += "AND (ComboID LIKE @Content OR ComboName LIKE @Content)";
                }
                else
                {
                    zSQL += " AND Name LIKE @Content ";
                }
                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    if (Content.Length > 1)
                    {
                        zCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = "%" + Content + "%";
                    }
                    else
                    {
                        zCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = Content + "%";
                    }

                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    string zResult = zCommand.ExecuteScalar().ToString();
                    int.TryParse(zResult, out zQuantity);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zQuantity;
            }
        }
        #endregion]
    }
}