﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using TN_Connection;
using TN_Tools;
using TN_User;

namespace TN_WinApp.IVT
{
    public partial class Frm_ComboEdit : Form
    {
        private string _FormUrl = "/IVT/Frm_ComboEdit";
        private string _ComboKey = "";
        public string ComboKey
        {
            get
            {
                return _ComboKey;
            }

            set
            {
                _ComboKey = value;
            }
        }
        public string FormUrl
        {
            get
            {
                return _FormUrl;
            }

            set
            {
                _FormUrl = value;
            }
        }

        public Frm_ComboEdit()
        {
            InitializeComponent();

            TitleForm.MouseDown += Frm_MouseDown;
            TitleForm.MouseMove += Frm_MouseMove;
            TitleForm.MouseUp += Frm_MouseUp;

            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;

            txtStandardCost.KeyPress += TextBoxNumber_KeyPress;
            txtStandardCost.Leave += (o, e) => { CalculatorCombo(); };

            txtSaleCost.KeyPress += TextBoxNumber_KeyPress;
            txtSaleCost.Leave += (o, e) => { CalculatorCombo(); };

            GVProduct.CellContentClick += GVProduct_CellContentClick;
            GVService.CellContentClick += GVService_CellContentClick;
            GVComboDetail.CellContentClick += GVComboDetail_CellContentClick;

            btnSearchProduct.Click += BtnSearchProduct_Click;
            btnSearchService.Click += BtnSearchService_Click;
            btnNew.Click += BtnNew_Click;
            btnDelete.Click += BtnDelete_Click;
            btnSave.Click += BtnSave_Click;

            this.Text = "Chi tiết gói sản phẩm";
            this.WindowState = FormWindowState.Maximized;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.FormBorderStyle = FormBorderStyle.None;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.Load += Frm_ComboEdit_Load;
        }

        private void Frm_ComboEdit_Load(object sender, EventArgs e)
        {
            Get_Auth();
            if (_RoleForm.RoleRead)
            {
                DateTime zFromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
                DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

                dteFromDate.Value = zFromDate;
                dteToDate.Value = zToDate;

                LoadCombobox();
                DesignLayout_GVComboDetail(GVComboDetail);
                DesignLayout_GVProduct(GVProduct);
                DesignLayout_GVService(GVService);
                LoadData();
            }
        }

        #region [Event]
        private void TextBoxNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            string zMessage = CheckBeforSave();
            if (zMessage.Length == 0)
            {
                string zResult = Save();
                if (zResult.Substring(0, 3) == "200" ||
                    zResult.Substring(0, 3) == "201")
                {
                    MessageBox.Show("Cập nhật gói sản phẩm thành công!");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Lỗi.Vui lòng liên hệ IT.Chi tiết lỗi mã : \n " + zResult);
                }
            }
            else
            {
                MessageBox.Show(zMessage);
            }
        }
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            if (ComboKey != "")
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    string zResult = Delete();
                    if (zResult.Substring(0, 3) == "200")
                    {
                        MessageBox.Show("Xóa thành công!");
                        ClearForm();
                    }
                    else
                    {
                        MessageBox.Show("Lỗi.Vui lòng liên hệ IT.Chi tiết lỗi mã: \n " + zResult);
                    }
                }
            }
        }
        private void BtnNew_Click(object sender, EventArgs e)
        {
            ClearForm();
        }
        private void BtnSearchProduct_Click(object sender, EventArgs e)
        {
            DataTable zTable = Access_Data.ListProduct(SessionUser.UserLogin.PartnerNumber, txtSearchProduct.Text.Trim(), cboCategoryProduct.SelectedValue.ToInt());
            LoadData_GVProduct(GVProduct, zTable);
        }
        private void BtnSearchService_Click(object sender, EventArgs e)
        {
            DataTable zTable = Access_Data.ListService(SessionUser.UserLogin.PartnerNumber, txtSearchProduct.Text.Trim(), cboCategoryService.SelectedValue.ToInt());
            LoadData_GVService(GVService, zTable);
        }
        #endregion

        #region [Left Panel]
        private void DesignLayout_GVProduct(DataGridView GV)
        {
            TNPaintControl.DrawGVStyle(ref GV);

            Image imgMinus = new Bitmap(TN_WinApp.Properties.Resources.image_plus);
            DataGridViewImageColumn GVImageCol = new DataGridViewImageColumn();
            GVImageCol.HeaderText = ".";
            GVImageCol.ImageLayout = DataGridViewImageCellLayout.Zoom;
            GVImageCol.Image = imgMinus;
            GVImageCol.Width = 32;
            GV.Columns.Add(GVImageCol); //

            GV.Columns.Add("No", "STT");
            GV.Columns.Add("ProductID", "Mã");
            GV.Columns.Add("ProductKey", "ProductKey");
            GV.Columns.Add("ProductName", "Tên");
            GV.Columns.Add("StandardUnitKey", "StandardUnitKey");
            GV.Columns.Add("StandardUnitName", "ĐVT");
            GV.Columns.Add("RentCost", "Giá thuê");
            GV.Columns.Add("Description", "Ghi chú");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["ProductKey"].Visible = false;

            GV.Columns["ProductID"].Width = 80;
            GV.Columns["ProductID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["ProductName"].Width = 300;
            GV.Columns["ProductName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["ProductName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["StandardUnitKey"].Visible = false;
            GV.Columns["StandardUnitName"].Width = 50;
            GV.Columns["StandardUnitName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["RentCost"].Width = 120;
            GV.Columns["RentCost"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["Description"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            GV.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["Description"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.ReadOnly = true;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = false;
            GV.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }
        private void LoadData_GVProduct(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();

            int i = 0;
            foreach (DataRow r in Table.Rows)
            {
                GV.Rows.Add();
                DataGridViewRow zGvRow = GV.Rows[i];
                zGvRow.Cells["No"].Value = (i + 1).ToString();
                zGvRow.Cells["ProductID"].Value = r["ProductID"].ToString().Trim();
                zGvRow.Cells["ProductName"].Value = r["ProductName"].ToString().Trim();
                zGvRow.Cells["ProductKey"].Value = r["ProductKey"].ToString();
                zGvRow.Cells["StandardUnitKey"].Value = r["StandardUnitKey"].ToString().Trim();
                zGvRow.Cells["StandardUnitName"].Value = r["StandardUnitName"].ToString().Trim();

                double RentCost = 0;
                if (r["RentCost"] != DBNull.Value)
                {
                    RentCost = r["RentCost"].ToDouble();
                    zGvRow.Cells["RentCost"].Value = RentCost.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                }

                i++;
            }
        }
        private void GVProduct_CellContentClick(object sender, EventArgs e)
        {
            if (GVProduct.CurrentCell.ColumnIndex == 0)
            {
                DataGridViewRow zGVRow = GVProduct.CurrentRow;
                ComboItem_FED_Info zItem = new ComboItem_FED_Info();

                zItem.ProductKey = zGVRow.Cells["ProductKey"].Value.ToString();
                zItem.ItemID = zGVRow.Cells["ProductID"].Value.ToString();
                zItem.ItemName = zGVRow.Cells["ProductName"].Value.ToString();
                zItem.Quantity = 1;

                if (zGVRow.Cells["RentCost"].Value != DBNull.Value)
                {
                    double Cost = double.Parse(zGVRow.Cells["RentCost"].Value.ToString(), CultureInfo.GetCultureInfo("vi-VN"));
                    zItem.StandardCost = Cost;
                }

                zItem.UnitKey = zGVRow.Cells["StandardUnitKey"].Value.ToInt();
                zItem.UnitName = zGVRow.Cells["StandardUnitName"].Value.ToString();

                AddItem(zItem);
                LoadData_GVComboDetail(GVComboDetail);
            }
        }

        private void DesignLayout_GVService(DataGridView GV)
        {
            TNPaintControl.DrawGVStyle(ref GV);

            Image imgMinus = new Bitmap(TN_WinApp.Properties.Resources.image_plus);
            DataGridViewImageColumn GVImageCol = new DataGridViewImageColumn();
            GVImageCol.HeaderText = ".";
            GVImageCol.ImageLayout = DataGridViewImageCellLayout.Zoom;
            GVImageCol.Image = imgMinus;
            GVImageCol.Width = 32;
            GV.Columns.Add(GVImageCol); //ServiceKey

            GV.Columns.Add("No", "STT");
            GV.Columns.Add("ServiceKey", "ServiceKey");
            GV.Columns.Add("ServiceID", "Mã");
            GV.Columns.Add("ServiceName", "Tên");
            GV.Columns.Add("StandardUnitKey", "StandardUnitKey");
            GV.Columns.Add("StandardUnitName", "ĐVT");
            GV.Columns.Add("StandardCost", "Giá thuê");

            GV.Columns["ServiceKey"].Visible = false;

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["ServiceID"].Width = 80;
            GV.Columns["ServiceID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["ServiceName"].Width = 350;
            GV.Columns["ServiceName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["ServiceName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["StandardUnitKey"].Visible = false;
            GV.Columns["StandardUnitName"].Width = 50;
            GV.Columns["StandardUnitName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["StandardCost"].Width = 120;
            GV.Columns["StandardCost"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.ReadOnly = true;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = false;
            GV.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }
        private void LoadData_GVService(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();

            int i = 0;
            foreach (DataRow r in Table.Rows)
            {
                GV.Rows.Add();
                DataGridViewRow zGvRow = GV.Rows[i];
                zGvRow.Cells["No"].Value = (i + 1).ToString();
                zGvRow.Cells["ServiceKey"].Value = r["ServiceKey"].ToString();
                zGvRow.Cells["ServiceID"].Value = r["ServiceID"].ToString().Trim();
                zGvRow.Cells["ServiceName"].Value = r["ServiceName"].ToString().Trim();

                zGvRow.Cells["StandardUnitKey"].Value = r["StandardUnitKey"].ToString().Trim();
                zGvRow.Cells["StandardUnitName"].Value = r["StandardUnitName"].ToString().Trim();

                double StandardCost = 0;
                if (r["StandardCost"] != DBNull.Value)
                {
                    StandardCost = r["StandardCost"].ToDouble();
                    zGvRow.Cells["StandardCost"].Value = StandardCost.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                }
                i++;
            }
        }
        private void GVService_CellContentClick(object sender, EventArgs e)
        {
            if (GVService.CurrentCell.ColumnIndex == 0)
            {
                DataGridViewRow zGVRow = GVService.CurrentRow;
                ComboItem_FED_Info zItem = new ComboItem_FED_Info();
                zItem.ServiceKey = zGVRow.Cells["ServiceKey"].Value.ToString();
                zItem.ItemID = zGVRow.Cells["ServiceID"].Value.ToString();
                zItem.ItemName = zGVRow.Cells["ServiceName"].Value.ToString();
                zItem.Quantity = 1;

                if (zGVRow.Cells["StandardCost"].Value != DBNull.Value)
                {
                    double StandardCost = double.Parse(zGVRow.Cells["StandardCost"].Value.ToString(), CultureInfo.GetCultureInfo("vi-VN"));
                    zItem.StandardCost = StandardCost;
                }

                zItem.UnitKey = zGVRow.Cells["StandardUnitKey"].Value.ToInt();
                zItem.UnitName = zGVRow.Cells["StandardUnitName"].Value.ToString();

                AddItem(zItem);
                LoadData_GVComboDetail(GVComboDetail);
            }
        }
        #endregion

        #region [Right Panel]
        private void GVComboDetail_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (GVComboDetail.CurrentCell.ColumnIndex == 9)
            {
                DataGridViewRow zGvRow = GVComboDetail.CurrentRow;
                string ID = zGvRow.Cells["ItemID"].Value.ToString();
                DeleteItem(ID);
                LoadData_GVComboDetail(GVComboDetail);
            }
        }
        List<ComboItem_FED_Info> _ListDetail = new List<ComboItem_FED_Info>();
        private void AddItem(ComboItem_FED_Info AddInItem)
        {
            bool isExist = false;
            for (int i = 0; i < _ListDetail.Count; i++)
            {
                ComboItem_FED_Info zExistItem = _ListDetail[i];
                if (zExistItem.ItemID == AddInItem.ItemID)
                {
                    zExistItem.Quantity = zExistItem.Quantity + 1;
                    zExistItem.ItemCost = zExistItem.Quantity * zExistItem.StandardCost;
                    isExist = true;
                    break;
                }
            }

            if (isExist == false)
            {
                AddInItem.ItemCost = AddInItem.Quantity * AddInItem.StandardCost;
                _ListDetail.Add(AddInItem);
            }
        }
        private void DeleteItem(string ItemID)
        {
            for (int i = 0; i < _ListDetail.Count; i++)
            {
                ComboItem_FED_Info zExistItem = _ListDetail[i];
                if (zExistItem.ItemID == ItemID)
                {
                    if (zExistItem.Quantity == 1)
                    {
                        _ListDetail.Remove(zExistItem);
                    }
                    else
                    {
                        zExistItem.Quantity = zExistItem.Quantity - 1;
                        zExistItem.ItemCost = zExistItem.Quantity * zExistItem.StandardCost;
                    }

                    break;
                }
            }
        }
        private void DesignLayout_GVComboDetail(DataGridView GV)
        {
            TNPaintControl.DrawGVStyle(ref GV);

            GV.Columns.Clear();
            GV.Columns.Add("No", "STT");    //0
            GV.Columns.Add("ItemKey", "ItemKey"); //1
            GV.Columns.Add("ItemID", "Mã sản phẩm"); //2
            GV.Columns.Add("ItemName", "Tên sản phẩm"); //3
            GV.Columns.Add("StandardUnitKey", "StandardUnitKey"); //4
            GV.Columns.Add("StandardUnitName", "Đơn vị tính"); //5
            GV.Columns.Add("Quantity", "Số lượng"); //6
            GV.Columns.Add("RentCost", "Đơn giá"); //7
            GV.Columns.Add("ItemCost", "Số tiền");//8

            Image imgMinus = new Bitmap(TN_WinApp.Properties.Resources.image_minus);
            DataGridViewImageColumn GVImageCol = new DataGridViewImageColumn();
            GVImageCol.HeaderText = ".";
            GVImageCol.ImageLayout = DataGridViewImageCellLayout.Zoom;
            GVImageCol.Image = imgMinus;
            GVImageCol.Width = 32;
            GV.Columns.Add(GVImageCol); //9

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["ItemKey"].Visible = false;

            GV.Columns["ItemID"].Width = 120;
            GV.Columns["ItemID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["ItemName"].Width = 450;
            GV.Columns["ItemName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["ItemName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["StandardUnitKey"].Visible = false;

            GV.Columns["StandardUnitName"].Width = 100;
            GV.Columns["StandardUnitName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["Quantity"].Width = 100;
            GV.Columns["Quantity"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["RentCost"].Width = 120;
            GV.Columns["RentCost"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["ItemCost"].Width = 120;
            GV.Columns["ItemCost"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.ReadOnly = true;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = false;
            GV.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }
        private void LoadData_GVComboDetail(DataGridView GV)
        {
            GV.Rows.Clear();
            double zTotal = 0;
            for (int i = 0; i < _ListDetail.Count; i++)
            {
                ComboItem_FED_Info zExistItem = _ListDetail[i];
                GV.Rows.Add();

                DataGridViewRow zGvRow = GV.Rows[i];
                zGvRow.Cells["No"].Value = (i + 1).ToString();
                zGvRow.Cells["ItemKey"].Value = zExistItem.ItemKey.Trim();
                zGvRow.Cells["ItemID"].Value = zExistItem.ItemID.Trim();
                zGvRow.Cells["ItemName"].Value = zExistItem.ItemName.ToString().Trim();
                zGvRow.Cells["Quantity"].Value = zExistItem.Quantity;
                zGvRow.Cells["StandardUnitKey"].Value = zExistItem.UnitKey.ToString().Trim();
                zGvRow.Cells["StandardUnitName"].Value = zExistItem.UnitName.ToString().Trim();
                zGvRow.Cells["RentCost"].Value = zExistItem.StandardCost.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                zGvRow.Cells["ItemCost"].Value = zExistItem.ItemCost.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                zTotal += zExistItem.ItemCost;
            }

            txtStandardCost.Text = zTotal.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
            CalculatorCombo();
        }

        private void CalculatorCombo()
        {
            double StandardCost = double.Parse(txtStandardCost.Text, CultureInfo.GetCultureInfo("vi-VN"));
            double SaleCost = double.Parse(txtSaleCost.Text, CultureInfo.GetCultureInfo("vi-VN"));
            double ComboCost = StandardCost - SaleCost;

            txtComboCost.Text = ComboCost.ToString("n0", CultureInfo.GetCultureInfo("vi-VN"));
            txtStandardCost.Text = StandardCost.ToString("n0", CultureInfo.GetCultureInfo("vi-VN"));
            txtSaleCost.Text = SaleCost.ToString("n0", CultureInfo.GetCultureInfo("vi-VN"));
        }
        #endregion

        #region [Process]
        private void LoadCombobox()
        {
            string zSQLCategoryPro = "SELECT CategoryKey, CategoryNameVN FROM PDT_Product_Category WHERE RecordStatus <> 99 AND PartnerNumber = '" + SessionUser.UserLogin.PartnerNumber + "'";
            LoadDataToToolbox.KryptonComboBox(cboCategoryProduct, zSQLCategoryPro, "--Chọn--");

            string zSQLCategorySer = "SELECT CategoryKey, CategoryNameVN FROM PDT_Service_Category WHERE RecordStatus <> 99 AND PartnerNumber = '" + SessionUser.UserLogin.PartnerNumber + "'";
            LoadDataToToolbox.KryptonComboBox(cboCategoryService, zSQLCategorySer, "--Chọn--");
        }
        private void LoadData()
        {
            Combo_FED_Info zInfo;
            if (ComboKey != "")
            {
                zInfo = new Combo_FED_Info(ComboKey);
            }
            else
            {
                zInfo = new Combo_FED_Info();
            }

            txtComboID.Text = zInfo.ComboID;
            txtComboName.Text = zInfo.ComboName;
            txtComboCost.Text = zInfo.ComboCost.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
            txtSaleCost.Text = zInfo.SaleCost.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
            txtStandardCost.Text = zInfo.StandardCost.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
            txtDescription.Text = zInfo.Description;
            if (zInfo.FromDate != null)
            {
                dteFromDate.Value = zInfo.FromDate.Value;
            }
            else
            {
                dteFromDate.Value = DateTime.MinValue;
            }

            if (zInfo.ToDate != null)
            {
                dteToDate.Value = zInfo.ToDate.Value;
            }
            else
            {
                dteToDate.Value = DateTime.MinValue;
            }

            DataTable zTable = Access_Data.ListDetail(ComboKey);
            if (zTable.Rows.Count > 0)
            {
                _ListDetail = zTable.CopyToList<ComboItem_FED_Info>();
                LoadData_GVComboDetail(GVComboDetail);
            }
        }
        private string CheckBeforSave()
        {
            string zResult = "";
            if (txtComboID.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng điền mã gói sản phẩm! \n";
            }

            if (ComboKey == "")
            {
                bool Count = Access_Data.CheckComboID(SessionUser.UserLogin.PartnerNumber, txtComboID.Text.Trim());
                if (!Count)
                {
                    zResult += "Vui lòng chọn mã gói khác! \n";
                }
            }
            if (txtComboName.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng điền tên gói sản phẩm! \n";
            }

            return zResult;
        }
        private string Delete()
        {
            Combo_FED_Info zInfo = new Combo_FED_Info();
            zInfo.ComboKey = ComboKey;
            zInfo.Delete();
            zInfo.Delete_Detail();
            return zInfo.Message;
        }
        private string Save()
        {
            #region [Combo_FED_Info]
            Combo_FED_Info zInfo;
            if (ComboKey == "")
            {
                zInfo = new Combo_FED_Info();
            }
            else
            {
                zInfo = new Combo_FED_Info(ComboKey);
            }

            zInfo.PartnerNumber = SessionUser.UserLogin.PartnerNumber;
            zInfo.ComboID = txtComboID.Text.Trim();
            zInfo.ComboName = txtComboName.Text.Trim();
            zInfo.Description = txtDescription.Text.Trim();

            if (dteFromDate.Value != DateTime.MinValue)
            {
                zInfo.FromDate = dteFromDate.Value;
            }
            if (dteToDate.Value != DateTime.MinValue)
            {
                zInfo.ToDate = dteToDate.Value;
            }
            if (txtComboCost.Text != string.Empty)
            {
                double zComboCost = double.Parse(txtComboCost.Text, CultureInfo.GetCultureInfo("vi-VN"));
                zInfo.ComboCost = zComboCost;
            }
            if (txtSaleCost.Text != string.Empty)
            {
                double SaleCost = double.Parse(txtSaleCost.Text, CultureInfo.GetCultureInfo("vi-VN"));
                zInfo.SaleCost = SaleCost;
            }
            if (txtStandardCost.Text != string.Empty)
            {
                double zComboCost = double.Parse(txtStandardCost.Text, CultureInfo.GetCultureInfo("vi-VN"));
                zInfo.StandardCost = zComboCost;
            }

            zInfo.CreatedBy = SessionUser.UserLogin.UserKey;
            zInfo.CreatedName = SessionUser.UserLogin.EmployeeName;
            zInfo.ModifiedBy = SessionUser.UserLogin.UserKey;
            zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;

            if (ComboKey == "")
            {
                zInfo.Create_ClientKey();
                ComboKey = zInfo.ComboKey;
            }
            else
            {
                zInfo.Update();
                ComboKey = zInfo.ComboKey;
            }
            #endregion

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                #region [ComboItem_FED_Info]
                zInfo.Empty_Detail();

                if (_ListDetail.Count > 0)
                {
                    foreach (ComboItem_FED_Info zItem in _ListDetail)
                    {
                        zItem.PartnerNumber = SessionUser.UserLogin.PartnerNumber;
                        zItem.ComboKey = ComboKey;
                        zItem.CreatedBy = SessionUser.UserLogin.UserKey;
                        zItem.CreatedName = SessionUser.UserLogin.EmployeeName;
                        zItem.ModifiedBy = SessionUser.UserLogin.UserKey;
                        zItem.ModifiedName = SessionUser.UserLogin.EmployeeName;

                        zItem.Create_ServerKey();
                    }
                }
                #endregion
            }

            return zInfo.Message;
        }
        private void ClearForm()
        {
            _ListDetail = new List<ComboItem_FED_Info>();
            ComboKey = "";

            GVComboDetail.Rows.Clear();

            txtComboCost.Text = "0";
            txtSaleCost.Text = "0";
            txtStandardCost.Text = "0";

            txtComboID.Clear();
            txtComboName.Clear();
            txtDescription.Clear();
        }
        #endregion

        #region [Dùng kéo rê form]
        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;



        private void Frm_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }
        private void Frm_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }
        private void Frm_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        #region [Auth]
        User_Role_Info _RoleForm = new User_Role_Info();
        private void Get_Auth()
        {
            List<User_Role_Info> zListRole = SessionUser.TableRole;
            if (zListRole.Count > 0)
            {
                try
                {
                    _RoleForm = zListRole.SingleOrDefault(r => r.RoleURL == FormUrl);
                    if (_RoleForm == null)
                    {
                        _RoleForm = new User_Role_Info();
                        MessageBox.Show("Form này chưa được thêm quyền, vui lòng liên hệ IT !.");
                        this.Close();
                    }
                    else
                    {
                        if (!_RoleForm.RoleRead)
                        {
                            MessageBox.Show("Bạn không có quyền xem, vui lòng liên hệ IT !.");
                            this.Close();
                        }
                        if (!_RoleForm.RoleEdit)
                        {
                            btnSave.Visible = false;
                        }
                        if (!_RoleForm.RoleAdd)
                        {
                            btnSave.Visible = false;
                        }
                        if (!_RoleForm.RoleDel)
                        {
                            btnDelete.Visible = false;
                        }
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Bạn chưa được phân quyền, vui lòng liên hệ IT !.");
            }
        }
        #endregion       

        #region[Access Info -Data]
        public class Access_Data
        {
            #region[Standard]
            public static bool CheckComboID(string PartnerNumber, string ComboID)
            {
                bool zResult = false;  //  Khong co
                DataTable zTable = new DataTable();
                string zSQL = @"SELECT Count(*) AS Amount FROM [dbo].[PDT_Combo_FED] WHERE PartnerNumber = @PartnerNumber AND ComboID = @ComboID AND RecordStatus != 99";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@ComboID", SqlDbType.NVarChar).Value = ComboID;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string Result = ex.ToString();
                }
                if (zTable.Rows.Count > 0)
                {
                    int zAmount = 0;
                    DataRow zRow = zTable.Rows[0];
                    zAmount = int.Parse(zRow["Amount"].ToString());
                    if (zAmount == 0)
                    {
                        zResult = true;
                    }
                    else
                    {
                        zResult = false;
                    }
                }
                return zResult;
            }
            #endregion

            public static DataTable ListProduct(string PartnerNumber, string Name, int Category)
            {
                DataTable zTable = new DataTable();
                string zSQL = @"
SELECT A.*    --, B.ColorName, B.SizeName
FROM PDT_Product_FED A
--LEFT JOIN PDT_Product_Property_FED B ON A.ProductKey = B.ProductKey
WHERE A.RecordStatus <> 99 
--AND B.RecordStatus <> 99  
AND A.PartnerNumber= @PartnerNumber ";

                if (Category != 0)
                {
                    zSQL += "AND A.CategoryKey = @CategoryKey";
                }
                if (Name.Trim().Length > 0)
                {
                    zSQL += " AND (A.ProductID LIKE @Name OR A.ProductName LIKE @Name)";
                }

                zSQL += " ORDER BY A.CreatedOn DESC";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Category;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
            public static DataTable ListService(string PartnerNumber, string Name, int Category)
            {
                DataTable zTable = new DataTable();
                string zSQL = @"
SELECT A.*
FROM PDT_Service A
WHERE A.RecordStatus <> 99  
AND A.PartnerNumber= @PartnerNumber ";

                if (Category != 0)
                {
                    zSQL = @"AND A.CategoryKey = @CategoryKey";
                }
                if (Name.Trim().Length > 0)
                {
                    zSQL += " AND (A.ServiceID LIKE @Name OR A.ServiceName LIKE @Name)";
                }

                zSQL += " ORDER BY A.CreatedOn DESC";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Category;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }

            public static DataTable ListDetail(string ComboKey)
            {
                DataTable zTable = new DataTable();
                string zSQL = @"
SELECT A.*
FROM PDT_Combo_Item_FED A
WHERE A.RecordStatus <> 99  
AND A.ComboKey= @ComboKey ";

                zSQL += " ORDER BY A.CreatedOn DESC";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = new Guid(ComboKey);
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
        }
        public class Combo_FED_Info
        {
            #region [ Field Name ]
            private string _ComboKey = "";
            private string _ComboID = "";
            private string _ComboName = "";
            private double _StandardCost = 0;
            private double _SaleCost = 0;
            private double _ComboCost = 0;
            private DateTime? _FromDate = null;
            private DateTime? _ToDate = null;
            private bool _Active;
            private string _Description = "";
            private string _Style = "";
            private string _Class = "";
            private string _Line = "";
            private string _PartnerNumber = "";
            private int _RecordStatus = 0;
            private DateTime? _CreatedOn = null;
            private string _CreatedBy = "";
            private string _CreatedName = "";
            private DateTime? _ModifiedOn = null;
            private string _ModifiedBy = "";
            private string _ModifiedName = "";
            private string _Message = "";
            #endregion

            #region [ Constructor Get Information ]
            public Combo_FED_Info()
            {
                Guid zNewID = Guid.NewGuid();
                _ComboKey = zNewID.ToString();
            }
            public Combo_FED_Info(string ComboKey)
            {
                string zSQL = "SELECT * FROM [dbo].[PDT_Combo_FED] WHERE ComboKey = @ComboKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = new Guid(ComboKey);
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _ComboKey = zReader["ComboKey"].ToString();
                        _ComboID = zReader["ComboID"].ToString();
                        _ComboName = zReader["ComboName"].ToString();
                        _StandardCost = double.Parse(zReader["StandardCost"].ToString());
                        _SaleCost = double.Parse(zReader["SaleCost"].ToString());
                        _ComboCost = double.Parse(zReader["ComboCost"].ToString());
                        if (zReader["FromDate"] != DBNull.Value)
                        {
                            _FromDate = (DateTime)zReader["FromDate"];
                        }

                        if (zReader["ToDate"] != DBNull.Value)
                        {
                            _ToDate = (DateTime)zReader["ToDate"];
                        }

                        _Active = (bool)zReader["Active"];
                        _Description = zReader["Description"].ToString();
                        _Style = zReader["Style"].ToString();
                        _Class = zReader["Class"].ToString();
                        _Line = zReader["Line"].ToString();
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }

            #endregion

            #region [ Properties ]
            public string ComboKey
            {
                get { return _ComboKey; }
                set { _ComboKey = value; }
            }
            public string ComboID
            {
                get { return _ComboID; }
                set { _ComboID = value; }
            }
            public string ComboName
            {
                get { return _ComboName; }
                set { _ComboName = value; }
            }
            public double StandardCost
            {
                get { return _StandardCost; }
                set { _StandardCost = value; }
            }
            public double SaleCost
            {
                get { return _SaleCost; }
                set { _SaleCost = value; }
            }
            public double ComboCost
            {
                get { return _ComboCost; }
                set { _ComboCost = value; }
            }
            public DateTime? FromDate
            {
                get { return _FromDate; }
                set { _FromDate = value; }
            }
            public DateTime? ToDate
            {
                get { return _ToDate; }
                set { _ToDate = value; }
            }
            public bool Active
            {
                get { return _Active; }
                set { _Active = value; }
            }
            public string Description
            {
                get { return _Description; }
                set { _Description = value; }
            }
            public string Style
            {
                get { return _Style; }
                set { _Style = value; }
            }
            public string Class
            {
                get { return _Class; }
                set { _Class = value; }
            }
            public string Line
            {
                get { return _Line; }
                set { _Line = value; }
            }
            public string PartnerNumber
            {
                get { return _PartnerNumber; }
                set { _PartnerNumber = value; }
            }
            public int RecordStatus
            {
                get { return _RecordStatus; }
                set { _RecordStatus = value; }
            }
            public DateTime? CreatedOn
            {
                get { return _CreatedOn; }
                set { _CreatedOn = value; }
            }
            public string CreatedBy
            {
                get { return _CreatedBy; }
                set { _CreatedBy = value; }
            }
            public string CreatedName
            {
                get { return _CreatedName; }
                set { _CreatedName = value; }
            }
            public DateTime? ModifiedOn
            {
                get { return _ModifiedOn; }
                set { _ModifiedOn = value; }
            }
            public string ModifiedBy
            {
                get { return _ModifiedBy; }
                set { _ModifiedBy = value; }
            }
            public string ModifiedName
            {
                get { return _ModifiedName; }
                set { _ModifiedName = value; }
            }
            public string Code
            {
                get
                {
                    if (_Message.Length >= 3)
                    {
                        return _Message.Substring(0, 3);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            public string Message
            {
                get { return _Message; }
                set { _Message = value; }
            }
            #endregion

            #region [ Constructor Update Information ]
            public string Create_ServerKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[PDT_Combo_FED] ("
            + " ComboID ,ComboName ,StandardCost ,SaleCost ,ComboCost ,FromDate ,ToDate ,Active ,Description ,Style ,Class ,Line ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@ComboID ,@ComboName ,@StandardCost ,@SaleCost ,@ComboCost ,@FromDate ,@ToDate ,@Active ,@Description ,@Style ,@Class ,@Line ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ComboID", SqlDbType.NVarChar).Value = _ComboID;
                    zCommand.Parameters.Add("@ComboName", SqlDbType.NVarChar).Value = _ComboName;
                    zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = _StandardCost;
                    zCommand.Parameters.Add("@SaleCost", SqlDbType.Money).Value = _SaleCost;
                    zCommand.Parameters.Add("@ComboCost", SqlDbType.Money).Value = _ComboCost;
                    if (_FromDate == null)
                    {
                        zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = _FromDate;
                    }

                    if (_ToDate == null)
                    {
                        zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = _ToDate;
                    }

                    zCommand.Parameters.Add("@Active", SqlDbType.Bit).Value = _Active;
                    zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@Line", SqlDbType.NChar).Value = _Line;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Create_ClientKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[PDT_Combo_FED] ("
            + " ComboKey ,ComboID ,ComboName ,StandardCost ,SaleCost ,ComboCost ,FromDate ,ToDate ,Active ,Description ,Style ,Class ,Line ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@ComboKey ,@ComboID ,@ComboName ,@StandardCost ,@SaleCost ,@ComboCost ,@FromDate ,@ToDate ,@Active ,@Description ,@Style ,@Class ,@Line ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ComboKey);
                    zCommand.Parameters.Add("@ComboID", SqlDbType.NVarChar).Value = _ComboID;
                    zCommand.Parameters.Add("@ComboName", SqlDbType.NVarChar).Value = _ComboName;
                    zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = _StandardCost;
                    zCommand.Parameters.Add("@SaleCost", SqlDbType.Money).Value = _SaleCost;
                    zCommand.Parameters.Add("@ComboCost", SqlDbType.Money).Value = _ComboCost;
                    if (_FromDate == null)
                    {
                        zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = _FromDate;
                    }

                    if (_ToDate == null)
                    {
                        zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = _ToDate;
                    }

                    zCommand.Parameters.Add("@Active", SqlDbType.Bit).Value = _Active;
                    zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@Line", SqlDbType.NChar).Value = _Line;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Update()
            {
                string zSQL = "UPDATE [dbo].[PDT_Combo_FED] SET "
                            + " ComboID = @ComboID,"
                            + " ComboName = @ComboName,"
                            + " StandardCost = @StandardCost,"
                            + " SaleCost = @SaleCost,"
                            + " ComboCost = @ComboCost,"
                            + " FromDate = @FromDate,"
                            + " ToDate = @ToDate,"
                            + " Active = @Active,"
                            + " Description = @Description,"
                            + " Style = @Style,"
                            + " Class = @Class,"
                            + " Line = @Line,"
                            + " PartnerNumber = @PartnerNumber,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedOn = GetDate(),"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName"
                           + " WHERE ComboKey = @ComboKey";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ComboKey);
                    zCommand.Parameters.Add("@ComboID", SqlDbType.NVarChar).Value = _ComboID;
                    zCommand.Parameters.Add("@ComboName", SqlDbType.NVarChar).Value = _ComboName;
                    zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = _StandardCost;
                    zCommand.Parameters.Add("@SaleCost", SqlDbType.Money).Value = _SaleCost;
                    zCommand.Parameters.Add("@ComboCost", SqlDbType.Money).Value = _ComboCost;
                    if (_FromDate == null)
                    {
                        zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = _FromDate;
                    }

                    if (_ToDate == null)
                    {
                        zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = _ToDate;
                    }

                    zCommand.Parameters.Add("@Active", SqlDbType.Bit).Value = _Active;
                    zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@Line", SqlDbType.NChar).Value = _Line;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Delete()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[PDT_Combo_FED] Set RecordStatus = 99 WHERE ComboKey = @ComboKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ComboKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Delete_Detail()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[PDT_Combo_Item_FED] Set RecordStatus = 99 WHERE ComboKey = @ComboKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ComboKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[PDT_Combo_FED] WHERE ComboKey = @ComboKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ComboKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty_Detail()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[PDT_Combo_Item_FED] WHERE ComboKey = @ComboKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ComboKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            #endregion
        }
        public class ComboItem_FED_Info
        {
            #region [ Field Name ]
            private string _ItemKey = "";
            private string _ComboKey = "";
            private string _ProductKey = "";
            private string _ServiceKey = "";
            private string _ItemID = "";
            private string _ItemName = "";
            private int _UnitKey = 0;
            private string _UnitName = "";
            private float _Quantity;
            private double _StandardCost = 0;
            private double _SaleCost = 0;
            private double _ItemCost = 0;
            private string _Style = "";
            private string _Class = "";
            private string _Line = "";
            private string _PartnerNumber = "";
            private int _RecordStatus = 0;
            private DateTime? _CreatedOn = null;
            private string _CreatedBy = "";
            private string _CreatedName = "";
            private DateTime? _ModifiedOn = null;
            private string _ModifiedBy = "";
            private string _ModifiedName = "";
            private string _Message = "";
            #endregion
            #region [ Constructor Get Information ]
            public ComboItem_FED_Info()
            {
                Guid zNewID = Guid.NewGuid();
                _ItemKey = zNewID.ToString();
            }
            public ComboItem_FED_Info(string ItemKey)
            {
                string zSQL = "SELECT * FROM [dbo].[PDT_Combo_Item_FED] WHERE ItemKey = @ItemKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ItemKey", SqlDbType.UniqueIdentifier).Value = new Guid(ItemKey);
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _ItemKey = zReader["ItemKey"].ToString();
                        _ComboKey = zReader["ComboKey"].ToString();
                        _ItemID = zReader["ItemID"].ToString();
                        _ItemName = zReader["ItemName"].ToString();
                        _UnitKey = int.Parse(zReader["UnitKey"].ToString());
                        _UnitName = zReader["UnitName"].ToString();
                        _Quantity = float.Parse(zReader["Quantity"].ToString());
                        _StandardCost = double.Parse(zReader["StandardCost"].ToString());
                        _SaleCost = double.Parse(zReader["SaleCost"].ToString());
                        _ItemCost = double.Parse(zReader["ItemCost"].ToString());
                        _Style = zReader["Style"].ToString();
                        _Class = zReader["Class"].ToString();
                        _Line = zReader["Line"].ToString();
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }
            #endregion
            #region [ Properties ]
            public string ItemKey
            {
                get { return _ItemKey; }
                set { _ItemKey = value; }
            }
            public string ComboKey
            {
                get { return _ComboKey; }
                set { _ComboKey = value; }
            }
            public string ItemID
            {
                get { return _ItemID; }
                set { _ItemID = value; }
            }
            public string ItemName
            {
                get { return _ItemName; }
                set { _ItemName = value; }
            }
            public int UnitKey
            {
                get { return _UnitKey; }
                set { _UnitKey = value; }
            }
            public string UnitName
            {
                get { return _UnitName; }
                set { _UnitName = value; }
            }
            public float Quantity
            {
                get { return _Quantity; }
                set { _Quantity = value; }
            }
            public double StandardCost
            {
                get { return _StandardCost; }
                set { _StandardCost = value; }
            }
            public double SaleCost
            {
                get { return _SaleCost; }
                set { _SaleCost = value; }
            }
            public double ItemCost
            {
                get { return _ItemCost; }
                set { _ItemCost = value; }
            }
            public string Style
            {
                get { return _Style; }
                set { _Style = value; }
            }
            public string Class
            {
                get { return _Class; }
                set { _Class = value; }
            }
            public string Line
            {
                get { return _Line; }
                set { _Line = value; }
            }
            public string PartnerNumber
            {
                get { return _PartnerNumber; }
                set { _PartnerNumber = value; }
            }
            public int RecordStatus
            {
                get { return _RecordStatus; }
                set { _RecordStatus = value; }
            }
            public DateTime? CreatedOn
            {
                get { return _CreatedOn; }
                set { _CreatedOn = value; }
            }
            public string CreatedBy
            {
                get { return _CreatedBy; }
                set { _CreatedBy = value; }
            }
            public string CreatedName
            {
                get { return _CreatedName; }
                set { _CreatedName = value; }
            }
            public DateTime? ModifiedOn
            {
                get { return _ModifiedOn; }
                set { _ModifiedOn = value; }
            }
            public string ModifiedBy
            {
                get { return _ModifiedBy; }
                set { _ModifiedBy = value; }
            }
            public string ModifiedName
            {
                get { return _ModifiedName; }
                set { _ModifiedName = value; }
            }
            public string Code
            {
                get
                {
                    if (_Message.Length >= 3)
                    {
                        return _Message.Substring(0, 3);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            public string Message
            {
                get { return _Message; }
                set { _Message = value; }
            }

            public string ProductKey
            {
                get
                {
                    return _ProductKey;
                }

                set
                {
                    _ProductKey = value;
                }
            }

            public string ServiceKey
            {
                get
                {
                    return _ServiceKey;
                }

                set
                {
                    _ServiceKey = value;
                }
            }
            #endregion
            #region [ Constructor Update Information ]
            public string Create_ServerKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[PDT_Combo_Item_FED] ("
             + " ComboKey, ProductKey,ServiceKey ,ItemID ,ItemName ,UnitKey ,UnitName ,Quantity ,StandardCost ,SaleCost ,ItemCost ,Style ,Class ,Line ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@ComboKey, @ProductKey, @ServiceKey ,@ItemID ,@ItemName ,@UnitKey ,@UnitName ,@Quantity ,@StandardCost ,@SaleCost ,@ItemCost ,@Style ,@Class ,@Line ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ComboKey);

                    if (_ProductKey != string.Empty && _ProductKey.Length == 36)
                    {
                        zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ProductKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }

                    if (_ServiceKey != string.Empty && _ServiceKey.Length == 36)
                    {
                        zCommand.Parameters.Add("@ServiceKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ServiceKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ServiceKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }

                    zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = _ItemID;
                    zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = _ItemName;
                    zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = _UnitKey;
                    zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = _UnitName;
                    zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = _Quantity;
                    zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = _StandardCost;
                    zCommand.Parameters.Add("@SaleCost", SqlDbType.Money).Value = _SaleCost;
                    zCommand.Parameters.Add("@ItemCost", SqlDbType.Money).Value = _ItemCost;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@Line", SqlDbType.NChar).Value = _Line;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Create_ClientKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[PDT_Combo_Item_FED] ("
             + " ItemKey ProductKey, ServiceKey, ComboKey ,ItemID ,ItemName ,UnitKey ,UnitName ,Quantity ,StandardCost ,SaleCost ,ItemCost ,Style ,Class ,Line ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@ItemKey, @ProductKey, @ServiceKey ,@ComboKey ,@ItemID ,@ItemName ,@UnitKey ,@UnitName ,@Quantity ,@StandardCost ,@SaleCost ,@ItemCost ,@Style ,@Class ,@Line ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ItemKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ItemKey);
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ComboKey);

                    if (_ProductKey != string.Empty && _ProductKey.Length == 36)
                    {
                        zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ProductKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }

                    if (_ServiceKey != string.Empty && _ServiceKey.Length == 36)
                    {
                        zCommand.Parameters.Add("@ServiceKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ServiceKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ServiceKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }

                    zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = _ItemID;
                    zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = _ItemName;
                    zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = _UnitKey;
                    zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = _UnitName;
                    zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = _Quantity;
                    zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = _StandardCost;
                    zCommand.Parameters.Add("@SaleCost", SqlDbType.Money).Value = _SaleCost;
                    zCommand.Parameters.Add("@ItemCost", SqlDbType.Money).Value = _ItemCost;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@Line", SqlDbType.NChar).Value = _Line;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Update()
            {
                string zSQL = "UPDATE [dbo].[PDT_Combo_Item_FED] SET "
                            + " ComboKey = @ComboKey,"
                            + " ItemID = @ItemID,"
                            + " ItemName = @ItemName,"
                            + " UnitKey = @UnitKey,"
                            + " UnitName = @UnitName,"
                            + " Quantity = @Quantity,"
                            + " StandardCost = @StandardCost,"
                            + " SaleCost = @SaleCost,"
                            + " ItemCost = @ItemCost,"
                            + " Style = @Style,"
                            + " Class = @Class,"
                            + " Line = @Line,"
                            + " PartnerNumber = @PartnerNumber,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedOn = GetDate(),"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName"
                           + " WHERE ItemKey = @ItemKey";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ItemKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ItemKey);
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ComboKey);
                    zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = _ItemID;
                    zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = _ItemName;
                    zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = _UnitKey;
                    zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = _UnitName;
                    zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = _Quantity;
                    zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = _StandardCost;
                    zCommand.Parameters.Add("@SaleCost", SqlDbType.Money).Value = _SaleCost;
                    zCommand.Parameters.Add("@ItemCost", SqlDbType.Money).Value = _ItemCost;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@Line", SqlDbType.NChar).Value = _Line;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Delete()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[PDT_Combo_Item_FED] Set RecordStatus = 99 WHERE ItemKey = @ItemKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ItemKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ItemKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[PDT_Combo_Item_FED] WHERE ItemKey = @ItemKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ItemKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ItemKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            #endregion
        }
        #endregion
    }
}
