﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using TN_Connection;
using TN_Tools;
using TN_User;

namespace TN_WinApp.IVT
{
    public partial class Frm_ServiceEdit : Form
    {
        private string _FormUrl = "/IVT/Frm_ServiceEdit";
        private string _ServiceKey = "";
        public string ServiceKey
        {
            get
            {
                return _ServiceKey;
            }

            set
            {
                _ServiceKey = value;
            }
        }
        public string FormUrl
        {
            get
            {
                return _FormUrl;
            }

            set
            {
                _FormUrl = value;
            }
        }

        public Frm_ServiceEdit()
        {
            InitializeComponent();

            TitleForm.MouseDown += Frm_MouseDown;
            TitleForm.MouseMove += Frm_MouseMove;
            TitleForm.MouseUp += Frm_MouseUp;

            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;

            btnAddNew.Click += BtnNewOrder_Click;
            btnSave.Click += BtnSave_Click;
            btnDelete.Click += BtnDelete_Click;

            txtStandardCost.KeyPress += TextBoxNumber_KeyPress;
            txtStandardCost.Leave += (o, e) => { txtStandardCost.Text = txtStandardCost.Text.FormatMoneyFromString(); };

            //format Form
            this.Text = "Thông tin dịch vụ";
            this.StartPosition = FormStartPosition.CenterScreen;
            this.FormBorderStyle = FormBorderStyle.None;
            this.ControlBox = false;
            this.DoubleBuffered = true;
        }

        private void Frm_ServiceEdit_Load(object sender, EventArgs e)
        {
            Get_Auth();
            if (_RoleForm.RoleRead)
            {
                LoadCombobox();
                LoadData();
            }
        }

        #region[Event]
        private void TextBoxNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void BtnNewOrder_Click(object sender, EventArgs e)
        {
            ClearForm();
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            string zResult = "";
            string zMessage = "";
            zMessage = CheckBeforSave();
            if (zMessage.Length == 0)
            {
                zResult = Save();
                if (zResult.Substring(0, 3) == "200" || zResult.Substring(0, 3) == "201")
                {
                    MessageBox.Show("Cập nhật thành công!");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Lỗi.Vui lòng liên hệ IT.Chi tiết lỗi mã : \n " + zResult);
                }
            }
            else
            {
                MessageBox.Show(zMessage);
            }
        }
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            if (ServiceKey != "")
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    Service_Info zInfo = new Service_Info(ServiceKey);
                    zInfo.Delete();
                    if (zInfo.Message.Substring(0, 3) == "200")
                    {
                        MessageBox.Show("Xóa thành công!");
                        ClearForm();
                    }
                    else
                    {
                        MessageBox.Show("Lỗi.Vui lòng liên hệ IT.Chi tiết lỗi mã: \n " + zInfo.Message);
                    }
                }
            }
        }
        #endregion

        #region[Process]
        void LoadCombobox()
        {
            string zCategory = @"SELECT CategoryKey,CategoryNameVN FROM[dbo].[PDT_Service_Category]
            WHERE RecordStatus<> 99 AND PartnerNumber = '" + SessionUser.UserLogin.PartnerNumber + "'";

            string zStatus = @"SELECT StatusKey,StatusNameVN FROM[dbo].[PDT_Product_Status]
            WHERE RecordStatus<> 99 AND PartnerNumber = '" + SessionUser.UserLogin.PartnerNumber + "'";

            string zUnit = @"SELECT UnitKey,UnitNameVN FROM[dbo].[PDT_Product_Unit]
            WHERE RecordStatus<> 99 AND PartnerNumber = '" + SessionUser.UserLogin.PartnerNumber + "'";

            LoadDataToToolbox.KryptonComboBox(cboCategory, zCategory, "--Chọn--");
            LoadDataToToolbox.KryptonComboBox(cboStatus, zStatus, "--Chọn--");
            LoadDataToToolbox.KryptonComboBox(cboUnit, zUnit, "--Chọn--");
        }

        private string CheckBeforSave()
        {
            string zResult = "";
            if (txtID.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng điền mã sản phẩm! \n";
            }

            if (ServiceKey == "")
            {
                bool Count = Access_Data.CheckServiceID(SessionUser.UserLogin.PartnerNumber, txtID.Text.Trim());
                if (!Count)
                {
                    zResult += "Vui lòng chọn mã sản phẩm khác! \n";
                }
            }
            if (txtName.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng điền tên sản phẩm! \n";
            }

            if (cboCategory.SelectedValue.ToInt() == 0)
            {
                zResult += "Vui lòng chọn danh mục sản phẩm! \n";
            }

            if (cboStatus.SelectedValue.ToInt() == 0)
            {
                zResult += "Vui lòng chọn tình trạng! \n";
            }

            return zResult;
        }
        private string Save()
        {
            Service_Info zInfo;
            if (ServiceKey == "")
            {
                zInfo = new Service_Info();
            }
            else
            {
                zInfo = new Service_Info(ServiceKey);
            }
            zInfo.ServiceID = txtID.Text.Trim().ToUpper();
            zInfo.ServiceName = txtName.Text.Trim();
            zInfo.Description = txtDescription.Text.Trim();
            zInfo.CategoryKey = cboCategory.SelectedValue.ToInt();
            zInfo.CategoryName = cboCategory.Text;
            zInfo.StatusKey = cboStatus.SelectedValue.ToInt();
            zInfo.StatusName = cboStatus.Text;
            zInfo.StandardUnitKey = cboUnit.SelectedValue.ToInt();
            zInfo.StandardUnitName = cboUnit.Text;

            if (txtStandardCost.Text != string.Empty)
            {
                double StandardCost = double.Parse(txtStandardCost.Text, CultureInfo.GetCultureInfo("vi-VN"));
                zInfo.StandardCost = StandardCost;
            }

            zInfo.PartnerNumber = SessionUser.UserLogin.PartnerNumber;

            if (ServiceKey == "")
            {
                zInfo.Create_ClientKey();
                ServiceKey = zInfo.ServiceKey;
            }
            else
            {
                zInfo.Update();
                ServiceKey = zInfo.ServiceKey;
            }

            return zInfo.Message;
        }
        private void ClearForm()
        {
            ServiceKey = "";
            txtID.Text = "";
            txtName.Text = "";
            cboCategory.SelectedValue = 0;
            cboStatus.SelectedValue = 0;
            txtDescription.Text = "";
            txtStandardCost.Text = "0";
            cboUnit.SelectedValue = 0;
        }
        private void LoadData()
        {
            Service_Info zInfo;
            if (ServiceKey == "")
            {
                zInfo = new Service_Info();               
            }
            else
            {
                zInfo = new Service_Info(ServiceKey);
            }
           
            txtID.Text = zInfo.ServiceID;
            txtName.Text = zInfo.ServiceName;
            cboCategory.SelectedValue = zInfo.CategoryKey;
            cboStatus.SelectedValue = zInfo.StatusKey;
            txtDescription.Text = zInfo.Description;
            txtStandardCost.Text = zInfo.StandardCost.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
            cboUnit.SelectedValue = zInfo.StandardUnitKey;
        }

        #endregion

        #region [Dùng kéo rê form]
        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;



        private void Frm_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }
        private void Frm_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }
        private void Frm_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        #region [Auth]
        User_Role_Info _RoleForm = new User_Role_Info();
        private void Get_Auth()
        {
            List<User_Role_Info> zListRole = SessionUser.TableRole;
            if (zListRole.Count > 0)
            {
                try
                {
                    _RoleForm = zListRole.SingleOrDefault(r => r.RoleURL == FormUrl);
                    if (_RoleForm == null)
                    {
                        _RoleForm = new User_Role_Info();
                        MessageBox.Show("Form này chưa được thêm quyền, vui lòng liên hệ IT !.");
                        this.Close();
                    }
                    else
                    {
                        if (!_RoleForm.RoleRead)
                        {
                            MessageBox.Show("Bạn không có quyền xem, vui lòng liên hệ IT !.");
                            this.Close();
                        }
                        if (!_RoleForm.RoleEdit)
                        {
                            btnSave.Visible = false;
                            btnAddNew.Visible = false;
                        }
                        if (!_RoleForm.RoleAdd)
                        {
                            btnSave.Visible = false;
                            btnAddNew.Visible = false;
                        }
                        if (!_RoleForm.RoleDel)
                        {
                            btnDelete.Visible = false;
                        }
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Bạn chưa được phân quyền, vui lòng liên hệ IT !.");
            }
        }
        #endregion       

        #region[Access Info -Data]
        public class Service_Info
        {

            #region [ Field Name ]
            private string _ServiceKey = "";
            private string _ServiceID = "";
            private string _ServiceName = "";
            private string _ServiceNumber = "";
            private string _ServiceSerial = "";
            private string _ServiceModel = "";
            private double _StandardCost = 0;
            private int _StandardUnitKey = 0;
            private string _StandardUnitName = "";
            private DateTime? _DiscontinuedDate = null;
            private float _SafetyInStock;
            private string _Style = "";
            private string _Class = "";
            private string _ServiceLine = "";
            private int _CategoryKey = 0;
            private string _CategoryName = "";
            private int _ServiceModeKey = 0;
            private string _PhotoPath = "";
            private int _StatusKey = 0;
            private string _StatusName = "";
            private string _Description = "";
            private string _PartnerNumber = "";
            private int _RecordStatus = 0;
            private DateTime? _CreatedOn = null;
            private string _CreatedBy = "";
            private string _CreatedName = "";
            private DateTime? _ModifiedOn = null;
            private string _ModifiedBy = "";
            private string _ModifiedName = "";
            private string _Message = "";
            #endregion

            #region [ Constructor Get Information ]
            public Service_Info()
            {
                Guid zNewID = Guid.NewGuid();
                _ServiceKey = zNewID.ToString();
            }
            public Service_Info(string ServiceKey)
            {
                string zSQL = "SELECT * FROM [dbo].[PDT_Service] WHERE ServiceKey = @ServiceKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ServiceKey", SqlDbType.UniqueIdentifier).Value = new Guid(ServiceKey);
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _ServiceKey = zReader["ServiceKey"].ToString();
                        _ServiceID = zReader["ServiceID"].ToString();
                        _ServiceName = zReader["ServiceName"].ToString();
                        _ServiceNumber = zReader["ServiceNumber"].ToString();
                        _ServiceSerial = zReader["ServiceSerial"].ToString();
                        _ServiceModel = zReader["ServiceModel"].ToString();
                        _StandardCost = double.Parse(zReader["StandardCost"].ToString());
                        _StandardUnitKey = int.Parse(zReader["StandardUnitKey"].ToString());
                        _StandardUnitName = zReader["StandardUnitName"].ToString();
                        if (zReader["DiscontinuedDate"] != DBNull.Value)
                        {
                            _DiscontinuedDate = (DateTime)zReader["DiscontinuedDate"];
                        }

                        _SafetyInStock = float.Parse(zReader["SafetyInStock"].ToString());
                        _Style = zReader["Style"].ToString();
                        _Class = zReader["Class"].ToString();
                        _ServiceLine = zReader["ServiceLine"].ToString();
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                        _CategoryName = zReader["CategoryName"].ToString();
                        _ServiceModeKey = int.Parse(zReader["ServiceModeKey"].ToString());
                        _PhotoPath = zReader["PhotoPath"].ToString();
                        _StatusKey = int.Parse(zReader["StatusKey"].ToString());
                        _StatusName = zReader["StatusName"].ToString();
                        _Description = zReader["Description"].ToString();
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }

            #endregion

            #region [ Properties ]
            public string ServiceKey
            {
                get { return _ServiceKey; }
                set { _ServiceKey = value; }
            }
            public string ServiceID
            {
                get { return _ServiceID; }
                set { _ServiceID = value; }
            }
            public string ServiceName
            {
                get { return _ServiceName; }
                set { _ServiceName = value; }
            }
            public string ServiceNumber
            {
                get { return _ServiceNumber; }
                set { _ServiceNumber = value; }
            }
            public string ServiceSerial
            {
                get { return _ServiceSerial; }
                set { _ServiceSerial = value; }
            }
            public string ServiceModel
            {
                get { return _ServiceModel; }
                set { _ServiceModel = value; }
            }
            public double StandardCost
            {
                get { return _StandardCost; }
                set { _StandardCost = value; }
            }
            public int StandardUnitKey
            {
                get { return _StandardUnitKey; }
                set { _StandardUnitKey = value; }
            }
            public string StandardUnitName
            {
                get { return _StandardUnitName; }
                set { _StandardUnitName = value; }
            }
            public DateTime? DiscontinuedDate
            {
                get { return _DiscontinuedDate; }
                set { _DiscontinuedDate = value; }
            }
            public float SafetyInStock
            {
                get { return _SafetyInStock; }
                set { _SafetyInStock = value; }
            }
            public string Style
            {
                get { return _Style; }
                set { _Style = value; }
            }
            public string Class
            {
                get { return _Class; }
                set { _Class = value; }
            }
            public string ServiceLine
            {
                get { return _ServiceLine; }
                set { _ServiceLine = value; }
            }
            public int CategoryKey
            {
                get { return _CategoryKey; }
                set { _CategoryKey = value; }
            }
            public string CategoryName
            {
                get { return _CategoryName; }
                set { _CategoryName = value; }
            }
            public int ServiceModeKey
            {
                get { return _ServiceModeKey; }
                set { _ServiceModeKey = value; }
            }
            public string PhotoPath
            {
                get { return _PhotoPath; }
                set { _PhotoPath = value; }
            }
            public int StatusKey
            {
                get { return _StatusKey; }
                set { _StatusKey = value; }
            }
            public string StatusName
            {
                get { return _StatusName; }
                set { _StatusName = value; }
            }
            public string Description
            {
                get { return _Description; }
                set { _Description = value; }
            }
            public string PartnerNumber
            {
                get { return _PartnerNumber; }
                set { _PartnerNumber = value; }
            }
            public int RecordStatus
            {
                get { return _RecordStatus; }
                set { _RecordStatus = value; }
            }
            public DateTime? CreatedOn
            {
                get { return _CreatedOn; }
                set { _CreatedOn = value; }
            }
            public string CreatedBy
            {
                get { return _CreatedBy; }
                set { _CreatedBy = value; }
            }
            public string CreatedName
            {
                get { return _CreatedName; }
                set { _CreatedName = value; }
            }
            public DateTime? ModifiedOn
            {
                get { return _ModifiedOn; }
                set { _ModifiedOn = value; }
            }
            public string ModifiedBy
            {
                get { return _ModifiedBy; }
                set { _ModifiedBy = value; }
            }
            public string ModifiedName
            {
                get { return _ModifiedName; }
                set { _ModifiedName = value; }
            }
            public string Code
            {
                get
                {
                    if (_Message.Length >= 3)
                    {
                        return _Message.Substring(0, 3);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            public string Message
            {
                get { return _Message; }
                set { _Message = value; }
            }
            #endregion

            #region [ Constructor Update Information ]

            public string Create_ServerKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[PDT_Service] ("
            + " ServiceID ,ServiceName ,ServiceNumber ,ServiceSerial ,ServiceModel ,StandardCost ,StandardUnitKey ,StandardUnitName ,DiscontinuedDate ,SafetyInStock ,Style ,Class ,ServiceLine ,CategoryKey ,CategoryName ,ServiceModeKey ,PhotoPath ,StatusKey ,StatusName ,Description ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@ServiceID ,@ServiceName ,@ServiceNumber ,@ServiceSerial ,@ServiceModel ,@StandardCost ,@StandardUnitKey ,@StandardUnitName ,@DiscontinuedDate ,@SafetyInStock ,@Style ,@Class ,@ServiceLine ,@CategoryKey ,@CategoryName ,@ServiceModeKey ,@PhotoPath ,@StatusKey ,@StatusName ,@Description ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ServiceID", SqlDbType.NVarChar).Value = _ServiceID;
                    zCommand.Parameters.Add("@ServiceName", SqlDbType.NVarChar).Value = _ServiceName;
                    zCommand.Parameters.Add("@ServiceNumber", SqlDbType.NVarChar).Value = _ServiceNumber;
                    zCommand.Parameters.Add("@ServiceSerial", SqlDbType.NVarChar).Value = _ServiceSerial;
                    zCommand.Parameters.Add("@ServiceModel", SqlDbType.NVarChar).Value = _ServiceModel;
                    zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = _StandardCost;
                    zCommand.Parameters.Add("@StandardUnitKey", SqlDbType.Int).Value = _StandardUnitKey;
                    zCommand.Parameters.Add("@StandardUnitName", SqlDbType.NVarChar).Value = _StandardUnitName;
                    if (_DiscontinuedDate == null)
                    {
                        zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = _DiscontinuedDate;
                    }

                    zCommand.Parameters.Add("@SafetyInStock", SqlDbType.Float).Value = _SafetyInStock;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@ServiceLine", SqlDbType.NChar).Value = _ServiceLine;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@ServiceModeKey", SqlDbType.Int).Value = _ServiceModeKey;
                    zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = _PhotoPath;
                    zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = _StatusKey;
                    zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = _StatusName;
                    zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Create_ClientKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[PDT_Service] ("
            + " ServiceKey ,ServiceID ,ServiceName ,ServiceNumber ,ServiceSerial ,ServiceModel ,StandardCost ,StandardUnitKey ,StandardUnitName ,DiscontinuedDate ,SafetyInStock ,Style ,Class ,ServiceLine ,CategoryKey ,CategoryName ,ServiceModeKey ,PhotoPath ,StatusKey ,StatusName ,Description ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@ServiceKey ,@ServiceID ,@ServiceName ,@ServiceNumber ,@ServiceSerial ,@ServiceModel ,@StandardCost ,@StandardUnitKey ,@StandardUnitName ,@DiscontinuedDate ,@SafetyInStock ,@Style ,@Class ,@ServiceLine ,@CategoryKey ,@CategoryName ,@ServiceModeKey ,@PhotoPath ,@StatusKey ,@StatusName ,@Description ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ServiceKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ServiceKey);
                    zCommand.Parameters.Add("@ServiceID", SqlDbType.NVarChar).Value = _ServiceID;
                    zCommand.Parameters.Add("@ServiceName", SqlDbType.NVarChar).Value = _ServiceName;
                    zCommand.Parameters.Add("@ServiceNumber", SqlDbType.NVarChar).Value = _ServiceNumber;
                    zCommand.Parameters.Add("@ServiceSerial", SqlDbType.NVarChar).Value = _ServiceSerial;
                    zCommand.Parameters.Add("@ServiceModel", SqlDbType.NVarChar).Value = _ServiceModel;
                    zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = _StandardCost;
                    zCommand.Parameters.Add("@StandardUnitKey", SqlDbType.Int).Value = _StandardUnitKey;
                    zCommand.Parameters.Add("@StandardUnitName", SqlDbType.NVarChar).Value = _StandardUnitName;
                    if (_DiscontinuedDate == null)
                    {
                        zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = _DiscontinuedDate;
                    }

                    zCommand.Parameters.Add("@SafetyInStock", SqlDbType.Float).Value = _SafetyInStock;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@ServiceLine", SqlDbType.NChar).Value = _ServiceLine;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@ServiceModeKey", SqlDbType.Int).Value = _ServiceModeKey;
                    zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = _PhotoPath;
                    zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = _StatusKey;
                    zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = _StatusName;
                    zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Update()
            {
                string zSQL = "UPDATE [dbo].[PDT_Service] SET "
                            + " ServiceID = @ServiceID,"
                            + " ServiceName = @ServiceName,"
                            + " ServiceNumber = @ServiceNumber,"
                            + " ServiceSerial = @ServiceSerial,"
                            + " ServiceModel = @ServiceModel,"
                            + " StandardCost = @StandardCost,"
                            + " StandardUnitKey = @StandardUnitKey,"
                            + " StandardUnitName = @StandardUnitName,"
                            + " DiscontinuedDate = @DiscontinuedDate,"
                            + " SafetyInStock = @SafetyInStock,"
                            + " Style = @Style,"
                            + " Class = @Class,"
                            + " ServiceLine = @ServiceLine,"
                            + " CategoryKey = @CategoryKey,"
                            + " CategoryName = @CategoryName,"
                            + " ServiceModeKey = @ServiceModeKey,"
                            + " PhotoPath = @PhotoPath,"
                            + " StatusKey = @StatusKey,"
                            + " StatusName = @StatusName,"
                            + " Description = @Description,"
                            + " PartnerNumber = @PartnerNumber,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedOn = GetDate(),"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName"
                           + " WHERE ServiceKey = @ServiceKey";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ServiceKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ServiceKey);
                    zCommand.Parameters.Add("@ServiceID", SqlDbType.NVarChar).Value = _ServiceID;
                    zCommand.Parameters.Add("@ServiceName", SqlDbType.NVarChar).Value = _ServiceName;
                    zCommand.Parameters.Add("@ServiceNumber", SqlDbType.NVarChar).Value = _ServiceNumber;
                    zCommand.Parameters.Add("@ServiceSerial", SqlDbType.NVarChar).Value = _ServiceSerial;
                    zCommand.Parameters.Add("@ServiceModel", SqlDbType.NVarChar).Value = _ServiceModel;
                    zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = _StandardCost;
                    zCommand.Parameters.Add("@StandardUnitKey", SqlDbType.Int).Value = _StandardUnitKey;
                    zCommand.Parameters.Add("@StandardUnitName", SqlDbType.NVarChar).Value = _StandardUnitName;
                    if (_DiscontinuedDate == null)
                    {
                        zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = _DiscontinuedDate;
                    }

                    zCommand.Parameters.Add("@SafetyInStock", SqlDbType.Float).Value = _SafetyInStock;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@ServiceLine", SqlDbType.NChar).Value = _ServiceLine;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@ServiceModeKey", SqlDbType.Int).Value = _ServiceModeKey;
                    zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = _PhotoPath;
                    zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = _StatusKey;
                    zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = _StatusName;
                    zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Delete()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[PDT_Service] Set RecordStatus = 99 WHERE ServiceKey = @ServiceKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ServiceKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ServiceKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[PDT_Service] WHERE ServiceKey = @ServiceKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ServiceKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ServiceKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            #endregion
        }
        public class Access_Data
        {
            #region[Standard]
            public static bool CheckServiceID(string PartnerNumber, string ServiceID)
            {
                bool zResult = false;  //  Khong co
                DataTable zTable = new DataTable();
                string zSQL = @"SELECT Count(*) AS Amount FROM [dbo].[PDT_Service] WHERE PartnerNumber = @PartnerNumber AND ServiceID = @ServiceID AND RecordStatus != 99";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@ServiceID", SqlDbType.NVarChar).Value = ServiceID;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string Result = ex.ToString();
                }
                if (zTable.Rows.Count > 0)
                {
                    int zAmount = 0;
                    DataRow zRow = zTable.Rows[0];
                    zAmount = int.Parse(zRow["Amount"].ToString());
                    if (zAmount == 0)
                    {
                        zResult = true;
                    }
                    else
                    {
                        zResult = false;
                    }
                }
                return zResult;
            }
            #endregion
        }
        #endregion
    }
}
