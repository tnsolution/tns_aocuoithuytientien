﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TN_Connection;
using TN_Tools;
using TN_User;

namespace TN_WinApp.IVT
{
    public partial class Frm_Category : Form
    {
        private string _FormUrl = "/IVT/Frm_Category";
        private int _CategoryKey = 0;
        public string FormUrl
        {
            get
            {
                return _FormUrl;
            }

            set
            {
                _FormUrl = value;
            }
        }

        public int CategoryKey
        {
            get
            {
                return _CategoryKey;
            }

            set
            {
                _CategoryKey = value;
            }
        }

        public Frm_Category()
        {
            InitializeComponent();
            TitleForm.MouseDown += Frm_MouseDown;
            TitleForm.MouseMove += Frm_MouseMove;
            TitleForm.MouseUp += Frm_MouseUp;

            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;

            btn_AddNew.Click += Btn_AddNew_Click;
            btn_Save.Click += Btn_Save_Click;
            btn_Delete.Click += Btn_Delete_Click;
            btn_Search.Click += Btn_Search_Click;

            txt_Rank.KeyPress += TextBoxNumber_KeyPress;
            LVData.Click += LVData_ItemActivate;

            this.Text = "Cài đặt loại áo";
            this.StartPosition = FormStartPosition.CenterScreen;
            this.FormBorderStyle = FormBorderStyle.None;
            this.ControlBox = false;
            this.DoubleBuffered = true;
        }

        private void Frm_Category_Load(object sender, EventArgs e)
        {
            Get_Auth();
            if (_RoleForm.RoleRead)
            {
                DesignLayout(LVData);
                LoadListView();
                LoadData();
            }
        }

        #region[Event]
        private void TextBoxNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void Btn_AddNew_Click(object sender, EventArgs e)
        {
            ClearForm();
        }
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            string zResult = "";
            string zMessage = "";
            zMessage = CheckBeforSave();
            if (zMessage.Length == 0)
            {
                zResult = Save();
                if (zResult.Substring(0, 3) == "200" || zResult.Substring(0, 3) == "201")
                {
                    MessageBox.Show("Cập nhật thành công!");
                    LoadData();
                    LoadListView();
                }
                else
                {
                    MessageBox.Show("Lỗi.Vui lòng liên hệ IT.Chi tiết lỗi mã : \n " + zResult);
                }
            }
            else
            {
                MessageBox.Show(zMessage);
            }
        }
        private void Btn_Delete_Click(object sender, EventArgs e)
        {
            if (CategoryKey != 0)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    Product_Category_Info zInfo = new Product_Category_Info(CategoryKey);
                    zInfo.Delete();
                    if (zInfo.Message.Substring(0, 3) == "200")
                    {
                        MessageBox.Show("Xóa thành công!");
                        ClearForm();
                        LoadListView();
                    }
                    else
                    {
                        MessageBox.Show("Lỗi.Vui lòng liên hệ IT.Chi tiết lỗi mã: \n " + zInfo.Message);
                    }
                }
            }
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            LoadListView();
        }
        #endregion

        #region[Process]

        private string CheckBeforSave()
        {
            string zResult = "";
            if (txt_Name.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng điền tên! \n";
            }

            if (CategoryKey == 0)
            {
                bool Count = Access_Data.CheckNameVN(SessionUser.UserLogin.PartnerNumber, txt_Name.Text.Trim());
                if (!Count)
                {
                    zResult += "Vui lòng chọn tên khác! \n";
                }
            }

            return zResult;
        }
        private string Save()
        {
            Product_Category_Info zInfo;
            if (CategoryKey == 0)
            {
                zInfo = new Product_Category_Info();
            }
            else
            {
                zInfo = new Product_Category_Info(CategoryKey);
            }
            zInfo.CategoryNameVN = txt_Name.Text.Trim();
            zInfo.Description = txt_Description.Text.Trim();
            zInfo.Rank = txt_Rank.Text.ToInt();

            zInfo.CreatedBy = SessionUser.UserLogin.UserKey;
            zInfo.CreatedName = SessionUser.UserLogin.EmployeeName;
            zInfo.ModifiedBy = SessionUser.UserLogin.UserKey;
            zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;

            zInfo.PartnerNumber = SessionUser.UserLogin.PartnerNumber;

            if (CategoryKey == 0)
            {
                zInfo.Create_ServerKey();
                CategoryKey = zInfo.CategoryKey;
            }
            else
            {
                zInfo.Update();
                CategoryKey = zInfo.CategoryKey;
            }

            return zInfo.Message;
        }
        private void ClearForm()
        {
            CategoryKey = 0;
            txt_Name.Text = "";
            txt_Description.Text = "";
            txt_Rank.Text = "0";
        }
        private void LoadData()
        {
            Product_Category_Info zInfo;
            if (CategoryKey == 0)
            {
                zInfo = new Product_Category_Info();
            }
            else
            {
                zInfo = new Product_Category_Info(CategoryKey);
            }

            txt_Name.Text = zInfo.CategoryNameVN;
            txt_Description.Text = zInfo.Description;
            txt_Rank.Text = zInfo.Rank.ToString();
        }

        #endregion

        #region [ListView]
        private void DesignLayout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Loại";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ghi chú";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Sắp xếp";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);


            TNPaintControl.DrawLVStyle(ref LVData);
        }
        private void LoadListView()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable zTable = Access_Data.List(SessionUser.UserLogin.PartnerNumber, txt_Search.Text.Trim());
            LV.Items.Clear();
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow zRow = zTable.Rows[i];
                lvi.Tag = zRow["CategoryKey"].ToString();
                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["CategoryNameVN"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["Description"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["Rank"].ToString();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;

        }
        private void LVData_ItemActivate(object sender, EventArgs e)
        {
            if (LVData.SelectedItems.Count > 0)
            {
                CategoryKey = LVData.SelectedItems[0].Tag.ToInt();
                LoadData();
            }
        }
        #endregion

        #region [Dùng kéo rê form]
        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;



        private void Frm_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }
        private void Frm_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }
        private void Frm_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        #region [Auth]
        User_Role_Info _RoleForm = new User_Role_Info();
        private void Get_Auth()
        {
            List<User_Role_Info> zListRole = SessionUser.TableRole;
            if (zListRole.Count > 0)
            {
                try
                {
                    _RoleForm = zListRole.SingleOrDefault(r => r.RoleURL == FormUrl);
                    if (_RoleForm == null)
                    {
                        _RoleForm = new User_Role_Info();
                        MessageBox.Show("Form này chưa được thêm quyền, vui lòng liên hệ IT !.");
                        this.Close();
                    }
                    else
                    {
                        if (!_RoleForm.RoleRead)
                        {
                            MessageBox.Show("Bạn không có quyền xem, vui lòng liên hệ IT !.");
                            this.Close();
                        }
                        if (!_RoleForm.RoleEdit)
                        {
                            btn_Save.Visible = false;
                            btn_AddNew.Visible = false;
                        }
                        if (!_RoleForm.RoleAdd)
                        {
                            btn_Save.Visible = false;
                            btn_AddNew.Visible = false;
                        }
                        if (!_RoleForm.RoleDel)
                        {
                            btn_Delete.Visible = false;
                        }
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Bạn chưa được phân quyền, vui lòng liên hệ IT !.");
            }
        }
        #endregion

        #region[Access Info -Data]
        public class Access_Data
        {
            public static bool CheckNameVN(string PartnerNumber, string CategoryNameVN)
            {
                bool zResult = false;  //  Khong co
                DataTable zTable = new DataTable();
                string zSQL = @"SELECT Count(*) AS Amount FROM [dbo].[PDT_Product_Category] WHERE PartnerNumber = @PartnerNumber AND CategoryNameVN = @CategoryNameVN AND RecordStatus != 99";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@CategoryNameVN", SqlDbType.NVarChar).Value = CategoryNameVN;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string Result = ex.ToString();
                }
                if (zTable.Rows.Count > 0)
                {
                    int zAmount = 0;
                    DataRow zRow = zTable.Rows[0];
                    zAmount = int.Parse(zRow["Amount"].ToString());
                    if (zAmount == 0)
                    {
                        zResult = true;
                    }
                    else
                    {
                        zResult = false;
                    }
                }
                return zResult;
            }
            public static DataTable List(string PartnerNumber, string Search)
            {
                DataTable zTable = new DataTable();
                string zSQL = "SELECT  * FROM [dbo].[PDT_Product_Category] WHERE PartnerNumber = @PartnerNumber AND RecordStatus != 99 ";
                if (Search.Length > 1)
                {
                    zSQL += "AND (CategoryNameVN LIKE @Content)";
                }
                zSQL += " ORDER BY CategoryNameVN";
                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = "%" + Search + "%";
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
        }
        public class Product_Category_Info
        {
            #region [ Field Name ]
            private int _CategoryKey = 0;
            private string _CategoryNameVN = "";
            private string _CategoryNameEN = "";
            private string _CategoryNameCN = "";
            private string _Description = "";
            private int _Parent = 0;
            private int _Rank = 0;
            private bool _Publish;
            private int _Level = 0;
            private string _PartnerNumber = "";
            private int _RecordStatus = 0;
            private DateTime? _CreatedOn = null;
            private string _CreatedBy = "";
            private string _CreatedName = "";
            private DateTime? _ModifiedOn = null;
            private string _ModifiedBy = "";
            private string _ModifiedName = "";
            private string _Message = "";
            #endregion

            #region [ Constructor Get Information ]
            public Product_Category_Info()
            {

            }
            public Product_Category_Info(int CategoryKey)
            {
                string zSQL = "SELECT * FROM [dbo].[PDT_Product_Category] WHERE CategoryKey = @CategoryKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                        _CategoryNameVN = zReader["CategoryNameVN"].ToString();
                        _CategoryNameEN = zReader["CategoryNameEN"].ToString();
                        _CategoryNameCN = zReader["CategoryNameCN"].ToString();
                        _Description = zReader["Description"].ToString();
                        _Parent = int.Parse(zReader["Parent"].ToString());
                        _Rank = int.Parse(zReader["Rank"].ToString());
                        _Publish = (bool)zReader["Publish"];
                        _Level = int.Parse(zReader["Level"].ToString());
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }

            #endregion

            #region [ Properties ]
            public int CategoryKey
            {
                get { return _CategoryKey; }
                set { _CategoryKey = value; }
            }
            public string CategoryNameVN
            {
                get { return _CategoryNameVN; }
                set { _CategoryNameVN = value; }
            }
            public string CategoryNameEN
            {
                get { return _CategoryNameEN; }
                set { _CategoryNameEN = value; }
            }
            public string CategoryNameCN
            {
                get { return _CategoryNameCN; }
                set { _CategoryNameCN = value; }
            }
            public string Description
            {
                get { return _Description; }
                set { _Description = value; }
            }
            public int Parent
            {
                get { return _Parent; }
                set { _Parent = value; }
            }
            public int Rank
            {
                get { return _Rank; }
                set { _Rank = value; }
            }
            public bool Publish
            {
                get { return _Publish; }
                set { _Publish = value; }
            }
            public int Level
            {
                get { return _Level; }
                set { _Level = value; }
            }
            public string PartnerNumber
            {
                get { return _PartnerNumber; }
                set { _PartnerNumber = value; }
            }
            public int RecordStatus
            {
                get { return _RecordStatus; }
                set { _RecordStatus = value; }
            }
            public DateTime? CreatedOn
            {
                get { return _CreatedOn; }
                set { _CreatedOn = value; }
            }
            public string CreatedBy
            {
                get { return _CreatedBy; }
                set { _CreatedBy = value; }
            }
            public string CreatedName
            {
                get { return _CreatedName; }
                set { _CreatedName = value; }
            }
            public DateTime? ModifiedOn
            {
                get { return _ModifiedOn; }
                set { _ModifiedOn = value; }
            }
            public string ModifiedBy
            {
                get { return _ModifiedBy; }
                set { _ModifiedBy = value; }
            }
            public string ModifiedName
            {
                get { return _ModifiedName; }
                set { _ModifiedName = value; }
            }
            public string Code
            {
                get
                {
                    if (_Message.Length >= 3)
                    {
                        return _Message.Substring(0, 3);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            public string Message
            {
                get { return _Message; }
                set { _Message = value; }
            }
            #endregion

            #region [ Constructor Update Information ]
            public string Create_ServerKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[PDT_Product_Category] ("
             + " CategoryNameVN ,CategoryNameEN ,CategoryNameCN ,Description ,Parent ,Rank ,Publish ,Level ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@CategoryNameVN ,@CategoryNameEN ,@CategoryNameCN ,@Description ,@Parent ,@Rank ,@Publish ,@Level ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@CategoryNameVN", SqlDbType.NVarChar).Value = _CategoryNameVN;
                    zCommand.Parameters.Add("@CategoryNameEN", SqlDbType.NVarChar).Value = _CategoryNameEN;
                    zCommand.Parameters.Add("@CategoryNameCN", SqlDbType.NVarChar).Value = _CategoryNameCN;
                    zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                    zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = _Parent;
                    zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                    zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = _Publish;
                    zCommand.Parameters.Add("@Level", SqlDbType.Int).Value = _Level;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Update()
            {
                string zSQL = "UPDATE [dbo].[PDT_Product_Category] SET "
                            + " CategoryNameVN = @CategoryNameVN,"
                            + " CategoryNameEN = @CategoryNameEN,"
                            + " CategoryNameCN = @CategoryNameCN,"
                            + " Description = @Description,"
                            + " Parent = @Parent,"
                            + " Rank = @Rank,"
                            + " Publish = @Publish,"
                            + " Level = @Level,"
                            + " PartnerNumber = @PartnerNumber,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedOn = GetDate(),"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName"
                           + " WHERE CategoryKey = @CategoryKey";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryNameVN", SqlDbType.NVarChar).Value = _CategoryNameVN;
                    zCommand.Parameters.Add("@CategoryNameEN", SqlDbType.NVarChar).Value = _CategoryNameEN;
                    zCommand.Parameters.Add("@CategoryNameCN", SqlDbType.NVarChar).Value = _CategoryNameCN;
                    zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                    zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = _Parent;
                    zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                    zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = _Publish;
                    zCommand.Parameters.Add("@Level", SqlDbType.Int).Value = _Level;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Delete()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[PDT_Product_Category] Set RecordStatus = 99 WHERE CategoryKey = @CategoryKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[PDT_Product_Category] WHERE CategoryKey = @CategoryKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            #endregion
        }
        #endregion
    }
}
