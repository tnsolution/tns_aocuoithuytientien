﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using TN_Connection;
using TN_Tools;
using TN_User;

namespace TN_WinApp.IVT
{
    public partial class Frm_ProductList : Form
    {
        private string _FormUrl = "/IVT/Frm_ProductList";
        public string FormUrl
        {
            get
            {
                return _FormUrl;
            }

            set
            {
                _FormUrl = value;
            }
        }
        public Frm_ProductList()
        {
            InitializeComponent();

            TitleForm.MouseDown += Frm_MouseDown;
            TitleForm.MouseMove += Frm_MouseMove;
            TitleForm.MouseUp += Frm_MouseUp;

            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;

            LVData.ItemActivate += LVData_ItemActivate;
            LVData.KeyDown += LVData_KeyDown;

            btnSearch.Click += BtnSearch_Click;
            btnAddNew.Click += BtnAddNew_Click;

            //format Form
            this.Text = "Danh sách sản phẩm";
            this.WindowState = FormWindowState.Maximized;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.FormBorderStyle = FormBorderStyle.None;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.Load += Frm_ProductList_Load;
        }

        private void Frm_ProductList_Load(object sender, EventArgs e)
        {
            Get_Auth();
            if (_RoleForm.RoleRead)
            {
                LoadCombobox();
                DesignLayout(LVData);
                LoadListView();
            }
        }

        #region [Process]
        private void LoadCombobox()
        {
            string zSQLCategory = "SELECT CategoryKey, CategoryNameVN FROM PDT_Product_Category WHERE RecordStatus <> 99 AND PartnerNumber = '" + SessionUser.UserLogin.PartnerNumber + "'";
            LoadDataToToolbox.KryptonComboBox(cboCategory, zSQLCategory, "--Chọn--");
        }
        private string Delete(string Key)
        {
            Frm_ProductEdit.Product_FED_Info zInfo = new Frm_ProductEdit.Product_FED_Info();
            zInfo.ProductKey = Key;
            zInfo.Delete();
            return zInfo.Message;
        }
        #endregion

        #region [Event]
        private void BtnSearch_Click(object sender, EventArgs e)
        {
            LoadListView();
        }
        private void BtnAddNew_Click(object sender, EventArgs e)
        {
            Frm_ProductEdit Frm = new Frm_ProductEdit();
            Frm.ProductKey = "";
            Frm.ShowDialog();
            LoadListView();
        }
        #endregion

        #region [ListView]
        private void DesignLayout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã sản phẩm";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Loại sản phẩm";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên sản phẩm";
            colHead.Width = 350;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Đơn vị";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead); 

            colHead = new ColumnHeader();
            colHead.Text = "Số lượng";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Giá gốc";
            colHead.Width = 130;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Giá bán ";
            colHead.Width = 130;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Giá thuê";
            colHead.Width = 130;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tình trạng";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ghi chú";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            TNPaintControl.DrawLVStyle(ref LVData);
        }
        private void LoadListView()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable zTable = Access_Data.List(SessionUser.UserLogin.PartnerNumber, txtSearch.Text.Trim(), cboCategory.SelectedValue.ToInt());
            LV.Items.Clear();
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow zRow = zTable.Rows[i];
                lvi.Tag = zRow["ProductKey"].ToString();
                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["ProductID"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["CategoryName"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["ProductName"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["StandardUnitName"].ToString();
                lvi.SubItems.Add(lvsi);

                double SafetyInStock = zRow["SafetyInStock"].ToDouble();
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = SafetyInStock.ToString("n0", CultureInfo.GetCultureInfo("vi-VN"));
                lvi.SubItems.Add(lvsi);

                double StandardCost = zRow["StandardCost"].ToDouble();
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = StandardCost.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                lvi.SubItems.Add(lvsi);

                

                double ProductPrice = zRow["ProductPrice"].ToDouble();
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = ProductPrice.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                lvi.SubItems.Add(lvsi);

                double RentCost = zRow["RentCost"].ToDouble();
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = RentCost.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["StatusName"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["Description"].ToString();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;

        }
        private void LVData_ItemActivate(object sender, EventArgs e)
        {
            if (LVData.SelectedItems.Count > 0)
            {
                Frm_ProductEdit Frm = new Frm_ProductEdit();
                Frm.ProductKey = LVData.SelectedItems[0].Tag.ToString();
                Frm.ShowDialog();
                LoadListView();
            }
        }
        private void LVData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes && LVData.SelectedItems.Count > 0)
                {
                    string Key = LVData.SelectedItems[0].Tag.ToString();
                    string zResult = Delete(Key);

                    if (zResult.Substring(0, 3) == "200" ||
                        zResult.Substring(0, 3) == "201")
                    {
                        MessageBox.Show("Đã xóa thành công !");
                        LoadListView();
                    }
                }
            }
        }
        #endregion

        #region [Dùng kéo rê form]
        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;



        private void Frm_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }
        private void Frm_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }
        private void Frm_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        #region [Auth]
        User_Role_Info _RoleForm = new User_Role_Info();
        private void Get_Auth()
        {
            List<User_Role_Info> zListRole = SessionUser.TableRole;
            if (zListRole.Count > 0)
            {
                try
                {
                    _RoleForm = zListRole.SingleOrDefault(r => r.RoleURL == FormUrl);
                    if (_RoleForm == null)
                    {
                        _RoleForm = new User_Role_Info();
                        MessageBox.Show("Form này chưa được thêm quyền, vui lòng liên hệ IT !.");
                        this.Close();
                    }
                    else
                    {
                        if (!_RoleForm.RoleRead)
                        {
                            MessageBox.Show("Bạn không có quyền xem, vui lòng liên hệ IT !.");
                            this.Close();
                        }
                        if (!_RoleForm.RoleEdit)
                        {

                        }
                        if (!_RoleForm.RoleAdd)
                        {

                        }
                        if (!_RoleForm.RoleDel)
                        {

                        }
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Bạn chưa được phân quyền, vui lòng liên hệ IT !.");
            }
        }
        #endregion       

        #region[Access_Data]
        public class Access_Data
        {
            public static DataTable List(string PartnerNumber, string Name, int CategoryKey)
            {
                DataTable zTable = new DataTable();
                string zSQL = @"
SELECT TOP 30 * 
FROM [dbo].[PDT_Product_FED] 
WHERE RecordStatus <> 99  
AND PartnerNumber= @PartnerNumber ";
                if (CategoryKey != 0)
                {
                    zSQL += " AND CategoryKey = @CategoryKey";
                }
                if (Name.Trim().Length > 0)
                {
                    zSQL += " AND (ProductID LIKE @Name OR ProductName LIKE @Name)";
                }

                zSQL += " ORDER BY CreatedOn DESC";
                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
            public static DataTable List(string PartnerNumber, string OrderBy, int PageNumber, int PageSize)
            {
                DataTable zTable = new DataTable();
                string zSQL = "SELECT  * FROM [dbo].[PDT_Product_FED] WHERE PartnerNumber = @PartnerNumber AND RecordStatus != 99 ";
                if (OrderBy.Trim().Length > 0)
                {
                    zSQL += " ORDER BY " + OrderBy;
                }

                zSQL += " OFFSET @PageSize * (@PageNumber - 1) ROWS " +
                      " FETCH NEXT @PageSize ROWS ONLY OPTION(RECOMPILE)";
                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = PageSize;
                    zCommand.Parameters.Add("@PageNumber", SqlDbType.Int).Value = PageNumber;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
            public static int Total_Record(string PartnerNumber)
            {
                int zQuantity = 0;
                string zSQL = "SELECT  Count(*) FROM [dbo].[PDT_Product_FED] WHERE PartnerNumber = @PartnerNumber AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    string zResult = zCommand.ExecuteScalar().ToString();
                    int.TryParse(zResult, out zQuantity);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zQuantity;
            }
            public static DataTable List_Search(string Content, string PartnerNumber, string OrderBy, int PageNumber, int PageSize)
            {
                DataTable zTable = new DataTable();
                string zSQL = "SELECT  * FROM [PDT_Product_FED] WHERE PartnerNumber = @PartnerNumber AND RecordStatus != 99 ";
                if (Content.Length > 1)
                {
                    zSQL += "AND (ProductID LIKE @Content OR ProductName LIKE @Content)";
                }
                else
                {
                    zSQL += " AND Name LIKE @Content ";
                }
                if (OrderBy.Trim().Length > 0)
                {
                    zSQL += " ORDER BY " + OrderBy;
                }

                zSQL += " OFFSET @PageSize * (@PageNumber - 1) ROWS " +
                      " FETCH NEXT @PageSize ROWS ONLY OPTION(RECOMPILE)";
                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    if (Content.Length > 1)
                    {
                        zCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = "%" + Content + "%";
                    }
                    else
                    {
                        zCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = Content + "%";
                    }

                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = PageSize;
                    zCommand.Parameters.Add("@PageNumber", SqlDbType.Int).Value = PageNumber;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
            public static int Total_Record(string Content, string PartnerNumber)
            {
                int zQuantity = 0;
                string zSQL = "SELECT  Count(*) FROM [dbo].[PDT_Product_FED] WHERE PartnerNumber = @PartnerNumber AND RecordStatus != 99 ";
                if (Content.Length > 1)
                {
                    zSQL += "AND (ProductID LIKE @Content OR ProductName LIKE @Content)";
                }
                else
                {
                    zSQL += " AND Name LIKE @Content ";
                }
                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    if (Content.Length > 1)
                    {
                        zCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = "%" + Content + "%";
                    }
                    else
                    {
                        zCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = Content + "%";
                    }

                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    string zResult = zCommand.ExecuteScalar().ToString();
                    int.TryParse(zResult, out zQuantity);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zQuantity;
            }
        }
        #endregion
    }
}
