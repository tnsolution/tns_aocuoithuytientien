﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using TN_Connection;

namespace TN_WinApp
{
    public partial class Frm_Connect : Form
    {
        private ConnectDataBaseInfo _ConnectDataBaseInfo = new ConnectDataBaseInfo();
        private bool _IsConnect = false;

        public bool IsConnect
        {
            get
            {
                return _IsConnect;
            }

            set
            {
                _IsConnect = value;
            }
        }
        public ConnectDataBaseInfo ConnectDataBaseInfo
        {
            get
            {
                return _ConnectDataBaseInfo;
            }

            set
            {
                _ConnectDataBaseInfo = value;
            }
        }

        public Frm_Connect()
        {
            InitializeComponent();

            btnConnect.Click += btnConnect_Click;
            btnClose.Click += btnClose_Click;

            RWConfig nFileConfig = new RWConfig();
            nFileConfig.ReadConfig();
            _ConnectDataBaseInfo = nFileConfig.ConnectInfo;
            if (!_ConnectDataBaseInfo.IsConnectLocal)
            {
                txtSQLServer.Text = _ConnectDataBaseInfo.DataSource;
                txtDataBase.Text = _ConnectDataBaseInfo.DataBase;
                txtUserName.Text = _ConnectDataBaseInfo.UserName;
                txtPassword.Text = _ConnectDataBaseInfo.Password;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnConnect_Click(object sender, EventArgs e)
        {
            Connect();
        }

        private void Connect()
        {
            this.Cursor = Cursors.WaitCursor;

            if (CheckConnect())
            {
                _IsConnect = true;
                this.DialogResult = DialogResult.Yes;
                this.Close();
            }
            else
            {
                _IsConnect = false;
                MessageBox.Show("Không thể kết nối vui lòng liên hệ quản trị phần mềm !.");
            }
            this.Cursor = Cursors.Default;
        }
        private bool CheckConnect()
        {
            _ConnectDataBaseInfo = new ConnectDataBaseInfo();

            bool isConnected = false;

            _ConnectDataBaseInfo.DataSource = txtSQLServer.Text.Trim();
            _ConnectDataBaseInfo.DataBase = txtDataBase.Text.Trim();
            _ConnectDataBaseInfo.UserName = txtUserName.Text.Trim();
            _ConnectDataBaseInfo.Password = txtPassword.Text.Trim();

            SqlConnection nConSQL = new SqlConnection();
            try
            {
                nConSQL.ConnectionString = _ConnectDataBaseInfo.ConnectionString;
                nConSQL.Open();

                if (nConSQL.State == ConnectionState.Open)
                {
                    isConnected = true;
                    RWConfig nFileConfig = new RWConfig();
                    nFileConfig.ConnectInfo = _ConnectDataBaseInfo;
                    nFileConfig.SaveConfig();
                }
            }
            catch (System.Exception err)
            {
                MessageBox.Show(err.Message);
                isConnected = false;
            }
            finally
            {
                nConSQL.Close();
            }
            return isConnected;
        }
    }
}
