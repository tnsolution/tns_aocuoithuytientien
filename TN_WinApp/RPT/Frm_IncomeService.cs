﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using TN_Connection;
using TN_Tools;
using TN_User;

namespace TN_WinApp.RPT
{
    public partial class Frm_IncomeService : Form
    {
        private string _FormUrl = "/RPT/Frm_IncomeService";
        public string FormUrl
        {
            get
            {
                return _FormUrl;
            }

            set
            {
                _FormUrl = value;
            }
        }

        public Frm_IncomeService()
        {
            InitializeComponent();

            TitleForm.MouseDown += Frm_MouseDown;
            TitleForm.MouseMove += Frm_MouseMove;
            TitleForm.MouseUp += Frm_MouseUp;

            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;

            btnSearch.Click += BtnSearch_Click;

            this.Text = "Doanh số thuê áo cưới";
            this.WindowState = FormWindowState.Maximized;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.FormBorderStyle = FormBorderStyle.None;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.Load += Frm_IncomeService_Load;
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            DateTime FromDate = new DateTime(dteFromDate.Value.Year, dteFromDate.Value.Month, dteFromDate.Value.Day, 0, 0, 0);
            DateTime ToDate = new DateTime(dteToDate.Value.Year, dteToDate.Value.Month, dteToDate.Value.Day, 23, 59, 59);
            dteFromDate.Value = FromDate;
            dteToDate.Value = ToDate;

            DataTable zTable = Access_Data.Report_Receipt(SessionUser.UserLogin.PartnerNumber, FromDate, ToDate);
            LoadData_GVProduct(GVData, zTable);
        }

        private void Frm_IncomeService_Load(object sender, EventArgs e)
        {
            Get_Auth();
            if (_RoleForm.RoleRead)
            {
                DateTime zDate = DateTime.Now;
                DateTime FromDate = new DateTime(zDate.Year, zDate.Month, 1, 0, 0, 0);
                DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
                ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
                dteFromDate.Value = FromDate;
                dteToDate.Value = ToDate;

                DataTable zTable = Access_Data.Report_Receipt(SessionUser.UserLogin.PartnerNumber, FromDate, ToDate);

                DesignLayout_GVProduct(GVData);
                LoadData_GVProduct(GVData, zTable);
            }
        }

        private void DesignLayout_GVProduct(DataGridView GV)
        {
            TNPaintControl.DrawGVStyle(ref GV);

            GV.Columns.Add("No", "STT");
            GV.Columns.Add("ReceiptDate", "Ngày thu");
            GV.Columns.Add("ReceiptID", "Số phiếu thu");
            GV.Columns.Add("CustomerName", "Khách hàng");
            GV.Columns.Add("CustomerPhone", "Điện thoại");
            GV.Columns.Add("DocumentID", "Mã đơn hàng");           
            GV.Columns.Add("AmountCurrencyMain", "Số tiền");
            GV.Columns.Add("ReceiptDescription", "Nội dung thu");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["ReceiptDate"].Width = 90;
            GV.Columns["ReceiptDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["CustomerName"].Width = 250;
            GV.Columns["CustomerName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["CustomerName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["CustomerPhone"].Width = 90;
            GV.Columns["CustomerPhone"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["DocumentID"].Width = 120;
            GV.Columns["DocumentID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["AmountCurrencyMain"].Width = 120;
            GV.Columns["AmountCurrencyMain"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["ReceiptDescription"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            GV.Columns["ReceiptDescription"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["ReceiptDescription"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.ReadOnly = true;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = false;
            GV.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }
        private void LoadData_GVProduct(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();

            int i = 0;
            foreach (DataRow r in Table.Rows)
            {
                GV.Rows.Add();
                DataGridViewRow zGvRow = GV.Rows[i];
                zGvRow.Cells["No"].Value = (i + 1).ToString();
                zGvRow.Cells["ReceiptDate"].Value = r["ReceiptDate"].ToDateString();
                zGvRow.Cells["ReceiptID"].Value = r["ReceiptID"].ToString();
                zGvRow.Cells["CustomerName"].Value = r["CustomerName"].ToString();
                zGvRow.Cells["CustomerPhone"].Value = r["CustomerPhone"].ToString().Trim();
                zGvRow.Cells["DocumentID"].Value = r["DocumentID"].ToString().Trim();
                zGvRow.Cells["ReceiptDescription"].Value = r["ReceiptDescription"].ToString().Trim();
                zGvRow.Cells["AmountCurrencyMain"].Value = r["AmountCurrencyMain"].FormatMoneyFromObject();               
                i++;
            }

            Sum_GVProduct(GVData);
        }
        private void Sum_GVProduct(DataGridView GV)
        {
            double AmountCurrencyMain = 0;

            foreach (DataGridViewRow r in GV.Rows)
            {
                AmountCurrencyMain += double.Parse(r.Cells["AmountCurrencyMain"].Value.ToString(), CultureInfo.GetCultureInfo("vi-VN"));
            }

            GV.Rows.Add();
            GV.Rows[GV.Rows.Count - 1].Cells["AmountCurrencyMain"].Value = AmountCurrencyMain.ToString("n0", CultureInfo.GetCultureInfo("vi-VN"));
            GV.Rows[GV.Rows.Count - 1].DefaultCellStyle.BackColor = Color.LightSteelBlue;
            GV.Rows[GV.Rows.Count - 1].DefaultCellStyle.ForeColor = Color.Maroon;
            GV.Rows[GV.Rows.Count - 1].DefaultCellStyle.Font = new Font("Tahoma", 11F, FontStyle.Bold);
            GV.Rows[GV.Rows.Count - 1].Height = 35;
        }

        #region [Dùng kéo rê form]
        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;



        private void Frm_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }
        private void Frm_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }
        private void Frm_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        #region [Auth]
        User_Role_Info _RoleForm = new User_Role_Info();
        private void Get_Auth()
        {
            List<User_Role_Info> zListRole = SessionUser.TableRole;
            if (zListRole.Count > 0)
            {
                try
                {
                    _RoleForm = zListRole.SingleOrDefault(r => r.RoleURL == FormUrl);
                    if (_RoleForm == null)
                    {
                        _RoleForm = new User_Role_Info();
                        MessageBox.Show("Form này chưa được thêm quyền, vui lòng liên hệ IT !.");
                        this.Close();
                    }
                    else
                    {
                        if (!_RoleForm.RoleRead)
                        {
                            MessageBox.Show("Bạn không có quyền xem, vui lòng liên hệ IT !.");
                            this.Close();
                        }
                        if (!_RoleForm.RoleEdit)
                        {

                        }
                        if (!_RoleForm.RoleAdd)
                        {

                        }
                        if (!_RoleForm.RoleDel)
                        {

                        }
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Bạn chưa được phân quyền, vui lòng liên hệ IT !.");
            }
        }
        #endregion

        #region [Access Info-Data]
        public class Access_Data
        {
            public static DataTable Report_Receipt(string PartnerNumber, DateTime? FromDate, DateTime? ToDate)
            {
                DataTable zTable = new DataTable();
                string zSQL = @"
SELECT A.*
FROM FNC_Receipt A
WHERE A.RecordStatus <> 99
AND A.PartnerNumber = @PartnerNumber
AND A.StyleKey = 2
--AND dbo.Get_OrderProductFromID_FED(DocumentID) <> 1 --THUE AO
";

                if (FromDate != null && ToDate != null)
                {
                    zSQL += " AND A.ReceiptDate >= @FromDate AND A.ReceiptDate <= @ToDate";
                }

                zSQL += " ORDER BY ReceiptDate DESC";
                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);

                    if (FromDate != null && ToDate != null)
                    {
                        zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                        zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                    }
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
        }
        #endregion
    }
}