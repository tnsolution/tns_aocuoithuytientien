﻿namespace Admin
{
    partial class Frm_UserSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_UserSetting));
            this.LVUserRole = new System.Windows.Forms.ListView();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.kryptonHeader4 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.LVPartnerRole = new System.Windows.Forms.ListView();
            this.PanelAction = new System.Windows.Forms.Panel();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.kryptonHeader5 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnAddUser = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.label6 = new System.Windows.Forms.Label();
            this.btnSearch = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.LVUser = new System.Windows.Forms.ListView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cboStatus = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.kryptonTextBox1 = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Panel_Left = new System.Windows.Forms.Panel();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.TitleForm = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.TabControl = new System.Windows.Forms.TabControl();
            this.kryptonHeader2 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.buttonSpecAny1 = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.buttonSpecAny3 = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.buttonSpecAny2 = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dte_ExpireDate = new TN_Tools.TNDateTime();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdo_Disable = new System.Windows.Forms.RadioButton();
            this.rdo_Enable = new System.Windows.Forms.RadioButton();
            this.btn_ResetPassword = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.txt_NewPassword = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_Password = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_UserName = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txt_SearchRole = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonButton1 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.PanelAction.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboStatus)).BeginInit();
            this.Panel_Left.SuspendLayout();
            this.TabControl.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // LVUserRole
            // 
            this.LVUserRole.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LVUserRole.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LVUserRole.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.LVUserRole.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LVUserRole.FullRowSelect = true;
            this.LVUserRole.GridLines = true;
            this.LVUserRole.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.LVUserRole.HideSelection = false;
            this.LVUserRole.Location = new System.Drawing.Point(0, 30);
            this.LVUserRole.Name = "LVUserRole";
            this.LVUserRole.Size = new System.Drawing.Size(392, 512);
            this.LVUserRole.TabIndex = 227;
            this.LVUserRole.UseCompatibleStateImageBehavior = false;
            this.LVUserRole.View = System.Windows.Forms.View.Details;
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(392, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader1.TabIndex = 226;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "Chi tiết quyền truy cập của tài khoản";
            // 
            // kryptonHeader4
            // 
            this.kryptonHeader4.AutoSize = false;
            this.kryptonHeader4.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader4.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader4.Name = "kryptonHeader4";
            this.kryptonHeader4.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader4.Size = new System.Drawing.Size(306, 30);
            this.kryptonHeader4.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader4.TabIndex = 226;
            this.kryptonHeader4.Values.Description = "";
            this.kryptonHeader4.Values.Heading = "Danh sách giao diện đã được chuyển giao";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.LVPartnerRole);
            this.splitContainer1.Panel1.Controls.Add(this.panel2);
            this.splitContainer1.Panel1.Controls.Add(this.kryptonHeader4);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.LVUserRole);
            this.splitContainer1.Panel2.Controls.Add(this.kryptonHeader1);
            this.splitContainer1.Size = new System.Drawing.Size(706, 544);
            this.splitContainer1.SplitterDistance = 308;
            this.splitContainer1.TabIndex = 226;
            // 
            // LVPartnerRole
            // 
            this.LVPartnerRole.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LVPartnerRole.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LVPartnerRole.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.LVPartnerRole.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LVPartnerRole.FullRowSelect = true;
            this.LVPartnerRole.GridLines = true;
            this.LVPartnerRole.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.LVPartnerRole.HideSelection = false;
            this.LVPartnerRole.Location = new System.Drawing.Point(0, 79);
            this.LVPartnerRole.Name = "LVPartnerRole";
            this.LVPartnerRole.Size = new System.Drawing.Size(306, 463);
            this.LVPartnerRole.TabIndex = 227;
            this.LVPartnerRole.UseCompatibleStateImageBehavior = false;
            this.LVPartnerRole.View = System.Windows.Forms.View.Details;
            // 
            // PanelAction
            // 
            this.PanelAction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.PanelAction.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelAction.Controls.Add(this.splitContainer1);
            this.PanelAction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelAction.Location = new System.Drawing.Point(3, 3);
            this.PanelAction.Name = "PanelAction";
            this.PanelAction.Size = new System.Drawing.Size(708, 546);
            this.PanelAction.TabIndex = 206;
            // 
            // tabPage1
            // 
            this.tabPage1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPage1.Controls.Add(this.PanelAction);
            this.tabPage1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.tabPage1.ForeColor = System.Drawing.Color.Navy;
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(718, 556);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Phân quyền tài khoản";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // kryptonHeader5
            // 
            this.kryptonHeader5.AutoSize = false;
            this.kryptonHeader5.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnAddUser});
            this.kryptonHeader5.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader5.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader5.Name = "kryptonHeader5";
            this.kryptonHeader5.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader5.Size = new System.Drawing.Size(294, 30);
            this.kryptonHeader5.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader5.TabIndex = 203;
            this.kryptonHeader5.Values.Description = "";
            this.kryptonHeader5.Values.Heading = "Danh sách";
            // 
            // btnAddUser
            // 
            this.btnAddUser.Text = "Thêm mới";
            this.btnAddUser.UniqueName = "2D72A1EE9183491A748AD353CD385141";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.DodgerBlue;
            this.label6.Dock = System.Windows.Forms.DockStyle.Right;
            this.label6.Location = new System.Drawing.Point(294, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(2, 724);
            this.label6.TabIndex = 150;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(247, 15);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btnSearch.Size = new System.Drawing.Size(40, 40);
            this.btnSearch.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btnSearch.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btnSearch.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btnSearch.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btnSearch.TabIndex = 150;
            this.btnSearch.Values.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Values.Image")));
            this.btnSearch.Values.Text = "";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(21, 19);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 16);
            this.label12.TabIndex = 147;
            this.label12.Text = "Tên đối tác";
            // 
            // comboBox2
            // 
            this.comboBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.comboBox2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.comboBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(1368, 232);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(234, 23);
            this.comboBox2.TabIndex = 6;
            // 
            // LVUser
            // 
            this.LVUser.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LVUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LVUser.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.LVUser.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LVUser.FullRowSelect = true;
            this.LVUser.GridLines = true;
            this.LVUser.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.LVUser.HideSelection = false;
            this.LVUser.Location = new System.Drawing.Point(0, 140);
            this.LVUser.Name = "LVUser";
            this.LVUser.Size = new System.Drawing.Size(294, 584);
            this.LVUser.TabIndex = 0;
            this.LVUser.UseCompatibleStateImageBehavior = false;
            this.LVUser.View = System.Windows.Forms.View.Details;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.cboStatus);
            this.panel4.Controls.Add(this.kryptonTextBox1);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.btnSearch);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.comboBox2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 30);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(294, 110);
            this.panel4.TabIndex = 204;
            // 
            // cboStatus
            // 
            this.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStatus.DropDownWidth = 119;
            this.cboStatus.Location = new System.Drawing.Point(95, 15);
            this.cboStatus.Name = "cboStatus";
            this.cboStatus.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.cboStatus.Size = new System.Drawing.Size(146, 22);
            this.cboStatus.StateCommon.ComboBox.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cboStatus.StateCommon.ComboBox.Border.Rounding = 4;
            this.cboStatus.StateCommon.ComboBox.Border.Width = 1;
            this.cboStatus.StateCommon.ComboBox.Content.Font = new System.Drawing.Font("Tahoma", 9F);
            this.cboStatus.StateCommon.Item.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cboStatus.StateCommon.Item.Border.Rounding = 4;
            this.cboStatus.StateCommon.Item.Border.Width = 1;
            this.cboStatus.StateCommon.Item.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cboStatus.TabIndex = 233;
            // 
            // kryptonTextBox1
            // 
            this.kryptonTextBox1.Location = new System.Drawing.Point(95, 43);
            this.kryptonTextBox1.Name = "kryptonTextBox1";
            this.kryptonTextBox1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonTextBox1.Size = new System.Drawing.Size(146, 26);
            this.kryptonTextBox1.StateCommon.Border.ColorAngle = 1F;
            this.kryptonTextBox1.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.kryptonTextBox1.StateCommon.Border.Rounding = 4;
            this.kryptonTextBox1.StateCommon.Border.Width = 1;
            this.kryptonTextBox1.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kryptonTextBox1.TabIndex = 229;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(7, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 16);
            this.label2.TabIndex = 147;
            this.label2.Text = "Tên tài khoản";
            // 
            // Panel_Left
            // 
            this.Panel_Left.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Left.Controls.Add(this.LVUser);
            this.Panel_Left.Controls.Add(this.panel4);
            this.Panel_Left.Controls.Add(this.kryptonHeader5);
            this.Panel_Left.Controls.Add(this.label6);
            this.Panel_Left.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Left.Location = new System.Drawing.Point(0, 42);
            this.Panel_Left.Name = "Panel_Left";
            this.Panel_Left.Size = new System.Drawing.Size(298, 726);
            this.Panel_Left.TabIndex = 233;
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            // 
            // TitleForm
            // 
            this.TitleForm.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.TitleForm.Dock = System.Windows.Forms.DockStyle.Top;
            this.TitleForm.Location = new System.Drawing.Point(0, 0);
            this.TitleForm.Name = "TitleForm";
            this.TitleForm.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.TitleForm.Size = new System.Drawing.Size(1024, 42);
            this.TitleForm.TabIndex = 232;
            this.TitleForm.Values.Description = "";
            this.TitleForm.Values.Heading = "Cài đặt tài khoản đổi tác";
            this.TitleForm.Values.Image = ((System.Drawing.Image)(resources.GetObject("TitleForm.Values.Image")));
            // 
            // TabControl
            // 
            this.TabControl.Controls.Add(this.tabPage1);
            this.TabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControl.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.TabControl.Location = new System.Drawing.Point(298, 183);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedIndex = 0;
            this.TabControl.Size = new System.Drawing.Size(726, 585);
            this.TabControl.TabIndex = 234;
            // 
            // kryptonHeader2
            // 
            this.kryptonHeader2.AutoSize = false;
            this.kryptonHeader2.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.buttonSpecAny1,
            this.buttonSpecAny3,
            this.buttonSpecAny2});
            this.kryptonHeader2.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader2.Location = new System.Drawing.Point(298, 42);
            this.kryptonHeader2.Name = "kryptonHeader2";
            this.kryptonHeader2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader2.Size = new System.Drawing.Size(726, 30);
            this.kryptonHeader2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader2.TabIndex = 235;
            this.kryptonHeader2.Values.Description = "";
            this.kryptonHeader2.Values.Heading = "Thông tin chi tiết tài khoản";
            // 
            // buttonSpecAny1
            // 
            this.buttonSpecAny1.Text = "Cập nhật";
            this.buttonSpecAny1.UniqueName = "1D9D00EB62594EA9ED9EAC1612B52709";
            // 
            // buttonSpecAny3
            // 
            this.buttonSpecAny3.Enabled = ComponentFactory.Krypton.Toolkit.ButtonEnabled.False;
            this.buttonSpecAny3.Text = "|";
            this.buttonSpecAny3.UniqueName = "057839015A394FA9158CC6B3B9AEDBBE";
            // 
            // buttonSpecAny2
            // 
            this.buttonSpecAny2.Text = "Xóa tài khoản";
            this.buttonSpecAny2.UniqueName = "D102D09DB69C47E986A245F9544B3A08";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.dte_ExpireDate);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.btn_ResetPassword);
            this.panel1.Controls.Add(this.txt_NewPassword);
            this.panel1.Controls.Add(this.txt_Password);
            this.panel1.Controls.Add(this.txt_UserName);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(298, 72);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(726, 111);
            this.panel1.TabIndex = 236;
            // 
            // dte_ExpireDate
            // 
            this.dte_ExpireDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dte_ExpireDate.CustomFormat = "dd/MM/yyyy";
            this.dte_ExpireDate.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.dte_ExpireDate.Location = new System.Drawing.Point(575, 10);
            this.dte_ExpireDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dte_ExpireDate.Name = "dte_ExpireDate";
            this.dte_ExpireDate.Size = new System.Drawing.Size(140, 26);
            this.dte_ExpireDate.TabIndex = 236;
            this.dte_ExpireDate.Value = new System.DateTime(((long)(0)));
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.rdo_Disable);
            this.groupBox1.Controls.Add(this.rdo_Enable);
            this.groupBox1.Location = new System.Drawing.Point(575, 30);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(140, 64);
            this.groupBox1.TabIndex = 237;
            this.groupBox1.TabStop = false;
            // 
            // rdo_Disable
            // 
            this.rdo_Disable.AutoSize = true;
            this.rdo_Disable.Font = new System.Drawing.Font("Tahoma", 9F);
            this.rdo_Disable.ForeColor = System.Drawing.Color.Navy;
            this.rdo_Disable.Location = new System.Drawing.Point(7, 40);
            this.rdo_Disable.Name = "rdo_Disable";
            this.rdo_Disable.Size = new System.Drawing.Size(114, 18);
            this.rdo_Disable.TabIndex = 0;
            this.rdo_Disable.TabStop = true;
            this.rdo_Disable.Text = "Không kích hoạt";
            this.rdo_Disable.UseVisualStyleBackColor = true;
            // 
            // rdo_Enable
            // 
            this.rdo_Enable.AutoSize = true;
            this.rdo_Enable.Font = new System.Drawing.Font("Tahoma", 9F);
            this.rdo_Enable.ForeColor = System.Drawing.Color.Navy;
            this.rdo_Enable.Location = new System.Drawing.Point(7, 15);
            this.rdo_Enable.Name = "rdo_Enable";
            this.rdo_Enable.Size = new System.Drawing.Size(76, 18);
            this.rdo_Enable.TabIndex = 0;
            this.rdo_Enable.TabStop = true;
            this.rdo_Enable.Text = "Kích hoạt";
            this.rdo_Enable.UseVisualStyleBackColor = true;
            // 
            // btn_ResetPassword
            // 
            this.btn_ResetPassword.Location = new System.Drawing.Point(391, 61);
            this.btn_ResetPassword.Name = "btn_ResetPassword";
            this.btn_ResetPassword.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_ResetPassword.Size = new System.Drawing.Size(110, 40);
            this.btn_ResetPassword.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_ResetPassword.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ResetPassword.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_ResetPassword.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ResetPassword.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ResetPassword.TabIndex = 235;
            this.btn_ResetPassword.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_ResetPassword.Values.Image")));
            this.btn_ResetPassword.Values.Text = "Cấp lại \r\nmật khẩu";
            // 
            // txt_NewPassword
            // 
            this.txt_NewPassword.Location = new System.Drawing.Point(149, 75);
            this.txt_NewPassword.Name = "txt_NewPassword";
            this.txt_NewPassword.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_NewPassword.ReadOnly = true;
            this.txt_NewPassword.Size = new System.Drawing.Size(236, 25);
            this.txt_NewPassword.StateCommon.Border.ColorAngle = 1F;
            this.txt_NewPassword.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_NewPassword.StateCommon.Border.Rounding = 4;
            this.txt_NewPassword.StateCommon.Border.Width = 1;
            this.txt_NewPassword.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.txt_NewPassword.TabIndex = 232;
            // 
            // txt_Password
            // 
            this.txt_Password.Location = new System.Drawing.Point(149, 43);
            this.txt_Password.Name = "txt_Password";
            this.txt_Password.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Password.PasswordChar = '*';
            this.txt_Password.ReadOnly = true;
            this.txt_Password.Size = new System.Drawing.Size(236, 25);
            this.txt_Password.StateCommon.Border.ColorAngle = 1F;
            this.txt_Password.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Password.StateCommon.Border.Rounding = 4;
            this.txt_Password.StateCommon.Border.Width = 1;
            this.txt_Password.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.txt_Password.TabIndex = 233;
            // 
            // txt_UserName
            // 
            this.txt_UserName.Location = new System.Drawing.Point(149, 11);
            this.txt_UserName.Name = "txt_UserName";
            this.txt_UserName.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_UserName.Size = new System.Drawing.Size(236, 25);
            this.txt_UserName.StateCommon.Border.ColorAngle = 1F;
            this.txt_UserName.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_UserName.StateCommon.Border.Rounding = 4;
            this.txt_UserName.StateCommon.Border.Width = 1;
            this.txt_UserName.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.txt_UserName.TabIndex = 234;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(87, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 16);
            this.label5.TabIndex = 230;
            this.label5.Text = "Mật khẩu";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(19, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 16);
            this.label1.TabIndex = 231;
            this.label1.Text = "Tài khoản đăng nhập";
            // 
            // comboBox1
            // 
            this.comboBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.comboBox1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.comboBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(1368, 232);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(234, 23);
            this.comboBox1.TabIndex = 6;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel2.Controls.Add(this.txt_SearchRole);
            this.panel2.Controls.Add(this.kryptonButton1);
            this.panel2.Controls.Add(this.comboBox3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 30);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(306, 49);
            this.panel2.TabIndex = 229;
            // 
            // txt_SearchRole
            // 
            this.txt_SearchRole.Location = new System.Drawing.Point(15, 11);
            this.txt_SearchRole.Name = "txt_SearchRole";
            this.txt_SearchRole.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_SearchRole.Size = new System.Drawing.Size(235, 26);
            this.txt_SearchRole.StateCommon.Border.ColorAngle = 1F;
            this.txt_SearchRole.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_SearchRole.StateCommon.Border.Rounding = 4;
            this.txt_SearchRole.StateCommon.Border.Width = 1;
            this.txt_SearchRole.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SearchRole.TabIndex = 229;
            // 
            // kryptonButton1
            // 
            this.kryptonButton1.Location = new System.Drawing.Point(263, 4);
            this.kryptonButton1.Name = "kryptonButton1";
            this.kryptonButton1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonButton1.Size = new System.Drawing.Size(40, 40);
            this.kryptonButton1.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.kryptonButton1.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonButton1.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.kryptonButton1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kryptonButton1.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonButton1.TabIndex = 150;
            this.kryptonButton1.Values.Image = ((System.Drawing.Image)(resources.GetObject("kryptonButton1.Values.Image")));
            this.kryptonButton1.Values.Text = "";
            // 
            // comboBox3
            // 
            this.comboBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.comboBox3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.comboBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(1368, 232);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(234, 23);
            this.comboBox3.TabIndex = 6;
            // 
            // Frm_UserSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.TabControl);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.kryptonHeader2);
            this.Controls.Add(this.Panel_Left);
            this.Controls.Add(this.TitleForm);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_UserSetting";
            this.Text = "Frm_UserSetting";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.PanelAction.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboStatus)).EndInit();
            this.Panel_Left.ResumeLayout(false);
            this.TabControl.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView LVUserRole;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader4;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListView LVPartnerRole;
        private System.Windows.Forms.Panel PanelAction;
        private System.Windows.Forms.TabPage tabPage1;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader5;
        private System.Windows.Forms.Label label6;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnSearch;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ListView LVUser;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel Panel_Left;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader TitleForm;
        private System.Windows.Forms.TabControl TabControl;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader2;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnAddUser;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox kryptonTextBox1;
        private System.Windows.Forms.Label label2;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny buttonSpecAny1;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny buttonSpecAny3;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny buttonSpecAny2;
        private System.Windows.Forms.Panel panel1;
        private TN_Tools.TNDateTime dte_ExpireDate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdo_Disable;
        private System.Windows.Forms.RadioButton rdo_Enable;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_ResetPassword;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_NewPassword;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Password;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_UserName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cboStatus;
        private System.Windows.Forms.Panel panel2;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_SearchRole;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton1;
        private System.Windows.Forms.ComboBox comboBox3;
    }
}