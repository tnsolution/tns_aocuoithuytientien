﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TN_Connection;
using TN_Tools;
using TN_User;

namespace TN_WinApp.HRM
{
    public partial class Frm_EmployeeList : Form
    {
        string _EmployeeKey = "";
        private string _FormUrl = "/HRM/Frm_EmployeeList";

        public string FormUrl
        {
            get
            {
                return _FormUrl;
            }

            set
            {
                _FormUrl = value;
            }
        }
        public string EmployeeKey
        {
            get
            {
                return _EmployeeKey;
            }

            set
            {
                _EmployeeKey = value;
            }
        }

        public Frm_EmployeeList()
        {
            InitializeComponent();

            TitleForm.MouseDown += Frm_MouseDown;
            TitleForm.MouseMove += Frm_MouseMove;
            TitleForm.MouseUp += Frm_MouseUp;

            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;

            btnSearch.Click += BtnSearch_Click;
            btnSave.Click += BtnSave_Click;
            btnAddNew.Click += BtnAddNew_Click;
            btnDelete.Click += BtnDelete_Click;

            LVData.Click += LVData_Click;
            LVData.KeyDown += LVData_KeyDown;

            //format Form
            this.Text = "Danh sách nhân viên";
            this.StartPosition = FormStartPosition.CenterScreen;
            this.FormBorderStyle = FormBorderStyle.None;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.Load += Frm_Employee_Load;
        }

        private void Frm_Employee_Load(object sender, EventArgs e)
        {
            Get_Auth();
            if (_RoleForm.RoleRead)
            {
                DesignLayout(LVData);
                LoadListView();

                string zSQLDepartment = @"SELECT OrganizationKey, OrganizationName FROM HRM_Organization WHERE RecordStatus <> 99 AND PartnerNumber = '" + SessionUser.UserLogin.PartnerNumber + "'";
                string zSQLJob = @"SELECT JobKey, JobNameVN FROM HRM_ListJob WHERE RecordStatus <> 99 AND PartnerNumber = '" + SessionUser.UserLogin.PartnerNumber + "'";
                string zSQLStatus = @"SELECT StatusKey, StatusNameVN FROM HRM_ListWorkingStatus";

                LoadDataToToolbox.KryptonComboBox(cboJob, zSQLJob, "--Chọn--");
                LoadDataToToolbox.KryptonComboBox(cboDepartment, zSQLDepartment, "--Chọn--");
                LoadDataToToolbox.KryptonComboBox(cboWorkingStatus, zSQLStatus, "--Chọn--");
            }
        }

        #region[Event]
        private void BtnSearch_Click(object sender, EventArgs e)
        {
            LoadListView();
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            string zResult = "";
            string zMessage = "";
            zMessage = CheckBeforSave();
            if (zMessage.Length == 0)
            {
                zResult = Save();
                if (zResult.Substring(0, 3) == "200" ||
                    zResult.Substring(0, 3) == "201")
                {
                    MessageBox.Show("Cập nhật nhân viên thành công!");
                    LoadListView();
                    LoadData();

                }
                else
                {
                    MessageBox.Show("Lỗi.Vui lòng liên hệ IT.Chi tiết lỗi mã : \n " + zResult);
                }
            }
            else
            {
                MessageBox.Show(zMessage);
            }
        }
        private void BtnAddNew_Click(object sender, EventArgs e)
        {
            ClearForm();
        }
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dlr == DialogResult.Yes)
            {
                string zResult = Delete();

                if (zResult.Substring(0, 3) == "200" ||
                    zResult.Substring(0, 3) == "201")
                {
                    MessageBox.Show("Đã xóa thành công !");
                    LoadListView();
                    ClearForm();
                }
                else
                {
                    MessageBox.Show("Lỗi.Vui lòng liên hệ IT.Chi tiết lỗi mã: \n " + zResult);
                }
            }
        }
        #endregion

        #region[Progess]
        private void ClearForm()
        {
            EmployeeKey = "";
            LVData.SelectedItems.Clear();
            txtAddress.Clear();
            txtCompanyPhone.Clear();
            txtEmployeeID.Clear();
            txtLastName.Clear();
            txtFirstName.Clear();
            txtNote.Clear();
            txtPassport.Clear();
            txtIssuePlace.Clear();
            dteBirthDay.Text = "";
            dteLeaveDate.Text = "";
            dteExpireDate.Text = "";
            dteStartDate.Text = "";

            cboDepartment.SelectedIndex = 0;
            cboJob.SelectedIndex = 0;
        }
        private void LoadData()
        {
            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            txtEmployeeID.Text = zInfo.EmployeeID;
            txtLastName.Text = zInfo.LastName;
            txtFirstName.Text = zInfo.FirstName;

            if (zInfo.IssueDate == null)
            {
                dteIssueDate.Value = DateTime.MinValue;
            }
            else
            {
                dteIssueDate.Value = zInfo.IssueDate.Value;
            }

            if (zInfo.ExpireDate == null)
            {
                dteExpireDate.Value = DateTime.MinValue;
            }
            else
            {
                dteExpireDate.Value = zInfo.ExpireDate.Value;
            }

            if (zInfo.Birthday == null)
            {
                dteBirthDay.Value = DateTime.MinValue;
            }
            else
            {
                dteBirthDay.Value = zInfo.Birthday.Value;
            }

            if (zInfo.Gender == 0)
            {
                radioFemale.Checked = true;
            }
            else
            {
                radioMale.Checked = true;
            }

            if (zInfo.StartingDate == null)
            {
                dteStartDate.Value = DateTime.MinValue;
            }
            else
            {
                dteStartDate.Value = zInfo.StartingDate.Value;
            }

            if (zInfo.LeavingDate == null)
            {
                dteLeaveDate.Value = DateTime.MinValue;
            }
            else
            {
                dteLeaveDate.Value = zInfo.LeavingDate.Value;
            }

            cboWorkingStatus.SelectedValue = zInfo.WorkingStatusKey;
            cboDepartment.SelectedValue = zInfo.OrganizationKey;
            cboJob.SelectedValue = zInfo.JobKey;

            txtAddress.Text = zInfo.AddressContact;
            txtCompanyPhone.Text = zInfo.CompanyPhone;
            txtNote.Text = zInfo.Note;
            //--
            txtPassport.Text = zInfo.PassportNumber;
            txtIssuePlace.Text = zInfo.IssuePlace;
        }
        private string CheckBeforSave()
        {
            string zResult = "";
            if (txtEmployeeID.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng điền mã nhân viên! \n";
                txtEmployeeID.StateCommon.Back.Color1 = Color.FromArgb(255, 102, 102);
            }
            else
            {
                txtEmployeeID.StateCommon.Back.Color1 = Color.White;
            }
            if (EmployeeKey == "")
            {
                bool Count = Access_Data.CheckEmployeeID(SessionUser.UserLogin.PartnerNumber, txtEmployeeID.Text.Trim());
                if (!Count)
                {
                    zResult += "Vui lòng chọn mã nhân viên khác! \n";
                    txtEmployeeID.StateCommon.Back.Color1 = Color.FromArgb(255, 102, 102);
                }
            }
            else
            {
                txtEmployeeID.StateCommon.Back.Color1 = Color.White;
            }
            if (txtLastName.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng điền họ và tên lót! \n";
                txtLastName.StateCommon.Back.Color1 = Color.FromArgb(255, 102, 102);
            }
            else
            {
                txtFirstName.StateCommon.Back.Color1 = Color.White;
            }
            if (txtFirstName.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng điền tên nhân viên! \n";
                txtFirstName.StateCommon.Back.Color1 = Color.FromArgb(255, 102, 102);
            }
            else
            {
                txtFirstName.StateCommon.Back.Color1 = Color.White;
            }
            if (txtPassport.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng điền CMND/Passport! \n";
                txtPassport.StateCommon.Back.Color1 = Color.FromArgb(255, 102, 102);
            }
            else
            {
                txtPassport.StateCommon.Back.Color1 = Color.White;
            }
            if (cboDepartment.SelectedValue.ToString() == "0")
            {
                zResult += "Vui lòng chọn cửa hàng! \n";
                cboDepartment.StateCommon.ComboBox.Back.Color1 = Color.FromArgb(255, 102, 102);
            }
            else
            {
                cboDepartment.StateCommon.ComboBox.Back.Color1 = Color.White;
            }
            return zResult;
        }
        private string Save()
        {
            Employee_Info zInfo;
            if (EmployeeKey != "")
            {
                zInfo = new Employee_Info(EmployeeKey);
            }
            else
            {
                zInfo = new Employee_Info();
            }
            zInfo.PartnerNumber = SessionUser.UserLogin.PartnerNumber;
            zInfo.JobKey = cboJob.SelectedValue.ToInt();
            zInfo.JobName = cboJob.Text;
            zInfo.OrganizationKey = cboDepartment.SelectedValue.ToInt();
            zInfo.OrganizationName = cboDepartment.Text;// (cboDepartment.SelectedItem);
            zInfo.WorkingStatusKey = cboWorkingStatus.SelectedValue.ToInt();
            zInfo.WorkingStatusName = cboWorkingStatus.Text;

            zInfo.EmployeeID = txtEmployeeID.Text.Trim().ToUpper();
            zInfo.LastName = txtLastName.Text.Trim();
            zInfo.FirstName = txtFirstName.Text.Trim();

            if (radioFemale.Checked == true)
            {
                zInfo.Gender = 0;
            }
            else
            {
                zInfo.Gender = 1;
            }

            if (dteBirthDay.Value == DateTime.MinValue)
            {
                zInfo.Birthday = null;
            }
            else
            {
                zInfo.Birthday = dteBirthDay.Value;
            }

            if (dteStartDate.Value == DateTime.MinValue)
            {
                zInfo.StartingDate = null;
            }
            else
            {
                zInfo.StartingDate = dteStartDate.Value;
            }

            if (dteLeaveDate.Value == DateTime.MinValue)
            {
                zInfo.LeavingDate = null;
            }
            else
            {
                zInfo.LeavingDate = dteLeaveDate.Value;
            }

            if (dteIssueDate.Value == DateTime.MinValue)
            {
                zInfo.IssueDate = null;
            }
            else
            {
                zInfo.IssueDate = dteIssueDate.Value;
            }

            if (dteExpireDate.Value == DateTime.MinValue)
            {
                zInfo.ExpireDate = null;
            }
            else
            {
                zInfo.ExpireDate = dteExpireDate.Value;
            }

            zInfo.AddressContact = txtAddress.Text.Trim();
            zInfo.CompanyPhone = txtCompanyPhone.Text.Trim();
            zInfo.Note = txtNote.Text.Trim();
            //--
            zInfo.PassportNumber = txtPassport.Text.Trim();
            zInfo.IssuePlace = txtIssuePlace.Text.Trim();

            if (EmployeeKey == "")
            {
                zInfo.Create_ClientKey();
                EmployeeKey = zInfo.EmployeeKey;
            }
            else
            {
                zInfo.Update();
                EmployeeKey = zInfo.EmployeeKey;
            }

            return zInfo.Message;
        }
        private string Delete()
        {
            Employee_Info zInfo = new Employee_Info();
            zInfo.EmployeeKey = EmployeeKey;
            zInfo.Delete();
            return zInfo.Message;
        }
        #endregion

        #region [ListView]
        private void DesignLayout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Nhân Viên";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Họ";
            colHead.Width = 140;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên";
            colHead.Width = 60;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            TNPaintControl.DrawLVStyle(ref LVData);
        }
        private void LoadListView()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable zTable = Access_Data.List(txtSearch.Text, SessionUser.UserLogin.PartnerNumber);
            LV.Items.Clear();
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow zRow = zTable.Rows[i];
                lvi.Tag = zRow["EmployeeKey"].ToString();
                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["EmployeeID"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["LastName"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["FirstName"].ToString();
                lvi.SubItems.Add(lvsi);


                LV.Items.Add(lvi);
            }
            this.Cursor = Cursors.Default;
        }
        private void LVData_Click(object sender, EventArgs e)
        {
            if (LVData.SelectedItems.Count > 0)
            {
                EmployeeKey = LVData.SelectedItems[0].Tag.ToString();
                LoadData();
            }
        }
        private void LVData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (_RoleForm.RoleDel)
                {
                    DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dlr == DialogResult.Yes && LVData.SelectedItems.Count > 0)
                    {
                        EmployeeKey = LVData.SelectedItems[0].Tag.ToString();
                        string zResult = Delete();

                        if (zResult.Substring(0, 3) == "200" ||
                            zResult.Substring(0, 3) == "201")
                        {
                            MessageBox.Show("Đã xóa thành công !");
                            LoadListView();
                            ClearForm();
                        }
                        else
                        {
                            MessageBox.Show("Lỗi.Vui lòng liên hệ IT.Chi tiết lỗi mã: \n " + zResult);
                        }
                    }
                }
            }
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;



        private void Frm_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }
        private void Frm_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }
        private void Frm_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.None;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [Auth]
        User_Role_Info _RoleForm = new User_Role_Info();
        private void Get_Auth()
        {
            List<User_Role_Info> zListRole = SessionUser.TableRole;
            if (zListRole.Count > 0)
            {
                try
                {
                    _RoleForm = zListRole.SingleOrDefault(r => r.RoleURL == FormUrl);
                    if (_RoleForm == null)
                    {
                        _RoleForm = new User_Role_Info();
                        MessageBox.Show("Form này chưa được thêm quyền, vui lòng liên hệ IT !.");
                        this.Close();
                    }
                    else
                    {
                        if (!_RoleForm.RoleRead)
                        {
                            MessageBox.Show("Bạn không có quyền xem, vui lòng liên hệ IT !.");
                            this.Close();
                        }
                        if (!_RoleForm.RoleEdit)
                        {
                            btnSave.Visible = false;
                        }
                        if (!_RoleForm.RoleAdd)
                        {
                            btnSave.Visible = false;
                        }
                        if (!_RoleForm.RoleDel)
                        {
                            btnDelete.Visible = false;
                        }
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Bạn chưa được phân quyền, vui lòng liên hệ IT !.");
            }
        }
        #endregion       

        #region [ Access Info-Data ]
        public class Employee_Info
        {
            #region [ Field Name ]
            private string _EmployeeKey = "";
            private string _EmployeeID = "";
            private string _LastName = "";
            private string _FirstName = "";
            private string _ReportToKey = "";
            private string _ReportToName = "";
            private int _OrganizationKey = 0;
            private string _OrganizationName = "";
            private string _OrganizationPath = "";
            private string _BranchKey = "";
            private string _BranchName = "";
            private int _JobKey = 0;
            private string _JobName = "";
            private int _PositionKey = 0;
            private string _PositionName = "";
            private int _WorkingStatusKey = 0;
            private string _WorkingStatusName = "";
            private DateTime? _StartingDate = null;
            private DateTime? _LeavingDate = null;
            private string _ContractTypeName = "";
            private int _ContractTypeKey = 0;
            private string _WorkingAddress = "";
            private string _WorkingLocation = "";
            private string _CompanyPhone = "";
            private string _CompanyEmail = "";
            private int _Gender = 0;
            private DateTime? _Birthday = null;
            private string _BirthPlace = "";
            private int _MaritalStatusKey = 0;
            private string _Nationality = "";
            private string _Ethnicity = "";
            private string _PassportNumber = "";
            private DateTime? _IssueDate = null;
            private DateTime? _ExpireDate = null;
            private string _IssuePlace = "";
            private string _AddressRegister = "";
            private string _CityRegister = "";
            private string _AddressContact = "";
            private string _CityContact = "";
            private string _MobiPhone = "";
            private string _Email = "";
            private Image _Photo;
            private string _PhotoPath = "";
            private string _TaxNumber = "";
            private string _BankAccount = "";
            private string _EmergencyContact = "";
            private string _BankName = "";
            private string _Note = "";
            private int _Slug = 0;
            private string _Style = "";
            private string _Class = "";
            private string _CodeLine = "";
            private string _PartnerNumber = "";
            private int _RecordStatus = 0;
            private string _CreatedBy = "";
            private string _CreatedName = "";
            private DateTime _CreatedOn;
            private string _ModifiedBy = "";
            private string _ModifiedName = "";
            private DateTime _ModifiedOn;
            private string _Message = "";
            #endregion

            #region [ Constructor Get Information ]
            public Employee_Info()
            {
                Guid zNewID = Guid.NewGuid();
                _EmployeeKey = zNewID.ToString();
            }
            public Employee_Info(string EmployeeKey)
            {
                string zSQL = "SELECT * FROM [dbo].[HRM_Employee] WHERE EmployeeKey = @EmployeeKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = new Guid(EmployeeKey);
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _EmployeeKey = zReader["EmployeeKey"].ToString();
                        _EmployeeID = zReader["EmployeeID"].ToString();
                        _LastName = zReader["LastName"].ToString();
                        _FirstName = zReader["FirstName"].ToString();
                        _ReportToKey = zReader["ReportToKey"].ToString();
                        _ReportToName = zReader["ReportToName"].ToString();
                        if (zReader["OrganizationKey"] != DBNull.Value)
                        {
                            _OrganizationKey = int.Parse(zReader["OrganizationKey"].ToString());
                        }

                        _OrganizationName = zReader["OrganizationName"].ToString();
                        _OrganizationPath = zReader["OrganizationPath"].ToString();
                        _BranchKey = zReader["BranchKey"].ToString();
                        _BranchName = zReader["BranchName"].ToString();
                        if (zReader["JobKey"] != DBNull.Value)
                        {
                            _JobKey = int.Parse(zReader["JobKey"].ToString());
                        }

                        _JobName = zReader["JobName"].ToString();
                        if (zReader["PositionKey"] != DBNull.Value)
                        {
                            _PositionKey = int.Parse(zReader["PositionKey"].ToString());
                        }

                        _PositionName = zReader["PositionName"].ToString();
                        if (zReader["WorkingStatusKey"] != DBNull.Value)
                        {
                            _WorkingStatusKey = int.Parse(zReader["WorkingStatusKey"].ToString());
                        }

                        _WorkingStatusName = zReader["WorkingStatusName"].ToString();
                        if (zReader["StartingDate"] != DBNull.Value)
                        {
                            _StartingDate = (DateTime)zReader["StartingDate"];
                        }

                        if (zReader["LeavingDate"] != DBNull.Value)
                        {
                            _LeavingDate = (DateTime)zReader["LeavingDate"];
                        }

                        _ContractTypeName = zReader["ContractTypeName"].ToString();
                        if (zReader["ContractTypeKey"] != DBNull.Value)
                        {
                            _ContractTypeKey = int.Parse(zReader["ContractTypeKey"].ToString());
                        }

                        _WorkingAddress = zReader["WorkingAddress"].ToString();
                        _WorkingLocation = zReader["WorkingLocation"].ToString();
                        _CompanyPhone = zReader["CompanyPhone"].ToString();
                        _CompanyEmail = zReader["CompanyEmail"].ToString();
                        if (zReader["Gender"] != DBNull.Value)
                        {
                            _Gender = int.Parse(zReader["Gender"].ToString());
                        }

                        if (zReader["Birthday"] != DBNull.Value)
                        {
                            _Birthday = (DateTime)zReader["Birthday"];
                        }

                        _BirthPlace = zReader["BirthPlace"].ToString();
                        if (zReader["MaritalStatusKey"] != DBNull.Value)
                        {
                            _MaritalStatusKey = int.Parse(zReader["MaritalStatusKey"].ToString());
                        }

                        _Nationality = zReader["Nationality"].ToString();
                        _Ethnicity = zReader["Ethnicity"].ToString();
                        _PassportNumber = zReader["PassportNumber"].ToString();
                        if (zReader["IssueDate"] != DBNull.Value)
                        {
                            _IssueDate = (DateTime)zReader["IssueDate"];
                        }

                        if (zReader["ExpireDate"] != DBNull.Value)
                        {
                            _ExpireDate = (DateTime)zReader["ExpireDate"];
                        }

                        _IssuePlace = zReader["IssuePlace"].ToString();
                        _EmergencyContact = zReader["EmergencyContact"].ToString();
                        _AddressRegister = zReader["AddressRegister"].ToString();
                        _CityRegister = zReader["CityRegister"].ToString();
                        _AddressContact = zReader["AddressContact"].ToString();
                        _CityContact = zReader["CityContact"].ToString();
                        _MobiPhone = zReader["MobiPhone"].ToString();
                        _Email = zReader["Email"].ToString();
                        _PhotoPath = zReader["PhotoPath"].ToString();
                        _TaxNumber = zReader["TaxNumber"].ToString();
                        _BankAccount = zReader["BankAccount"].ToString();
                        _BankName = zReader["BankName"].ToString();
                        _Note = zReader["Note"].ToString();
                        if (zReader["Slug"] != DBNull.Value)
                        {
                            _Slug = int.Parse(zReader["Slug"].ToString());
                        }

                        _Style = zReader["Style"].ToString();
                        _Class = zReader["Class"].ToString();
                        _CodeLine = zReader["CodeLine"].ToString();
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }
            public Employee_Info(string EmployeeKey, string PartnerNumber)
            {
                string zSQL = "SELECT * FROM [dbo].[HRM_Employee] WHERE EmployeeKey = @EmployeeKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = new Guid(EmployeeKey);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _EmployeeKey = zReader["EmployeeKey"].ToString();
                        _EmployeeID = zReader["EmployeeID"].ToString();
                        _LastName = zReader["LastName"].ToString();
                        _FirstName = zReader["FirstName"].ToString();
                        _ReportToKey = zReader["ReportToKey"].ToString();
                        _ReportToName = zReader["ReportToName"].ToString();
                        if (zReader["OrganizationKey"] != DBNull.Value)
                        {
                            _OrganizationKey = int.Parse(zReader["OrganizationKey"].ToString());
                        }

                        _OrganizationName = zReader["OrganizationName"].ToString();
                        _OrganizationPath = zReader["OrganizationPath"].ToString();
                        _BranchKey = zReader["BranchKey"].ToString();
                        _BranchName = zReader["BranchName"].ToString();
                        if (zReader["JobKey"] != DBNull.Value)
                        {
                            _JobKey = int.Parse(zReader["JobKey"].ToString());
                        }

                        _JobName = zReader["JobName"].ToString();
                        if (zReader["PositionKey"] != DBNull.Value)
                        {
                            _PositionKey = int.Parse(zReader["PositionKey"].ToString());
                        }

                        _PositionName = zReader["PositionName"].ToString();
                        if (zReader["WorkingStatusKey"] != DBNull.Value)
                        {
                            _WorkingStatusKey = int.Parse(zReader["WorkingStatusKey"].ToString());
                        }

                        _WorkingStatusName = zReader["WorkingStatusName"].ToString();
                        if (zReader["StartingDate"] != DBNull.Value)
                        {
                            _StartingDate = (DateTime)zReader["StartingDate"];
                        }

                        if (zReader["LeavingDate"] != DBNull.Value)
                        {
                            _LeavingDate = (DateTime)zReader["LeavingDate"];
                        }

                        _ContractTypeName = zReader["ContractTypeName"].ToString();
                        if (zReader["ContractTypeKey"] != DBNull.Value)
                        {
                            _ContractTypeKey = int.Parse(zReader["ContractTypeKey"].ToString());
                        }

                        _WorkingAddress = zReader["WorkingAddress"].ToString();
                        _WorkingLocation = zReader["WorkingLocation"].ToString();
                        _CompanyPhone = zReader["CompanyPhone"].ToString();
                        _CompanyEmail = zReader["CompanyEmail"].ToString();
                        if (zReader["Gender"] != DBNull.Value)
                        {
                            _Gender = int.Parse(zReader["Gender"].ToString());
                        }

                        if (zReader["Birthday"] != DBNull.Value)
                        {
                            _Birthday = (DateTime)zReader["Birthday"];
                        }

                        _BirthPlace = zReader["BirthPlace"].ToString();
                        if (zReader["MaritalStatusKey"] != DBNull.Value)
                        {
                            _MaritalStatusKey = int.Parse(zReader["MaritalStatusKey"].ToString());
                        }

                        _Nationality = zReader["Nationality"].ToString();
                        _Ethnicity = zReader["Ethnicity"].ToString();
                        _PassportNumber = zReader["PassportNumber"].ToString();
                        if (zReader["IssueDate"] != DBNull.Value)
                        {
                            _IssueDate = (DateTime)zReader["IssueDate"];
                        }

                        if (zReader["ExpireDate"] != DBNull.Value)
                        {
                            _ExpireDate = (DateTime)zReader["ExpireDate"];
                        }

                        _IssuePlace = zReader["IssuePlace"].ToString();
                        _EmergencyContact = zReader["EmergencyContact"].ToString();
                        _AddressRegister = zReader["AddressRegister"].ToString();
                        _CityRegister = zReader["CityRegister"].ToString();
                        _AddressContact = zReader["AddressContact"].ToString();
                        _CityContact = zReader["CityContact"].ToString();
                        _MobiPhone = zReader["MobiPhone"].ToString();
                        _Email = zReader["Email"].ToString();
                        _PhotoPath = zReader["PhotoPath"].ToString();
                        _TaxNumber = zReader["TaxNumber"].ToString();
                        _BankAccount = zReader["BankAccount"].ToString();
                        _BankName = zReader["BankName"].ToString();
                        _Note = zReader["Note"].ToString();
                        if (zReader["Slug"] != DBNull.Value)
                        {
                            _Slug = int.Parse(zReader["Slug"].ToString());
                        }

                        _Style = zReader["Style"].ToString();
                        _Class = zReader["Class"].ToString();
                        _CodeLine = zReader["CodeLine"].ToString();
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }

            #endregion

            #region [ Properties ]
            public string EmployeeKey
            {
                get { return _EmployeeKey; }
                set { _EmployeeKey = value; }
            }
            public string EmployeeID
            {
                get { return _EmployeeID; }
                set { _EmployeeID = value; }
            }
            public string LastName
            {
                get { return _LastName; }
                set { _LastName = value; }
            }
            public string FirstName
            {
                get { return _FirstName; }
                set { _FirstName = value; }
            }
            public string ReportToKey
            {
                get { return _ReportToKey; }
                set { _ReportToKey = value; }
            }
            public string ReportToName
            {
                get { return _ReportToName; }
                set { _ReportToName = value; }
            }
            public int OrganizationKey
            {
                get { return _OrganizationKey; }
                set { _OrganizationKey = value; }
            }
            public string OrganizationName
            {
                get { return _OrganizationName; }
                set { _OrganizationName = value; }
            }
            public string OrganizationPath
            {
                get { return _OrganizationPath; }
                set { _OrganizationPath = value; }
            }
            public string BranchKey
            {
                get { return _BranchKey; }
                set { _BranchKey = value; }
            }
            public string BranchName
            {
                get { return _BranchName; }
                set { _BranchName = value; }
            }
            public int JobKey
            {
                get { return _JobKey; }
                set { _JobKey = value; }
            }
            public string JobName
            {
                get { return _JobName; }
                set { _JobName = value; }
            }
            public int PositionKey
            {
                get { return _PositionKey; }
                set { _PositionKey = value; }
            }
            public string PositionName
            {
                get { return _PositionName; }
                set { _PositionName = value; }
            }
            public int WorkingStatusKey
            {
                get { return _WorkingStatusKey; }
                set { _WorkingStatusKey = value; }
            }
            public string WorkingStatusName
            {
                get { return _WorkingStatusName; }
                set { _WorkingStatusName = value; }
            }
            public DateTime? StartingDate
            {
                get { return _StartingDate; }
                set { _StartingDate = value; }
            }
            public DateTime? LeavingDate
            {
                get { return _LeavingDate; }
                set { _LeavingDate = value; }
            }
            public string ContractTypeName
            {
                get { return _ContractTypeName; }
                set { _ContractTypeName = value; }
            }
            public int ContractTypeKey
            {
                get { return _ContractTypeKey; }
                set { _ContractTypeKey = value; }
            }
            public string WorkingAddress
            {
                get { return _WorkingAddress; }
                set { _WorkingAddress = value; }
            }
            public string EmergencyContact
            {
                get { return _EmergencyContact; }
                set { _EmergencyContact = value; }
            }
            public string WorkingLocation
            {
                get { return _WorkingLocation; }
                set { _WorkingLocation = value; }
            }
            public string CompanyPhone
            {
                get { return _CompanyPhone; }
                set { _CompanyPhone = value; }
            }
            public string CompanyEmail
            {
                get { return _CompanyEmail; }
                set { _CompanyEmail = value; }
            }
            public int Gender
            {
                get { return _Gender; }
                set { _Gender = value; }
            }
            public DateTime? Birthday
            {
                get { return _Birthday; }
                set { _Birthday = value; }
            }
            public string BirthPlace
            {
                get { return _BirthPlace; }
                set { _BirthPlace = value; }
            }
            public int MaritalStatusKey
            {
                get { return _MaritalStatusKey; }
                set { _MaritalStatusKey = value; }
            }
            public string Nationality
            {
                get { return _Nationality; }
                set { _Nationality = value; }
            }
            public string Ethnicity
            {
                get { return _Ethnicity; }
                set { _Ethnicity = value; }
            }
            public string PassportNumber
            {
                get { return _PassportNumber; }
                set { _PassportNumber = value; }
            }
            public DateTime? IssueDate
            {
                get { return _IssueDate; }
                set { _IssueDate = value; }
            }
            public DateTime? ExpireDate
            {
                get { return _ExpireDate; }
                set { _ExpireDate = value; }
            }
            public string IssuePlace
            {
                get { return _IssuePlace; }
                set { _IssuePlace = value; }
            }
            public string AddressRegister
            {
                get { return _AddressRegister; }
                set { _AddressRegister = value; }
            }
            public string CityRegister
            {
                get { return _CityRegister; }
                set { _CityRegister = value; }
            }
            public string AddressContact
            {
                get { return _AddressContact; }
                set { _AddressContact = value; }
            }
            public string CityContact
            {
                get { return _CityContact; }
                set { _CityContact = value; }
            }
            public string MobiPhone
            {
                get { return _MobiPhone; }
                set { _MobiPhone = value; }
            }
            public string Email
            {
                get { return _Email; }
                set { _Email = value; }
            }
            public Image Photo
            {
                get { return _Photo; }
                set { _Photo = value; }
            }
            public string PhotoPath
            {
                get { return _PhotoPath; }
                set { _PhotoPath = value; }
            }
            public string TaxNumber
            {
                get { return _TaxNumber; }
                set { _TaxNumber = value; }
            }
            public string BankAccount
            {
                get { return _BankAccount; }
                set { _BankAccount = value; }
            }
            public string BankName
            {
                get { return _BankName; }
                set { _BankName = value; }
            }
            public string Note
            {
                get { return _Note; }
                set { _Note = value; }
            }
            public int Slug
            {
                get { return _Slug; }
                set { _Slug = value; }
            }
            public string Style
            {
                get { return _Style; }
                set { _Style = value; }
            }
            public string Class
            {
                get { return _Class; }
                set { _Class = value; }
            }
            public string CodeLine
            {
                get { return _CodeLine; }
                set { _CodeLine = value; }
            }
            public string PartnerNumber
            {
                get { return _PartnerNumber; }
                set { _PartnerNumber = value; }
            }
            public int RecordStatus
            {
                get { return _RecordStatus; }
                set { _RecordStatus = value; }
            }
            public string CreatedBy
            {
                get { return _CreatedBy; }
                set { _CreatedBy = value; }
            }
            public string CreatedName
            {
                get { return _CreatedName; }
                set { _CreatedName = value; }
            }
            public DateTime CreatedOn
            {
                get { return _CreatedOn; }
                set { _CreatedOn = value; }
            }
            public string ModifiedBy
            {
                get { return _ModifiedBy; }
                set { _ModifiedBy = value; }
            }
            public string ModifiedName
            {
                get { return _ModifiedName; }
                set { _ModifiedName = value; }
            }
            public DateTime ModifiedOn
            {
                get { return _ModifiedOn; }
                set { _ModifiedOn = value; }
            }
            public string Code
            {
                get
                {
                    if (_Message.Length >= 3)
                    {
                        return _Message.Substring(0, 3);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            public string Message
            {
                get { return _Message; }
                set { _Message = value; }
            }
            #endregion

            #region [ Constructor Update Information ]
            public string Create_ServerKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[HRM_Employee] ("
            + " EmployeeID ,LastName ,FirstName ,ReportToKey ,ReportToName ,OrganizationKey ,OrganizationName ,OrganizationPath,BranchKey,BranchName ,JobKey ,JobName ,PositionKey ,PositionName ,WorkingStatusKey ,WorkingStatusName ,StartingDate ,LeavingDate ,ContractTypeKey,ContractTypeName ,WorkingAddress ,WorkingLocation ,CompanyPhone ,CompanyEmail ,Gender ,Birthday ,BirthPlace ,MaritalStatusKey ,Nationality ,Ethnicity ,PassportNumber ,IssueDate ,ExpireDate ,IssuePlace ,AddressRegister ,CityRegister ,AddressContact ,CityContact ,MobiPhone ,Email  ,PhotoPath ,TaxNumber ,BankAccount ,BankName,EmergencyContact ,Note ,Slug ,Style ,Class ,CodeLine ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName )"
             + " VALUES ( "
             + "@EmployeeID ,@LastName ,@FirstName ,@ReportToKey ,@ReportToName ,@OrganizationKey ,@OrganizationName ,@OrganizationPath ,BranchKey,BranchName,@JobKey ,@JobName ,@PositionKey ,@PositionName ,@WorkingStatusKey ,@WorkingStatusName ,@StartingDate ,@LeavingDate ,@ContractTypeKey,@ContractTypeName ,@WorkingAddress ,@WorkingLocation ,@CompanyPhone ,@CompanyEmail ,@Gender ,@Birthday ,@BirthPlace ,@MaritalStatusKey ,@Nationality ,@Ethnicity ,@PassportNumber ,@IssueDate ,@ExpireDate ,@IssuePlace ,@AddressRegister ,@CityRegister ,@AddressContact ,@CityContact ,@MobiPhone ,@Email ,@PhotoPath ,@TaxNumber ,@BankAccount ,@BankName,@EmergencyContact ,@Note ,@Slug ,@Style ,@Class ,@CodeLine ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName )";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID;
                    zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName;
                    zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName;
                    if (_ReportToKey.Trim().Length == 0)
                    {
                        zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ReportToKey);
                    }

                    zCommand.Parameters.Add("@ReportToName", SqlDbType.NVarChar).Value = _ReportToName;
                    zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = _OrganizationKey;
                    zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = _OrganizationName;
                    zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = _OrganizationPath;
                    if (_BranchKey.Trim().Length == 0)
                    {
                        zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = _BranchKey;
                    }

                    zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = _BranchName;
                    zCommand.Parameters.Add("@JobKey", SqlDbType.Int).Value = _JobKey;
                    zCommand.Parameters.Add("@JobName", SqlDbType.NVarChar).Value = _JobName;
                    zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = _PositionKey;
                    zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = _PositionName;
                    zCommand.Parameters.Add("@WorkingStatusKey", SqlDbType.Int).Value = _WorkingStatusKey;
                    zCommand.Parameters.Add("@WorkingStatusName", SqlDbType.NVarChar).Value = _WorkingStatusName;
                    if (_StartingDate == null)
                    {
                        zCommand.Parameters.Add("@StartingDate", SqlDbType.Date).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@StartingDate", SqlDbType.Date).Value = _StartingDate;
                    }

                    if (_LeavingDate == null)
                    {
                        zCommand.Parameters.Add("@LeavingDate", SqlDbType.Date).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@LeavingDate", SqlDbType.Date).Value = _LeavingDate;
                    }

                    zCommand.Parameters.Add("@ContractTypeKey", SqlDbType.Int).Value = _ContractTypeKey;
                    zCommand.Parameters.Add("@ContractTypeName", SqlDbType.NVarChar).Value = _ContractTypeName;
                    zCommand.Parameters.Add("@WorkingAddress", SqlDbType.NVarChar).Value = _WorkingAddress;
                    zCommand.Parameters.Add("@WorkingLocation", SqlDbType.NVarChar).Value = _WorkingLocation;
                    zCommand.Parameters.Add("@CompanyPhone", SqlDbType.NVarChar).Value = _CompanyPhone;
                    zCommand.Parameters.Add("@CompanyEmail", SqlDbType.NVarChar).Value = _CompanyEmail;
                    zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = _Gender;
                    if (_Birthday == null)
                    {
                        zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = _Birthday;
                    }

                    zCommand.Parameters.Add("@BirthPlace", SqlDbType.NVarChar).Value = _BirthPlace;
                    zCommand.Parameters.Add("@MaritalStatusKey", SqlDbType.Int).Value = _MaritalStatusKey;
                    zCommand.Parameters.Add("@Nationality", SqlDbType.NVarChar).Value = _Nationality;
                    zCommand.Parameters.Add("@Ethnicity", SqlDbType.NVarChar).Value = _Ethnicity;
                    zCommand.Parameters.Add("@PassportNumber", SqlDbType.NVarChar).Value = _PassportNumber;
                    if (_IssueDate == null)
                    {
                        zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = _IssueDate;
                    }

                    if (_ExpireDate == null)
                    {
                        zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = _ExpireDate;
                    }

                    zCommand.Parameters.Add("@IssuePlace", SqlDbType.NVarChar).Value = _IssuePlace;
                    zCommand.Parameters.Add("@AddressRegister", SqlDbType.NVarChar).Value = _AddressRegister;
                    zCommand.Parameters.Add("@CityRegister", SqlDbType.NVarChar).Value = _CityRegister;
                    zCommand.Parameters.Add("@AddressContact", SqlDbType.NVarChar).Value = _AddressContact;
                    zCommand.Parameters.Add("@CityContact", SqlDbType.NVarChar).Value = _CityContact;
                    zCommand.Parameters.Add("@MobiPhone", SqlDbType.NVarChar).Value = _MobiPhone;
                    zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = _Email;
                    zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = _PhotoPath;
                    zCommand.Parameters.Add("@TaxNumber", SqlDbType.NVarChar).Value = _TaxNumber;
                    zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = _BankAccount;
                    zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = _BankName;
                    zCommand.Parameters.Add("@EmergencyContact", SqlDbType.NVarChar).Value = _EmergencyContact;
                    zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = _Note;
                    zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = _CodeLine;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Create_ClientKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[HRM_Employee] ("
            + " EmployeeKey ,EmployeeID ,LastName ,FirstName ,ReportToKey ,ReportToName ,OrganizationKey ,OrganizationName ,OrganizationPath ,JobKey ,JobName ,PositionKey ,PositionName ,WorkingStatusKey ,WorkingStatusName ,StartingDate ,LeavingDate ,ContractTypeKey,ContractTypeName ,WorkingAddress ,WorkingLocation ,CompanyPhone ,CompanyEmail ,Gender ,Birthday ,BirthPlace ,MaritalStatusKey ,Nationality ,Ethnicity ,PassportNumber ,IssueDate ,ExpireDate ,IssuePlace ,AddressRegister ,CityRegister ,AddressContact ,CityContact ,MobiPhone ,Email ,PhotoPath ,TaxNumber ,BankAccount ,BankName,EmergencyContact ,Note ,Slug ,Style ,Class ,CodeLine ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName )"
             + " VALUES ( "
             + "@EmployeeKey ,@EmployeeID ,@LastName ,@FirstName ,@ReportToKey ,@ReportToName ,@OrganizationKey ,@OrganizationName ,@OrganizationPath ,@JobKey ,@JobName ,@PositionKey ,@PositionName ,@WorkingStatusKey ,@WorkingStatusName ,@StartingDate ,@LeavingDate ,@ContractTypeKey,@ContractTypeName ,@WorkingAddress ,@WorkingLocation ,@CompanyPhone ,@CompanyEmail ,@Gender ,@Birthday ,@BirthPlace ,@MaritalStatusKey ,@Nationality ,@Ethnicity ,@PassportNumber ,@IssueDate ,@ExpireDate ,@IssuePlace ,@AddressRegister ,@CityRegister ,@AddressContact ,@CityContact ,@MobiPhone ,@Email ,@PhotoPath ,@TaxNumber ,@BankAccount ,@BankName,@EmergencyContact ,@Note ,@Slug ,@Style ,@Class ,@CodeLine ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName )";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = new Guid(_EmployeeKey);
                    zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID;
                    zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName;
                    zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName;
                    if (_ReportToKey.Trim().Length == 0)
                    {
                        zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ReportToKey);
                    }

                    zCommand.Parameters.Add("@ReportToName", SqlDbType.NVarChar).Value = _ReportToName;
                    zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = _OrganizationKey;
                    zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = _OrganizationName;
                    zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = _OrganizationPath;
                    if (_BranchKey.Trim().Length == 0)
                    {
                        zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = _BranchKey;
                    }

                    zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = _BranchName;
                    zCommand.Parameters.Add("@JobKey", SqlDbType.Int).Value = _JobKey;
                    zCommand.Parameters.Add("@JobName", SqlDbType.NVarChar).Value = _JobName;
                    zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = _PositionKey;
                    zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = _PositionName;
                    zCommand.Parameters.Add("@WorkingStatusKey", SqlDbType.Int).Value = _WorkingStatusKey;
                    zCommand.Parameters.Add("@WorkingStatusName", SqlDbType.NVarChar).Value = _WorkingStatusName;
                    if (_StartingDate == null)
                    {
                        zCommand.Parameters.Add("@StartingDate", SqlDbType.Date).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@StartingDate", SqlDbType.Date).Value = _StartingDate;
                    }

                    if (_LeavingDate == null)
                    {
                        zCommand.Parameters.Add("@LeavingDate", SqlDbType.Date).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@LeavingDate", SqlDbType.Date).Value = _LeavingDate;
                    }

                    zCommand.Parameters.Add("@ContractTypeKey", SqlDbType.Int).Value = _ContractTypeKey;
                    zCommand.Parameters.Add("@ContractTypeName", SqlDbType.NVarChar).Value = _ContractTypeName;
                    zCommand.Parameters.Add("@WorkingAddress", SqlDbType.NVarChar).Value = _WorkingAddress;
                    zCommand.Parameters.Add("@WorkingLocation", SqlDbType.NVarChar).Value = _WorkingLocation;
                    zCommand.Parameters.Add("@CompanyPhone", SqlDbType.NVarChar).Value = _CompanyPhone;
                    zCommand.Parameters.Add("@CompanyEmail", SqlDbType.NVarChar).Value = _CompanyEmail;
                    zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = _Gender;
                    if (_Birthday == null)
                    {
                        zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = _Birthday;
                    }

                    zCommand.Parameters.Add("@BirthPlace", SqlDbType.NVarChar).Value = _BirthPlace;
                    zCommand.Parameters.Add("@MaritalStatusKey", SqlDbType.Int).Value = _MaritalStatusKey;
                    zCommand.Parameters.Add("@Nationality", SqlDbType.NVarChar).Value = _Nationality;
                    zCommand.Parameters.Add("@Ethnicity", SqlDbType.NVarChar).Value = _Ethnicity;
                    zCommand.Parameters.Add("@PassportNumber", SqlDbType.NVarChar).Value = _PassportNumber;
                    if (_IssueDate == null)
                    {
                        zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = _IssueDate;
                    }

                    if (_ExpireDate == null)
                    {
                        zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = _ExpireDate;
                    }

                    zCommand.Parameters.Add("@IssuePlace", SqlDbType.NVarChar).Value = _IssuePlace;
                    zCommand.Parameters.Add("@AddressRegister", SqlDbType.NVarChar).Value = _AddressRegister;
                    zCommand.Parameters.Add("@CityRegister", SqlDbType.NVarChar).Value = _CityRegister;
                    zCommand.Parameters.Add("@AddressContact", SqlDbType.NVarChar).Value = _AddressContact;
                    zCommand.Parameters.Add("@CityContact", SqlDbType.NVarChar).Value = _CityContact;
                    zCommand.Parameters.Add("@MobiPhone", SqlDbType.NVarChar).Value = _MobiPhone;
                    zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = _Email;
                    zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = _PhotoPath;
                    zCommand.Parameters.Add("@TaxNumber", SqlDbType.NVarChar).Value = _TaxNumber;
                    zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = _BankAccount;
                    zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = _BankName;
                    zCommand.Parameters.Add("@EmergencyContact", SqlDbType.NVarChar).Value = _EmergencyContact;
                    zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = _Note;
                    zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = _CodeLine;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Update()
            {
                string zSQL = "UPDATE [dbo].[HRM_Employee] SET "
                            + " EmployeeID = @EmployeeID,"
                            + " LastName = @LastName,"
                            + " FirstName = @FirstName,"
                            + " ReportToKey = @ReportToKey,"
                            + " ReportToName = @ReportToName,"
                            + " OrganizationKey = @OrganizationKey,"
                            + " OrganizationName = @OrganizationName,"
                            + " OrganizationPath = @OrganizationPath,"
                            + " BranchKey = @BranchKey,"
                            + " BranchName = @BranchName,"
                            + " JobKey = @JobKey,"
                            + " JobName = @JobName,"
                            + " PositionKey = @PositionKey,"
                            + " PositionName = @PositionName,"
                            + " WorkingStatusKey = @WorkingStatusKey,"
                            + " WorkingStatusName = @WorkingStatusName,"
                            + " StartingDate = @StartingDate,"
                            + " LeavingDate = @LeavingDate,"
                            + " ContractTypeKey = @ContractTypeKey,"
                            + " ContractTypeName = @ContractTypeName,"
                            + " WorkingAddress = @WorkingAddress,"
                            + " WorkingLocation = @WorkingLocation,"
                            + " CompanyPhone = @CompanyPhone,"
                            + " CompanyEmail = @CompanyEmail,"
                            + " Gender = @Gender,"
                            + " Birthday = @Birthday,"
                            + " BirthPlace = @BirthPlace,"
                            + " MaritalStatusKey = @MaritalStatusKey,"
                            + " Nationality = @Nationality,"
                            + " Ethnicity = @Ethnicity,"
                            + " PassportNumber = @PassportNumber,"
                            + " IssueDate = @IssueDate,"
                            + " ExpireDate = @ExpireDate,"
                            + " IssuePlace = @IssuePlace,"
                            + " AddressRegister = @AddressRegister,"
                            + " CityRegister = @CityRegister,"
                            + " AddressContact = @AddressContact,"
                            + " CityContact = @CityContact,"
                            + " MobiPhone = @MobiPhone,"
                            + " Email = @Email,"
                            + " PhotoPath = @PhotoPath,"
                            + " TaxNumber = @TaxNumber,"
                            + " BankAccount = @BankAccount,"
                            + " BankName = @BankName,"
                            + " EmergencyContact = @EmergencyContact,"
                            + " Note = @Note,"
                            + " Slug = @Slug,"
                            + " Style = @Style,"
                            + " Class = @Class,"
                            + " CodeLine = @CodeLine,"
                            + " PartnerNumber = @PartnerNumber,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName,"
                            + " ModifiedOn = GetDate()"
                           + " WHERE EmployeeKey = @EmployeeKey";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = new Guid(_EmployeeKey);
                    zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID;
                    zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName;
                    zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName;
                    if (_ReportToKey.Trim().Length == 0)
                    {
                        zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ReportToKey);
                    }

                    zCommand.Parameters.Add("@ReportToName", SqlDbType.NVarChar).Value = _ReportToName;
                    zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = _OrganizationKey;
                    zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = _OrganizationName;
                    zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = _OrganizationPath;
                    if (_BranchKey.Trim().Length == 0)
                    {
                        zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = _BranchKey;
                    }

                    zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = _BranchName;
                    zCommand.Parameters.Add("@JobKey", SqlDbType.Int).Value = _JobKey;
                    zCommand.Parameters.Add("@JobName", SqlDbType.NVarChar).Value = _JobName;
                    zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = _PositionKey;
                    zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = _PositionName;
                    zCommand.Parameters.Add("@WorkingStatusKey", SqlDbType.Int).Value = _WorkingStatusKey;
                    zCommand.Parameters.Add("@WorkingStatusName", SqlDbType.NVarChar).Value = _WorkingStatusName;
                    if (_StartingDate == null)
                    {
                        zCommand.Parameters.Add("@StartingDate", SqlDbType.Date).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@StartingDate", SqlDbType.Date).Value = _StartingDate;
                    }

                    if (_LeavingDate == null)
                    {
                        zCommand.Parameters.Add("@LeavingDate", SqlDbType.Date).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@LeavingDate", SqlDbType.Date).Value = _LeavingDate;
                    }

                    zCommand.Parameters.Add("@ContractTypeKey", SqlDbType.Int).Value = _ContractTypeKey;
                    zCommand.Parameters.Add("@ContractTypeName", SqlDbType.NVarChar).Value = _ContractTypeName;
                    zCommand.Parameters.Add("@WorkingAddress", SqlDbType.NVarChar).Value = _WorkingAddress;
                    zCommand.Parameters.Add("@WorkingLocation", SqlDbType.NVarChar).Value = _WorkingLocation;
                    zCommand.Parameters.Add("@CompanyPhone", SqlDbType.NVarChar).Value = _CompanyPhone;
                    zCommand.Parameters.Add("@CompanyEmail", SqlDbType.NVarChar).Value = _CompanyEmail;
                    zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = _Gender;
                    if (_Birthday == null)
                    {
                        zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = _Birthday;
                    }

                    zCommand.Parameters.Add("@BirthPlace", SqlDbType.NVarChar).Value = _BirthPlace;
                    zCommand.Parameters.Add("@MaritalStatusKey", SqlDbType.Int).Value = _MaritalStatusKey;
                    zCommand.Parameters.Add("@Nationality", SqlDbType.NVarChar).Value = _Nationality;
                    zCommand.Parameters.Add("@Ethnicity", SqlDbType.NVarChar).Value = _Ethnicity;
                    zCommand.Parameters.Add("@PassportNumber", SqlDbType.NVarChar).Value = _PassportNumber;
                    if (_IssueDate == null)
                    {
                        zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = _IssueDate;
                    }

                    if (_ExpireDate == null)
                    {
                        zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = _ExpireDate;
                    }

                    zCommand.Parameters.Add("@IssuePlace", SqlDbType.NVarChar).Value = _IssuePlace;
                    zCommand.Parameters.Add("@AddressRegister", SqlDbType.NVarChar).Value = _AddressRegister;
                    zCommand.Parameters.Add("@CityRegister", SqlDbType.NVarChar).Value = _CityRegister;
                    zCommand.Parameters.Add("@AddressContact", SqlDbType.NVarChar).Value = _AddressContact;
                    zCommand.Parameters.Add("@CityContact", SqlDbType.NVarChar).Value = _CityContact;
                    zCommand.Parameters.Add("@MobiPhone", SqlDbType.NVarChar).Value = _MobiPhone;
                    zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = _Email;
                    zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = _PhotoPath;
                    zCommand.Parameters.Add("@TaxNumber", SqlDbType.NVarChar).Value = _TaxNumber;
                    zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = _BankAccount;
                    zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = _BankName;
                    zCommand.Parameters.Add("@EmergencyContact", SqlDbType.NVarChar).Value = _EmergencyContact;
                    zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = _Note;
                    zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = _CodeLine;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Delete()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[HRM_Employee] Set RecordStatus = 99 WHERE EmployeeKey = @EmployeeKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = new Guid(_EmployeeKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[HRM_Employee] WHERE EmployeeKey = @EmployeeKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = new Guid(_EmployeeKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            #endregion
        }
        public class Access_Data
        {
            #region[Standard]
            public static DataTable List(string Name, string PartnerNumber)
            {
                DataTable zTable = new DataTable();
                string zSQL = @"
                SELECT EmployeeKey,EmployeeID,LastName ,FirstName FROM [dbo].[HRM_Employee] 
                WHERE RecordStatus <> 99 ";
                if (Name.Trim().Length > 0)
                {
                    zSQL += " AND(LastName + ' ' + FirstName) LIKE @Name";
                }

                zSQL += " AND PartnerNumber= @PartnerNumber   ORDER BY  FirstName";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string Result = ex.ToString();
                }
                return zTable;
            }
            #endregion
            public static bool CheckEmployeeID(string PartnerNumber, string EmployeeID)
            {
                bool zResult = false;  //  Khong co
                DataTable zTable = new DataTable();
                string zSQL = @"SELECT Count(*) AS Amount FROM [dbo].[HRM_Employee] WHERE PartnerNumber = @PartnerNumber AND EmployeeID = @EmployeeID AND RecordStatus != 99";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = EmployeeID;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string Result = ex.ToString();
                }
                if (zTable.Rows.Count > 0)
                {
                    int zAmount = 0;
                    DataRow zRow = zTable.Rows[0];
                    zAmount = int.Parse(zRow["Amount"].ToString());
                    if (zAmount == 0)
                    {
                        zResult = true;
                    }
                    else
                    {
                        zResult = false;
                    }
                }
                return zResult;
            }
        }
        #endregion
    }
}