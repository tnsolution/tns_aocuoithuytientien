﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TN_Tools;
using TN_User;

namespace TN_WinApp.Auth
{
    public partial class Frm_UserList : Form
    {
        private string _UserKey = "";
        private string _FormUrl = "/Auth/Frm_UserList";

        public string FormUrl
        {
            get
            {
                return _FormUrl;
            }

            set
            {
                _FormUrl = value;
            }
        }
        public string UserKey
        {
            get
            {
                return _UserKey;
            }

            set
            {
                _UserKey = value;
            }
        }

        public Frm_UserList()
        {
            InitializeComponent();

            #region [Event Form]
            TitleForm.MouseDown += Frm_MouseDown;
            TitleForm.MouseMove += Frm_MouseMove;
            TitleForm.MouseUp += Frm_MouseUp;

            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;

            btnNew.Click += BtnNew_Click;
            btnResetPassword.Click += BtnResetPassword_Click;
            btnSearch.Click += BtnSearch_Click;
            btnSave.Click += BtnSave_Click;
            btnDel.Click += BtnDel_Click;

            GVRoleForm.CellContentClick += GVRoleForm_CellContentClick;
            GVRoleForm.CellValueChanged += GVRoleForm_CellValueChanged;

            LVData.Click += LVData_Click;
            #endregion

            //format Form
            Panel_Info.Enabled = false;
            this.Text = "Danh sách người dùng";
            this.StartPosition = FormStartPosition.CenterScreen;
            this.FormBorderStyle = FormBorderStyle.None;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.Load += Frm_UserList_Load;
        }

        private void Frm_UserList_Load(object sender, EventArgs e)
        {
            Get_Auth();
            if (_RoleForm.RoleRead)
            {
                DesignLayout_GVRoleForm(GVRoleForm);
                LoadCombobox();
                DesignLayout(LVData);
                LoadListView();
            }
        }

        #region [GridView]        
        private void DesignLayout_GVRoleForm(DataGridView GV)
        {
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("RoleID", "Mã");
            GV.Columns.Add("RoleName", "Tên giao diện form");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["RoleID"].Width = 80;
            GV.Columns["RoleID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["RoleName"].Width = 200;
            GV.Columns["RoleName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            DataGridViewCheckBoxColumn zColumn;

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "All";
            zColumn.HeaderText = "Tất cả";
            GV.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Read";
            zColumn.HeaderText = "Xem";
            GV.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Add";
            zColumn.HeaderText = "Thêm";
            GV.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Edit";
            zColumn.HeaderText = "Sửa";
            GV.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Delete";
            zColumn.HeaderText = "Xóa";
            GV.Columns.Add(zColumn);

            GV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = false;

            TNPaintControl.DrawGVStyle(ref GV);
            GV.Rows.Clear();
        }
        private void LoadData_GVRoleForm(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();

            int i = 0;
            foreach (DataRow zRow in Table.Rows)
            {
                GV.Rows.Add();
                DataGridViewRow zRowView = GV.Rows[i];
                zRowView.Cells["No"].Value = (i + 1).ToString();
                zRowView.Cells["RoleID"].Tag = zRow["RoleKey"].ToString().Trim();
                zRowView.Cells["RoleID"].Value = zRow["RoleID"].ToString().Trim();
                zRowView.Cells["RoleName"].Value = zRow["RoleName"].ToString().Trim();

                if (zRow["RoleAll"].ToBool() == true)
                {
                    zRowView.Cells["All"].Value = true;
                }

                if (zRow["RoleRead"].ToBool() == true)
                {
                    zRowView.Cells["Read"].Value = true;
                }

                if (zRow["RoleAdd"].ToBool() == true)
                {
                    zRowView.Cells["Add"].Value = true;
                }

                if (zRow["RoleEdit"].ToBool() == true)
                {
                    zRowView.Cells["Edit"].Value = true;
                }

                if (zRow["RoleDel"].ToBool() == true)
                {
                    zRowView.Cells["Delete"].Value = true;
                }

                i++;
            }
        }
        private void GVRoleForm_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow zRowEdit = GVRoleForm.Rows[e.RowIndex];
            if (zRowEdit.Cells["RoleID"].Tag != null)
            {
                if (e.ColumnIndex == 3 && e.RowIndex != -1)
                {
                    if (Convert.ToBoolean(zRowEdit.Cells[e.ColumnIndex].Value) == true)
                    {
                        zRowEdit.Cells["Read"].Value = true;
                        zRowEdit.Cells["Add"].Value = true;
                        zRowEdit.Cells["Edit"].Value = true;
                        zRowEdit.Cells["Delete"].Value = true;
                    }
                    else
                    {
                        zRowEdit.Cells["Read"].Value = false;
                        zRowEdit.Cells["Add"].Value = false;
                        zRowEdit.Cells["Edit"].Value = false;
                        zRowEdit.Cells["Delete"].Value = false;
                    }
                }
            }
        }
        private void GVRoleForm_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            GVRoleForm.EndEdit(DataGridViewDataErrorContexts.Commit);
        }
        #endregion

        #region [ListView]
        private void DesignLayout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên người dùng";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            TNPaintControl.DrawLVStyle(ref LVData);
        }
        private void LoadListView()
        {
            string Error = "";
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable zTable = User_Data.List(SessionUser.UserLogin.PartnerNumber, txtSearch.Text.Trim(), out Error);
            LV.Items.Clear();
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow zRow = zTable.Rows[i];
                lvi.Tag = zRow["UserKey"].ToString();
                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["UserName"].ToString();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }
            this.Cursor = Cursors.Default;
            if (Error != string.Empty)
            {
                MessageBox.Show(Error);
            }
        }
        private void LVData_Click(object sender, EventArgs e)
        {
            if (LVData.SelectedItems.Count > 0)
            {
                UserKey = LVData.SelectedItems[0].Tag.ToString();
                LoadData();
                LoadRole();
            }
        }
        #endregion

        #region[Event Action]
        private void BtnNew_Click(object sender, EventArgs e)
        {
            ClearForm();
        }
        private void BtnDel_Click(object sender, EventArgs e)
        {
            DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dlr == DialogResult.Yes)
            {
                User_Info zInfo = new User_Info(UserKey);
                zInfo.Delete();
                if (zInfo.Message.Substring(0, 3) == "200")
                {
                    MessageBox.Show("Xóa thành công!");
                    LoadListView();
                    ClearForm();
                }
                else
                {
                    MessageBox.Show("Lỗi.Vui lòng liên hệ IT.Chi tiết lỗi mã: \n " + zInfo.Message);
                }
            }
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            string zResult = "";
            string zMessage = "";
            zMessage = CheckBeforSave();
            if (zMessage.Length == 0)
            {
                zResult = Save();
                if (zResult.Substring(0, 3) == "200")
                {
                    MessageBox.Show("Cập nhật người dùng thành công!");
                    SaveRole();
                    LoadListView();
                    LoadData();
                }
                else
                {
                    MessageBox.Show("Lỗi.Vui lòng liên hệ IT.Chi tiết lỗi mã : \n " + zResult);
                }
            }
            else
            {
                MessageBox.Show(zMessage);
            }
        }
        private void BtnSearch_Click(object sender, EventArgs e)
        {
            LoadListView();
        }
        private void BtnResetPassword_Click(object sender, EventArgs e)
        {
            txtNewPassword.Text = RandomGenerator.RandomPassword();

            User_Info zInfo = new User_Info(SessionUser.UserLogin.PartnerNumber, UserKey);
            zInfo.ResetPass(txtNewPassword.Text);

            if (zInfo.Message.Substring(0, 3) == "200")
            {
                MessageBox.Show("Đã đổi mật khẩu thành công !");
            }
            else
            {
                MessageBox.Show("Lỗi.Vui lòng liên hệ IT.Chi tiết lỗi mã: \n " + zInfo.Message);
            }
        }
        #endregion

        #region[Process]
        private void LoadCombobox()
        {
            string zSQLEmployee = @"SELECT EmployeeKey, LastName + ' ' + FirstName AS EmployeeName FROM HRM_Employee WHERE RecordStatus <> 99 AND PartnerNumber = '" + SessionUser.UserLogin.PartnerNumber + "'";
            LoadDataToToolbox.KryptonComboBox(cboEmployee, zSQLEmployee, "--Chọn--");
        }
        private void ClearForm()
        {
            UserKey = "";
            LVData.SelectedItems.Clear();
            txtPassword.Clear();
            txtUserName.Clear();
            dteExpireDay.Text = "";
            rdoDisable.Checked = false;
            rdoEnable.Checked = false;
            cboEmployee.SelectedIndex = 0;
        }
        private void LoadData()
        {
            Panel_Info.Enabled = true;

            User_Info zInfo = new User_Info(UserKey);
            txtUserName.Text = zInfo.Name;
            txtPassword.Text = zInfo.Password;
            dteExpireDay.Value = zInfo.ExpireDate;
            cboEmployee.SelectedValue = zInfo.EmployeeKey;

            if (zInfo.Activate)
            {
                rdoEnable.Checked = true;
            }
            else
            {
                rdoDisable.Checked = true;
            }
        }
        private string CheckBeforSave()
        {
            string zResult = "";
            if (UserKey == "")
            {
                bool Count = User_Data.CheckUserName(txtUserName.Text.Trim());
                if (!Count)
                {
                    zResult += "Vui lòng chọn tên người dùng khác ! \n";
                }
            }
            if (cboEmployee.SelectedValue.ToString() == "0")
            {
                zResult += "Vui lòng nhân viên sử dụng tài khoản phần mềm ! \n";
            }
            if (!rdoEnable.Checked && !rdoDisable.Checked)
            {
                zResult += "Vui lòng chọn trạng thái người dùng ! \n";
            }

            return zResult;
        }
        private string Save()
        {
            User_Info zInfo = new User_Info(UserKey);
            zInfo.Name = txtUserName.Text.Trim();

            HRM.Frm_EmployeeList.Employee_Info zEmployee = new HRM.Frm_EmployeeList.Employee_Info(cboEmployee.SelectedValue.ToString(), SessionUser.UserLogin.PartnerNumber);

            zInfo.EmployeeKey = zEmployee.EmployeeKey;
            zInfo.EmployeeID = zEmployee.EmployeeID;
            zInfo.EmployeeName = zEmployee.LastName + " " + zEmployee.FirstName;
            zInfo.ExpireDate = dteExpireDay.Value;
            if (rdoEnable.Checked)
            {
                zInfo.Activate = true;
            }
            else
            {
                zInfo.Activate = false;
            }

            if (UserKey == "")
            {
                zInfo.Create_ClientKey();
                UserKey = zInfo.UserKey;
            }
            else
            {
                zInfo.Update();
                UserKey = zInfo.UserKey;
            }
            return zInfo.Message;
        }
        private void SaveRole()
        {
            string zMessage = "";
            int n = GVRoleForm.Rows.Count;
            for (int i = 0; i < n - 1; i++)
            {
                if (GVRoleForm.Rows[i].Cells["RoleID"].Tag != null)
                {
                    string Name = GVRoleForm.Rows[i].Cells["RoleName"].Value.ToString();
                    User_Role_Info zAccess_Role = new User_Role_Info();

                    zAccess_Role.UserKey = UserKey;
                    zAccess_Role.RoleKey = GVRoleForm.Rows[i].Cells["RoleID"].Tag.ToString();

                    zAccess_Role.RoleAll = Convert.ToBoolean(GVRoleForm.Rows[i].Cells["All"].Value);
                    zAccess_Role.RoleRead = Convert.ToBoolean(GVRoleForm.Rows[i].Cells["Read"].Value);
                    zAccess_Role.RoleAdd = Convert.ToBoolean(GVRoleForm.Rows[i].Cells["Add"].Value);
                    zAccess_Role.RoleEdit = Convert.ToBoolean(GVRoleForm.Rows[i].Cells["Edit"].Value);
                    zAccess_Role.RoleDel = Convert.ToBoolean(GVRoleForm.Rows[i].Cells["Delete"].Value);                    

                    zAccess_Role.Update();
                    zMessage += zAccess_Role.Message;
                }
            }
            //_Message_Role = zMessage;
        }
        private void LoadRole()
        {
            DataTable zTable = Role_Data.ListRole_UserOfPartner(SessionUser.UserLogin.PartnerNumber, UserKey);
            LoadData_GVRoleForm(GVRoleForm, zTable);
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;



        private void Frm_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }
        private void Frm_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }
        private void Frm_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.None;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [Auth]
        User_Role_Info _RoleForm = new User_Role_Info();
        private void Get_Auth()
        {
            List<User_Role_Info> zListRole = SessionUser.TableRole;
            if (zListRole.Count > 0)
            {
                try
                {
                    _RoleForm = zListRole.SingleOrDefault(r => r.RoleURL == _FormUrl);
                    if (_RoleForm == null)
                    {
                        _RoleForm = new User_Role_Info();
                        MessageBox.Show("Form này chưa được thêm quyền, vui lòng liên hệ IT !.");
                        this.Close();
                    }
                    else
                    {
                        if (!_RoleForm.RoleRead)
                        {
                            MessageBox.Show("Bạn không có quyền xem, vui lòng liên hệ IT !.");
                            this.Close();
                        }
                        if (!_RoleForm.RoleEdit)
                        {
                            btnSave.Visible = false;
                            btnResetPassword.Visible = false;
                        }
                        if (!_RoleForm.RoleAdd)
                        {
                            btnSave.Visible = false;
                        }
                        if (!_RoleForm.RoleDel)
                        {
                            //btnDelete.Visible = false;
                        }
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Bạn chưa được phân quyền, vui lòng liên hệ IT !.");
            }
        }
        #endregion       
    }
}