﻿using System.Windows.Forms;
using TN_User;

namespace TN_WinApp
{
    public partial class Frm_ChangePass : Form
    {
        public Frm_ChangePass()
        {
            InitializeComponent();
            btn_Change.Click += Btn_Change_Click;
        }

        private void Btn_Change_Click(object sender, System.EventArgs e)
        {
            User_Info zUser = new User_Info(SessionUser.UserLogin.UserKey);
            string zResult = zUser.ChangePass(txt_PasswordOld.Text.Trim(), txt_PasswordNew.Text.Trim());
            if (zResult != "0" &&
                zUser.Code != "200" &&
                zUser.Code != "201")
            {
                MessageBox.Show(zUser.Message);
            }
            else
            {
                this.Close();
            }
        }

        private void btnClose_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }
    }
}
