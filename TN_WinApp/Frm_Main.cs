﻿using System;
using System.Drawing;
using System.Windows.Forms;
using TN_Connection;
using TN_Tools;
using TN_User;

namespace TN_WinApp
{
    public partial class Frm_Main : Form
    {
        public Frm_Main()
        {
            this.Load += Frm_Main_Load;

            InitializeComponent();

            TitleForm.MouseDown += Frm_MouseDown;
            TitleForm.MouseMove += Frm_MouseMove;
            TitleForm.MouseUp += Frm_MouseUp;

            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;

            btnChangePass.Click += btnChangePass_Click;
            btnExit.Click += btnExit_Click;

            btnStatusTrue.Click += btnStatusTrue_Click;
            btnStatusFalse.Click += btnStatusFalse_Click;

            #region [Left Menu Click]
            MN_List.Click += MN_List_Click;
            MN_Category.Click += MN_Category_Click;
            MN_Order.Click += MN_Order_Click;
            MN_Report.Click += MN_Report_Click;
            MN_Reciept.Click += MN_Reciept_Click;
            MN_Follow.Click += MN_Follow_Click;
            #endregion

            #region [Right Menu Click]
            SubMN_CategoryProduct.Click += (o, e) => {
                IVT.Frm_Category Frm = new IVT.Frm_Category();
                Frm.ShowDialog();
            };
            SubMN_OrderAll.Click += (o, e) =>
            {
                FNC.Frm_OrderList Frm = new FNC.Frm_OrderList();
                Frm.CategoryKey = 3;
                Frm.ShowDialog();
            };
            SubMN_OrderDress.Click += (o, e) =>
            {
                FNC.Frm_OrderList Frm = new FNC.Frm_OrderList();
                Frm.CategoryKey = 1;
                Frm.ShowDialog();
            };
            SubMN_OrderService.Click += (o, e) =>
            {
                FNC.Frm_OrderList Frm = new FNC.Frm_OrderList();
                Frm.CategoryKey = 2;
                Frm.ShowDialog();
            };
            SubMN_Combo.Click += (o, e) =>
            {
                IVT.Frm_ComboList Frm = new IVT.Frm_ComboList();
                Frm.ShowDialog();
            };
            SubMN_Service.Click += (o, e) =>
            {
                IVT.Frm_ServiceList Frm = new IVT.Frm_ServiceList();
                Frm.ShowDialog();
            };
            SubMN_Customer.Click += (o, e) =>
            {
                CRM.Frm_CustomerList Frm = new CRM.Frm_CustomerList();
                Frm.ShowDialog();
            };
            SubMN_Employee.Click += (o, e) =>
            {
                HRM.Frm_EmployeeList Frm = new HRM.Frm_EmployeeList();
                Frm.ShowDialog();
            };
            SubMN_Receipt.Click += (o, e) =>
            {
                FNC.Frm_ReceiptList Frm = new FNC.Frm_ReceiptList();
                Frm.ShowDialog();
            };
            SubMN_UserRole.Click += (o, e) =>
            {
                Auth.Frm_UserList Frm = new Auth.Frm_UserList();
                Frm.ShowDialog();
            };
            SubMN_Product.Click += (o, e) =>
            {
                IVT.Frm_ProductList Frm = new IVT.Frm_ProductList();
                Frm.ShowDialog();
            };
            SubMN_Color.Click += (o, e) =>
            {
                IVT.Frm_Color Frm = new IVT.Frm_Color();
                Frm.ShowDialog();
            };
            SubMN_Size.Click += (o, e) =>
            {
                IVT.Frm_Size Frm = new IVT.Frm_Size();
                Frm.ShowDialog();
            };
            SubMN_Unit.Click += (o, e) =>
            {
                IVT.Frm_Unit Frm = new IVT.Frm_Unit();
                Frm.ShowDialog();
            };
            SubMN_RptProduct.Click += (o, e) =>
            {
                RPT.Frm_IncomeProduct Frm = new RPT.Frm_IncomeProduct();
                Frm.ShowDialog();
            };
            SubMN_RptServices.Click += (o, e) =>
            {
                RPT.Frm_IncomeService Frm = new RPT.Frm_IncomeService();
                Frm.ShowDialog();
            };
            #endregion

            this.DoubleBuffered = true;
            InitConnect();
        }

        private void MN_Follow_Click(object sender, EventArgs e)
        {
            Frm_Follow Frm = new Frm_Follow();
            Frm.ShowDialog();
        }

        #region [ Menu Left]
        private void MN_Reciept_Click(object sender, EventArgs e)
        {
            FNC.Frm_ReceiptEdit Frm = new FNC.Frm_ReceiptEdit();
            Frm.ShowDialog();
        }
        private void MN_Report_Click(object sender, EventArgs e)
        {
            ShowPanel(sender as Control);
        }
        private void MN_List_Click(object sender, EventArgs e)
        {
            ShowPanel(sender as Control);
        }
        private void MN_Order_Click(object sender, EventArgs e)
        {
            FNC.Frm_OrderEdit Frm = new FNC.Frm_OrderEdit();
            Frm.ShowDialog();
        }
        private void MN_Category_Click(object sender, EventArgs e)
        {
            ShowPanel(sender as Control);
        }
        #endregion

        #region [ Event Form Main Form]
        private void btnExit_Click(object sender, EventArgs e)
        {
            Frm_Login Frm = new Frm_Login();
            Frm.ShowDialog();
        }

        private void btnChangePass_Click(object sender, EventArgs e)
        {
            Frm_ChangePass Frm = new Frm_ChangePass();
            Frm.ShowDialog();
        }

        #region [Dùng kéo rê form]
        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn thoát phần mềm !.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;

        private void Frm_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }
        private void Frm_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }
        private void Frm_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion
        #endregion

        private void Frm_Main_Load(object sender, EventArgs e)
        {
            lblUserName.Text = "Nhân viên: ";
            lblTime.Text = "Ngày làm việc: " + DateTime.Now.ToString("dd/MM/yyyy");
        }

        #region [Process]
        private void ShowPanel(Control Ctrl)
        {
            string MenuName = Ctrl.Name;
            string TitleText = Ctrl.Text;
            string PanelName = "";
            string[] Temp = MenuName.Split('_');
            PanelName = "PN_" + Temp[1];

            foreach (Control ctr in Panel_Right.Controls)
            {
                if (ctr.Name == PanelName)
                {
                    ctr.Visible = true;
                    ctr.Location = new System.Drawing.Point(6, 6);
                }
                else
                {
                    ctr.Visible = false;
                }
            }

            txtTitle.Text = "Chức năng: " + TitleText;
        }
        #endregion

        #region [Initial Connection]
        private void InitConnect()
        {
            bool IsConnect = false;
            RWConfig nConfig = new RWConfig();
            ConnectDataBaseInfo nConnectInfo = new ConnectDataBaseInfo();
            if (nConfig.ReadConfig())
            {
                nConnectInfo = nConfig.ConnectInfo;
                ConnectDataBase nConnect = new ConnectDataBase(nConnectInfo.ConnectionString);
                if (nConnect.Message.Length > 0)
                {
                    IsConnect = false;
                }
                else
                {
                    IsConnect = true;
                }
            }
            else
            {
                Frm_Connect Frm = new Frm_Connect();
                Frm.ShowDialog();

                if (Frm.IsConnect)
                {
                    IsConnect = true;
                }
                else
                {
                    IsConnect = false;
                }
            }

            if (IsConnect)
            {
                //tạo ConnectionString trong các công cụ hổ trợ
                LoadDataToToolbox.ConnectionString = ConnectDataBase.ConnectionString;

                Frm_Login Frm = new Frm_Login();
                Frm.ShowDialog();

                btnStatusFalse.Visible = false;
                btnStatusTrue.Visible = true;
            }
            else
            {
                Application.Exit();
            }

            if (SessionUser.UserLogin != null)
            {
                lblUserName.Text = SessionUser.UserLogin.EmployeeName;
            }
            else
            {
                Application.Exit();
            }
        }

        private void btnStatusFalse_Click(object sender, EventArgs e)
        {
            Frm_Connect frm = new Frm_Connect();
            frm.ShowDialog();
            ConnectDataBase.ConnectionString = frm.ConnectDataBaseInfo.ConnectionString;

            if (frm.IsConnect)
            {
                btnStatusFalse.Visible = false;
                btnStatusTrue.Visible = true;
            }
            else
            {
                btnStatusFalse.Visible = true;
                btnStatusTrue.Visible = false;
            }
        }

        private void btnStatusTrue_Click(object sender, EventArgs e)
        {
            Frm_Connect frm = new Frm_Connect();
            frm.ShowDialog();
            ConnectDataBase.ConnectionString = frm.ConnectDataBaseInfo.ConnectionString;

            if (frm.IsConnect)
            {
                btnStatusFalse.Visible = false;
                btnStatusTrue.Visible = true;
            }
            else
            {
                btnStatusFalse.Visible = true;
                btnStatusTrue.Visible = false;
            }
        }
        #endregion

        private void kryptonButton1_Click(object sender, EventArgs e)
        {
            Frm_PartnerList Frm = new Frm_PartnerList();
            Frm.ShowDialog();
        }
    }
}
