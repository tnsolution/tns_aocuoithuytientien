﻿namespace TN_WinApp
{
    partial class Frm_ChangePass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_ChangePass));
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_Change = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.txt_PasswordOld = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_PasswordNew = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_Replay = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.Header = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(99, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 16);
            this.label3.TabIndex = 148;
            this.label3.Text = "Mật khẩu cũ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(91, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 16);
            this.label1.TabIndex = 150;
            this.label1.Text = "Mật khẩu mới";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(40, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 16);
            this.label2.TabIndex = 152;
            this.label2.Text = "Nhập lại mật khẩu mới";
            // 
            // btn_Change
            // 
            this.btn_Change.Location = new System.Drawing.Point(134, 152);
            this.btn_Change.Name = "btn_Change";
            this.btn_Change.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Change.Size = new System.Drawing.Size(120, 40);
            this.btn_Change.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Change.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Change.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Change.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Change.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Change.TabIndex = 153;
            this.btn_Change.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Change.Values.Image")));
            this.btn_Change.Values.Text = "Cập nhật";
            // 
            // txt_PasswordOld
            // 
            this.txt_PasswordOld.Location = new System.Drawing.Point(178, 54);
            this.txt_PasswordOld.Name = "txt_PasswordOld";
            this.txt_PasswordOld.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_PasswordOld.PasswordChar = '●';
            this.txt_PasswordOld.Size = new System.Drawing.Size(162, 26);
            this.txt_PasswordOld.StateCommon.Border.ColorAngle = 1F;
            this.txt_PasswordOld.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_PasswordOld.StateCommon.Border.Rounding = 4;
            this.txt_PasswordOld.StateCommon.Border.Width = 1;
            this.txt_PasswordOld.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PasswordOld.TabIndex = 154;
            this.txt_PasswordOld.UseSystemPasswordChar = true;
            // 
            // txt_PasswordNew
            // 
            this.txt_PasswordNew.Location = new System.Drawing.Point(178, 85);
            this.txt_PasswordNew.Name = "txt_PasswordNew";
            this.txt_PasswordNew.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_PasswordNew.PasswordChar = '●';
            this.txt_PasswordNew.Size = new System.Drawing.Size(162, 26);
            this.txt_PasswordNew.StateCommon.Border.ColorAngle = 1F;
            this.txt_PasswordNew.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_PasswordNew.StateCommon.Border.Rounding = 4;
            this.txt_PasswordNew.StateCommon.Border.Width = 1;
            this.txt_PasswordNew.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PasswordNew.TabIndex = 154;
            this.txt_PasswordNew.UseSystemPasswordChar = true;
            // 
            // txt_Replay
            // 
            this.txt_Replay.Location = new System.Drawing.Point(178, 117);
            this.txt_Replay.Name = "txt_Replay";
            this.txt_Replay.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Replay.PasswordChar = '●';
            this.txt_Replay.Size = new System.Drawing.Size(162, 26);
            this.txt_Replay.StateCommon.Border.ColorAngle = 1F;
            this.txt_Replay.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Replay.StateCommon.Border.Rounding = 4;
            this.txt_Replay.StateCommon.Border.Width = 1;
            this.txt_Replay.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Replay.TabIndex = 154;
            this.txt_Replay.UseSystemPasswordChar = true;
            // 
            // Header
            // 
            this.Header.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnClose});
            this.Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.Header.Location = new System.Drawing.Point(0, 0);
            this.Header.Name = "Header";
            this.Header.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.Header.Size = new System.Drawing.Size(389, 42);
            this.Header.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.Header.TabIndex = 163;
            this.Header.Values.Description = "";
            this.Header.Values.Heading = "Đổi mật khẩu";
            this.Header.Values.Image = null;
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(389, 204);
            this.panel1.TabIndex = 164;
            // 
            // Frm_ChangePass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(389, 204);
            this.Controls.Add(this.Header);
            this.Controls.Add(this.txt_Replay);
            this.Controls.Add(this.txt_PasswordNew);
            this.Controls.Add(this.txt_PasswordOld);
            this.Controls.Add(this.btn_Change);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 9F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_ChangePass";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đổi Mật Khẩu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Change;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_PasswordOld;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_PasswordNew;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Replay;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader Header;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Panel panel1;
    }
}