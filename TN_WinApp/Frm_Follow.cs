﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using TN_Connection;
using TN_Tools;
using TN_User;

namespace TN_WinApp
{
    public partial class Frm_Follow : Form
    {
        private string _FormUrl = "/Frm_Follow";
        public string FormUrl
        {
            get
            {
                return _FormUrl;
            }

            set
            {
                _FormUrl = value;
            }
        }
        public DataTable _Table = new DataTable();

        public Frm_Follow()
        {
            InitializeComponent();

            TitleForm.MouseDown += Frm_MouseDown;
            TitleForm.MouseMove += Frm_MouseMove;
            TitleForm.MouseUp += Frm_MouseUp;

            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;

            btnPrint.Click += BtnPrint_Click;
            btnSearch.Click += BtnSearch_Click; ;

            this.Text = "Doanh số thuê áo cưới";
            this.WindowState = FormWindowState.Maximized;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.FormBorderStyle = FormBorderStyle.None;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.Load += Frm_Follow_Load; ;
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            Frm_FollowPrint frm = new Frm_FollowPrint();
            frm._FromDate = dteFromDate.Value;
            frm._ToDate = dteToDate.Value;
            frm._TableFollow = _Table;
            frm.ShowDialog();
        }

        private void Frm_Follow_Load(object sender, EventArgs e)
        {
            Get_Auth();
            if (_RoleForm.RoleRead)
            {
                DateTime zDate = DateTime.Now;
                DateTime FromDate = new DateTime(zDate.Year, zDate.Month, 1, 0, 0, 0);
                DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
                ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
                dteFromDate.Value = FromDate;
                dteToDate.Value = ToDate;

                int Status = cboStatus.SelectedValue.ToInt();
                _Table = Access_Data.Report_Order(SessionUser.UserLogin.PartnerNumber, FromDate, ToDate, Status, txt_Search.Text.Trim());

                DesignLayout_GVProduct(GVData);
                LoadData_GVProduct(GVData, _Table);

                LoadDataToToolbox.KryptonComboBox(cboStatus, "SELECT * FROM FNC_OrderStatus", "--Chọn--");
            }
        }

        #region [Event]
        private void BtnSearch_Click(object sender, EventArgs e)
        {
            DateTime FromDate = new DateTime(dteFromDate.Value.Year, dteFromDate.Value.Month, dteFromDate.Value.Day, 0, 0, 0);
            DateTime ToDate = new DateTime(dteToDate.Value.Year, dteToDate.Value.Month, dteToDate.Value.Day, 23, 59, 59);
            dteFromDate.Value = FromDate;
            dteToDate.Value = ToDate;

            int Status = cboStatus.SelectedValue.ToInt();
            _Table = Access_Data.Report_Order(SessionUser.UserLogin.PartnerNumber, FromDate, ToDate, Status, txt_Search.Text.Trim());
            LoadData_GVProduct(GVData, _Table);
        }
        #endregion

        #region [Process]
        private void DesignLayout_GVProduct(DataGridView GV)
        {
            TNPaintControl.DrawGVStyle(ref GV);

            GV.Columns.Add("No", "STT");
            GV.Columns.Add("OrderDate", "Ngày nhận");
            GV.Columns.Add("OrderID", "Số đơn hàng");
            GV.Columns.Add("BuyerName", "Khách hàng");
            GV.Columns.Add("BuyerPhone", "Điện thoại");
            GV.Columns.Add("Female", "Áo nữ");
            GV.Columns.Add("Male", "Áo nam");
            GV.Columns.Add("DeliverDate", "Ngày giao");
            GV.Columns.Add("ReturnDate", "Ngày trả");
            GV.Columns.Add("TotalAmount", "Tổng tiền");
            GV.Columns.Add("SaleAmount", "Giảm giá");
            GV.Columns.Add("AmountCurrencyMain", "Đã thu");
            GV.Columns.Add("AmountRemaining", "Còn lại");
            GV.Columns.Add("StatusPayment", "Thanh toán");
            GV.Columns.Add("OrderDescription", "Ghi chú");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["OrderDate"].Width = 90;
            GV.Columns["OrderDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["OrderID"].Width = 120;
            GV.Columns["OrderID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["BuyerName"].Width = 150;
            GV.Columns["BuyerName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["BuyerName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["BuyerPhone"].Width = 90;
            GV.Columns["BuyerPhone"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["Female"].Width = 200;
            GV.Columns["Female"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["Female"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["Male"].Width = 200;
            GV.Columns["Male"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["Male"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["DeliverDate"].Width = 90;
            GV.Columns["DeliverDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["ReturnDate"].Width = 90;
            GV.Columns["ReturnDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["TotalAmount"].Width = 90;
            GV.Columns["TotalAmount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["SaleAmount"].Width = 120;
            GV.Columns["SaleAmount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["AmountCurrencyMain"].Width = 120;
            GV.Columns["AmountCurrencyMain"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["AmountRemaining"].Width = 120;
            GV.Columns["AmountRemaining"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["StatusPayment"].Width = 120;
            GV.Columns["StatusPayment"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["OrderDescription"].Width = 250;
            GV.Columns["OrderDescription"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["OrderDescription"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.ReadOnly = true;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = false;
            GV.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }
        private void LoadData_GVProduct(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();

            int i = 0;
            foreach (DataRow r in Table.Rows)
            {
                GV.Rows.Add();
                DataGridViewRow zGvRow = GV.Rows[i];
                zGvRow.Cells["No"].Value = (i + 1).ToString();
                zGvRow.Cells["OrderDate"].Value = r["OrderDate"].ToDateString();
                zGvRow.Cells["OrderID"].Value = r["OrderID"].ToString();
                zGvRow.Cells["BuyerName"].Value = r["BuyerName"].ToString();
                zGvRow.Cells["BuyerPhone"].Value = r["BuyerPhone"].ToString().Trim();
                zGvRow.Cells["Female"].Value = r["Female"].ToString().Trim();
                zGvRow.Cells["Male"].Value = r["Male"].ToString().Trim();

                zGvRow.Cells["OrderDescription"].Value = r["OrderDescription"].ToString().Trim();
                zGvRow.Cells["DeliverDate"].Value = r["DeliverDate"].ToDateString();
                zGvRow.Cells["ReturnDate"].Value = r["ReturnDate"].ToString().Trim();
                zGvRow.Cells["TotalAmount"].Value = r["TotalAmount"].FormatMoneyFromObject();
                zGvRow.Cells["SaleAmount"].Value = r["SaleAmount"].FormatMoneyFromObject();
                zGvRow.Cells["AmountCurrencyMain"].Value = r["AmountCurrencyMain"].FormatMoneyFromObject();

                double TotalAmount = r["TotalAmount"].ToDouble();
                double SaleAmount = r["SaleAmount"].ToDouble();
                double AmountCurrencyMain = r["AmountCurrencyMain"].ToDouble();
                double AmountRemaining = TotalAmount - SaleAmount - AmountCurrencyMain;
                if (AmountRemaining > 0)
                {
                    zGvRow.Cells["AmountRemaining"].Value = AmountRemaining.FormatMoneyFromObject();
                    zGvRow.Cells["StatusPayment"].Value = "Chưa thanh toán đủ";
                    zGvRow.DefaultCellStyle.ForeColor = Color.OrangeRed;
                    zGvRow.DefaultCellStyle.Font = new Font("Tahoma", 9F, FontStyle.Bold);
                }
                else
                {
                    zGvRow.Cells["AmountRemaining"].Value = 0;
                    zGvRow.Cells["StatusPayment"].Value = "Đã thanh toán xong";
                }

                i++;
            }

            Sum_GVProduct(GVData);
        }
        private void Sum_GVProduct(DataGridView GV)
        {
            double TotalAmount = 0;
            double SaleAmount = 0;
            double AmountCurrencyMain = 0;
            double AmountRemaining = 0;

            foreach (DataGridViewRow r in GV.Rows)
            {
                TotalAmount += double.Parse(r.Cells["TotalAmount"].Value.ToString(), CultureInfo.GetCultureInfo("vi-VN"));
                SaleAmount += double.Parse(r.Cells["SaleAmount"].Value.ToString(), CultureInfo.GetCultureInfo("vi-VN"));
                AmountCurrencyMain += double.Parse(r.Cells["AmountCurrencyMain"].Value.ToString(), CultureInfo.GetCultureInfo("vi-VN"));
                AmountRemaining += double.Parse(r.Cells["AmountRemaining"].Value.ToString(), CultureInfo.GetCultureInfo("vi-VN"));
            }

            GV.Rows.Add();
            GV.Rows[GV.Rows.Count - 1].Cells["TotalAmount"].Value = TotalAmount.ToString("n0", CultureInfo.GetCultureInfo("vi-VN"));
            GV.Rows[GV.Rows.Count - 1].Cells["SaleAmount"].Value = SaleAmount.ToString("n0", CultureInfo.GetCultureInfo("vi-VN"));
            GV.Rows[GV.Rows.Count - 1].Cells["AmountCurrencyMain"].Value = AmountCurrencyMain.ToString("n0", CultureInfo.GetCultureInfo("vi-VN"));
            GV.Rows[GV.Rows.Count - 1].Cells["AmountRemaining"].Value = AmountRemaining.ToString("n0", CultureInfo.GetCultureInfo("vi-VN"));

            GV.Rows[GV.Rows.Count - 1].DefaultCellStyle.ForeColor = Color.Maroon;
            GV.Rows[GV.Rows.Count - 1].DefaultCellStyle.Font = new Font("Tahoma", 10F, FontStyle.Bold);
        }
        #endregion

        #region [Dùng kéo rê form]
        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;



        private void Frm_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }
        private void Frm_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }
        private void Frm_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        #region [Auth]
        User_Role_Info _RoleForm = new User_Role_Info();
        private void Get_Auth()
        {
            List<User_Role_Info> zListRole = SessionUser.TableRole;
            if (zListRole.Count > 0)
            {
                try
                {
                    _RoleForm = zListRole.SingleOrDefault(r => r.RoleURL == FormUrl);
                    if (_RoleForm == null)
                    {
                        _RoleForm = new User_Role_Info();
                        MessageBox.Show("Form này chưa được thêm quyền, vui lòng liên hệ IT !.");
                        this.Close();
                    }
                    else
                    {
                        if (!_RoleForm.RoleRead)
                        {
                            MessageBox.Show("Bạn không có quyền xem, vui lòng liên hệ IT !.");
                            this.Close();
                        }
                        if (!_RoleForm.RoleEdit)
                        {

                        }
                        if (!_RoleForm.RoleAdd)
                        {

                        }
                        if (!_RoleForm.RoleDel)
                        {

                        }
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Bạn chưa được phân quyền, vui lòng liên hệ IT !.");
            }
        }
        #endregion

        #region [Access Info-Data]
        public class Access_Data
        {
            public static DataTable Report_Order(string PartnerNumber, DateTime? FromDate, DateTime? ToDate, int Status, string Search)
            {
                DataTable zTable = new DataTable();
                string zSQL = @"SELECT * FROM (
SELECT 
A.OrderDate, A.OrderID, A.BuyerName, A.BuyerPhone,
dbo.Get_OrderItemFemale_FED(A.OrderKey) AS Female,
dbo.Get_OrderItemMale_FED(A.OrderKey) AS Male,
A.OrderDescription, A.OrderStatusName,
A.DeliverDate, A.ReturnDate, A.TotalAmount, A.SaleAmount, 
dbo.SUM_ORDER_FED(A.OrderID) AS AmountCurrencyMain,
A.CreatedOn
FROM FNC_Order_FED A
WHERE A.RecordStatus <> 99  
AND A.CategoryKey = 1
AND A.PartnerNumber = @PartnerNumber";

                if (Status != 0)
                {
                    zSQL += " AND A.OrderStatusKey = @Status";
                }

                if (FromDate != null && ToDate != null)
                {
                    zSQL += " AND A.OrderDate >= @FromDate AND A.OrderDate <= @ToDate";
                }

                zSQL += ") X";
                if (Search != string.Empty)
                {
                    zSQL += " WHERE (Female LIKE @Search OR Male LIKE @Search)";
                }

                zSQL += " ORDER BY OrderDate DESC";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                    zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Status;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                    if (FromDate != null && ToDate != null)
                    {
                        zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                        zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                    }

                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
        }
        #endregion
    }
}
