﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TN_Connection;
using TN_User;

namespace TN_WinApp.CRM
{
    public partial class Frm_CustomerList : Form
    {
        private string _FormUrl = "/CRM/Frm_CustomerList";
        private string _CustomerKey = "";

        public string CustomerKey
        {
            get
            {
                return _CustomerKey;
            }

            set
            {
                _CustomerKey = value;
            }
        }
        public string FormUrl
        {
            get
            {
                return _FormUrl;
            }

            set
            {
                _FormUrl = value;
            }
        }

        public Frm_CustomerList()
        {
            InitializeComponent();

            TitleForm.MouseDown += Frm_MouseDown;
            TitleForm.MouseMove += Frm_MouseMove;
            TitleForm.MouseUp += Frm_MouseUp;

            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;

            btnSearch.Click += BtnSearch_Click;
            btnSave.Click += BtnSave_Click;
            btnAddNew.Click += BtnAddNew_Click;
            btnDelete.Click += BtnDelete_Click;

            LVData.Click += LVData_Click;
            LVData.KeyDown += LVData_KeyDown;

            //format Form
            this.Text = "Danh sách khách hàng";
            this.WindowState = FormWindowState.Maximized;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.FormBorderStyle = FormBorderStyle.None;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.Load += Frm_CustomerList_Load;
        }

        private void Frm_CustomerList_Load(object sender, EventArgs e)
        {
            Get_Auth();
            if (_RoleForm.RoleRead)
            {
                DesignLayout(LVData);
                LoadListView();
                ClearForm();
            }
        }

        #region [ListView]
        private void DesignLayout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Khách hàng";
            colHead.Width = 450;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            TN_Tools.TNPaintControl.DrawLVStyle(ref LVData);
        }
        private void LoadListView()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable zTable = Access_Data.List(txtSearch.Text, SessionUser.UserLogin.PartnerNumber);
            LV.Items.Clear();
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow zRow = zTable.Rows[i];
                lvi.Tag = zRow["CustomerKey"].ToString();
                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["CustomerID"].ToString();
                lvi.SubItems.Add(lvsi);

                string zName = "(" + zRow["Aliases"].ToString() + ")-" + zRow["FullName"].ToString();
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zName;
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }
            this.Cursor = Cursors.Default;

        }
        private void LVData_Click(object sender, EventArgs e)
        {
            CustomerKey = LVData.SelectedItems[0].Tag.ToString();
            LoadData();
        }
        private void LVData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (_RoleForm.RoleDel)
                {
                    DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dlr == DialogResult.Yes && LVData.SelectedItems.Count > 0)
                    {
                        CustomerKey = LVData.SelectedItems[0].Tag.ToString();
                        string zResult = Delete(CustomerKey);

                        if (zResult.Substring(0, 3) == "200" ||
                            zResult.Substring(0, 3) == "201")
                        {
                            MessageBox.Show("Đã xóa thành công !");
                            LoadListView();
                            ClearForm();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Bạn không có quyền xóa !");
                }
            }
        }
        #endregion

        #region[Event Action]
        private void BtnSearch_Click(object sender, EventArgs e)
        {
            LoadListView();
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            string zResult = "";
            string zMessage = "";
            zMessage = CheckBeforSave();
            if (zMessage.Length == 0)
            {
                zResult = Save();
                if (zResult.Substring(0, 3) == "200" ||
                    zResult.Substring(0, 3) == "201")
                {
                    MessageBox.Show("Cập nhật khách hàng thành công !");
                    LoadListView();
                    LoadData();

                }
                else
                {
                    MessageBox.Show("Lỗi.Vui lòng liên hệ IT.Chi tiết lỗi mã: \n " + zResult);
                }
            }
            else
            {
                MessageBox.Show(zMessage);
            }
        }
        private void BtnAddNew_Click(object sender, EventArgs e)
        {
            ClearForm();
        }
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            if (CustomerKey != "")
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    Customer_Info zInfo = new Customer_Info();
                    zInfo.CustomerKey = CustomerKey;
                    zInfo.Delete();
                    Person_Info zPerson = new Person_Info();
                    zPerson.CustomerPerson_Info(zInfo.CustomerKey);
                    zPerson.Delete();
                    if (zInfo.Message.Substring(0, 3) == "200")
                    {
                        MessageBox.Show("Xóa thành công!");
                        LoadListView();
                        ClearForm();
                    }
                    else
                    {
                        MessageBox.Show("Lỗi.Vui lòng liên hệ IT.Chi tiết lỗi mã: \n " + zInfo.Message);
                    }

                }
            }
        }
        #endregion

        #region[Process]
        private void ClearForm()
        {
            CustomerKey = "";
            txtCustomerID.Text = "";
            txtAliases.Text = "";
            txtBank.Text = "";
            txtBankName.Text = "";
            txtAddress.Text = "";
            txtNote.Text = "";
            txtPhone.Text = "";
            txtEmail.Text = "";
            dteBirthDay.Value = DateTime.Now;
            txtLastName.Text = "";
            txtFirstName.Text = "";
            radioFemale.Checked = true;
            txtPassport.Text = "";
            txtIssuePlace.Text = "";
            dteIssueDate.Value = DateTime.MinValue;
            dteExpireDate.Value = DateTime.MinValue;

        }
        private void LoadData()
        {
            Customer_Info zInfo = new Customer_Info(CustomerKey);
            CustomerKey = zInfo.CustomerKey;
            txtCustomerID.Text = zInfo.CustomerID;
            txtAliases.Text = zInfo.Aliases;
            txtBank.Text = zInfo.BankAccount;
            txtBankName.Text = zInfo.BankName;
            txtNote.Text = zInfo.Note;
            txtPhone.Text = zInfo.Phone;
            txtEmail.Text = zInfo.Email;
            txtAddress.Text = zInfo.Address;
            //Thông tin cá nhân
            Person_Info zPerson = new Person_Info();
            zPerson.CustomerPerson_Info(zInfo.CustomerKey);

            txtLastName.Text = zPerson.LastName;
            txtFirstName.Text = zPerson.FirstName;
            if (zPerson.BirthDay == null)
            {
                dteBirthDay.Value = DateTime.MinValue;
            }
            else
            {
                dteBirthDay.Value = zPerson.BirthDay.Value;
            }

            if (zPerson.Gender == 0)
            {
                radioFemale.Checked = true;
            }
            else
            {
                radioMale.Checked = true;
            }

            txtPassport.Text = zPerson.PassportNumber;
            txtIssuePlace.Text = zPerson.IssuePlace;
            if (zPerson.IssueDate == null)
            {
                dteIssueDate.Value = DateTime.MinValue;
            }
            else
            {
                dteIssueDate.Value = zPerson.IssueDate.Value;
            }

            if (zPerson.ExpireDate == null)
            {
                dteExpireDate.Value = DateTime.MinValue;
            }
            else
            {
                dteExpireDate.Value = zPerson.ExpireDate.Value;
            }
        }
        private string CheckBeforSave()
        {
            string zResult = "";
            if (txtCustomerID.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng điền mã khách hàng! \n";
                txtCustomerID.StateCommon.Back.Color1 = Color.FromArgb(255, 102, 102);
            }
            else
            {
                txtCustomerID.StateCommon.Back.Color1 = Color.White;
            }
            if (CustomerKey == "")
            {
                bool Count = Access_Data.CheckCustomerID(SessionUser.UserLogin.PartnerNumber, txtCustomerID.Text.Trim());
                if (!Count)
                {
                    zResult += "Vui lòng chọn mã khách hàng khác! \n";
                    txtCustomerID.StateCommon.Back.Color1 = Color.FromArgb(255, 102, 102);
                }
            }
            else
            {
                txtCustomerID.StateCommon.Back.Color1 = Color.White;
            }
            if (txtLastName.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng điền họ và tên lót! \n";
                txtLastName.StateCommon.Back.Color1 = Color.FromArgb(255, 102, 102);
            }
            else
            {
                txtFirstName.StateCommon.Back.Color1 = Color.White;
            }
            if (txtFirstName.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng điền tên khách hàng! \n";
                txtFirstName.StateCommon.Back.Color1 = Color.FromArgb(255, 102, 102);
            }
            else
            {
                txtFirstName.StateCommon.Back.Color1 = Color.White;
            }
            if (txtAliases.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng điền tên gợi nhớ! \n";
                txtAliases.StateCommon.Back.Color1 = Color.FromArgb(255, 102, 102);
            }
            else
            {
                txtAliases.StateCommon.Back.Color1 = Color.White;
            }
            if (txtPhone.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng điền số điện thoại! \n";
                txtPhone.StateCommon.Back.Color1 = Color.FromArgb(255, 102, 102);
            }
            else
            {
                txtPhone.StateCommon.Back.Color1 = Color.White;
            }
            if (txtAddress.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng nhập địa chỉ khách hàngi! \n";
                txtAddress.StateCommon.Back.Color1 = Color.FromArgb(255, 102, 102);
            }
            else
            {
                txtAddress.StateCommon.Back.Color1 = Color.White;
            }
            return zResult;
        }
        private string Save()
        {
            string zMesage = "";
            #region [Customer_Info]
            Customer_Info zInfo;
            if (CustomerKey == "")
            {
                zInfo = new Customer_Info();
            }
            else
            {
                zInfo = new Customer_Info(CustomerKey);
            }
            zInfo.CustomerID = txtCustomerID.Text.Trim().ToUpper();
            zInfo.FullName = txtLastName.Text.Trim() + " " + txtFirstName.Text.Trim();
            zInfo.Aliases = txtAliases.Text.Trim();
            zInfo.Phone = txtPhone.Text.Trim();
            zInfo.Email = txtEmail.Text.Trim();
            zInfo.Note = txtNote.Text.Trim();
            zInfo.Address = txtAddress.Text.Trim();
            zInfo.PartnerNumber = SessionUser.UserLogin.PartnerNumber;
            zInfo.BankAccount = txtBank.Text.Trim();
            zInfo.BankName = txtBankName.Text.Trim();
            if (CustomerKey == "")
            {
                zInfo.Create_ClientKey();
            }
            else
            {
                zInfo.Update();
            }

            //-------------------------------------------------------
            #endregion
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                #region [Person_Info]
                Person_Info zPerson = new Person_Info();
                zPerson.CustomerPerson_Info(zInfo.CustomerKey);
                zPerson.CustomerKey = zInfo.CustomerKey;
                zPerson.LastName = txtLastName.Text.Trim();
                zPerson.FirstName = txtFirstName.Text.Trim();
                if (dteBirthDay.Value == DateTime.MinValue)
                {
                    zPerson.BirthDay = null;
                }
                else
                {
                    zPerson.BirthDay = dteBirthDay.Value;
                }

                if (radioFemale.Checked == true)
                {
                    zPerson.Gender = 0;
                }
                else
                {
                    zPerson.Gender = 1;
                }

                zPerson.PassportNumber = txtPassport.Text;
                zPerson.IssuePlace = txtIssuePlace.Text;
                if (dteIssueDate.Value == DateTime.MinValue)
                {
                    zPerson.IssueDate = null;
                }
                else
                {
                    zPerson.IssueDate = dteIssueDate.Value;
                }

                if (dteExpireDate.Value == DateTime.MinValue)
                {
                    zPerson.ExpireDate = null;
                }
                else
                {
                    zPerson.ExpireDate = dteExpireDate.Value;
                }

                zPerson.PartnerNumber = SessionUser.UserLogin.PartnerNumber;

                if (zPerson.Code == "404")
                {
                    zPerson.Create_ClientKey();
                }
                else
                {
                    zPerson.Update();
                }

                //-------------------------------------------------------
                zMesage = zPerson.Message;
                #endregion
            }
            return zMesage;
        }
        private string Delete(string CustomerKey)
        {
            Customer_Info zInfo = new Customer_Info();
            zInfo.CustomerKey = CustomerKey;
            zInfo.Delete();

            Person_Info zPerson = new Person_Info();
            zPerson.CustomerPerson_Info(zInfo.CustomerKey);
            zPerson.Delete();
            return zInfo.Message;
        }
        #endregion
                
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;



        private void Frm_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }
        private void Frm_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }
        private void Frm_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.None;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [Auth]
        User_Role_Info _RoleForm = new User_Role_Info();
        private void Get_Auth()
        {
            List<User_Role_Info> zListRole = SessionUser.TableRole;
            if (zListRole.Count > 0)
            {
                try
                {
                    _RoleForm = zListRole.SingleOrDefault(r => r.RoleURL == _FormUrl);
                    if (_RoleForm == null)
                    {
                        _RoleForm = new User_Role_Info();
                        MessageBox.Show("Form này chưa được thêm quyền, vui lòng liên hệ IT !.");
                        this.Close();
                    }
                    else
                    {
                        if (!_RoleForm.RoleRead)
                        {
                            MessageBox.Show("Bạn không có quyền xem, vui lòng liên hệ IT !.");
                            this.Close();
                        }
                        if (!_RoleForm.RoleEdit)
                        {
                            btnSave.Visible = false;
                        }
                        if (!_RoleForm.RoleAdd)
                        {
                            btnSave.Visible = false;
                        }
                        if (!_RoleForm.RoleDel)
                        {
                            btnDelete.Visible = false;
                        }
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Bạn chưa được phân quyền, vui lòng liên hệ IT !.");
            }
        }
        #endregion       

        #region [ Access Data ]
        public class Customer_Info
        {
            #region [ Field Name ]
            private string _CustomerKey = "";
            private string _CustomerID = "";
            private string _FullName = "";
            private string _CompanyName = "";
            private string _Aliases = "";
            private string _JobTitle = "";
            private int _CategoryKey = 0;
            private string _CategoryName = "";
            private int _CustomerType = 0;
            private string _CustomerTypeName = "";
            private int _CustomerVendor = 0;
            private string _TaxNumber = "";
            private string _Address = "";
            private string _City = "";
            private string _Country = "";
            private string _ZipCode = "";
            private string _Phone = "";
            private string _Email = "";
            private string _Note = "";
            private string _Style = "";
            private string _Class = "";
            private string _CodeLine = "";
            private string _BankAccount = "";
            private string _BankName = "";
            private string _PartnerNumber = "";
            private int _RecordStatus = 0;
            private DateTime? _CreatedOn = null;
            private string _CreatedBy = "";
            private string _CreatedName = "";
            private DateTime? _ModifiedOn = null;
            private string _ModifiedBy = "";
            private string _ModifiedName = "";
            private string _Message = "";
            #endregion

            #region [ Constructor Get Information ]
            public Customer_Info()
            {
                Guid zNewID = Guid.NewGuid();
                _CustomerKey = zNewID.ToString();
            }
            public Customer_Info(string CustomerKey)
            {
                string zSQL = "SELECT * FROM [dbo].[CRM_Customer] WHERE CustomerKey = @CustomerKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    if (CustomerKey.Length > 0)
                    {
                        zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(CustomerKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }

                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _CustomerKey = zReader["CustomerKey"].ToString();
                        _CustomerID = zReader["CustomerID"].ToString();
                        _FullName = zReader["FullName"].ToString();
                        _CompanyName = zReader["CompanyName"].ToString();
                        _Aliases = zReader["Aliases"].ToString();
                        _JobTitle = zReader["JobTitle"].ToString();
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                        _CategoryName = zReader["CategoryName"].ToString();
                        _CustomerType = int.Parse(zReader["CustomerType"].ToString());
                        _CustomerTypeName = zReader["CustomerTypeName"].ToString();
                        _CustomerVendor = int.Parse(zReader["CustomerVendor"].ToString());
                        _TaxNumber = zReader["TaxNumber"].ToString();
                        _Address = zReader["Address"].ToString();
                        _City = zReader["City"].ToString();
                        _Country = zReader["Country"].ToString();
                        _ZipCode = zReader["ZipCode"].ToString();
                        _Phone = zReader["Phone"].ToString();
                        _Email = zReader["Email"].ToString();
                        _Note = zReader["Note"].ToString();
                        _Style = zReader["Style"].ToString();
                        _Class = zReader["Class"].ToString();
                        _CodeLine = zReader["CodeLine"].ToString();
                        _BankAccount = zReader["BankAccount"].ToString();
                        _BankName = zReader["BankName"].ToString();
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }
            public Customer_Info(string CustomerKey, string PartnerNumber)
            {
                string zSQL = "SELECT * FROM [dbo].[CRM_Customer] WHERE CustomerKey = @CustomerKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    if (CustomerKey.Length > 0)
                    {
                        zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(CustomerKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }

                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _CustomerKey = zReader["CustomerKey"].ToString();
                        _CustomerID = zReader["CustomerID"].ToString();
                        _FullName = zReader["FullName"].ToString();
                        _CompanyName = zReader["CompanyName"].ToString();
                        _Aliases = zReader["Aliases"].ToString();
                        _JobTitle = zReader["JobTitle"].ToString();
                        if (zReader["CategoryKey"] != DBNull.Value)
                        {
                            _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                        }

                        _CategoryName = zReader["CategoryName"].ToString();
                        if (zReader["CustomerType"] != DBNull.Value)
                        {
                            _CustomerType = int.Parse(zReader["CustomerType"].ToString());
                        }

                        _CustomerTypeName = zReader["CustomerTypeName"].ToString();
                        if (zReader["CustomerVendor"] != DBNull.Value)
                        {
                            _CustomerVendor = int.Parse(zReader["CustomerVendor"].ToString());
                        }

                        _TaxNumber = zReader["TaxNumber"].ToString();
                        _Address = zReader["Address"].ToString();
                        _City = zReader["City"].ToString();
                        _Country = zReader["Country"].ToString();
                        _ZipCode = zReader["ZipCode"].ToString();
                        _Phone = zReader["Phone"].ToString();
                        _Email = zReader["Email"].ToString();
                        _Note = zReader["Note"].ToString();
                        _Style = zReader["Style"].ToString();
                        _Class = zReader["Class"].ToString();
                        _CodeLine = zReader["CodeLine"].ToString();
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }
            #endregion

            #region [ Properties ]
            public string CustomerKey
            {
                get { return _CustomerKey; }
                set { _CustomerKey = value; }
            }
            public string CustomerID
            {
                get { return _CustomerID; }
                set { _CustomerID = value; }
            }
            public string FullName
            {
                get { return _FullName; }
                set { _FullName = value; }
            }
            public string CompanyName
            {
                get { return _CompanyName; }
                set { _CompanyName = value; }
            }
            public string Aliases
            {
                get { return _Aliases; }
                set { _Aliases = value; }
            }
            public string JobTitle
            {
                get { return _JobTitle; }
                set { _JobTitle = value; }
            }
            public int CategoryKey
            {
                get { return _CategoryKey; }
                set { _CategoryKey = value; }
            }
            public string CategoryName
            {
                get { return _CategoryName; }
                set { _CategoryName = value; }
            }
            public int CustomerType
            {
                get { return _CustomerType; }
                set { _CustomerType = value; }
            }
            public string CustomerTypeName
            {
                get { return _CustomerTypeName; }
                set { _CustomerTypeName = value; }
            }
            public int CustomerVendor
            {
                get { return _CustomerVendor; }
                set { _CustomerVendor = value; }
            }
            public string TaxNumber
            {
                get { return _TaxNumber; }
                set { _TaxNumber = value; }
            }
            public string Address
            {
                get { return _Address; }
                set { _Address = value; }
            }
            public string City
            {
                get { return _City; }
                set { _City = value; }
            }
            public string Country
            {
                get { return _Country; }
                set { _Country = value; }
            }
            public string ZipCode
            {
                get { return _ZipCode; }
                set { _ZipCode = value; }
            }
            public string Phone
            {
                get { return _Phone; }
                set { _Phone = value; }
            }
            public string Email
            {
                get { return _Email; }
                set { _Email = value; }
            }
            public string Note
            {
                get { return _Note; }
                set { _Note = value; }
            }
            public string Style
            {
                get { return _Style; }
                set { _Style = value; }
            }
            public string Class
            {
                get { return _Class; }
                set { _Class = value; }
            }
            public string CodeLine
            {
                get { return _CodeLine; }
                set { _CodeLine = value; }
            }
            public string BankAccount
            {
                get { return _BankAccount; }
                set { _BankAccount = value; }
            }
            public string BankName
            {
                get { return _BankName; }
                set { _BankName = value; }
            }
            public string PartnerNumber
            {
                get { return _PartnerNumber; }
                set { _PartnerNumber = value; }
            }
            public int RecordStatus
            {
                get { return _RecordStatus; }
                set { _RecordStatus = value; }
            }
            public DateTime? CreatedOn
            {
                get { return _CreatedOn; }
                set { _CreatedOn = value; }
            }
            public string CreatedBy
            {
                get { return _CreatedBy; }
                set { _CreatedBy = value; }
            }
            public string CreatedName
            {
                get { return _CreatedName; }
                set { _CreatedName = value; }
            }
            public DateTime? ModifiedOn
            {
                get { return _ModifiedOn; }
                set { _ModifiedOn = value; }
            }
            public string ModifiedBy
            {
                get { return _ModifiedBy; }
                set { _ModifiedBy = value; }
            }
            public string ModifiedName
            {
                get { return _ModifiedName; }
                set { _ModifiedName = value; }
            }
            public string Code
            {
                get
                {
                    if (_Message.Length >= 3)
                    {
                        return _Message.Substring(0, 3);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            public string Message
            {
                get { return _Message; }
                set { _Message = value; }
            }
            #endregion

            #region [ Constructor Update Information ]

            public string Create_ServerKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[CRM_Customer] ("
            + " CustomerID ,FullName ,CompanyName ,Aliases ,JobTitle ,CategoryKey ,CategoryName ,CustomerType ,CustomerTypeName ,CustomerVendor ,TaxNumber ,Address ,City ,Country ,ZipCode ,Phone ,Email ,Note ,Style ,Class ,CodeLine ,BankAccount ,BankName ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@CustomerID ,@FullName ,@CompanyName ,@Aliases ,@JobTitle ,@CategoryKey ,@CategoryName ,@CustomerType ,@CustomerTypeName ,@CustomerVendor ,@TaxNumber ,@Address ,@City ,@Country ,@ZipCode ,@Phone ,@Email ,@Note ,@Style ,@Class ,@CodeLine ,@BankAccount ,@BankName ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = _CustomerID;
                    zCommand.Parameters.Add("@FullName", SqlDbType.NVarChar).Value = _FullName;
                    zCommand.Parameters.Add("@CompanyName", SqlDbType.NVarChar).Value = _CompanyName;
                    zCommand.Parameters.Add("@Aliases", SqlDbType.NVarChar).Value = _Aliases;
                    zCommand.Parameters.Add("@JobTitle", SqlDbType.NVarChar).Value = _JobTitle;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@CustomerType", SqlDbType.Int).Value = _CustomerType;
                    zCommand.Parameters.Add("@CustomerTypeName", SqlDbType.NVarChar).Value = _CustomerTypeName;
                    zCommand.Parameters.Add("@CustomerVendor", SqlDbType.Int).Value = _CustomerVendor;
                    zCommand.Parameters.Add("@TaxNumber", SqlDbType.NVarChar).Value = _TaxNumber;
                    zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                    zCommand.Parameters.Add("@City", SqlDbType.NVarChar).Value = _City;
                    zCommand.Parameters.Add("@Country", SqlDbType.NVarChar).Value = _Country;
                    zCommand.Parameters.Add("@ZipCode", SqlDbType.NVarChar).Value = _ZipCode;
                    zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = _Phone;
                    zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = _Email;
                    zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = _Note;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = _CodeLine;
                    zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = _BankAccount;
                    zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = _BankName;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Create_ClientKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[CRM_Customer] ("
            + " CustomerKey ,CustomerID ,FullName ,CompanyName ,Aliases ,JobTitle ,CategoryKey ,CategoryName ,CustomerType ,CustomerTypeName ,CustomerVendor ,TaxNumber ,Address ,City ,Country ,ZipCode ,Phone ,Email ,Note ,Style ,Class ,CodeLine ,BankAccount ,BankName ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@CustomerKey ,@CustomerID ,@FullName ,@CompanyName ,@Aliases ,@JobTitle ,@CategoryKey ,@CategoryName ,@CustomerType ,@CustomerTypeName ,@CustomerVendor ,@TaxNumber ,@Address ,@City ,@Country ,@ZipCode ,@Phone ,@Email ,@Note ,@Style ,@Class ,@CodeLine ,@BankAccount ,@BankName ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_CustomerKey);
                    zCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = _CustomerID;
                    zCommand.Parameters.Add("@FullName", SqlDbType.NVarChar).Value = _FullName;
                    zCommand.Parameters.Add("@CompanyName", SqlDbType.NVarChar).Value = _CompanyName;
                    zCommand.Parameters.Add("@Aliases", SqlDbType.NVarChar).Value = _Aliases;
                    zCommand.Parameters.Add("@JobTitle", SqlDbType.NVarChar).Value = _JobTitle;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@CustomerType", SqlDbType.Int).Value = _CustomerType;
                    zCommand.Parameters.Add("@CustomerTypeName", SqlDbType.NVarChar).Value = _CustomerTypeName;
                    zCommand.Parameters.Add("@CustomerVendor", SqlDbType.Int).Value = _CustomerVendor;
                    zCommand.Parameters.Add("@TaxNumber", SqlDbType.NVarChar).Value = _TaxNumber;
                    zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                    zCommand.Parameters.Add("@City", SqlDbType.NVarChar).Value = _City;
                    zCommand.Parameters.Add("@Country", SqlDbType.NVarChar).Value = _Country;
                    zCommand.Parameters.Add("@ZipCode", SqlDbType.NVarChar).Value = _ZipCode;
                    zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = _Phone;
                    zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = _Email;
                    zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = _Note;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = _CodeLine;
                    zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = _BankAccount;
                    zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = _BankName;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Update()
            {
                string zSQL = "UPDATE [dbo].[CRM_Customer] SET "
                            + " CustomerID = @CustomerID,"
                            + " FullName = @FullName,"
                            + " CompanyName = @CompanyName,"
                            + " Aliases = @Aliases,"
                            + " JobTitle = @JobTitle,"
                            + " CategoryKey = @CategoryKey,"
                            + " CategoryName = @CategoryName,"
                            + " CustomerType = @CustomerType,"
                            + " CustomerTypeName = @CustomerTypeName,"
                            + " CustomerVendor = @CustomerVendor,"
                            + " TaxNumber = @TaxNumber,"
                            + " Address = @Address,"
                            + " City = @City,"
                            + " Country = @Country,"
                            + " ZipCode = @ZipCode,"
                            + " Phone = @Phone,"
                            + " Email = @Email,"
                            + " Note = @Note,"
                            + " Style = @Style,"
                            + " Class = @Class,"
                            + " CodeLine = @CodeLine,"
                            + " BankAccount = @BankAccount,"
                            + " BankName = @BankName,"
                            + " PartnerNumber = @PartnerNumber,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedOn = GetDate(),"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName"
                           + " WHERE CustomerKey = @CustomerKey";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_CustomerKey);
                    zCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = _CustomerID;
                    zCommand.Parameters.Add("@FullName", SqlDbType.NVarChar).Value = _FullName;
                    zCommand.Parameters.Add("@CompanyName", SqlDbType.NVarChar).Value = _CompanyName;
                    zCommand.Parameters.Add("@Aliases", SqlDbType.NVarChar).Value = _Aliases;
                    zCommand.Parameters.Add("@JobTitle", SqlDbType.NVarChar).Value = _JobTitle;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@CustomerType", SqlDbType.Int).Value = _CustomerType;
                    zCommand.Parameters.Add("@CustomerTypeName", SqlDbType.NVarChar).Value = _CustomerTypeName;
                    zCommand.Parameters.Add("@CustomerVendor", SqlDbType.Int).Value = _CustomerVendor;
                    zCommand.Parameters.Add("@TaxNumber", SqlDbType.NVarChar).Value = _TaxNumber;
                    zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                    zCommand.Parameters.Add("@City", SqlDbType.NVarChar).Value = _City;
                    zCommand.Parameters.Add("@Country", SqlDbType.NVarChar).Value = _Country;
                    zCommand.Parameters.Add("@ZipCode", SqlDbType.NVarChar).Value = _ZipCode;
                    zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = _Phone;
                    zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = _Email;
                    zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = _Note;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = _CodeLine;
                    zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = _BankAccount;
                    zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = _BankName;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Delete()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[CRM_Customer] Set RecordStatus = 99 WHERE CustomerKey = @CustomerKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_CustomerKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[CRM_Customer] WHERE CustomerKey = @CustomerKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_CustomerKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            #endregion
        }

        public class Access_Data
        {
            #region[Standard]
            public static DataTable List(string Name, string PartnerNumber)
            {
                DataTable zTable = new DataTable();
                string zSQL = @"
                SELECT CustomerKey, CustomerID, FullName, Aliases FROM [dbo].[CRM_Customer] 
                WHERE RecordStatus <> 99 ";
                if (Name.Trim().Length > 0)
                {
                    zSQL += " AND FullName LIKE @Name";
                }

                zSQL += " AND PartnerNumber = @PartnerNumber   ORDER BY  CreatedOn DESC";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string Result = ex.ToString();
                }
                return zTable;
            }
            public static bool CheckCustomerID(string PartnerNumber, string EmployeeID)
            {
                bool zResult = false;  //  Khong co
                DataTable zTable = new DataTable();
                string zSQL = @"SELECT Count(*) AS Amount FROM [dbo].[CRM_Customer] WHERE PartnerNumber = @PartnerNumber AND CustomerID = @CustomerID AND RecordStatus != 99";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = EmployeeID;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string Result = ex.ToString();
                }
                if (zTable.Rows.Count > 0)
                {
                    int zAmount = 0;
                    DataRow zRow = zTable.Rows[0];
                    zAmount = int.Parse(zRow["Amount"].ToString());
                    if (zAmount == 0)
                    {
                        zResult = true;
                    }
                    else
                    {
                        zResult = false;
                    }
                }
                return zResult;
            }
            #endregion
        }

        //CRM_Person
        public class Person_Info
        {

            #region [ Field Name ]
            private string _PersonKey = "";
            private string _PersonID = "";
            private string _CustomerKey = "";
            private string _LastName = "";
            private string _FirstName = "";
            private DateTime? _BirthDay = null;
            private int _Gender = 0;
            private string _BirthPlace = "";
            private string _Nationality = "";
            private string _PassportNumber = "";
            private DateTime? _IssueDate = null;
            private DateTime? _ExpireDate = null;
            private string _IssuePlace = "";
            private string _Department = "";
            private string _AddressRegister = "";
            private string _AddressContact = "";
            private string _AddressBusiness = "";
            private string _MobiPhone = "";
            private string _PartnerNumber = "";
            private int _RecordStatus = 0;
            private DateTime? _CreatedOn = null;
            private string _CreatedBy = "";
            private string _CreatedName = "";
            private DateTime? _ModifiedOn = null;
            private string _ModifiedBy = "";
            private string _ModifiedName = "";
            private string _Message = "";
            #endregion

            #region [ Constructor Get Information ]
            public Person_Info()
            {
                Guid zNewID = Guid.NewGuid();
                _PersonKey = zNewID.ToString();
            }
            public Person_Info(string PersonKey)
            {
                string zSQL = "SELECT * FROM [dbo].[CRM_Person] WHERE PersonKey = @PersonKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    if (PersonKey.Length > 0)
                    {
                        zCommand.Parameters.Add("@PersonKey", SqlDbType.UniqueIdentifier).Value = new Guid(PersonKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@PersonKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }

                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _PersonKey = zReader["PersonKey"].ToString();
                        _PersonID = zReader["PersonID"].ToString();
                        _CustomerKey = zReader["CustomerKey"].ToString();
                        _LastName = zReader["LastName"].ToString();
                        _FirstName = zReader["FirstName"].ToString();
                        if (zReader["BirthDay"] != DBNull.Value)
                        {
                            _BirthDay = (DateTime)zReader["BirthDay"];
                        }

                        _Gender = int.Parse(zReader["Gender"].ToString());
                        _BirthPlace = zReader["BirthPlace"].ToString();
                        _Nationality = zReader["Nationality"].ToString();
                        _PassportNumber = zReader["PassportNumber"].ToString();
                        if (zReader["IssueDate"] != DBNull.Value)
                        {
                            _IssueDate = (DateTime)zReader["IssueDate"];
                        }

                        if (zReader["ExpireDate"] != DBNull.Value)
                        {
                            _ExpireDate = (DateTime)zReader["ExpireDate"];
                        }

                        _IssuePlace = zReader["IssuePlace"].ToString();
                        _Department = zReader["Department"].ToString();
                        _AddressRegister = zReader["AddressRegister"].ToString();
                        _AddressContact = zReader["AddressContact"].ToString();
                        _AddressBusiness = zReader["AddressBusiness"].ToString();
                        _MobiPhone = zReader["MobiPhone"].ToString();
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }
            public void CustomerPerson_Info(string CustomerKey)
            {
                string zSQL = "SELECT * FROM [dbo].[CRM_Person] WHERE CustomerKey = @CustomerKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    if (CustomerKey.Length > 0)
                    {
                        zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(CustomerKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }

                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _PersonKey = zReader["PersonKey"].ToString();
                        _PersonID = zReader["PersonID"].ToString();
                        _CustomerKey = zReader["CustomerKey"].ToString();
                        _LastName = zReader["LastName"].ToString();
                        _FirstName = zReader["FirstName"].ToString();
                        if (zReader["BirthDay"] != DBNull.Value)
                        {
                            _BirthDay = (DateTime)zReader["BirthDay"];
                        }

                        _Gender = int.Parse(zReader["Gender"].ToString());
                        _BirthPlace = zReader["BirthPlace"].ToString();
                        _Nationality = zReader["Nationality"].ToString();
                        _PassportNumber = zReader["PassportNumber"].ToString();
                        if (zReader["IssueDate"] != DBNull.Value)
                        {
                            _IssueDate = (DateTime)zReader["IssueDate"];
                        }

                        if (zReader["ExpireDate"] != DBNull.Value)
                        {
                            _ExpireDate = (DateTime)zReader["ExpireDate"];
                        }

                        _IssuePlace = zReader["IssuePlace"].ToString();
                        _Department = zReader["Department"].ToString();
                        _AddressRegister = zReader["AddressRegister"].ToString();
                        _AddressContact = zReader["AddressContact"].ToString();
                        _AddressBusiness = zReader["AddressBusiness"].ToString();
                        _MobiPhone = zReader["MobiPhone"].ToString();
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }
            #endregion

            #region [ Properties ]
            public string PersonKey
            {
                get { return _PersonKey; }
                set { _PersonKey = value; }
            }
            public string PersonID
            {
                get { return _PersonID; }
                set { _PersonID = value; }
            }
            public string CustomerKey
            {
                get { return _CustomerKey; }
                set { _CustomerKey = value; }
            }
            public string LastName
            {
                get { return _LastName; }
                set { _LastName = value; }
            }
            public string FirstName
            {
                get { return _FirstName; }
                set { _FirstName = value; }
            }
            public DateTime? BirthDay
            {
                get { return _BirthDay; }
                set { _BirthDay = value; }
            }
            public int Gender
            {
                get { return _Gender; }
                set { _Gender = value; }
            }
            public string BirthPlace
            {
                get { return _BirthPlace; }
                set { _BirthPlace = value; }
            }
            public string Nationality
            {
                get { return _Nationality; }
                set { _Nationality = value; }
            }
            public string PassportNumber
            {
                get { return _PassportNumber; }
                set { _PassportNumber = value; }
            }
            public DateTime? IssueDate
            {
                get { return _IssueDate; }
                set { _IssueDate = value; }
            }
            public DateTime? ExpireDate
            {
                get { return _ExpireDate; }
                set { _ExpireDate = value; }
            }
            public string IssuePlace
            {
                get { return _IssuePlace; }
                set { _IssuePlace = value; }
            }
            public string Department
            {
                get { return _Department; }
                set { _Department = value; }
            }
            public string AddressRegister
            {
                get { return _AddressRegister; }
                set { _AddressRegister = value; }
            }
            public string AddressContact
            {
                get { return _AddressContact; }
                set { _AddressContact = value; }
            }
            public string AddressBusiness
            {
                get { return _AddressBusiness; }
                set { _AddressBusiness = value; }
            }
            public string MobiPhone
            {
                get { return _MobiPhone; }
                set { _MobiPhone = value; }
            }
            public string PartnerNumber
            {
                get { return _PartnerNumber; }
                set { _PartnerNumber = value; }
            }
            public int RecordStatus
            {
                get { return _RecordStatus; }
                set { _RecordStatus = value; }
            }
            public DateTime? CreatedOn
            {
                get { return _CreatedOn; }
                set { _CreatedOn = value; }
            }
            public string CreatedBy
            {
                get { return _CreatedBy; }
                set { _CreatedBy = value; }
            }
            public string CreatedName
            {
                get { return _CreatedName; }
                set { _CreatedName = value; }
            }
            public DateTime? ModifiedOn
            {
                get { return _ModifiedOn; }
                set { _ModifiedOn = value; }
            }
            public string ModifiedBy
            {
                get { return _ModifiedBy; }
                set { _ModifiedBy = value; }
            }
            public string ModifiedName
            {
                get { return _ModifiedName; }
                set { _ModifiedName = value; }
            }
            public string Code
            {
                get
                {
                    if (_Message.Length >= 3)
                    {
                        return _Message.Substring(0, 3);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            public string Message
            {
                get { return _Message; }
                set { _Message = value; }
            }
            #endregion

            #region [ Constructor Update Information ]

            public string Create_ServerKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[CRM_Person] ("
            + " PersonID ,CustomerKey ,LastName ,FirstName ,BirthDay ,Gender ,BirthPlace ,Nationality ,PassportNumber ,IssueDate ,ExpireDate ,IssuePlace ,Department ,AddressRegister ,AddressContact ,AddressBusiness ,MobiPhone ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@PersonID ,@CustomerKey ,@LastName ,@FirstName ,@BirthDay ,@Gender ,@BirthPlace ,@Nationality ,@PassportNumber ,@IssueDate ,@ExpireDate ,@IssuePlace ,@Department ,@AddressRegister ,@AddressContact ,@AddressBusiness ,@MobiPhone ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@PersonID", SqlDbType.NVarChar).Value = _PersonID;
                    if (_CustomerKey.Length > 0)
                    {
                        zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_CustomerKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }

                    zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName;
                    zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName;
                    if (_BirthDay == null)
                    {
                        zCommand.Parameters.Add("@BirthDay", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@BirthDay", SqlDbType.DateTime).Value = _BirthDay;
                    }

                    zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = _Gender;
                    zCommand.Parameters.Add("@BirthPlace", SqlDbType.NVarChar).Value = _BirthPlace;
                    zCommand.Parameters.Add("@Nationality", SqlDbType.NVarChar).Value = _Nationality;
                    zCommand.Parameters.Add("@PassportNumber", SqlDbType.NVarChar).Value = _PassportNumber;
                    if (_IssueDate == null)
                    {
                        zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = _IssueDate;
                    }

                    if (_ExpireDate == null)
                    {
                        zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = _ExpireDate;
                    }

                    zCommand.Parameters.Add("@IssuePlace", SqlDbType.NVarChar).Value = _IssuePlace;
                    zCommand.Parameters.Add("@Department", SqlDbType.NVarChar).Value = _Department;
                    zCommand.Parameters.Add("@AddressRegister", SqlDbType.NVarChar).Value = _AddressRegister;
                    zCommand.Parameters.Add("@AddressContact", SqlDbType.NVarChar).Value = _AddressContact;
                    zCommand.Parameters.Add("@AddressBusiness", SqlDbType.NVarChar).Value = _AddressBusiness;
                    zCommand.Parameters.Add("@MobiPhone", SqlDbType.NVarChar).Value = _MobiPhone;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Create_ClientKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[CRM_Person] ("
            + " PersonKey ,PersonID ,CustomerKey ,LastName ,FirstName ,BirthDay ,Gender ,BirthPlace ,Nationality ,PassportNumber ,IssueDate ,ExpireDate ,IssuePlace ,Department ,AddressRegister ,AddressContact ,AddressBusiness ,MobiPhone ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@PersonKey ,@PersonID ,@CustomerKey ,@LastName ,@FirstName ,@BirthDay ,@Gender ,@BirthPlace ,@Nationality ,@PassportNumber ,@IssueDate ,@ExpireDate ,@IssuePlace ,@Department ,@AddressRegister ,@AddressContact ,@AddressBusiness ,@MobiPhone ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    if (_PersonKey.Length > 0)
                    {
                        zCommand.Parameters.Add("@PersonKey", SqlDbType.UniqueIdentifier).Value = new Guid(_PersonKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@PersonKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }

                    zCommand.Parameters.Add("@PersonID", SqlDbType.NVarChar).Value = _PersonID;
                    if (_CustomerKey.Length > 0)
                    {
                        zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_CustomerKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }

                    zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName;
                    zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName;
                    if (_BirthDay == null)
                    {
                        zCommand.Parameters.Add("@BirthDay", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@BirthDay", SqlDbType.DateTime).Value = _BirthDay;
                    }

                    zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = _Gender;
                    zCommand.Parameters.Add("@BirthPlace", SqlDbType.NVarChar).Value = _BirthPlace;
                    zCommand.Parameters.Add("@Nationality", SqlDbType.NVarChar).Value = _Nationality;
                    zCommand.Parameters.Add("@PassportNumber", SqlDbType.NVarChar).Value = _PassportNumber;
                    if (_IssueDate == null)
                    {
                        zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = _IssueDate;
                    }

                    if (_ExpireDate == null)
                    {
                        zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = _ExpireDate;
                    }

                    zCommand.Parameters.Add("@IssuePlace", SqlDbType.NVarChar).Value = _IssuePlace;
                    zCommand.Parameters.Add("@Department", SqlDbType.NVarChar).Value = _Department;
                    zCommand.Parameters.Add("@AddressRegister", SqlDbType.NVarChar).Value = _AddressRegister;
                    zCommand.Parameters.Add("@AddressContact", SqlDbType.NVarChar).Value = _AddressContact;
                    zCommand.Parameters.Add("@AddressBusiness", SqlDbType.NVarChar).Value = _AddressBusiness;
                    zCommand.Parameters.Add("@MobiPhone", SqlDbType.NVarChar).Value = _MobiPhone;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Update()
            {
                string zSQL = "UPDATE [dbo].[CRM_Person] SET "
                            + " PersonID = @PersonID,"
                            + " CustomerKey = @CustomerKey,"
                            + " LastName = @LastName,"
                            + " FirstName = @FirstName,"
                            + " BirthDay = @BirthDay,"
                            + " Gender = @Gender,"
                            + " BirthPlace = @BirthPlace,"
                            + " Nationality = @Nationality,"
                            + " PassportNumber = @PassportNumber,"
                            + " IssueDate = @IssueDate,"
                            + " ExpireDate = @ExpireDate,"
                            + " IssuePlace = @IssuePlace,"
                            + " Department = @Department,"
                            + " AddressRegister = @AddressRegister,"
                            + " AddressContact = @AddressContact,"
                            + " AddressBusiness = @AddressBusiness,"
                            + " MobiPhone = @MobiPhone,"
                            + " PartnerNumber = @PartnerNumber,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedOn = GetDate(),"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName"
                           + " WHERE PersonKey = @PersonKey";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    if (_PersonKey.Length > 0)
                    {
                        zCommand.Parameters.Add("@PersonKey", SqlDbType.UniqueIdentifier).Value = new Guid(_PersonKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@PersonKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }

                    zCommand.Parameters.Add("@PersonID", SqlDbType.NVarChar).Value = _PersonID;
                    if (_CustomerKey.Length > 0)
                    {
                        zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_CustomerKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }

                    zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName;
                    zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName;
                    if (_BirthDay == null)
                    {
                        zCommand.Parameters.Add("@BirthDay", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@BirthDay", SqlDbType.DateTime).Value = _BirthDay;
                    }

                    zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = _Gender;
                    zCommand.Parameters.Add("@BirthPlace", SqlDbType.NVarChar).Value = _BirthPlace;
                    zCommand.Parameters.Add("@Nationality", SqlDbType.NVarChar).Value = _Nationality;
                    zCommand.Parameters.Add("@PassportNumber", SqlDbType.NVarChar).Value = _PassportNumber;
                    if (_IssueDate == null)
                    {
                        zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = _IssueDate;
                    }

                    if (_ExpireDate == null)
                    {
                        zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = _ExpireDate;
                    }

                    zCommand.Parameters.Add("@IssuePlace", SqlDbType.NVarChar).Value = _IssuePlace;
                    zCommand.Parameters.Add("@Department", SqlDbType.NVarChar).Value = _Department;
                    zCommand.Parameters.Add("@AddressRegister", SqlDbType.NVarChar).Value = _AddressRegister;
                    zCommand.Parameters.Add("@AddressContact", SqlDbType.NVarChar).Value = _AddressContact;
                    zCommand.Parameters.Add("@AddressBusiness", SqlDbType.NVarChar).Value = _AddressBusiness;
                    zCommand.Parameters.Add("@MobiPhone", SqlDbType.NVarChar).Value = _MobiPhone;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Delete()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[CRM_Person] Set RecordStatus = 99 WHERE PersonKey = @PersonKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PersonKey", SqlDbType.UniqueIdentifier).Value = new Guid(_PersonKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[CRM_Person] WHERE PersonKey = @PersonKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PersonKey", SqlDbType.UniqueIdentifier).Value = new Guid(_PersonKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            #endregion
        }
        #endregion
    }
}