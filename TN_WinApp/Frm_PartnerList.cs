﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using TN_Connection;
using TN_Tools;

namespace TN_WinApp
{
    public partial class Frm_PartnerList : Form
    {
        string _PartnerKey = "";
        //private string _FormUrl = "/HRM/Frm_EmployeeList";

        //public string FormUrl
        //{
        //    get
        //    {
        //        return _FormUrl;
        //    }

        //    set
        //    {
        //        _FormUrl = value;
        //    }
        //}
        public Frm_PartnerList()
        {
            InitializeComponent();
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;

            btn_Search.Click += BtnSearch_Click;
            btnSave.Click += BtnSave_Click;
            btnAddNew.Click += BtnAddNew_Click;
            btnDelete.Click += BtnDelete_Click;

            LVData.Click += LVData_Click;
            //format Form
            this.Text = "Danh sách đối tác";
            this.StartPosition = FormStartPosition.CenterScreen;
            this.FormBorderStyle = FormBorderStyle.None;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            DesignLayout(LVData);
        }

        private void Frm_PartnerList_Load(object sender, EventArgs e)
        {
            //Get_Auth();
            //if (_RoleForm.RoleRead)
            //{

            string zSQLSearch = @"SELECT BusinessKey, BusinessName FROM SYS_Business ORDER BY Rank";
            string zSQLBusiness = @"SELECT BusinessKey, BusinessName FROM SYS_Business ORDER BY Rank";
            string zSQLPlayRound = @"SELECT GroundKey,GroundName FROM [dbo].[SYS_Play_Ground]";
            LoadDataToToolbox.KryptonComboBox(cbo_Search, zSQLSearch, "--Chọn--");
            LoadDataToToolbox.KryptonComboBox(cbo_Business, zSQLBusiness, "");
            LoadDataToToolbox.KryptonComboBox(cboPlayGround, zSQLPlayRound, "--Chọn--");
            LoadListView();
            //}
        }


        private void BtnSearch_Click(object sender, EventArgs e)
        {
            LoadListView();
        }
        private void BtnAddNew_Click(object sender, EventArgs e)
        {
            ClearForm();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            string zResult = "";
            string zMessage = "";
            zMessage = CheckBeforSave();
            if (zMessage.Length == 0)
            {
                zResult = Save();
                if (zResult.Substring(0, 3) == "200" ||
                    zResult.Substring(0, 3) == "201")
                {
                    MessageBox.Show("Cập nhật nhân viên thành công!");
                    LoadListView();
                    LoadData();

                }
                else
                {
                    MessageBox.Show("Lỗi.Vui lòng liên hệ IT.Chi tiết lỗi mã : \n " + zResult);
                }
            }
            else
            {
                MessageBox.Show(zMessage);
            }
        }
        private string CheckBeforSave()
        {
            string zResult = "";
            if (txtPartnerName.Text.Trim().Length == 0)
            {
                zResult += "Vui tên đối tác! \n";
                txtPartnerName.StateCommon.Back.Color1 = Color.FromArgb(255, 102, 102);
            }
            else
            {
                txtPartnerName.StateCommon.Back.Color1 = Color.White;
            }
            
            
            return zResult;
        }

        private string Save()
        {
            Partner_Info zInfo;
            if (_PartnerKey != "")
            {
                zInfo = new Partner_Info(_PartnerKey);
            }
            else
            {
                zInfo = new Partner_Info();
            }
            zInfo.PartnerID = txtPartnerID.Text.Trim();
            zInfo.PartnerName = txtPartnerName.Text.Trim();
            if(cbo_Active.SelectedIndex==0)
            {
                zInfo.Active = false;
                zInfo.DateActive = null;
            }
           else
            {
                zInfo.Active = true;
                zInfo.DateActive = DateTime.Now;
            }
            zInfo.BusinessKey = cbo_Business.SelectedValue.ToInt();
            zInfo.BusinessName = cbo_Business.SelectedText;
            if (cbo_PlayVip.SelectedIndex == 0)
            {
                zInfo.PlayVip = false;
            }
            else
            {
                zInfo.PlayVip = true;
            }
            zInfo.PlayGround = cboPlayGround.SelectedValue.ToString();

           

            if (_PartnerKey == "")
            {
                zInfo.Create_ClientKey();
                _PartnerKey = zInfo.PartnerNumber;
            }
            else
            {
                zInfo.Update();
                _PartnerKey = zInfo.PartnerNumber;
            }

            return zInfo.Message;
        }
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dlr == DialogResult.Yes)
            {
                string zResult = Delete();

                if (zResult.Substring(0, 3) == "200" ||
                    zResult.Substring(0, 3) == "201")
                {
                    MessageBox.Show("Đã xóa thành công !");
                    LoadListView();
                    ClearForm();
                }
                else
                {
                    MessageBox.Show("Lỗi.Vui lòng liên hệ IT.Chi tiết lỗi mã: \n " + zResult);
                }
            }
        }
        private string Delete()
        {
            Partner_Info zInfo = new Partner_Info();
            zInfo.PartnerNumber = _PartnerKey;
            zInfo.Delete();
            return zInfo.Message;
        }
        #region[Progess]
        private void ClearForm()
        {
            _PartnerKey = "";
            LVData.SelectedItems.Clear();
            txtPartnerNumber.Clear();
            txtPartnerID.Clear();
            txtPartnerName.Clear();
            cbo_Active.SelectedIndex=0;
            cbo_PlayVip.SelectedIndex=0;
            cboPlayGround.SelectedIndex = 0;
            dteJoinDate.Value = DateTime.MinValue;
        }
        private void LoadData()
        {
            Partner_Info zInfo = new Partner_Info(_PartnerKey);
            txtPartnerNumber.Text = zInfo.PartnerNumber;
            txtPartnerID.Text = zInfo.PartnerID;
            txtPartnerName.Text = zInfo.PartnerName;
            cbo_Business.SelectedValue = zInfo.BusinessKey;
            cboPlayGround.SelectedValue = zInfo.PlayGround;
            if (zInfo.JoinDate == null)
            {
                dteJoinDate.Value = DateTime.MinValue;
            }
            else
            {
                dteJoinDate.Value = zInfo.JoinDate.Value;
            }

            if (zInfo.PlayVip == true)
            {
                cbo_PlayVip.SelectedIndex = 0;
            }
            else
            {
                cbo_PlayVip.SelectedIndex = 1;
            }

            if (zInfo.Active == true)
            {
                cbo_Active.SelectedIndex=0;
            }
            else
            {
                cbo_Active.SelectedIndex = 1;
            }

           
        }
        #endregion
        #region [ListView]
        private void DesignLayout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã ID";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên";
            colHead.Width = 140;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            TNPaintControl.DrawLVStyle(ref LVData);
        }
        private void LoadListView()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable zTable = Access_Data.List( (int)cbo_Search.SelectedValue, txtSearch.Text);
            LV.Items.Clear();
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow zRow = zTable.Rows[i];
                lvi.Tag = zRow["PartnerNumber"].ToString();
                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["PartnerID"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["PartnerName"].ToString();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }
            this.Cursor = Cursors.Default;
        }
        private void LVData_Click(object sender, EventArgs e)
        {
            if (LVData.SelectedItems.Count > 0)
            {
                   _PartnerKey = LVData.SelectedItems[0].Tag.ToString();
                    LoadData();
            }
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;



        private void Frm_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }
        private void Frm_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }
        private void Frm_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.None;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion


        #region [ Access Info-Data ]
        public class Partner_Info
        {

            #region [ Field Name ]
            private string _PartnerNumber = "";
            private string _PartnerName = "";
            private string _PartnerID = "";
            private int _BusinessKey = 0;
            private string _BusinessName = "";
            private string _Logo_Small = "";
            private string _Logo_Large = "";
            private int _AccountAmount = 0;
            private string _Logo_Mobile = "";
            private string _HomepageDesktop = "";
            private string _HomepageMobile = "";
            private string _PlayGround = "";
            private bool _PlayMaster;
            private bool _PlayVip;
            private DateTime? _JoinDate = null;
            private bool _Active;
            private DateTime? _DateActive = null;
            private int _RecordStatus = 0;
            private DateTime? _CreatedOn = null;
            private string _CreatedBy = "";
            private string _CreatedName = "";
            private DateTime? _ModifiedOn = null;
            private string _ModifiedBy = "";
            private string _ModifiedName = "";
            private string _Message = "";
            #endregion

            #region [ Constructor Get Information ]
            public Partner_Info()
            {
                Guid zNewID = Guid.NewGuid();
                _PartnerNumber = zNewID.ToString();
            }
            public Partner_Info(string PartnerNumber)
            {
                string zSQL = "SELECT * FROM [dbo].[SYS_Partner] WHERE PartnerNumber = @PartnerNumber AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _PartnerName = zReader["PartnerName"].ToString();
                        _PartnerID = zReader["PartnerID"].ToString();
                        _BusinessKey = int.Parse(zReader["BusinessKey"].ToString());
                        _BusinessName = zReader["BusinessName"].ToString();
                        _Logo_Small = zReader["Logo_Small"].ToString();
                        _Logo_Large = zReader["Logo_Large"].ToString();
                        _AccountAmount = int.Parse(zReader["AccountAmount"].ToString());
                        _Logo_Mobile = zReader["Logo_Mobile"].ToString();
                        _HomepageDesktop = zReader["HomepageDesktop"].ToString();
                        _HomepageMobile = zReader["HomepageMobile"].ToString();
                        _PlayGround = zReader["PlayGround"].ToString();
                        _PlayMaster = (bool)zReader["PlayMaster"];
                        _PlayVip = (bool)zReader["PlayVip"];
                        if (zReader["JoinDate"] != DBNull.Value)
                            _JoinDate = (DateTime)zReader["JoinDate"];
                        _Active = (bool)zReader["Active"];
                        if (zReader["DateActive"] != DBNull.Value)
                            _DateActive = (DateTime)zReader["DateActive"];
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }

            #endregion

            #region [ Properties ]
            public string PartnerNumber
            {
                get { return _PartnerNumber; }
                set { _PartnerNumber = value; }
            }
            public string PartnerName
            {
                get { return _PartnerName; }
                set { _PartnerName = value; }
            }
            public string PartnerID
            {
                get { return _PartnerID; }
                set { _PartnerID = value; }
            }
            public int BusinessKey
            {
                get { return _BusinessKey; }
                set { _BusinessKey = value; }
            }
            public string BusinessName
            {
                get { return _BusinessName; }
                set { _BusinessName = value; }
            }
            public string Logo_Small
            {
                get { return _Logo_Small; }
                set { _Logo_Small = value; }
            }
            public string Logo_Large
            {
                get { return _Logo_Large; }
                set { _Logo_Large = value; }
            }
            public int AccountAmount
            {
                get { return _AccountAmount; }
                set { _AccountAmount = value; }
            }
            public string Logo_Mobile
            {
                get { return _Logo_Mobile; }
                set { _Logo_Mobile = value; }
            }
            public string HomepageDesktop
            {
                get { return _HomepageDesktop; }
                set { _HomepageDesktop = value; }
            }
            public string HomepageMobile
            {
                get { return _HomepageMobile; }
                set { _HomepageMobile = value; }
            }
            public string PlayGround
            {
                get { return _PlayGround; }
                set { _PlayGround = value; }
            }
            public bool PlayMaster
            {
                get { return _PlayMaster; }
                set { _PlayMaster = value; }
            }
            public bool PlayVip
            {
                get { return _PlayVip; }
                set { _PlayVip = value; }
            }
            public DateTime? JoinDate
            {
                get { return _JoinDate; }
                set { _JoinDate = value; }
            }
            public bool Active
            {
                get { return _Active; }
                set { _Active = value; }
            }
            public DateTime? DateActive
            {
                get { return _DateActive; }
                set { _DateActive = value; }
            }
            public int RecordStatus
            {
                get { return _RecordStatus; }
                set { _RecordStatus = value; }
            }
            public DateTime? CreatedOn
            {
                get { return _CreatedOn; }
                set { _CreatedOn = value; }
            }
            public string CreatedBy
            {
                get { return _CreatedBy; }
                set { _CreatedBy = value; }
            }
            public string CreatedName
            {
                get { return _CreatedName; }
                set { _CreatedName = value; }
            }
            public DateTime? ModifiedOn
            {
                get { return _ModifiedOn; }
                set { _ModifiedOn = value; }
            }
            public string ModifiedBy
            {
                get { return _ModifiedBy; }
                set { _ModifiedBy = value; }
            }
            public string ModifiedName
            {
                get { return _ModifiedName; }
                set { _ModifiedName = value; }
            }
            public string Code
            {
                get
                {
                    if (_Message.Length >= 3)
                        return _Message.Substring(0, 3);
                    else return "";
                }
            }
            public string Message
            {
                get { return _Message; }
                set { _Message = value; }
            }
            #endregion

            #region [ Constructor Update Information ]

            public string Create_ServerKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[SYS_Partner] ("
            + " PartnerName ,PartnerID ,BusinessKey ,BusinessName ,Logo_Small ,Logo_Large ,AccountAmount ,Logo_Mobile ,HomepageDesktop ,HomepageMobile ,PlayGround ,PlayMaster ,PlayVip ,JoinDate ,Active ,DateActive ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@PartnerName ,@PartnerID ,@BusinessKey ,@BusinessName ,@Logo_Small ,@Logo_Large ,@AccountAmount ,@Logo_Mobile ,@HomepageDesktop ,@HomepageMobile ,@PlayGround ,@PlayMaster ,@PlayVip ,@JoinDate ,@Active ,@DateActive ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@PartnerName", SqlDbType.NVarChar).Value = _PartnerName;
                    zCommand.Parameters.Add("@PartnerID", SqlDbType.NVarChar).Value = _PartnerID;
                    zCommand.Parameters.Add("@BusinessKey", SqlDbType.Int).Value = _BusinessKey;
                    zCommand.Parameters.Add("@BusinessName", SqlDbType.NVarChar).Value = _BusinessName;
                    zCommand.Parameters.Add("@Logo_Small", SqlDbType.NVarChar).Value = _Logo_Small;
                    zCommand.Parameters.Add("@Logo_Large", SqlDbType.NVarChar).Value = _Logo_Large;
                    zCommand.Parameters.Add("@AccountAmount", SqlDbType.Int).Value = _AccountAmount;
                    zCommand.Parameters.Add("@Logo_Mobile", SqlDbType.NVarChar).Value = _Logo_Mobile;
                    zCommand.Parameters.Add("@HomepageDesktop", SqlDbType.NVarChar).Value = _HomepageDesktop;
                    zCommand.Parameters.Add("@HomepageMobile", SqlDbType.NVarChar).Value = _HomepageMobile;
                    zCommand.Parameters.Add("@PlayGround", SqlDbType.NVarChar).Value = _PlayGround;
                    zCommand.Parameters.Add("@PlayMaster", SqlDbType.Bit).Value = _PlayMaster;
                    zCommand.Parameters.Add("@PlayVip", SqlDbType.Bit).Value = _PlayVip;
                    if (_JoinDate == null)
                        zCommand.Parameters.Add("@JoinDate", SqlDbType.DateTime).Value = DBNull.Value;
                    else
                        zCommand.Parameters.Add("@JoinDate", SqlDbType.DateTime).Value = _JoinDate;
                    zCommand.Parameters.Add("@Active", SqlDbType.Bit).Value = _Active;
                    if (_DateActive == null)
                        zCommand.Parameters.Add("@DateActive", SqlDbType.DateTime).Value = DBNull.Value;
                    else
                        zCommand.Parameters.Add("@DateActive", SqlDbType.DateTime).Value = _DateActive;
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Create_ClientKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[SYS_Partner] ("
            + " PartnerNumber ,PartnerName ,PartnerID ,BusinessKey ,BusinessName ,Logo_Small ,Logo_Large ,AccountAmount ,Logo_Mobile ,HomepageDesktop ,HomepageMobile ,PlayGround ,PlayMaster ,PlayVip ,JoinDate ,Active ,DateActive ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@PartnerNumber ,@PartnerName ,@PartnerID ,@BusinessKey ,@BusinessName ,@Logo_Small ,@Logo_Large ,@AccountAmount ,@Logo_Mobile ,@HomepageDesktop ,@HomepageMobile ,@PlayGround ,@PlayMaster ,@PlayVip ,@JoinDate ,@Active ,@DateActive ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@PartnerName", SqlDbType.NVarChar).Value = _PartnerName;
                    zCommand.Parameters.Add("@PartnerID", SqlDbType.NVarChar).Value = _PartnerID;
                    zCommand.Parameters.Add("@BusinessKey", SqlDbType.Int).Value = _BusinessKey;
                    zCommand.Parameters.Add("@BusinessName", SqlDbType.NVarChar).Value = _BusinessName;
                    zCommand.Parameters.Add("@Logo_Small", SqlDbType.NVarChar).Value = _Logo_Small;
                    zCommand.Parameters.Add("@Logo_Large", SqlDbType.NVarChar).Value = _Logo_Large;
                    zCommand.Parameters.Add("@AccountAmount", SqlDbType.Int).Value = _AccountAmount;
                    zCommand.Parameters.Add("@Logo_Mobile", SqlDbType.NVarChar).Value = _Logo_Mobile;
                    zCommand.Parameters.Add("@HomepageDesktop", SqlDbType.NVarChar).Value = _HomepageDesktop;
                    zCommand.Parameters.Add("@HomepageMobile", SqlDbType.NVarChar).Value = _HomepageMobile;
                    zCommand.Parameters.Add("@PlayGround", SqlDbType.NVarChar).Value = _PlayGround;
                    zCommand.Parameters.Add("@PlayMaster", SqlDbType.Bit).Value = _PlayMaster;
                    zCommand.Parameters.Add("@PlayVip", SqlDbType.Bit).Value = _PlayVip;
                    if (_JoinDate == null)
                        zCommand.Parameters.Add("@JoinDate", SqlDbType.DateTime).Value = DBNull.Value;
                    else
                        zCommand.Parameters.Add("@JoinDate", SqlDbType.DateTime).Value = _JoinDate;
                    zCommand.Parameters.Add("@Active", SqlDbType.Bit).Value = _Active;
                    if (_DateActive == null)
                        zCommand.Parameters.Add("@DateActive", SqlDbType.DateTime).Value = DBNull.Value;
                    else
                        zCommand.Parameters.Add("@DateActive", SqlDbType.DateTime).Value = _DateActive;
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Update()
            {
                string zSQL = "UPDATE [dbo].[SYS_Partner] SET "
                            + " PartnerName = @PartnerName,"
                            + " PartnerID = @PartnerID,"
                            + " BusinessKey = @BusinessKey,"
                            + " BusinessName = @BusinessName,"
                            + " Logo_Small = @Logo_Small,"
                            + " Logo_Large = @Logo_Large,"
                            + " AccountAmount = @AccountAmount,"
                            + " Logo_Mobile = @Logo_Mobile,"
                            + " HomepageDesktop = @HomepageDesktop,"
                            + " HomepageMobile = @HomepageMobile,"
                            + " PlayGround = @PlayGround,"
                            + " PlayMaster = @PlayMaster,"
                            + " PlayVip = @PlayVip,"
                            + " JoinDate = @JoinDate,"
                            + " Active = @Active,"
                            + " DateActive = @DateActive,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedOn = GetDate(),"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName"
                           + " WHERE PartnerNumber = @PartnerNumber";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@PartnerName", SqlDbType.NVarChar).Value = _PartnerName;
                    zCommand.Parameters.Add("@PartnerID", SqlDbType.NVarChar).Value = _PartnerID;
                    zCommand.Parameters.Add("@BusinessKey", SqlDbType.Int).Value = _BusinessKey;
                    zCommand.Parameters.Add("@BusinessName", SqlDbType.NVarChar).Value = _BusinessName;
                    zCommand.Parameters.Add("@Logo_Small", SqlDbType.NVarChar).Value = _Logo_Small;
                    zCommand.Parameters.Add("@Logo_Large", SqlDbType.NVarChar).Value = _Logo_Large;
                    zCommand.Parameters.Add("@AccountAmount", SqlDbType.Int).Value = _AccountAmount;
                    zCommand.Parameters.Add("@Logo_Mobile", SqlDbType.NVarChar).Value = _Logo_Mobile;
                    zCommand.Parameters.Add("@HomepageDesktop", SqlDbType.NVarChar).Value = _HomepageDesktop;
                    zCommand.Parameters.Add("@HomepageMobile", SqlDbType.NVarChar).Value = _HomepageMobile;
                    zCommand.Parameters.Add("@PlayGround", SqlDbType.NVarChar).Value = _PlayGround;
                    zCommand.Parameters.Add("@PlayMaster", SqlDbType.Bit).Value = _PlayMaster;
                    zCommand.Parameters.Add("@PlayVip", SqlDbType.Bit).Value = _PlayVip;
                    if (_JoinDate == null)
                        zCommand.Parameters.Add("@JoinDate", SqlDbType.DateTime).Value = DBNull.Value;
                    else
                        zCommand.Parameters.Add("@JoinDate", SqlDbType.DateTime).Value = _JoinDate;
                    zCommand.Parameters.Add("@Active", SqlDbType.Bit).Value = _Active;
                    if (_DateActive == null)
                        zCommand.Parameters.Add("@DateActive", SqlDbType.DateTime).Value = DBNull.Value;
                    else
                        zCommand.Parameters.Add("@DateActive", SqlDbType.DateTime).Value = _DateActive;
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Delete()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[SYS_Partner] Set RecordStatus = 99 WHERE PartnerNumber = @PartnerNumber";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[SYS_Partner] WHERE PartnerNumber = @PartnerNumber";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            #endregion
        }
        public class Access_Data
        {
            public static DataTable List(int Business, string Search)
            {
                {
                    DataTable zTable = new DataTable();
                    string zSQL = "SELECT  * FROM SYS_Partner WHERE RecordStatus != 99";
                    if (Business != 0)
                    {
                        zSQL += " AND BusinessKey = @Business ";
                    }
                    if (Search.Trim().Length > 0)
                    {
                        zSQL += " AND ( PartnerNumber LIKE @Search OR PartnerName LIKE @Search OR PartnerID LIKE @Search) ";
                    }
                    string zConnectionString = ConnectDataBase.ConnectionString;
                    try
                    {
                        SqlConnection zConnect = new SqlConnection(zConnectionString);
                        zConnect.Open();
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.Parameters.Add("@Business", SqlDbType.Int).Value = Business;
                        zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                        zAdapter.Fill(zTable);
                        zCommand.Dispose();
                        zConnect.Close();
                    }
                    catch (Exception ex)
                    {
                        string zstrMessage = ex.ToString();
                    }
                    return zTable;
                }
            }



            //#region [Auth]
            //User_Role_Info _RoleForm = new User_Role_Info();
            //private void Get_Auth()
            //{
            //    List<User_Role_Info> zListRole = SessionUser.TableRole;
            //    if (zListRole.Count > 0)
            //    {
            //        try
            //        {
            //            _RoleForm = zListRole.SingleOrDefault(r => r.RoleURL == FormUrl);
            //            if (_RoleForm == null)
            //            {
            //                _RoleForm = new User_Role_Info();
            //                MessageBox.Show("Form này chưa được thêm quyền, vui lòng liên hệ IT !.");
            //                this.Close();
            //            }
            //            else
            //            {
            //                if (!_RoleForm.RoleRead)
            //                {
            //                    MessageBox.Show("Bạn không có quyền xem, vui lòng liên hệ IT !.");
            //                    this.Close();
            //                }
            //                if (!_RoleForm.RoleEdit)
            //                {
            //                    btnSave.Visible = false;
            //                }
            //                if (!_RoleForm.RoleAdd)
            //                {
            //                    btnSave.Visible = false;
            //                }
            //                if (!_RoleForm.RoleDel)
            //                {
            //                    btnDelete.Visible = false;
            //                }
            //            }
            //        }
            //        catch (Exception Ex)
            //        {
            //            MessageBox.Show(Ex.ToString());
            //        }
            //    }
            //    else
            //    {
            //        MessageBox.Show("Bạn chưa được phân quyền, vui lòng liên hệ IT !.");
            //    }
            //}
            //#endregion
        }
        #endregion
    }
}