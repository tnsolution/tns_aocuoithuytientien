﻿using System;
using System.Data;
using System.Windows.Forms;

namespace TN_WinApp
{
    public partial class Frm_FollowPrint : Form
    {
        public DateTime _FromDate = DateTime.Now;
        public DateTime _ToDate = DateTime.Now;
        public DataTable _TableFollow = new DataTable();
        public Frm_FollowPrint()
        {
            InitializeComponent();
            this.Load += Frm_FollowPrint_Load; ;
            btnClose.Click += (o, e) => { this.Close(); };
        }

        private void Frm_FollowPrint_Load(object sender, EventArgs e)
        {
            DataSet Ds = new DataSet();
            DataTable ztblReport = Ds.Tables["OrderFollow"];

            foreach (DataRow r in _TableFollow.Rows)
            {
                DataRow rReport = ztblReport.NewRow();

                rReport["OrderDate"] = r["OrderDate"].ToDateString();
                rReport["DeliverDate"] = r["DeliverDate"].ToDateString();
                rReport["ReturnDate"] = r["ReturnDate"].ToDateString();

                rReport["OrderID"] = r["OrderID"].ToString();
                rReport["BuyerName"] = r["BuyerName"].ToString();
                rReport["BuyerPhone"] = r["BuyerPhone"].ToString();
                rReport["Female"] = r["Female"].ToString();
                rReport["Male"] = r["Male"].ToString();
                rReport["OrderDescription"] = r["OrderDescription"].ToString();

                ztblReport.Rows.Add(rReport);
            }

            rptFollow rpt = new rptFollow();
            rpt.Database.Tables["OrderFollow"].SetDataSource(ztblReport);
            rpt.SetParameterValue("FromDate", _FromDate.ToString("dd/MM/yyyy"));
            rpt.SetParameterValue("ToDate", _ToDate.ToString("dd/MM/yyyy"));
            rptViewer.ReportSource = rpt;
        }
    }
}
