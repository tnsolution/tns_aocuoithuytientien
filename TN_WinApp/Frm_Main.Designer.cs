﻿namespace TN_WinApp
{
    partial class Frm_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Main));
            this.TitleForm = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.Panel_Left = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.MN_Follow = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.MN_List = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.MN_Order = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.MN_Reciept = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.MN_Report = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.MN_Category = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Header = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.btnExit = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.btnChangePass = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Footer = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnStatusTrue = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnStatusFalse = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.Panel_Right = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.PN_List = new System.Windows.Forms.Panel();
            this.SubMN_Receipt = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.SubMN_Employee = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.SubMN_Customer = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.SubMN_OrderAll = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.SubMN_OrderDress = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.SubMN_OrderService = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.PN_Report = new System.Windows.Forms.Panel();
            this.SubMN_RptServices = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.SubMN_RptProduct = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.PN_Category = new System.Windows.Forms.Panel();
            this.SubMN_UserRole = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.SubMN_Service = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.SubMN_Combo = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.SubMN_Unit = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.SubMN_Size = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.SubMN_CategoryProduct = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.SubMN_Color = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.SubMN_Product = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.txtTitle = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Boder = new System.Windows.Forms.Panel();
            this.kryptonButton1 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            ((System.ComponentModel.ISupportInitialize)(this.Panel_Left)).BeginInit();
            this.Panel_Left.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Header)).BeginInit();
            this.Header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Panel_Right)).BeginInit();
            this.Panel_Right.SuspendLayout();
            this.PN_List.SuspendLayout();
            this.PN_Report.SuspendLayout();
            this.PN_Category.SuspendLayout();
            this.SuspendLayout();
            // 
            // TitleForm
            // 
            this.TitleForm.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.TitleForm.Dock = System.Windows.Forms.DockStyle.Top;
            this.TitleForm.Location = new System.Drawing.Point(0, 0);
            this.TitleForm.Name = "TitleForm";
            this.TitleForm.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.TitleForm.Size = new System.Drawing.Size(1366, 42);
            this.TitleForm.StateCommon.Content.ShortText.Color1 = System.Drawing.Color.Navy;
            this.TitleForm.TabIndex = 185;
            this.TitleForm.Values.Description = "";
            this.TitleForm.Values.Heading = "CỬA HÀNG ÁO CƯỚI THỦY TIÊN CẦN THƠ";
            this.TitleForm.Values.Image = ((System.Drawing.Image)(resources.GetObject("TitleForm.Values.Image")));
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // Panel_Left
            // 
            this.Panel_Left.AutoScroll = true;
            this.Panel_Left.BackColor = System.Drawing.Color.Transparent;
            this.Panel_Left.Controls.Add(this.MN_Follow);
            this.Panel_Left.Controls.Add(this.MN_List);
            this.Panel_Left.Controls.Add(this.MN_Order);
            this.Panel_Left.Controls.Add(this.MN_Reciept);
            this.Panel_Left.Controls.Add(this.MN_Report);
            this.Panel_Left.Controls.Add(this.kryptonHeader1);
            this.Panel_Left.Controls.Add(this.kryptonButton1);
            this.Panel_Left.Controls.Add(this.MN_Category);
            this.Panel_Left.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Left.Location = new System.Drawing.Point(0, 107);
            this.Panel_Left.Name = "Panel_Left";
            this.Panel_Left.Size = new System.Drawing.Size(247, 619);
            this.Panel_Left.StateCommon.Color1 = System.Drawing.Color.White;
            this.Panel_Left.StateCommon.Color2 = System.Drawing.Color.LightGreen;
            this.Panel_Left.StateCommon.ColorAngle = 0F;
            this.Panel_Left.StateCommon.ColorStyle = ComponentFactory.Krypton.Toolkit.PaletteColorStyle.SolidRightLine;
            this.Panel_Left.TabIndex = 0;
            // 
            // MN_Follow
            // 
            this.MN_Follow.Location = new System.Drawing.Point(125, 238);
            this.MN_Follow.Name = "MN_Follow";
            this.MN_Follow.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.MN_Follow.Size = new System.Drawing.Size(110, 95);
            this.MN_Follow.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.MN_Follow.StateCommon.Border.Rounding = 10;
            this.MN_Follow.StateCommon.Border.Width = 1;
            this.MN_Follow.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_Follow.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_Follow.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MN_Follow.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.MN_Follow.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_Follow.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_Follow.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.MN_Follow.TabIndex = 5;
            this.MN_Follow.Tag = "";
            this.MN_Follow.Values.Image = ((System.Drawing.Image)(resources.GetObject("MN_Follow.Values.Image")));
            this.MN_Follow.Values.Text = "Lịch giao trả";
            // 
            // MN_List
            // 
            this.MN_List.Location = new System.Drawing.Point(9, 137);
            this.MN_List.Name = "MN_List";
            this.MN_List.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.MN_List.Size = new System.Drawing.Size(110, 95);
            this.MN_List.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.MN_List.StateCommon.Border.Rounding = 10;
            this.MN_List.StateCommon.Border.Width = 1;
            this.MN_List.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_List.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_List.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MN_List.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.MN_List.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_List.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_List.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.MN_List.TabIndex = 2;
            this.MN_List.Tag = "";
            this.MN_List.Values.Image = ((System.Drawing.Image)(resources.GetObject("MN_List.Values.Image")));
            this.MN_List.Values.Text = "Danh sách";
            // 
            // MN_Order
            // 
            this.MN_Order.Location = new System.Drawing.Point(9, 36);
            this.MN_Order.Name = "MN_Order";
            this.MN_Order.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.MN_Order.Size = new System.Drawing.Size(110, 95);
            this.MN_Order.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.MN_Order.StateCommon.Border.Rounding = 10;
            this.MN_Order.StateCommon.Border.Width = 1;
            this.MN_Order.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_Order.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_Order.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MN_Order.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.MN_Order.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_Order.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_Order.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.MN_Order.TabIndex = 0;
            this.MN_Order.Tag = "";
            this.MN_Order.Values.Image = ((System.Drawing.Image)(resources.GetObject("MN_Order.Values.Image")));
            this.MN_Order.Values.Text = "Lập nhanh\r\nbiên nhận";
            // 
            // MN_Reciept
            // 
            this.MN_Reciept.Location = new System.Drawing.Point(125, 36);
            this.MN_Reciept.Name = "MN_Reciept";
            this.MN_Reciept.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.MN_Reciept.Size = new System.Drawing.Size(110, 95);
            this.MN_Reciept.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.MN_Reciept.StateCommon.Border.Rounding = 10;
            this.MN_Reciept.StateCommon.Border.Width = 1;
            this.MN_Reciept.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_Reciept.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_Reciept.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MN_Reciept.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.MN_Reciept.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_Reciept.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_Reciept.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.MN_Reciept.TabIndex = 1;
            this.MN_Reciept.Tag = "";
            this.MN_Reciept.Values.Image = ((System.Drawing.Image)(resources.GetObject("MN_Reciept.Values.Image")));
            this.MN_Reciept.Values.Text = "Lập nhanh\r\nphiếu thu";
            // 
            // MN_Report
            // 
            this.MN_Report.Location = new System.Drawing.Point(125, 137);
            this.MN_Report.Name = "MN_Report";
            this.MN_Report.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.MN_Report.Size = new System.Drawing.Size(110, 95);
            this.MN_Report.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.MN_Report.StateCommon.Border.Rounding = 10;
            this.MN_Report.StateCommon.Border.Width = 1;
            this.MN_Report.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_Report.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_Report.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MN_Report.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.MN_Report.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_Report.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_Report.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.MN_Report.TabIndex = 3;
            this.MN_Report.Tag = "";
            this.MN_Report.Values.Image = ((System.Drawing.Image)(resources.GetObject("MN_Report.Values.Image")));
            this.MN_Report.Values.Text = "Báo cáo";
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.Size = new System.Drawing.Size(247, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kryptonHeader1.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonHeader1.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonHeader1.TabIndex = 186;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "Quản lý";
            // 
            // MN_Category
            // 
            this.MN_Category.Location = new System.Drawing.Point(9, 238);
            this.MN_Category.Name = "MN_Category";
            this.MN_Category.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.MN_Category.Size = new System.Drawing.Size(110, 95);
            this.MN_Category.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.MN_Category.StateCommon.Border.Rounding = 10;
            this.MN_Category.StateCommon.Border.Width = 1;
            this.MN_Category.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_Category.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_Category.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MN_Category.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.MN_Category.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.MN_Category.TabIndex = 4;
            this.MN_Category.Tag = "";
            this.MN_Category.Values.Image = ((System.Drawing.Image)(resources.GetObject("MN_Category.Values.Image")));
            this.MN_Category.Values.Text = "Cài đặt";
            // 
            // Header
            // 
            this.Header.Controls.Add(this.btnExit);
            this.Header.Controls.Add(this.lblUserName);
            this.Header.Controls.Add(this.lblTime);
            this.Header.Controls.Add(this.btnChangePass);
            this.Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.Header.Location = new System.Drawing.Point(0, 42);
            this.Header.Name = "Header";
            this.Header.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.Header.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.HeaderPrimary;
            this.Header.Size = new System.Drawing.Size(1366, 65);
            this.Header.StateCommon.Color1 = System.Drawing.Color.White;
            this.Header.StateCommon.Color2 = System.Drawing.Color.LightGreen;
            this.Header.StateCommon.ColorAngle = 87F;
            this.Header.TabIndex = 187;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Location = new System.Drawing.Point(1231, 12);
            this.btnExit.Name = "btnExit";
            this.btnExit.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btnExit.Size = new System.Drawing.Size(120, 40);
            this.btnExit.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnExit.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btnExit.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btnExit.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btnExit.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btnExit.TabIndex = 184;
            this.btnExit.Values.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Values.Image")));
            this.btnExit.Values.Text = "Đăng xuất";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblUserName.ForeColor = System.Drawing.Color.Green;
            this.lblUserName.Location = new System.Drawing.Point(12, 12);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(80, 16);
            this.lblUserName.TabIndex = 182;
            this.lblUserName.Text = "Nhân viên: ";
            this.lblUserName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.BackColor = System.Drawing.Color.Transparent;
            this.lblTime.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblTime.ForeColor = System.Drawing.Color.Green;
            this.lblTime.Location = new System.Drawing.Point(12, 33);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(190, 16);
            this.lblTime.TabIndex = 183;
            this.lblTime.Text = "Ngày Làm Việc: 17/06/2020";
            // 
            // btnChangePass
            // 
            this.btnChangePass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChangePass.Location = new System.Drawing.Point(1105, 12);
            this.btnChangePass.Name = "btnChangePass";
            this.btnChangePass.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btnChangePass.Size = new System.Drawing.Size(120, 40);
            this.btnChangePass.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnChangePass.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btnChangePass.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btnChangePass.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btnChangePass.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChangePass.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btnChangePass.TabIndex = 180;
            this.btnChangePass.Values.Image = ((System.Drawing.Image)(resources.GetObject("btnChangePass.Values.Image")));
            this.btnChangePass.Values.Text = "Đổi mật khẩu";
            // 
            // Footer
            // 
            this.Footer.AutoSize = false;
            this.Footer.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnStatusTrue,
            this.btnStatusFalse});
            this.Footer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Footer.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.Footer.Location = new System.Drawing.Point(0, 726);
            this.Footer.Name = "Footer";
            this.Footer.Size = new System.Drawing.Size(1366, 42);
            this.Footer.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Footer.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.Footer.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.Footer.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.Footer.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.Footer.TabIndex = 188;
            this.Footer.Values.Description = "";
            this.Footer.Values.Heading = "31 Trần Việt Châu, Phường An Hòa, Quận Ninh Kiều, Thành Phố Cần Thơ";
            this.Footer.Values.Image = null;
            // 
            // btnStatusTrue
            // 
            this.btnStatusTrue.Image = ((System.Drawing.Image)(resources.GetObject("btnStatusTrue.Image")));
            this.btnStatusTrue.UniqueName = "8E502225B94D4AF13C95A94B4F412F70";
            // 
            // btnStatusFalse
            // 
            this.btnStatusFalse.Image = ((System.Drawing.Image)(resources.GetObject("btnStatusFalse.Image")));
            this.btnStatusFalse.UniqueName = "35C51493D5994EA865B65D2FAB5A5AEC";
            // 
            // Panel_Right
            // 
            this.Panel_Right.AutoScroll = true;
            this.Panel_Right.BackColor = System.Drawing.Color.Transparent;
            this.Panel_Right.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Panel_Right.Controls.Add(this.PN_List);
            this.Panel_Right.Controls.Add(this.PN_Report);
            this.Panel_Right.Controls.Add(this.PN_Category);
            this.Panel_Right.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Right.Location = new System.Drawing.Point(247, 137);
            this.Panel_Right.Name = "Panel_Right";
            this.Panel_Right.Size = new System.Drawing.Size(1119, 589);
            this.Panel_Right.StateCommon.Color1 = System.Drawing.Color.White;
            this.Panel_Right.StateCommon.Color2 = System.Drawing.Color.LightGreen;
            this.Panel_Right.StateCommon.ColorAngle = 0F;
            this.Panel_Right.StateCommon.Image = ((System.Drawing.Image)(resources.GetObject("Panel_Right.StateCommon.Image")));
            this.Panel_Right.StateCommon.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.TopMiddle;
            this.Panel_Right.TabIndex = 189;
            // 
            // PN_List
            // 
            this.PN_List.Controls.Add(this.SubMN_Receipt);
            this.PN_List.Controls.Add(this.SubMN_Employee);
            this.PN_List.Controls.Add(this.SubMN_Customer);
            this.PN_List.Controls.Add(this.SubMN_OrderAll);
            this.PN_List.Controls.Add(this.SubMN_OrderDress);
            this.PN_List.Controls.Add(this.SubMN_OrderService);
            this.PN_List.Location = new System.Drawing.Point(369, 6);
            this.PN_List.Name = "PN_List";
            this.PN_List.Size = new System.Drawing.Size(358, 238);
            this.PN_List.TabIndex = 198;
            this.PN_List.Visible = false;
            // 
            // SubMN_Receipt
            // 
            this.SubMN_Receipt.Location = new System.Drawing.Point(7, 120);
            this.SubMN_Receipt.Name = "SubMN_Receipt";
            this.SubMN_Receipt.Size = new System.Drawing.Size(110, 110);
            this.SubMN_Receipt.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_Receipt.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_Receipt.StateCommon.Back.ColorAngle = 75F;
            this.SubMN_Receipt.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.SubMN_Receipt.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.SubMN_Receipt.StateCommon.Border.Rounding = 10;
            this.SubMN_Receipt.StateCommon.Border.Width = 2;
            this.SubMN_Receipt.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Receipt.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Receipt.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SubMN_Receipt.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.SubMN_Receipt.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Receipt.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Receipt.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.SubMN_Receipt.TabIndex = 206;
            this.SubMN_Receipt.Tag = "";
            this.SubMN_Receipt.Values.Image = ((System.Drawing.Image)(resources.GetObject("SubMN_Receipt.Values.Image")));
            this.SubMN_Receipt.Values.Text = "Danh sách \r\nphiếu thu";
            // 
            // SubMN_Employee
            // 
            this.SubMN_Employee.Location = new System.Drawing.Point(123, 120);
            this.SubMN_Employee.Name = "SubMN_Employee";
            this.SubMN_Employee.Size = new System.Drawing.Size(110, 110);
            this.SubMN_Employee.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_Employee.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_Employee.StateCommon.Back.ColorAngle = 75F;
            this.SubMN_Employee.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.SubMN_Employee.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.SubMN_Employee.StateCommon.Border.Rounding = 10;
            this.SubMN_Employee.StateCommon.Border.Width = 2;
            this.SubMN_Employee.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Employee.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Employee.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SubMN_Employee.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.SubMN_Employee.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Employee.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Employee.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.SubMN_Employee.TabIndex = 205;
            this.SubMN_Employee.Tag = "";
            this.SubMN_Employee.Values.Image = ((System.Drawing.Image)(resources.GetObject("SubMN_Employee.Values.Image")));
            this.SubMN_Employee.Values.Text = "Nhân sự";
            // 
            // SubMN_Customer
            // 
            this.SubMN_Customer.Location = new System.Drawing.Point(239, 120);
            this.SubMN_Customer.Name = "SubMN_Customer";
            this.SubMN_Customer.Size = new System.Drawing.Size(110, 110);
            this.SubMN_Customer.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_Customer.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_Customer.StateCommon.Back.ColorAngle = 75F;
            this.SubMN_Customer.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.SubMN_Customer.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.SubMN_Customer.StateCommon.Border.Rounding = 10;
            this.SubMN_Customer.StateCommon.Border.Width = 2;
            this.SubMN_Customer.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Customer.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Customer.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SubMN_Customer.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.SubMN_Customer.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Customer.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Customer.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.SubMN_Customer.TabIndex = 205;
            this.SubMN_Customer.Tag = "";
            this.SubMN_Customer.Values.Image = ((System.Drawing.Image)(resources.GetObject("SubMN_Customer.Values.Image")));
            this.SubMN_Customer.Values.Text = "Khách hàng\r\n";
            // 
            // SubMN_OrderAll
            // 
            this.SubMN_OrderAll.Location = new System.Drawing.Point(239, 3);
            this.SubMN_OrderAll.Name = "SubMN_OrderAll";
            this.SubMN_OrderAll.Size = new System.Drawing.Size(110, 110);
            this.SubMN_OrderAll.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_OrderAll.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_OrderAll.StateCommon.Back.ColorAngle = 75F;
            this.SubMN_OrderAll.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.SubMN_OrderAll.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.SubMN_OrderAll.StateCommon.Border.Rounding = 10;
            this.SubMN_OrderAll.StateCommon.Border.Width = 2;
            this.SubMN_OrderAll.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_OrderAll.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_OrderAll.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SubMN_OrderAll.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.SubMN_OrderAll.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_OrderAll.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_OrderAll.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.SubMN_OrderAll.TabIndex = 206;
            this.SubMN_OrderAll.Tag = "";
            this.SubMN_OrderAll.Values.Image = ((System.Drawing.Image)(resources.GetObject("SubMN_OrderAll.Values.Image")));
            this.SubMN_OrderAll.Values.Text = "Biên nhận \r\nthuê áo && \r\ndịch vụ";
            // 
            // SubMN_OrderDress
            // 
            this.SubMN_OrderDress.Location = new System.Drawing.Point(7, 4);
            this.SubMN_OrderDress.Name = "SubMN_OrderDress";
            this.SubMN_OrderDress.Size = new System.Drawing.Size(110, 110);
            this.SubMN_OrderDress.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_OrderDress.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_OrderDress.StateCommon.Back.ColorAngle = 75F;
            this.SubMN_OrderDress.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.SubMN_OrderDress.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.SubMN_OrderDress.StateCommon.Border.Rounding = 10;
            this.SubMN_OrderDress.StateCommon.Border.Width = 2;
            this.SubMN_OrderDress.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_OrderDress.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_OrderDress.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SubMN_OrderDress.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.SubMN_OrderDress.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_OrderDress.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_OrderDress.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.SubMN_OrderDress.TabIndex = 206;
            this.SubMN_OrderDress.Tag = "";
            this.SubMN_OrderDress.Values.Image = ((System.Drawing.Image)(resources.GetObject("SubMN_OrderDress.Values.Image")));
            this.SubMN_OrderDress.Values.Text = "Biên nhận \r\nthuê áo";
            // 
            // SubMN_OrderService
            // 
            this.SubMN_OrderService.Location = new System.Drawing.Point(123, 4);
            this.SubMN_OrderService.Name = "SubMN_OrderService";
            this.SubMN_OrderService.Size = new System.Drawing.Size(110, 110);
            this.SubMN_OrderService.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_OrderService.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_OrderService.StateCommon.Back.ColorAngle = 75F;
            this.SubMN_OrderService.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.SubMN_OrderService.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.SubMN_OrderService.StateCommon.Border.Rounding = 10;
            this.SubMN_OrderService.StateCommon.Border.Width = 2;
            this.SubMN_OrderService.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_OrderService.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_OrderService.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SubMN_OrderService.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.SubMN_OrderService.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_OrderService.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_OrderService.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.SubMN_OrderService.TabIndex = 206;
            this.SubMN_OrderService.Tag = "";
            this.SubMN_OrderService.Values.Image = ((System.Drawing.Image)(resources.GetObject("SubMN_OrderService.Values.Image")));
            this.SubMN_OrderService.Values.Text = "Biên nhận\r\nthuê dịch vụ";
            // 
            // PN_Report
            // 
            this.PN_Report.Controls.Add(this.SubMN_RptServices);
            this.PN_Report.Controls.Add(this.SubMN_RptProduct);
            this.PN_Report.Location = new System.Drawing.Point(733, 6);
            this.PN_Report.Name = "PN_Report";
            this.PN_Report.Size = new System.Drawing.Size(358, 238);
            this.PN_Report.TabIndex = 198;
            this.PN_Report.Visible = false;
            // 
            // SubMN_RptServices
            // 
            this.SubMN_RptServices.Location = new System.Drawing.Point(123, 4);
            this.SubMN_RptServices.Name = "SubMN_RptServices";
            this.SubMN_RptServices.Size = new System.Drawing.Size(110, 110);
            this.SubMN_RptServices.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_RptServices.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_RptServices.StateCommon.Back.ColorAngle = 75F;
            this.SubMN_RptServices.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.SubMN_RptServices.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.SubMN_RptServices.StateCommon.Border.Rounding = 10;
            this.SubMN_RptServices.StateCommon.Border.Width = 2;
            this.SubMN_RptServices.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_RptServices.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_RptServices.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SubMN_RptServices.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.SubMN_RptServices.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_RptServices.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_RptServices.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.SubMN_RptServices.TabIndex = 206;
            this.SubMN_RptServices.Tag = "";
            this.SubMN_RptServices.Values.Image = ((System.Drawing.Image)(resources.GetObject("SubMN_RptServices.Values.Image")));
            this.SubMN_RptServices.Values.Text = "Doanh số\r\ndịch vụ";
            // 
            // SubMN_RptProduct
            // 
            this.SubMN_RptProduct.Location = new System.Drawing.Point(7, 4);
            this.SubMN_RptProduct.Name = "SubMN_RptProduct";
            this.SubMN_RptProduct.Size = new System.Drawing.Size(110, 110);
            this.SubMN_RptProduct.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_RptProduct.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_RptProduct.StateCommon.Back.ColorAngle = 75F;
            this.SubMN_RptProduct.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.SubMN_RptProduct.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.SubMN_RptProduct.StateCommon.Border.Rounding = 10;
            this.SubMN_RptProduct.StateCommon.Border.Width = 2;
            this.SubMN_RptProduct.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_RptProduct.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_RptProduct.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SubMN_RptProduct.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.SubMN_RptProduct.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_RptProduct.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_RptProduct.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.SubMN_RptProduct.TabIndex = 206;
            this.SubMN_RptProduct.Tag = "";
            this.SubMN_RptProduct.Values.Image = ((System.Drawing.Image)(resources.GetObject("SubMN_RptProduct.Values.Image")));
            this.SubMN_RptProduct.Values.Text = "Doanh số \r\náo cưới";
            // 
            // PN_Category
            // 
            this.PN_Category.Controls.Add(this.SubMN_UserRole);
            this.PN_Category.Controls.Add(this.SubMN_Service);
            this.PN_Category.Controls.Add(this.SubMN_Combo);
            this.PN_Category.Controls.Add(this.SubMN_Unit);
            this.PN_Category.Controls.Add(this.SubMN_Size);
            this.PN_Category.Controls.Add(this.SubMN_CategoryProduct);
            this.PN_Category.Controls.Add(this.SubMN_Color);
            this.PN_Category.Controls.Add(this.SubMN_Product);
            this.PN_Category.Location = new System.Drawing.Point(6, 6);
            this.PN_Category.Name = "PN_Category";
            this.PN_Category.Size = new System.Drawing.Size(357, 355);
            this.PN_Category.TabIndex = 198;
            this.PN_Category.Visible = false;
            // 
            // SubMN_UserRole
            // 
            this.SubMN_UserRole.Location = new System.Drawing.Point(238, 236);
            this.SubMN_UserRole.Name = "SubMN_UserRole";
            this.SubMN_UserRole.Size = new System.Drawing.Size(110, 110);
            this.SubMN_UserRole.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_UserRole.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_UserRole.StateCommon.Back.ColorAngle = 75F;
            this.SubMN_UserRole.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.SubMN_UserRole.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.SubMN_UserRole.StateCommon.Border.Rounding = 10;
            this.SubMN_UserRole.StateCommon.Border.Width = 2;
            this.SubMN_UserRole.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_UserRole.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_UserRole.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SubMN_UserRole.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.SubMN_UserRole.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_UserRole.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_UserRole.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.SubMN_UserRole.TabIndex = 205;
            this.SubMN_UserRole.Tag = "";
            this.SubMN_UserRole.Values.Image = ((System.Drawing.Image)(resources.GetObject("SubMN_UserRole.Values.Image")));
            this.SubMN_UserRole.Values.Text = "Người dùng &&\r\nphân quyền";
            // 
            // SubMN_Service
            // 
            this.SubMN_Service.Location = new System.Drawing.Point(122, 4);
            this.SubMN_Service.Name = "SubMN_Service";
            this.SubMN_Service.Size = new System.Drawing.Size(110, 110);
            this.SubMN_Service.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_Service.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_Service.StateCommon.Back.ColorAngle = 75F;
            this.SubMN_Service.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.SubMN_Service.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.SubMN_Service.StateCommon.Border.Rounding = 10;
            this.SubMN_Service.StateCommon.Border.Width = 2;
            this.SubMN_Service.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Service.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Service.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SubMN_Service.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.SubMN_Service.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Service.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Service.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.SubMN_Service.TabIndex = 204;
            this.SubMN_Service.Tag = "";
            this.SubMN_Service.Values.Image = ((System.Drawing.Image)(resources.GetObject("SubMN_Service.Values.Image")));
            this.SubMN_Service.Values.Text = "Dịch vụ";
            // 
            // SubMN_Combo
            // 
            this.SubMN_Combo.Location = new System.Drawing.Point(238, 4);
            this.SubMN_Combo.Name = "SubMN_Combo";
            this.SubMN_Combo.Size = new System.Drawing.Size(110, 110);
            this.SubMN_Combo.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_Combo.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_Combo.StateCommon.Back.ColorAngle = 75F;
            this.SubMN_Combo.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.SubMN_Combo.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.SubMN_Combo.StateCommon.Border.Rounding = 10;
            this.SubMN_Combo.StateCommon.Border.Width = 2;
            this.SubMN_Combo.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Combo.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Combo.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SubMN_Combo.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.SubMN_Combo.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Combo.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Combo.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.SubMN_Combo.TabIndex = 204;
            this.SubMN_Combo.Tag = "";
            this.SubMN_Combo.Values.Image = ((System.Drawing.Image)(resources.GetObject("SubMN_Combo.Values.Image")));
            this.SubMN_Combo.Values.Text = "Gói \r\nsản phẩm && \r\nDịch vụ\r\n";
            // 
            // SubMN_Unit
            // 
            this.SubMN_Unit.Location = new System.Drawing.Point(238, 120);
            this.SubMN_Unit.Name = "SubMN_Unit";
            this.SubMN_Unit.Size = new System.Drawing.Size(110, 110);
            this.SubMN_Unit.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_Unit.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_Unit.StateCommon.Back.ColorAngle = 75F;
            this.SubMN_Unit.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.SubMN_Unit.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.SubMN_Unit.StateCommon.Border.Rounding = 10;
            this.SubMN_Unit.StateCommon.Border.Width = 2;
            this.SubMN_Unit.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Unit.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Unit.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SubMN_Unit.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.SubMN_Unit.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Unit.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Unit.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.SubMN_Unit.TabIndex = 204;
            this.SubMN_Unit.Tag = "";
            this.SubMN_Unit.Values.Image = ((System.Drawing.Image)(resources.GetObject("SubMN_Unit.Values.Image")));
            this.SubMN_Unit.Values.Text = "Đơn vị tính";
            // 
            // SubMN_Size
            // 
            this.SubMN_Size.Location = new System.Drawing.Point(122, 120);
            this.SubMN_Size.Name = "SubMN_Size";
            this.SubMN_Size.Size = new System.Drawing.Size(110, 110);
            this.SubMN_Size.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_Size.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_Size.StateCommon.Back.ColorAngle = 75F;
            this.SubMN_Size.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.SubMN_Size.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.SubMN_Size.StateCommon.Border.Rounding = 10;
            this.SubMN_Size.StateCommon.Border.Width = 2;
            this.SubMN_Size.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Size.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Size.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SubMN_Size.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.SubMN_Size.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Size.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Size.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.SubMN_Size.TabIndex = 204;
            this.SubMN_Size.Tag = "";
            this.SubMN_Size.Values.Image = ((System.Drawing.Image)(resources.GetObject("SubMN_Size.Values.Image")));
            this.SubMN_Size.Values.Text = "Kích cỡ";
            // 
            // SubMN_CategoryProduct
            // 
            this.SubMN_CategoryProduct.Location = new System.Drawing.Point(122, 236);
            this.SubMN_CategoryProduct.Name = "SubMN_CategoryProduct";
            this.SubMN_CategoryProduct.Size = new System.Drawing.Size(110, 110);
            this.SubMN_CategoryProduct.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_CategoryProduct.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_CategoryProduct.StateCommon.Back.ColorAngle = 75F;
            this.SubMN_CategoryProduct.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.SubMN_CategoryProduct.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.SubMN_CategoryProduct.StateCommon.Border.Rounding = 10;
            this.SubMN_CategoryProduct.StateCommon.Border.Width = 2;
            this.SubMN_CategoryProduct.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_CategoryProduct.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_CategoryProduct.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SubMN_CategoryProduct.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.SubMN_CategoryProduct.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_CategoryProduct.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_CategoryProduct.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.SubMN_CategoryProduct.TabIndex = 204;
            this.SubMN_CategoryProduct.Tag = "";
            this.SubMN_CategoryProduct.Values.Image = ((System.Drawing.Image)(resources.GetObject("SubMN_CategoryProduct.Values.Image")));
            this.SubMN_CategoryProduct.Values.Text = "Loại áo";
            // 
            // SubMN_Color
            // 
            this.SubMN_Color.Location = new System.Drawing.Point(6, 120);
            this.SubMN_Color.Name = "SubMN_Color";
            this.SubMN_Color.Size = new System.Drawing.Size(110, 110);
            this.SubMN_Color.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_Color.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_Color.StateCommon.Back.ColorAngle = 75F;
            this.SubMN_Color.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.SubMN_Color.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.SubMN_Color.StateCommon.Border.Rounding = 10;
            this.SubMN_Color.StateCommon.Border.Width = 2;
            this.SubMN_Color.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Color.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Color.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SubMN_Color.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.SubMN_Color.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Color.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Color.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.SubMN_Color.TabIndex = 204;
            this.SubMN_Color.Tag = "";
            this.SubMN_Color.Values.Image = ((System.Drawing.Image)(resources.GetObject("SubMN_Color.Values.Image")));
            this.SubMN_Color.Values.Text = "Màu";
            // 
            // SubMN_Product
            // 
            this.SubMN_Product.Location = new System.Drawing.Point(6, 4);
            this.SubMN_Product.Name = "SubMN_Product";
            this.SubMN_Product.Size = new System.Drawing.Size(110, 110);
            this.SubMN_Product.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_Product.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SubMN_Product.StateCommon.Back.ColorAngle = 75F;
            this.SubMN_Product.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.SubMN_Product.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.SubMN_Product.StateCommon.Border.Rounding = 10;
            this.SubMN_Product.StateCommon.Border.Width = 2;
            this.SubMN_Product.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Product.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Product.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SubMN_Product.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.SubMN_Product.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Product.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.SubMN_Product.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.SubMN_Product.TabIndex = 204;
            this.SubMN_Product.Tag = "";
            this.SubMN_Product.Values.Image = ((System.Drawing.Image)(resources.GetObject("SubMN_Product.Values.Image")));
            this.SubMN_Product.Values.Text = "Sản phẩm";
            // 
            // txtTitle
            // 
            this.txtTitle.AutoSize = false;
            this.txtTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtTitle.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.txtTitle.Location = new System.Drawing.Point(247, 107);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(1119, 30);
            this.txtTitle.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtTitle.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.txtTitle.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.txtTitle.TabIndex = 190;
            this.txtTitle.Values.Description = "";
            this.txtTitle.Values.Heading = "Chức năng";
            // 
            // Panel_Boder
            // 
            this.Panel_Boder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Boder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Boder.Location = new System.Drawing.Point(0, 0);
            this.Panel_Boder.Name = "Panel_Boder";
            this.Panel_Boder.Size = new System.Drawing.Size(1366, 768);
            this.Panel_Boder.TabIndex = 191;
            // 
            // kryptonButton1
            // 
            this.kryptonButton1.Location = new System.Drawing.Point(9, 339);
            this.kryptonButton1.Name = "kryptonButton1";
            this.kryptonButton1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonButton1.Size = new System.Drawing.Size(110, 95);
            this.kryptonButton1.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.kryptonButton1.StateCommon.Border.Rounding = 10;
            this.kryptonButton1.StateCommon.Border.Width = 1;
            this.kryptonButton1.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonButton1.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonButton1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kryptonButton1.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonButton1.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.kryptonButton1.TabIndex = 4;
            this.kryptonButton1.Tag = "";
            this.kryptonButton1.Values.Image = ((System.Drawing.Image)(resources.GetObject("kryptonButton1.Values.Image")));
            this.kryptonButton1.Values.Text = "Cài đặt";
            this.kryptonButton1.Click += new System.EventHandler(this.kryptonButton1_Click);
            // 
            // Frm_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.ControlBox = false;
            this.Controls.Add(this.Panel_Right);
            this.Controls.Add(this.txtTitle);
            this.Controls.Add(this.Panel_Left);
            this.Controls.Add(this.Header);
            this.Controls.Add(this.TitleForm);
            this.Controls.Add(this.Footer);
            this.Controls.Add(this.Panel_Boder);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Tahoma", 9F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.Panel_Left)).EndInit();
            this.Panel_Left.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Header)).EndInit();
            this.Header.ResumeLayout(false);
            this.Header.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Panel_Right)).EndInit();
            this.Panel_Right.ResumeLayout(false);
            this.PN_List.ResumeLayout(false);
            this.PN_Report.ResumeLayout(false);
            this.PN_Category.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader TitleForm;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel Header;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblTime;
        //private ComponentFactory.Krypton.Toolkit.KryptonButton btnExit;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnChangePass;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader Footer;
        private ComponentFactory.Krypton.Toolkit.KryptonButton MN_Order;
        private ComponentFactory.Krypton.Toolkit.KryptonButton MN_Report;
        private ComponentFactory.Krypton.Toolkit.KryptonButton MN_Category;
        private System.Windows.Forms.Panel PN_Category;
        private ComponentFactory.Krypton.Toolkit.KryptonButton SubMN_Product;
        private System.Windows.Forms.Panel PN_List;
        private ComponentFactory.Krypton.Toolkit.KryptonButton SubMN_OrderService;
        private ComponentFactory.Krypton.Toolkit.KryptonButton SubMN_OrderDress;
        private System.Windows.Forms.Panel PN_Report;
        private ComponentFactory.Krypton.Toolkit.KryptonButton SubMN_RptServices;
        private ComponentFactory.Krypton.Toolkit.KryptonButton SubMN_RptProduct;
        private ComponentFactory.Krypton.Toolkit.KryptonButton SubMN_Customer;
        private ComponentFactory.Krypton.Toolkit.KryptonButton SubMN_Employee;
        private ComponentFactory.Krypton.Toolkit.KryptonButton SubMN_OrderAll;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnStatusTrue;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnStatusFalse;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader txtTitle;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnExit;
        private ComponentFactory.Krypton.Toolkit.KryptonButton SubMN_UserRole;
        private ComponentFactory.Krypton.Toolkit.KryptonButton MN_Reciept;
        private ComponentFactory.Krypton.Toolkit.KryptonButton MN_List;
        private ComponentFactory.Krypton.Toolkit.KryptonButton SubMN_Receipt;
        private ComponentFactory.Krypton.Toolkit.KryptonButton MN_Follow;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel Panel_Right;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel Panel_Left;
        private ComponentFactory.Krypton.Toolkit.KryptonButton SubMN_Service;
        private ComponentFactory.Krypton.Toolkit.KryptonButton SubMN_Combo;
        private System.Windows.Forms.Panel Panel_Boder;
        private ComponentFactory.Krypton.Toolkit.KryptonButton SubMN_Unit;
        private ComponentFactory.Krypton.Toolkit.KryptonButton SubMN_Size;
        private ComponentFactory.Krypton.Toolkit.KryptonButton SubMN_Color;
        private ComponentFactory.Krypton.Toolkit.KryptonButton SubMN_CategoryProduct;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton1;
    }
}