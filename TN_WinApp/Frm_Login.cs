﻿using System.Data;
using System.Windows.Forms;
using TN_Tools;
using TN_User;

namespace TN_WinApp
{
    public partial class Frm_Login : Form
    {
        XMLEncryptor _XMLEncrypt;
        DataTable _Table;      
        System.Data.DataSet _DataSet;

        public Frm_Login()
        {
            InitializeComponent();

            Load += Frm_Login_Load;

            btn_Login.Click += btn_Login_Click;
            btn_Cancel.Click += tn_Cancel_Click;
        }

        private void Frm_Login_Load(object sender, System.EventArgs e)
        {
            _XMLEncrypt = new XMLEncryptor(SessionUser.ClientUser, SessionUser.ClientPass);
            _DataSet = _XMLEncrypt.ReadEncryptedXML("remember.ini");
            if (_DataSet != null)
            {
                _Table = _DataSet.Tables["Remember"];
                txt_User.Text = _Table.Rows[0]["UserName"].ToString();
                txt_Password.Text = _Table.Rows[0]["Password"].ToString();

                chkRemember.Checked = true;
            }
        }

        private void tn_Cancel_Click(object sender, System.EventArgs e)
        {
            Application.Exit();
        }

        private void btn_Login_Click(object sender, System.EventArgs e)
        {
            string[] zResult = User_Data.CheckUser(txt_User.Text, txt_Password.Text);

            if (zResult[0] == "ERR")
            {
                switch (zResult[1])
                {
                    case "CheckUser_Error01":
                        lbl_Message.Text = "Vui lòng kiểm tra lại tên Đăng Nhập";
                        break;
                    case "CheckUser_Error02":
                        lbl_Message.Text = "Vui lòng kiểm tra Mật Khẩu";
                        break;
                    case "CheckUser_Error03":
                        lbl_Message.Text = "Tài khoản này chưa kích hoạt hoặc bị khóa";
                        break;
                    case "CheckUser_Error04":
                        lbl_Message.Text = "Tài khoản này hết hạn";
                        break;
                }

                lbl_Message.Visible = true;
            }
            else
            {
                if (chkRemember.Checked)
                {
                    _Table = new DataTable("Remember");
                    _Table.Columns.Add("UserName");
                    _Table.Columns.Add("Password");

                    DataRow r = _Table.NewRow();
                    r["UserName"] = txt_User.Text.Trim();
                    r["Password"] = txt_Password.Text.Trim();
                    _Table.Rows.Add(r);

                    _DataSet = new DataSet();
                    _DataSet.Tables.Add(_Table);
                    _XMLEncrypt = new XMLEncryptor(SessionUser.ClientUser, SessionUser.ClientPass);
                    _XMLEncrypt.WriteEncryptedXML(_DataSet, "remember.ini");
                }

                string zUserKey = zResult[1];
                SessionUser zUserLogin = new SessionUser(zUserKey);
                this.Close();
            }
        }

        private void btnClose_Click(object sender, System.EventArgs e)
        {
            Application.Exit();
        }
    }
}
