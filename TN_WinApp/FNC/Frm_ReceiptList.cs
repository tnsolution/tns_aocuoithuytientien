﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TN_Connection;
using TN_Tools;
using TN_User;

namespace TN_WinApp.FNC
{
    public partial class Frm_ReceiptList : Form
    {
        private string _FormUrl = "/FNC/Frm_ReceiptList";
        public string FormUrl
        {
            get
            {
                return _FormUrl;
            }

            set
            {
                _FormUrl = value;
            }
        }

        public Frm_ReceiptList()
        {
            InitializeComponent();

            TitleForm.MouseDown += Frm_MouseDown;
            TitleForm.MouseMove += Frm_MouseMove;
            TitleForm.MouseUp += Frm_MouseUp;

            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;

            btnAddNew.Click += BtnAddNew_Click;
            btnSearch.Click += BtnSearch_Click;
            LVData.ItemActivate += LVData_ItemActivate;
            LVData.KeyDown += LVData_KeyDown;

            this.Text = "Thông tin phiếu thu";
            this.WindowState = FormWindowState.Maximized;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.FormBorderStyle = FormBorderStyle.None;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.Load += Frm_ReceiptList_Load;
        }

        private void Frm_ReceiptList_Load(object sender, EventArgs e)
        {
            Get_Auth();
            if (_RoleForm.RoleRead)
            {
                DateTime zDate = DateTime.Now;
                DateTime FromDate = new DateTime(zDate.Year, zDate.Month, 1, 0, 0, 0);
                DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
                ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
                dteFromDate.Value = FromDate;
                dteToDate.Value = ToDate;
                DesignLayout(LVData);
                LoadListView();
            }
        }

        #region [Process]
        private string Delete(string ReceiptKey)
        {
            Receipt_Info zInfo = new Receipt_Info();
            zInfo.ReceiptKey = ReceiptKey;
            zInfo.Delete();

            UpdateOrderMoneyAfterDelete(zInfo.DocumentID);

            return zInfo.Message;
        }
        private string UpdateOrderMoneyAfterDelete(string DocumentID)
        {
            DataTable zTable = Access_Data.ListOrderReciept(SessionUser.UserLogin.PartnerNumber, DocumentID);
            List<Receipt_Info> ListOrderReceipt = zTable.CopyToList<Receipt_Info>();

            double zTotal = 0;
            for (int i = 0; i < ListOrderReceipt.Count; i++)
            {
                Receipt_Info zExistItem = ListOrderReceipt[i];
                zTotal += zExistItem.AmountCurrencyMain;
            }

            Order_FED_Info zInfo = new Order_FED_Info(DocumentID, true);
            zInfo.ReceiptAmount = zTotal;
            zInfo.Update();
            if (zInfo.Code != "200" &&
                zInfo.Code != "201")
            {
                MessageBox.Show(zInfo.Message, "Lỗi xóa phiếu thu vui lòng liên hệ IT !.", MessageBoxButtons.OK);
            }

            return zInfo.Message;
        }
        #endregion

        #region [Event]
        private void BtnSearch_Click(object sender, EventArgs e)
        {
            LoadListView();
        }
        private void BtnAddNew_Click(object sender, EventArgs e)
        {
            Frm_ReceiptEdit Frm = new Frm_ReceiptEdit();
            Frm.ReceiptKey = string.Empty;
            Frm.ShowDialog();
            if (Frm.ReceiptKey != string.Empty)
            {
                LoadListView();
            }
        }
        #endregion

        #region [ListView]
        private void DesignLayout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã phiếu";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Biên nhận";
            colHead.Width = 250;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Nội dung thu";
            colHead.Width = 350;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số tiền";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Khách hàng/ người nộp";
            colHead.Width = 250;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Người lập phiếu";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            TNPaintControl.DrawLVStyle(ref LVData);
        }
        private void LoadListView()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable zTable = Access_Data.ListReceipt(SessionUser.UserLogin.PartnerNumber, txt_SearchID.Text.Trim(), dteFromDate.Value, dteToDate.Value);
            LV.Items.Clear();
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow zRow = zTable.Rows[i];
                lvi.Tag = zRow["ReceiptKey"].ToString();
                lvi.BackColor = Color.White;

                DateTime zDate = DateTime.Parse(zRow["ReceiptDate"].ToString());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zDate.ToString("dd/MM/yyyy");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["ReceiptID"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["DocumentID"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["ReceiptDescription"].ToString();
                lvi.SubItems.Add(lvsi);

                float zAmount = 0;
                if (zRow["AmountCurrencyMain"] != null)
                {
                    zAmount = zRow["AmountCurrencyMain"].ToFloat();
                }
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zAmount.ToString("n2");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["CustomerName"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["CreatedName"].ToString();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;

        }
        private void LVData_ItemActivate(object sender, EventArgs e)
        {
            if (LVData.SelectedItems.Count > 0)
            {
                string Key = LVData.SelectedItems[0].Tag.ToString();
                Frm_ReceiptEdit Frm = new Frm_ReceiptEdit();
                Frm.ReceiptKey = Key;
                Frm.Show();
            }
        }
        private void LVData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (_RoleForm.RoleDel)
                {
                    DialogResult dlr = MessageBox.Show("Thao tác này sẽ xóa tất cả mọi thông tin liên quan đến doanh thu !.", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                    string Message = "";
                    if (dlr == DialogResult.Yes && LVData.SelectedItems.Count > 0)
                    {
                        for (int i = 0; i < LVData.SelectedItems.Count; i++)
                        {
                            string Key = LVData.SelectedItems[i].Tag.ToString();
                            string zResult = Delete(Key);

                            if (zResult.Substring(0, 3) == "200" ||
                                zResult.Substring(0, 3) == "201")
                            {
                                Message = string.Empty;
                            }
                            else
                            {
                                Message = zResult;
                            }
                        }

                        if (Message == string.Empty)
                        {
                            LoadListView();
                        }
                        else
                        {
                            MessageBox.Show(Message);
                        }
                    }
                }
            }
        }
        #endregion

        #region [Dùng kéo rê form]
        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;



        private void Frm_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }
        private void Frm_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }
        private void Frm_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        #region [Auth]
        User_Role_Info _RoleForm = new User_Role_Info();
        private void Get_Auth()
        {
            List<User_Role_Info> zListRole = SessionUser.TableRole;
            if (zListRole.Count > 0)
            {
                try
                {
                    _RoleForm = zListRole.SingleOrDefault(r => r.RoleURL == FormUrl);
                    if (_RoleForm == null)
                    {
                        _RoleForm = new User_Role_Info();
                        MessageBox.Show("Form này chưa được thêm quyền, vui lòng liên hệ IT !.");
                        this.Close();
                    }
                    else
                    {
                        if (!_RoleForm.RoleRead)
                        {
                            MessageBox.Show("Bạn không có quyền xem, vui lòng liên hệ IT !.");
                            this.Close();
                        }
                        if (!_RoleForm.RoleEdit)
                        {

                        }
                        if (!_RoleForm.RoleAdd)
                        {

                        }
                        if (!_RoleForm.RoleDel)
                        {

                        }
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Bạn chưa được phân quyền, vui lòng liên hệ IT !.");
            }
        }
        #endregion       

        #region[Access_Data]
        public class Access_Data
        {
            public static DataTable ListOrderReciept(string PartnerNumber, string OrderID)
            {
                DataTable zTable = new DataTable();
                string zSQL = "SELECT * FROM [dbo].[FNC_Receipt] WHERE PartnerNumber = @PartnerNumber AND DocumentID = @OrderID AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = OrderID;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
            public static DataTable ListReceipt(string PartnerNumber, string ReceiptID, DateTime FromDate, DateTime ToDate)
            {
                DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
                DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

                DataTable zTable = new DataTable();
                string zSQL = @"SELECT * FROM [dbo].[FNC_Receipt] 
WHERE RecordStatus <> 99  
AND PartnerNumber= @PartnerNumber
AND ReceiptDate BETWEEN @FromDate AND @ToDate";
                if (ReceiptID.Trim().Length > 0)
                {
                    zSQL += " AND ReceiptID LIKE @Name";
                }

                zSQL += " ORDER BY ReceiptDate DESC";
                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + ReceiptID + "%"; ;
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
        }
        public class Receipt_Info
        {
            #region [ Field Name ]
            private string _ReceiptKey = "";
            private string _ReceiptID = "";
            private DateTime? _ReceiptDate = null;
            private string _ReceiptDescription = "";
            private string _DocumentID = "";
            private int _CategoryKey = 0;
            private string _CategoryName = "";
            private int _StyleKey = 0;
            private string _CustomerKey = "";
            private string _CustomerName = "";
            private string _CustomerPhone = "";
            private string _Depositor = "";
            private string _Address = "";
            private double _AmountCurrencyMain = 0;
            private double _AmountCurrencyForeign = 0;
            private string _CurrencyIDForeign = "";
            private double _CurrencyRate = 0;
            private string _BankName = "";
            private string _BankAccount = "";
            private double _BankVAT = 0;
            private double _BankFee = 0;
            private bool _IsFeeInside;
            private int _Slug = 0;
            private string _DebitNo = "";
            private string _CreditNo = "";
            private string _PartnerNumber = "";
            private int _RecordStatus = 0;
            private DateTime? _CreatedOn = null;
            private string _CreatedBy = "";
            private string _CreatedName = "";
            private DateTime? _ModifiedOn = null;
            private string _ModifiedBy = "";
            private string _ModifiedName = "";
            private string _Message = "";
            #endregion

            #region [ Constructor Get Information ]
            public Receipt_Info()
            {
                Guid zNewID = Guid.NewGuid();
                _ReceiptKey = zNewID.ToString();
            }
            public Receipt_Info(string ReceiptKey)
            {
                string zSQL = "SELECT * FROM [dbo].[FNC_Receipt] WHERE ReceiptKey = @ReceiptKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ReceiptKey", SqlDbType.UniqueIdentifier).Value = new Guid(ReceiptKey);
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _ReceiptKey = zReader["ReceiptKey"].ToString();
                        _ReceiptID = zReader["ReceiptID"].ToString();
                        if (zReader["ReceiptDate"] != DBNull.Value)
                        {
                            _ReceiptDate = (DateTime)zReader["ReceiptDate"];
                        }

                        _ReceiptDescription = zReader["ReceiptDescription"].ToString();
                        _DocumentID = zReader["DocumentID"].ToString();
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                        _CategoryName = zReader["CategoryName"].ToString();
                        _StyleKey = int.Parse(zReader["StyleKey"].ToString());
                        _CustomerKey = zReader["CustomerKey"].ToString();
                        _CustomerName = zReader["CustomerName"].ToString();
                        _CustomerPhone = zReader["CustomerPhone"].ToString();
                        _Depositor = zReader["Depositor"].ToString();
                        _Address = zReader["Address"].ToString();
                        _AmountCurrencyMain = double.Parse(zReader["AmountCurrencyMain"].ToString());
                        _AmountCurrencyForeign = double.Parse(zReader["AmountCurrencyForeign"].ToString());
                        _CurrencyIDForeign = zReader["CurrencyIDForeign"].ToString();
                        _CurrencyRate = double.Parse(zReader["CurrencyRate"].ToString());
                        _BankName = zReader["BankName"].ToString();
                        _BankAccount = zReader["BankAccount"].ToString();
                        _BankVAT = double.Parse(zReader["BankVAT"].ToString());
                        _BankFee = double.Parse(zReader["BankFee"].ToString());
                        _IsFeeInside = (bool)zReader["IsFeeInside"];
                        _Slug = int.Parse(zReader["Slug"].ToString());
                        _DebitNo = zReader["DebitNo"].ToString();
                        _CreditNo = zReader["CreditNo"].ToString();
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }

            #endregion

            #region [ Properties ]
            public string ReceiptKey
            {
                get { return _ReceiptKey; }
                set { _ReceiptKey = value; }
            }
            public string ReceiptID
            {
                get { return _ReceiptID; }
                set { _ReceiptID = value; }
            }
            public DateTime? ReceiptDate
            {
                get { return _ReceiptDate; }
                set { _ReceiptDate = value; }
            }
            public string ReceiptDescription
            {
                get { return _ReceiptDescription; }
                set { _ReceiptDescription = value; }
            }
            public string DocumentID
            {
                get { return _DocumentID; }
                set { _DocumentID = value; }
            }
            public int CategoryKey
            {
                get { return _CategoryKey; }
                set { _CategoryKey = value; }
            }
            public string CategoryName
            {
                get { return _CategoryName; }
                set { _CategoryName = value; }
            }
            public int StyleKey
            {
                get { return _StyleKey; }
                set { _StyleKey = value; }
            }
            public string CustomerKey
            {
                get { return _CustomerKey; }
                set { _CustomerKey = value; }
            }
            public string CustomerName
            {
                get { return _CustomerName; }
                set { _CustomerName = value; }
            }
            public string CustomerPhone
            {
                get { return _CustomerPhone; }
                set { _CustomerPhone = value; }
            }
            public string Depositor
            {
                get { return _Depositor; }
                set { _Depositor = value; }
            }
            public string Address
            {
                get { return _Address; }
                set { _Address = value; }
            }
            public double AmountCurrencyMain
            {
                get { return _AmountCurrencyMain; }
                set { _AmountCurrencyMain = value; }
            }
            public double AmountCurrencyForeign
            {
                get { return _AmountCurrencyForeign; }
                set { _AmountCurrencyForeign = value; }
            }
            public string CurrencyIDForeign
            {
                get { return _CurrencyIDForeign; }
                set { _CurrencyIDForeign = value; }
            }
            public double CurrencyRate
            {
                get { return _CurrencyRate; }
                set { _CurrencyRate = value; }
            }
            public string BankName
            {
                get { return _BankName; }
                set { _BankName = value; }
            }
            public string BankAccount
            {
                get { return _BankAccount; }
                set { _BankAccount = value; }
            }
            public double BankVAT
            {
                get { return _BankVAT; }
                set { _BankVAT = value; }
            }
            public double BankFee
            {
                get { return _BankFee; }
                set { _BankFee = value; }
            }
            public bool IsFeeInside
            {
                get { return _IsFeeInside; }
                set { _IsFeeInside = value; }
            }
            public int Slug
            {
                get { return _Slug; }
                set { _Slug = value; }
            }
            public string DebitNo
            {
                get { return _DebitNo; }
                set { _DebitNo = value; }
            }
            public string CreditNo
            {
                get { return _CreditNo; }
                set { _CreditNo = value; }
            }
            public string PartnerNumber
            {
                get { return _PartnerNumber; }
                set { _PartnerNumber = value; }
            }
            public int RecordStatus
            {
                get { return _RecordStatus; }
                set { _RecordStatus = value; }
            }
            public DateTime? CreatedOn
            {
                get { return _CreatedOn; }
                set { _CreatedOn = value; }
            }
            public string CreatedBy
            {
                get { return _CreatedBy; }
                set { _CreatedBy = value; }
            }
            public string CreatedName
            {
                get { return _CreatedName; }
                set { _CreatedName = value; }
            }
            public DateTime? ModifiedOn
            {
                get { return _ModifiedOn; }
                set { _ModifiedOn = value; }
            }
            public string ModifiedBy
            {
                get { return _ModifiedBy; }
                set { _ModifiedBy = value; }
            }
            public string ModifiedName
            {
                get { return _ModifiedName; }
                set { _ModifiedName = value; }
            }
            public string Code
            {
                get
                {
                    if (_Message.Length >= 3)
                    {
                        return _Message.Substring(0, 3);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            public string Message
            {
                get { return _Message; }
                set { _Message = value; }
            }
            #endregion

            #region [ Constructor Update Information ]
            public string Create_ServerKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[FNC_Receipt] ("
             + " ReceiptID ,ReceiptDate ,ReceiptDescription ,DocumentID ,CategoryKey ,CategoryName ,StyleKey ,CustomerKey ,CustomerName ,CustomerPhone ,Depositor ,Address ,AmountCurrencyMain ,AmountCurrencyForeign ,CurrencyIDForeign ,CurrencyRate ,BankName ,BankAccount ,BankVAT ,BankFee ,IsFeeInside ,Slug ,DebitNo ,CreditNo ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@ReceiptID ,@ReceiptDate ,@ReceiptDescription ,@DocumentID ,@CategoryKey ,@CategoryName ,@StyleKey ,@CustomerKey ,@CustomerName ,@CustomerPhone ,@Depositor ,@Address ,@AmountCurrencyMain ,@AmountCurrencyForeign ,@CurrencyIDForeign ,@CurrencyRate ,@BankName ,@BankAccount ,@BankVAT ,@BankFee ,@IsFeeInside ,@Slug ,@DebitNo ,@CreditNo ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ReceiptID", SqlDbType.NVarChar).Value = _ReceiptID;
                    if (_ReceiptDate == null)
                    {
                        zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = _ReceiptDate;
                    }

                    zCommand.Parameters.Add("@ReceiptDescription", SqlDbType.NVarChar).Value = _ReceiptDescription;
                    zCommand.Parameters.Add("@DocumentID", SqlDbType.NVarChar).Value = _DocumentID;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@StyleKey", SqlDbType.Int).Value = _StyleKey;
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_CustomerKey);
                    zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = _CustomerName;
                    zCommand.Parameters.Add("@CustomerPhone", SqlDbType.NVarChar).Value = _CustomerPhone;
                    zCommand.Parameters.Add("@Depositor", SqlDbType.NVarChar).Value = _Depositor;
                    zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                    zCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = _AmountCurrencyMain;
                    zCommand.Parameters.Add("@AmountCurrencyForeign", SqlDbType.Money).Value = _AmountCurrencyForeign;
                    zCommand.Parameters.Add("@CurrencyIDForeign", SqlDbType.NChar).Value = _CurrencyIDForeign;
                    zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = _CurrencyRate;
                    zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = _BankName;
                    zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = _BankAccount;
                    zCommand.Parameters.Add("@BankVAT", SqlDbType.Money).Value = _BankVAT;
                    zCommand.Parameters.Add("@BankFee", SqlDbType.Money).Value = _BankFee;
                    zCommand.Parameters.Add("@IsFeeInside", SqlDbType.Bit).Value = _IsFeeInside;
                    zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                    zCommand.Parameters.Add("@DebitNo", SqlDbType.NVarChar).Value = _DebitNo;
                    zCommand.Parameters.Add("@CreditNo", SqlDbType.NVarChar).Value = _CreditNo;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Create_ClientKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[FNC_Receipt] ("
            + " ReceiptKey ,ReceiptID ,ReceiptDate ,ReceiptDescription ,DocumentID ,CategoryKey ,CategoryName ,StyleKey ,CustomerKey ,CustomerName ,CustomerPhone ,Depositor ,Address ,AmountCurrencyMain ,AmountCurrencyForeign ,CurrencyIDForeign ,CurrencyRate ,BankName ,BankAccount ,BankVAT ,BankFee ,IsFeeInside ,Slug ,DebitNo ,CreditNo ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@ReceiptKey ,@ReceiptID ,@ReceiptDate ,@ReceiptDescription ,@DocumentID ,@CategoryKey ,@CategoryName ,@StyleKey ,@CustomerKey ,@CustomerName ,@CustomerPhone ,@Depositor ,@Address ,@AmountCurrencyMain ,@AmountCurrencyForeign ,@CurrencyIDForeign ,@CurrencyRate ,@BankName ,@BankAccount ,@BankVAT ,@BankFee ,@IsFeeInside ,@Slug ,@DebitNo ,@CreditNo ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ReceiptKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ReceiptKey);
                    zCommand.Parameters.Add("@ReceiptID", SqlDbType.NVarChar).Value = _ReceiptID;
                    if (_ReceiptDate == null)
                    {
                        zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = _ReceiptDate;
                    }

                    zCommand.Parameters.Add("@ReceiptDescription", SqlDbType.NVarChar).Value = _ReceiptDescription;
                    zCommand.Parameters.Add("@DocumentID", SqlDbType.NVarChar).Value = _DocumentID;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@StyleKey", SqlDbType.Int).Value = _StyleKey;
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_CustomerKey);
                    zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = _CustomerName;
                    zCommand.Parameters.Add("@CustomerPhone", SqlDbType.NVarChar).Value = _CustomerPhone;
                    zCommand.Parameters.Add("@Depositor", SqlDbType.NVarChar).Value = _Depositor;
                    zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                    zCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = _AmountCurrencyMain;
                    zCommand.Parameters.Add("@AmountCurrencyForeign", SqlDbType.Money).Value = _AmountCurrencyForeign;
                    zCommand.Parameters.Add("@CurrencyIDForeign", SqlDbType.NChar).Value = _CurrencyIDForeign;
                    zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = _CurrencyRate;
                    zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = _BankName;
                    zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = _BankAccount;
                    zCommand.Parameters.Add("@BankVAT", SqlDbType.Money).Value = _BankVAT;
                    zCommand.Parameters.Add("@BankFee", SqlDbType.Money).Value = _BankFee;
                    zCommand.Parameters.Add("@IsFeeInside", SqlDbType.Bit).Value = _IsFeeInside;
                    zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                    zCommand.Parameters.Add("@DebitNo", SqlDbType.NVarChar).Value = _DebitNo;
                    zCommand.Parameters.Add("@CreditNo", SqlDbType.NVarChar).Value = _CreditNo;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Update()
            {
                string zSQL = "UPDATE [dbo].[FNC_Receipt] SET "
                            + " ReceiptID = @ReceiptID,"
                            + " ReceiptDate = @ReceiptDate,"
                            + " ReceiptDescription = @ReceiptDescription,"
                            + " DocumentID = @DocumentID,"
                            + " CategoryKey = @CategoryKey,"
                            + " CategoryName = @CategoryName,"
                            + " StyleKey = @StyleKey,"
                            + " CustomerKey = @CustomerKey,"
                            + " CustomerName = @CustomerName,"
                            + " CustomerPhone = @CustomerPhone,"
                            + " Depositor = @Depositor,"
                            + " Address = @Address,"
                            + " AmountCurrencyMain = @AmountCurrencyMain,"
                            + " AmountCurrencyForeign = @AmountCurrencyForeign,"
                            + " CurrencyIDForeign = @CurrencyIDForeign,"
                            + " CurrencyRate = @CurrencyRate,"
                            + " BankName = @BankName,"
                            + " BankAccount = @BankAccount,"
                            + " BankVAT = @BankVAT,"
                            + " BankFee = @BankFee,"
                            + " IsFeeInside = @IsFeeInside,"
                            + " Slug = @Slug,"
                            + " DebitNo = @DebitNo,"
                            + " CreditNo = @CreditNo,"
                            + " PartnerNumber = @PartnerNumber,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedOn = GetDate(),"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName"
                           + " WHERE ReceiptKey = @ReceiptKey";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ReceiptKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ReceiptKey);
                    zCommand.Parameters.Add("@ReceiptID", SqlDbType.NVarChar).Value = _ReceiptID;
                    if (_ReceiptDate == null)
                    {
                        zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = _ReceiptDate;
                    }

                    zCommand.Parameters.Add("@ReceiptDescription", SqlDbType.NVarChar).Value = _ReceiptDescription;
                    zCommand.Parameters.Add("@DocumentID", SqlDbType.NVarChar).Value = _DocumentID;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@StyleKey", SqlDbType.Int).Value = _StyleKey;
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_CustomerKey);
                    zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = _CustomerName;
                    zCommand.Parameters.Add("@CustomerPhone", SqlDbType.NVarChar).Value = _CustomerPhone;
                    zCommand.Parameters.Add("@Depositor", SqlDbType.NVarChar).Value = _Depositor;
                    zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                    zCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = _AmountCurrencyMain;
                    zCommand.Parameters.Add("@AmountCurrencyForeign", SqlDbType.Money).Value = _AmountCurrencyForeign;
                    zCommand.Parameters.Add("@CurrencyIDForeign", SqlDbType.NChar).Value = _CurrencyIDForeign;
                    zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = _CurrencyRate;
                    zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = _BankName;
                    zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = _BankAccount;
                    zCommand.Parameters.Add("@BankVAT", SqlDbType.Money).Value = _BankVAT;
                    zCommand.Parameters.Add("@BankFee", SqlDbType.Money).Value = _BankFee;
                    zCommand.Parameters.Add("@IsFeeInside", SqlDbType.Bit).Value = _IsFeeInside;
                    zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                    zCommand.Parameters.Add("@DebitNo", SqlDbType.NVarChar).Value = _DebitNo;
                    zCommand.Parameters.Add("@CreditNo", SqlDbType.NVarChar).Value = _CreditNo;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Delete()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[FNC_Receipt] Set RecordStatus = 99 WHERE ReceiptKey = @ReceiptKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ReceiptKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ReceiptKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[FNC_Receipt] WHERE ReceiptKey = @ReceiptKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ReceiptKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ReceiptKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            #endregion
        }
        public class Order_FED_Info
        {
            #region [ Field Name ]
            private string _OrderKey = "";
            private string _OrderID = "";
            private DateTime? _OrderDate = null;
            private string _OrderDescription = "";
            private string _SellerKey = "";
            private string _SellerName = "";
            private string _BuyerKey = "";
            private string _BuyerName = "";
            private string _BuyerPhone = "";
            private string _BuyerAddress = "";
            private int _OrderStatusKey = 0;
            private string _OrderStatusName = "";
            private int _PaymentStatusKey = 0;
            private string _PaymentStatusName = "";
            private DateTime? _DeliverDate = null;
            private DateTime? _ReturnDate = null;
            private int _CategoryKey = 0;
            private string _CategoryName = "";
            private int _OrganizationKey = 0;
            private string _OrganizationID = "";
            private string _OrganizationName = "";
            private double _TotalAmount = 0;
            private double _SaleAmount = 0;
            private double _OrderAmount = 0;
            private double _ReceiptAmount = 0;
            private string _PartnerNumber = "";
            private int _RecordStatus = 0;
            private DateTime? _CreatedOn = null;
            private string _CreatedBy = "";
            private string _CreatedName = "";
            private DateTime? _ModifiedOn = null;
            private string _ModifiedBy = "";
            private string _ModifiedName = "";
            private string _Message = "";
            #endregion

            #region [ Constructor Get Information ]
            public Order_FED_Info()
            {
                Guid zNewID = Guid.NewGuid();
                _OrderKey = zNewID.ToString();
            }
            public Order_FED_Info(string OrderKey)
            {
                string zSQL = "SELECT * FROM [dbo].[FNC_Order_FED] WHERE OrderKey = @OrderKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(OrderKey);
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _OrderKey = zReader["OrderKey"].ToString();
                        _OrderID = zReader["OrderID"].ToString();
                        if (zReader["OrderDate"] != DBNull.Value)
                        {
                            _OrderDate = (DateTime)zReader["OrderDate"];
                        }

                        _OrderDescription = zReader["OrderDescription"].ToString();
                        _SellerKey = zReader["SellerKey"].ToString();
                        _SellerName = zReader["SellerName"].ToString();
                        _BuyerKey = zReader["BuyerKey"].ToString();
                        _BuyerName = zReader["BuyerName"].ToString();
                        _BuyerPhone = zReader["BuyerPhone"].ToString();
                        _BuyerAddress = zReader["BuyerAddress"].ToString();
                        _OrderStatusKey = int.Parse(zReader["OrderStatusKey"].ToString());
                        _OrderStatusName = zReader["OrderStatusName"].ToString();
                        _PaymentStatusKey = int.Parse(zReader["PaymentStatusKey"].ToString());
                        _PaymentStatusName = zReader["PaymentStatusName"].ToString();
                        if (zReader["DeliverDate"] != DBNull.Value)
                        {
                            _DeliverDate = (DateTime)zReader["DeliverDate"];
                        }

                        if (zReader["ReturnDate"] != DBNull.Value)
                        {
                            _ReturnDate = (DateTime)zReader["ReturnDate"];
                        }

                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                        _CategoryName = zReader["CategoryName"].ToString();
                        _OrganizationKey = int.Parse(zReader["OrganizationKey"].ToString());
                        _OrganizationID = zReader["OrganizationID"].ToString();
                        _OrganizationName = zReader["OrganizationName"].ToString();
                        _TotalAmount = double.Parse(zReader["TotalAmount"].ToString());
                        _SaleAmount = double.Parse(zReader["SaleAmount"].ToString());
                        _OrderAmount = double.Parse(zReader["OrderAmount"].ToString());
                        _ReceiptAmount = double.Parse(zReader["ReceiptAmount"].ToString());
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }
            public Order_FED_Info(string OrderID, bool Receipt)
            {
                string zSQL = "SELECT * FROM [dbo].[FNC_Order_FED] WHERE OrderID = @OrderID AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = OrderID;
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _OrderKey = zReader["OrderKey"].ToString();
                        _OrderID = zReader["OrderID"].ToString();
                        if (zReader["OrderDate"] != DBNull.Value)
                        {
                            _OrderDate = (DateTime)zReader["OrderDate"];
                        }

                        _OrderDescription = zReader["OrderDescription"].ToString();
                        _SellerKey = zReader["SellerKey"].ToString();
                        _SellerName = zReader["SellerName"].ToString();
                        _BuyerKey = zReader["BuyerKey"].ToString();
                        _BuyerName = zReader["BuyerName"].ToString();
                        _BuyerPhone = zReader["BuyerPhone"].ToString();
                        _BuyerAddress = zReader["BuyerAddress"].ToString();
                        _OrderStatusKey = int.Parse(zReader["OrderStatusKey"].ToString());
                        _OrderStatusName = zReader["OrderStatusName"].ToString();
                        _PaymentStatusKey = int.Parse(zReader["PaymentStatusKey"].ToString());
                        _PaymentStatusName = zReader["PaymentStatusName"].ToString();
                        if (zReader["DeliverDate"] != DBNull.Value)
                        {
                            _DeliverDate = (DateTime)zReader["DeliverDate"];
                        }

                        if (zReader["ReturnDate"] != DBNull.Value)
                        {
                            _ReturnDate = (DateTime)zReader["ReturnDate"];
                        }

                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                        _CategoryName = zReader["CategoryName"].ToString();
                        _OrganizationKey = int.Parse(zReader["OrganizationKey"].ToString());
                        _OrganizationID = zReader["OrganizationID"].ToString();
                        _OrganizationName = zReader["OrganizationName"].ToString();
                        _TotalAmount = double.Parse(zReader["TotalAmount"].ToString());
                        _SaleAmount = double.Parse(zReader["SaleAmount"].ToString());
                        _OrderAmount = double.Parse(zReader["OrderAmount"].ToString());
                        _ReceiptAmount = double.Parse(zReader["ReceiptAmount"].ToString());
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }
            #endregion

            #region [ Properties ]
            public string OrderKey
            {
                get { return _OrderKey; }
                set { _OrderKey = value; }
            }
            public string OrderID
            {
                get { return _OrderID; }
                set { _OrderID = value; }
            }
            public DateTime? OrderDate
            {
                get { return _OrderDate; }
                set { _OrderDate = value; }
            }
            public string OrderDescription
            {
                get { return _OrderDescription; }
                set { _OrderDescription = value; }
            }
            public string SellerKey
            {
                get { return _SellerKey; }
                set { _SellerKey = value; }
            }
            public string SellerName
            {
                get { return _SellerName; }
                set { _SellerName = value; }
            }
            public string BuyerKey
            {
                get { return _BuyerKey; }
                set { _BuyerKey = value; }
            }
            public string BuyerName
            {
                get { return _BuyerName; }
                set { _BuyerName = value; }
            }
            public string BuyerPhone
            {
                get { return _BuyerPhone; }
                set { _BuyerPhone = value; }
            }
            public string BuyerAddress
            {
                get { return _BuyerAddress; }
                set { _BuyerAddress = value; }
            }
            public int OrderStatusKey
            {
                get { return _OrderStatusKey; }
                set { _OrderStatusKey = value; }
            }
            public string OrderStatusName
            {
                get { return _OrderStatusName; }
                set { _OrderStatusName = value; }
            }
            public int PaymentStatusKey
            {
                get { return _PaymentStatusKey; }
                set { _PaymentStatusKey = value; }
            }
            public string PaymentStatusName
            {
                get { return _PaymentStatusName; }
                set { _PaymentStatusName = value; }
            }
            public DateTime? DeliverDate
            {
                get { return _DeliverDate; }
                set { _DeliverDate = value; }
            }
            public DateTime? ReturnDate
            {
                get { return _ReturnDate; }
                set { _ReturnDate = value; }
            }
            public int CategoryKey
            {
                get { return _CategoryKey; }
                set { _CategoryKey = value; }
            }
            public string CategoryName
            {
                get { return _CategoryName; }
                set { _CategoryName = value; }
            }
            public int OrganizationKey
            {
                get { return _OrganizationKey; }
                set { _OrganizationKey = value; }
            }
            public string OrganizationID
            {
                get { return _OrganizationID; }
                set { _OrganizationID = value; }
            }
            public string OrganizationName
            {
                get { return _OrganizationName; }
                set { _OrganizationName = value; }
            }
            public double TotalAmount
            {
                get { return _TotalAmount; }
                set { _TotalAmount = value; }
            }
            public double SaleAmount
            {
                get { return _SaleAmount; }
                set { _SaleAmount = value; }
            }
            public double OrderAmount
            {
                get { return _OrderAmount; }
                set { _OrderAmount = value; }
            }
            public double ReceiptAmount
            {
                get { return _ReceiptAmount; }
                set { _ReceiptAmount = value; }
            }
            public string PartnerNumber
            {
                get { return _PartnerNumber; }
                set { _PartnerNumber = value; }
            }
            public int RecordStatus
            {
                get { return _RecordStatus; }
                set { _RecordStatus = value; }
            }
            public DateTime? CreatedOn
            {
                get { return _CreatedOn; }
                set { _CreatedOn = value; }
            }
            public string CreatedBy
            {
                get { return _CreatedBy; }
                set { _CreatedBy = value; }
            }
            public string CreatedName
            {
                get { return _CreatedName; }
                set { _CreatedName = value; }
            }
            public DateTime? ModifiedOn
            {
                get { return _ModifiedOn; }
                set { _ModifiedOn = value; }
            }
            public string ModifiedBy
            {
                get { return _ModifiedBy; }
                set { _ModifiedBy = value; }
            }
            public string ModifiedName
            {
                get { return _ModifiedName; }
                set { _ModifiedName = value; }
            }
            public string Code
            {
                get
                {
                    if (_Message.Length >= 3)
                    {
                        return _Message.Substring(0, 3);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            public string Message
            {
                get { return _Message; }
                set { _Message = value; }
            }
            #endregion

            #region [ Constructor Update Information ]

            public string Create_ServerKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[FNC_Order_FED] ("
            + " OrderID ,OrderDate ,OrderDescription ,SellerKey ,SellerName ,BuyerKey ,BuyerName ,BuyerPhone ,BuyerAddress ,OrderStatusKey ,OrderStatusName ,PaymentStatusKey ,PaymentStatusName ,DeliverDate ,ReturnDate ,CategoryKey ,CategoryName ,OrganizationKey ,OrganizationID ,OrganizationName ,TotalAmount ,SaleAmount ,OrderAmount ,ReceiptAmount ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@OrderID ,@OrderDate ,@OrderDescription ,@SellerKey ,@SellerName ,@BuyerKey ,@BuyerName ,@BuyerPhone ,@BuyerAddress ,@OrderStatusKey ,@OrderStatusName ,@PaymentStatusKey ,@PaymentStatusName ,@DeliverDate ,@ReturnDate ,@CategoryKey ,@CategoryName ,@OrganizationKey ,@OrganizationID ,@OrganizationName ,@TotalAmount ,@SaleAmount ,@OrderAmount ,@ReceiptAmount ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID;
                    if (_OrderDate == null)
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                    }

                    zCommand.Parameters.Add("@OrderDescription", SqlDbType.NVarChar).Value = _OrderDescription;
                    zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_SellerKey);
                    zCommand.Parameters.Add("@SellerName", SqlDbType.NVarChar).Value = _SellerName;
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_BuyerKey);
                    zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = _BuyerName;
                    zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = _BuyerPhone;
                    zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = _BuyerAddress;
                    zCommand.Parameters.Add("@OrderStatusKey", SqlDbType.Int).Value = _OrderStatusKey;
                    zCommand.Parameters.Add("@OrderStatusName", SqlDbType.NVarChar).Value = _OrderStatusName;
                    zCommand.Parameters.Add("@PaymentStatusKey", SqlDbType.Int).Value = _PaymentStatusKey;
                    zCommand.Parameters.Add("@PaymentStatusName", SqlDbType.NVarChar).Value = _PaymentStatusName;
                    if (_DeliverDate == null)
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = _DeliverDate;
                    }

                    if (_ReturnDate == null)
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = _ReturnDate;
                    }

                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = _OrganizationKey;
                    zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = _OrganizationID;
                    zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = _OrganizationName;
                    zCommand.Parameters.Add("@TotalAmount", SqlDbType.Money).Value = _TotalAmount;
                    zCommand.Parameters.Add("@SaleAmount", SqlDbType.Money).Value = _SaleAmount;
                    zCommand.Parameters.Add("@OrderAmount", SqlDbType.Money).Value = _OrderAmount;
                    zCommand.Parameters.Add("@ReceiptAmount", SqlDbType.Money).Value = _ReceiptAmount;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Create_ClientKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[FNC_Order_FED] ("
            + " OrderKey ,OrderID ,OrderDate ,OrderDescription ,SellerKey ,SellerName ,BuyerKey ,BuyerName ,BuyerPhone ,BuyerAddress ,OrderStatusKey ,OrderStatusName ,PaymentStatusKey ,PaymentStatusName ,DeliverDate ,ReturnDate ,CategoryKey ,CategoryName ,OrganizationKey ,OrganizationID ,OrganizationName ,TotalAmount ,SaleAmount ,OrderAmount ,ReceiptAmount ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@OrderKey ,@OrderID ,@OrderDate ,@OrderDescription ,@SellerKey ,@SellerName ,@BuyerKey ,@BuyerName ,@BuyerPhone ,@BuyerAddress ,@OrderStatusKey ,@OrderStatusName ,@PaymentStatusKey ,@PaymentStatusName ,@DeliverDate ,@ReturnDate ,@CategoryKey ,@CategoryName ,@OrganizationKey ,@OrganizationID ,@OrganizationName ,@TotalAmount ,@SaleAmount ,@OrderAmount ,@ReceiptAmount ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID;
                    if (_OrderDate == null)
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                    }

                    zCommand.Parameters.Add("@OrderDescription", SqlDbType.NVarChar).Value = _OrderDescription;
                    zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_SellerKey);
                    zCommand.Parameters.Add("@SellerName", SqlDbType.NVarChar).Value = _SellerName;
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_BuyerKey);
                    zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = _BuyerName;
                    zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = _BuyerPhone;
                    zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = _BuyerAddress;
                    zCommand.Parameters.Add("@OrderStatusKey", SqlDbType.Int).Value = _OrderStatusKey;
                    zCommand.Parameters.Add("@OrderStatusName", SqlDbType.NVarChar).Value = _OrderStatusName;
                    zCommand.Parameters.Add("@PaymentStatusKey", SqlDbType.Int).Value = _PaymentStatusKey;
                    zCommand.Parameters.Add("@PaymentStatusName", SqlDbType.NVarChar).Value = _PaymentStatusName;
                    if (_DeliverDate == null)
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = _DeliverDate;
                    }

                    if (_ReturnDate == null)
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = _ReturnDate;
                    }

                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = _OrganizationKey;
                    zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = _OrganizationID;
                    zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = _OrganizationName;
                    zCommand.Parameters.Add("@TotalAmount", SqlDbType.Money).Value = _TotalAmount;
                    zCommand.Parameters.Add("@SaleAmount", SqlDbType.Money).Value = _SaleAmount;
                    zCommand.Parameters.Add("@OrderAmount", SqlDbType.Money).Value = _OrderAmount;
                    zCommand.Parameters.Add("@ReceiptAmount", SqlDbType.Money).Value = _ReceiptAmount;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Update()
            {
                string zSQL = "UPDATE [dbo].[FNC_Order_FED] SET "
                            + " OrderID = @OrderID,"
                            + " OrderDate = @OrderDate,"
                            + " OrderDescription = @OrderDescription,"
                            + " SellerKey = @SellerKey,"
                            + " SellerName = @SellerName,"
                            + " BuyerKey = @BuyerKey,"
                            + " BuyerName = @BuyerName,"
                            + " BuyerPhone = @BuyerPhone,"
                            + " BuyerAddress = @BuyerAddress,"
                            + " OrderStatusKey = @OrderStatusKey,"
                            + " OrderStatusName = @OrderStatusName,"
                            + " PaymentStatusKey = @PaymentStatusKey,"
                            + " PaymentStatusName = @PaymentStatusName,"
                            + " DeliverDate = @DeliverDate,"
                            + " ReturnDate = @ReturnDate,"
                            + " CategoryKey = @CategoryKey,"
                            + " CategoryName = @CategoryName,"
                            + " OrganizationKey = @OrganizationKey,"
                            + " OrganizationID = @OrganizationID,"
                            + " OrganizationName = @OrganizationName,"
                            + " TotalAmount = @TotalAmount,"
                            + " SaleAmount = @SaleAmount,"
                            + " OrderAmount = @OrderAmount,"
                            + " ReceiptAmount = @ReceiptAmount,"
                            + " PartnerNumber = @PartnerNumber,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedOn = GetDate(),"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName"
                           + " WHERE OrderKey = @OrderKey";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID;
                    if (_OrderDate == null)
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                    }

                    zCommand.Parameters.Add("@OrderDescription", SqlDbType.NVarChar).Value = _OrderDescription;
                    zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_SellerKey);
                    zCommand.Parameters.Add("@SellerName", SqlDbType.NVarChar).Value = _SellerName;
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_BuyerKey);
                    zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = _BuyerName;
                    zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = _BuyerPhone;
                    zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = _BuyerAddress;
                    zCommand.Parameters.Add("@OrderStatusKey", SqlDbType.Int).Value = _OrderStatusKey;
                    zCommand.Parameters.Add("@OrderStatusName", SqlDbType.NVarChar).Value = _OrderStatusName;
                    zCommand.Parameters.Add("@PaymentStatusKey", SqlDbType.Int).Value = _PaymentStatusKey;
                    zCommand.Parameters.Add("@PaymentStatusName", SqlDbType.NVarChar).Value = _PaymentStatusName;
                    if (_DeliverDate == null)
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = _DeliverDate;
                    }

                    if (_ReturnDate == null)
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = _ReturnDate;
                    }

                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = _OrganizationKey;
                    zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = _OrganizationID;
                    zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = _OrganizationName;
                    zCommand.Parameters.Add("@TotalAmount", SqlDbType.Money).Value = _TotalAmount;
                    zCommand.Parameters.Add("@SaleAmount", SqlDbType.Money).Value = _SaleAmount;
                    zCommand.Parameters.Add("@OrderAmount", SqlDbType.Money).Value = _OrderAmount;
                    zCommand.Parameters.Add("@ReceiptAmount", SqlDbType.Money).Value = _ReceiptAmount;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Delete()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[FNC_Order_FED] Set RecordStatus = 99 WHERE OrderKey = @OrderKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[FNC_Order_FED] WHERE OrderKey = @OrderKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            #endregion
        }
        #endregion]
    }
}
