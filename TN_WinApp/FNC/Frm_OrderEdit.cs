﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using TN_Connection;
using TN_Tools;
using TN_User;

namespace TN_WinApp.FNC
{
    public partial class Frm_OrderEdit : Form
    {
        private string _FormUrl = "/FNC/Frm_OrderEdit";
        private string _OrderKey = "";

        public string OrderKey
        {
            get
            {
                return _OrderKey;
            }

            set
            {
                _OrderKey = value;
            }
        }
        public string FormUrl
        {
            get
            {
                return _FormUrl;
            }

            set
            {
                _FormUrl = value;
            }
        }

        public Frm_OrderEdit()
        {
            InitializeComponent();

            TitleForm.MouseDown += Frm_MouseDown;
            TitleForm.MouseMove += Frm_MouseMove;
            TitleForm.MouseUp += Frm_MouseUp;

            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;

            txtCustomerName.KeyUp += TxtCustomerName_KeyUp;
            txtCustomerName.Leave += TxtCustomerName_Leave;

            txtSaleAmount.KeyPress += TextBoxNumber_KeyPress;
            txtSaleAmount.Leave += (o, e) => { CalculatorOrder(); };

            btnSearchProduct.Click += BtnSearchProduct_Click;
            btnSearchService.Click += BtnSearchService_Click;

            btnSearchCustomer.Click += BtnSearchCustomer_Click;
            btnReciept.Click += BtnReciept_Click;
            btnNew.Click += BtnNew_Click;
            btnDelete.Click += BtnDelete_Click;
            btnPrint.Click += BtnPrint_Click;
            btnSave.Click += BtnSave_Click;

            LVSearch.ItemSelectionChanged += LVSearch_ItemSelectionChanged;
            LVSearch.ItemActivate += LVSearch_ItemActivate;
            LVSearch.KeyDown += LVSearch_KeyDown;

            GVEmployee.CellContentClick += GVEmployee_CellContentClick;
            GVProduct.CellContentClick += GVProduct_CellContentClick;
            GVService.CellContentClick += GVService_CellContentClick;
            GVCombo.CellContentClick += GVCombo_CellContentClick;

            GVOrderEmployee.CellContentClick += GvOrderEmployee_CellContentClick;
            GVOrderDetail.CellContentClick += GVOrderDetail_CellContentClick;

            this.Text = "Chi tiết biên nhận/ đơn hàng";
            this.WindowState = FormWindowState.Maximized;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.FormBorderStyle = FormBorderStyle.None;
            this.ShowInTaskbar = true;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.Load += Frm_OrderEdit_Load;
        }

        private void Frm_OrderEdit_Load(object sender, EventArgs e)
        {
            Get_Auth();
            if (_RoleForm.RoleRead)
            {
                LoadCombobox();
                DesignLayout();
                LoadData();
                ReloadForm();
            }
        }

        #region [Event]
        private void TextBoxNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
        private void BtnSearchService_Click(object sender, EventArgs e)
        {
            DataTable zTable = Access_Data.ListService(SessionUser.UserLogin.PartnerNumber, txtSearchProduct.Text.Trim(), cboCategoryService.SelectedValue.ToInt());
            LoadData_GVService(GVService, zTable);
        }
        private void BtnSearchProduct_Click(object sender, EventArgs e)
        {
            DataTable zTable = Access_Data.ListProduct(SessionUser.UserLogin.PartnerNumber, txtSearchProduct.Text.Trim(), cboCategoryProduct.SelectedValue.ToInt());
            LoadData_GVProduct(GVProduct, zTable);
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            string zMessage = CheckBeforSave();
            if (zMessage.Length == 0)
            {
                string zResult = Save();
                if (zResult.Substring(0, 3) == "200" ||
                    zResult.Substring(0, 3) == "201")
                {
                    MessageBox.Show("Cập nhật đơn hàng thành công!");
                    ReloadForm();
                }
                else
                {
                    MessageBox.Show("Lỗi.Vui lòng liên hệ IT. Chi tiết lỗi mã : \n " + zResult);
                }
            }
            else
            {
                MessageBox.Show(zMessage);
            }
        }
        private void BtnPrint_Click(object sender, EventArgs e)
        {
            Frm_OrderPrint Frm = new Frm_OrderPrint();
            Frm.OrderKey = _OrderKey;
            Frm.ShowDialog();
        }
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            DialogResult dlr = MessageBox.Show("Thao tác này sẽ xóa tất cả mọi thông tin liên quan đến đơn hàng này !.", "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (OrderKey != "" && dlr == DialogResult.Yes)
            {
                string zResult = Delete();

                if (zResult.Substring(0, 3) == "200" ||
                    zResult.Substring(0, 3) == "201")
                {
                    MessageBox.Show("Đã xóa thành công !");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Lỗi.Vui lòng liên hệ IT.Chi tiết lỗi mã: \n " + zResult);
                }
            }
        }
        private void BtnNew_Click(object sender, EventArgs e)
        {
            ClearForm();
        }
        private void BtnReciept_Click(object sender, EventArgs e)
        {
            if (OrderKey != "")
            {
                Frm_ReceiptEdit Frm = new Frm_ReceiptEdit();
                Frm.OrderID = txtOrderID.Text.Trim();
                Frm.ShowDialog();

                DataTable zTable = Access_Data.ListOrderReciept(SessionUser.UserLogin.PartnerNumber, txtOrderID.Text.Trim());
                _ListOrderReceipt = zTable.CopyToList<Receipt_Info>();
                LoadData_GVOrderReceipt(GVReceipt);
            }
            else
            {
                MessageBox.Show("Bạn phải lập đơn hàng trước !.");
            }
        }
        #endregion

        #region [Process]      
        /// <summary>
        /// Dùng ẩn hiện các nút action theo tình huống dữ liệu
        /// </summary>
        private void ReloadForm()
        {
            if (_OrderKey == "")
            {
                btnPrint.Enabled = false;
                btnReciept.Enabled = false;
                btnDelete.Enabled = false;

                GVCombo.ClearSelection();
                GVEmployee.ClearSelection();
                GVService.ClearSelection();
                GVReceipt.ClearSelection();
                GVOrderDetail.ClearSelection();
                GVOrderEmployee.ClearSelection();
            }
            else
            {
                btnPrint.Enabled = true;
                btnReciept.Enabled = true;
                btnDelete.Enabled = true;
            }
        }
        private void DesignLayout()
        {
            DataTable zTable = new DataTable();
            DesignLayout_LVSearch(LVSearch);
            zTable = Access_Data.ListCustomer(SessionUser.UserLogin.PartnerNumber, string.Empty);
            LoadData_LVSearch(LVSearch, zTable);

            DesignLayout_GVCombo(GVCombo);
            zTable = Access_Data.ListCombo(SessionUser.UserLogin.PartnerNumber, string.Empty, null, null);
            LoadData_GVCombo(GVCombo, zTable);

            DesignLayout_GVEmployee(GVEmployee);
            zTable = Access_Data.ListEmployee(SessionUser.UserLogin.PartnerNumber, string.Empty);
            LoadData_GVEmployee(GVEmployee, zTable);

            DesignLayout_GVProduct(GVProduct);
            //zTable = Access_Data.ListProduct(SessionUser.UserLogin.PartnerNumber, string.Empty, 0);
            //LoadData_GVProduct(GVProduct, zTable);

            DesignLayout_GVService(GVService);
            //zTable = Access_Data.ListService(SessionUser.UserLogin.PartnerNumber, string.Empty, 0);
            //LoadData_GVService(GVService, zTable);

            DesignLayout_GVOrderDetail(GVOrderDetail);
            DesignLayout_GVOrderEmployeeDetail(GVOrderEmployee);
            DesignLayout_GVOrderReceipt(GVReceipt);
        }
        private void LoadCombobox()
        {
            string zSQLCategoryPro = "SELECT CategoryKey, CategoryNameVN FROM PDT_Product_Category WHERE RecordStatus <> 99 AND PartnerNumber = '" + SessionUser.UserLogin.PartnerNumber + "'";
            LoadDataToToolbox.KryptonComboBox(cboCategoryProduct, zSQLCategoryPro, "--Chọn--");

            string zSQLCategorySer = "SELECT CategoryKey, CategoryNameVN FROM PDT_Service_Category WHERE RecordStatus <> 99 AND PartnerNumber = '" + SessionUser.UserLogin.PartnerNumber + "'";
            LoadDataToToolbox.KryptonComboBox(cboCategoryService, zSQLCategorySer, "--Chọn--");

            LoadDataToToolbox.KryptonComboBox(cboPayment, "SELECT * FROM FNC_Order_PaymentStatus", "--Chọn--");
            LoadDataToToolbox.KryptonComboBox(cboStatus, "SELECT * FROM FNC_OrderStatus", "--Chọn--");
        }
        private void LoadData()
        {
            Order_FED_Info zInfo;
            if (OrderKey != "")
            {
                zInfo = new Order_FED_Info(OrderKey);

                txtOrderID.Text = zInfo.OrderID;
                dteDeliverDate.Value = zInfo.DeliverDate.Value;
                dteOrderDate.Value = zInfo.OrderDate.Value;
                dteReturnDate.Value = zInfo.ReturnDate.Value;

                txtDepartment.Text = zInfo.OrganizationName;
                txtDepartment.Tag = zInfo.OrganizationKey;
            }
            else
            {
                zInfo = new Order_FED_Info();

                cboStatus.SelectedIndex = 1;
                cboPayment.SelectedIndex = 1;

                txtOrderID.Text = Access_Data.AutoOrderID("ĐH", SessionUser.UserLogin.PartnerNumber);
                dteDeliverDate.Value = DateTime.Now;
                dteOrderDate.Value = DateTime.Now;
                dteReturnDate.Value = DateTime.Now.AddDays(7);

                txtDepartment.Text = SessionUser.UserLogin.OrganizationName;
                txtDepartment.Tag = SessionUser.UserLogin.OrganizationKey;
            }

            txtCustomerName.Tag = zInfo.BuyerKey;
            txtCustomerName.Text = zInfo.BuyerName.Trim();
            txtCustomerAddress.Text = zInfo.BuyerAddress.Trim();
            txtCustomerPhone.Text = zInfo.BuyerPhone.Trim();
            txtOrderDescription.Text = zInfo.OrderDescription.Trim();

            txtReceiptAmount.Text = zInfo.ReceiptAmount.ToString("n0", CultureInfo.GetCultureInfo("vi-VN"));
            txtSaleAmount.Text = zInfo.SaleAmount.ToString("n0", CultureInfo.GetCultureInfo("vi-VN"));

            cboStatus.SelectedValue = zInfo.OrderStatusKey;
            cboPayment.SelectedValue = zInfo.PaymentStatusKey;

            switch (zInfo.CategoryKey)
            {
                case 1:
                    rdoDress.Checked = true;
                    break;

                case 2:
                    rdoService.Checked = true;
                    break;

                default:
                    rdoAll.Checked = true;
                    break;
            }

            DataTable zTable = new DataTable();

            zTable = Access_Data.ListOrderDetail(SessionUser.UserLogin.PartnerNumber, OrderKey);
            if (zTable.Rows.Count > 0)
            {
                _ListOrderDetails = zTable.CopyToList<Order_Item_FED_Info>();
                LoadData_GVOrderDetail(GVOrderDetail);
            }
            zTable = Access_Data.ListOrderEmployee(SessionUser.UserLogin.PartnerNumber, OrderKey);
            if (zTable.Rows.Count > 0)
            {
                _ListOrderEmployee = zTable.CopyToList<Order_Employee_FED_Info>();
                LoadData_GVOrderEmployeeDetail(GVOrderEmployee);
            }
            zTable = Access_Data.ListOrderReciept(SessionUser.UserLogin.PartnerNumber, zInfo.OrderID);
            if (zTable.Rows.Count > 0)
            {
                _ListOrderReceipt = zTable.CopyToList<Receipt_Info>();
                LoadData_GVOrderReceipt(GVReceipt);
            }
        }
        private string Save()
        {
            string Message = "";

            Order_FED_Info zInfo;
            if (OrderKey != "")
            {
                zInfo = new Order_FED_Info(OrderKey);
            }
            else
            {
                zInfo = new Order_FED_Info();
                zInfo.OrderID = txtOrderID.Text.Trim();
            }

            if (rdoDress.Checked)
            {
                zInfo.CategoryKey = 1;
                zInfo.CategoryName = rdoDress.Text;
            }

            if (rdoService.Checked)
            {
                zInfo.CategoryKey = 2;
                zInfo.CategoryName = rdoService.Text;
            }

            if (rdoAll.Checked)
            {
                zInfo.CategoryKey = 3;
                zInfo.CategoryName = rdoAll.Text;
            }

            zInfo.PartnerNumber = SessionUser.UserLogin.PartnerNumber;
            zInfo.BuyerName = txtCustomerName.Text.Trim();
            zInfo.BuyerKey = txtCustomerName.Tag.ToString();
            zInfo.BuyerAddress = txtCustomerAddress.Text.Trim();
            zInfo.BuyerPhone = txtCustomerPhone.Text.Trim();
            zInfo.OrderDate = dteOrderDate.Value;
            zInfo.DeliverDate = dteDeliverDate.Value;
            zInfo.ReturnDate = dteReturnDate.Value;
            zInfo.OrderStatusKey = cboStatus.SelectedValue.ToInt();
            zInfo.OrderStatusName = cboStatus.Text;
            zInfo.PaymentStatusKey = cboPayment.SelectedValue.ToInt();
            zInfo.PaymentStatusName = cboPayment.Text;
            zInfo.OrderDescription = txtOrderDescription.Text;

            if (txtTotalAmount.Text != string.Empty)
            {
                zInfo.TotalAmount = double.Parse(txtTotalAmount.Text, CultureInfo.GetCultureInfo("vi-VN"));
            }
            if (txtSaleAmount.Text != string.Empty)
            {
                zInfo.SaleAmount = double.Parse(txtSaleAmount.Text, CultureInfo.GetCultureInfo("vi-VN"));
            }
            if (txtReceiptAmount.Text != string.Empty)
            {
                zInfo.ReceiptAmount = double.Parse(txtReceiptAmount.Text, CultureInfo.GetCultureInfo("vi-VN"));
            }
            if (txtOrderAmount.Text != string.Empty)
            {
                zInfo.OrderAmount = double.Parse(txtOrderAmount.Text, CultureInfo.GetCultureInfo("vi-VN"));
            }

            zInfo.OrganizationName = SessionUser.UserLogin.OrganizationName;
            zInfo.OrganizationKey = SessionUser.UserLogin.OrganizationKey;

            zInfo.CreatedBy = SessionUser.UserLogin.UserKey;
            zInfo.CreatedName = SessionUser.UserLogin.EmployeeName;
            zInfo.ModifiedBy = SessionUser.UserLogin.UserKey;
            zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;

            if (OrderKey != "")
            {
                zInfo.Update();
            }
            else
            {
                zInfo.Create_ClientKey();
            }

            OrderKey = zInfo.OrderKey;
            Message = zInfo.Message;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zInfo.Delete_Detail();
                if (_ListOrderDetails.Count > 0)
                {

                    foreach (Order_Item_FED_Info zItem in _ListOrderDetails)
                    {
                        zItem.OrderKey = OrderKey;
                        zItem.PartnerNumber = SessionUser.UserLogin.PartnerNumber;

                        zItem.CreatedBy = SessionUser.UserLogin.UserKey;
                        zItem.CreatedName = SessionUser.UserLogin.EmployeeName;
                        zItem.ModifiedBy = SessionUser.UserLogin.UserKey;
                        zItem.ModifiedName = SessionUser.UserLogin.EmployeeName;

                        zItem.Create_ServerKey();

                        if (zItem.Code != "200" &&
                            zItem.Code != "201")
                        {
                            Message = zItem.Message;
                            break;
                        }
                    }
                }

                if (_ListOrderEmployee.Count > 0)
                {
                    foreach (Order_Employee_FED_Info zEmployee in _ListOrderEmployee)
                    {
                        zEmployee.OrderKey = OrderKey;
                        zEmployee.PartnerNumber = SessionUser.UserLogin.PartnerNumber;

                        zEmployee.CreatedBy = SessionUser.UserLogin.UserKey;
                        zEmployee.CreatedName = SessionUser.UserLogin.EmployeeName;
                        zEmployee.ModifiedBy = SessionUser.UserLogin.UserKey;
                        zEmployee.ModifiedName = SessionUser.UserLogin.EmployeeName;

                        zEmployee.Create_ServerKey();

                        if (zEmployee.Code != "200" &&
                            zEmployee.Code != "201")
                        {
                            Message = zEmployee.Message;
                            break;
                        }
                    }
                }
            }
            return Message;
        }
        private string CheckBeforSave()
        {
            string zResult = "";
            if (txtOrderID.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng điền mã biên nhận ! \n";
                txtOrderID.StateCommon.Back.Color1 = Color.FromArgb(255, 102, 102);
            }
            else
            {
                txtOrderID.StateCommon.Back.Color1 = Color.White;
            }

            if (txtCustomerName.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng nhập thông tin khách hàng ! \n";
                txtCustomerName.StateCommon.Back.Color1 = Color.FromArgb(255, 102, 102);
            }
            else
            {
                txtCustomerName.StateCommon.Back.Color1 = Color.White;
            }

            if (txtReceiptAmount.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng nhập số tiền thu ! \n";
                txtReceiptAmount.StateCommon.Back.Color1 = Color.FromArgb(255, 102, 102);
            }
            else
            {
                txtReceiptAmount.StateCommon.Back.Color1 = Color.White;
            }

            if (OrderKey == "")
            {
                bool Count = Access_Data.CheckOrderID(SessionUser.UserLogin.PartnerNumber, txtOrderID.Text.Trim());
                if (!Count)
                {
                    zResult += "Vui lòng chọn mã phiếu biên nhận ! \n";
                    txtOrderID.StateCommon.Back.Color1 = Color.FromArgb(255, 102, 102);
                }
            }
            else
            {
                txtOrderID.StateCommon.Back.Color1 = Color.White;
            }

            bool zWarning = false;
            for (int i = 0; i < GVOrderDetail.Rows.Count; i++)
            {
                DataGridViewRow GvRow = GVOrderDetail.Rows[i];
                string ItemID = GvRow.Cells["ItemID"].Value.ToString();
                bool Exists = Access_Data.CheckItemID(ItemID);
                if (!Exists)
                {
                    zWarning = true;
                    GvRow.DefaultCellStyle.BackColor = Color.OrangeRed;
                }
                else
                {
                    GvRow.DefaultCellStyle.BackColor = Color.White;
                }
            }

            if (zWarning)
            {
                //MessageBox.Show("Có sản phẩm chưa giao đang trùng, vui lòng kiểm tra lại thông tin !. ");
                zResult = "Có sản phẩm chưa giao đang trùng, vui lòng kiểm tra lại thông tin !. ";
            }
         
            return zResult;
        }
        private string Delete()
        {
            Order_FED_Info zInfo = new Order_FED_Info(OrderKey);
            zInfo.ModifiedBy = SessionUser.UserLogin.UserKey;
            zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
            zInfo.Delete();
            zInfo.Delete_Detail();
            return zInfo.Message;
        }
        private void ClearForm()
        {
            OrderKey = "";
            _ListOrderReceipt = new List<Receipt_Info>();
            _ListOrderDetails = new List<Order_Item_FED_Info>();
            _ListOrderEmployee = new List<Order_Employee_FED_Info>();

            txtOrderID.Text = Access_Data.AutoOrderID("ĐH", SessionUser.UserLogin.PartnerNumber);
            txtCustomerName.Tag = null;
            txtCustomerAddress.Clear();
            txtCustomerName.Clear();
            txtCustomerPassport.Clear();
            txtCustomerPhone.Clear();
            txtDepartment.Clear();

            dteDeliverDate.Value = DateTime.Now;
            dteOrderDate.Value = DateTime.Now;
            dteReturnDate.Value = DateTime.Now;

            cboStatus.SelectedIndex = 1;
            cboPayment.SelectedIndex = 1;

            txtSaleAmount.Text = "0";
            txtOrderAmount.Text = "0";
            txtReceiptAmount.Text = "0";
            txtTotalAmount.Text = "0";

            GVOrderDetail.Rows.Clear();
            GVOrderEmployee.Rows.Clear();
            GVReceipt.Rows.Clear();

            GVCombo.ClearSelection();
            GVEmployee.ClearSelection();
            GVService.ClearSelection();
            GVReceipt.ClearSelection();
            GVOrderDetail.ClearSelection();
            GVOrderEmployee.ClearSelection();
        }
        #endregion

        #region [Search]
        private void BtnSearchCustomer_Click(object sender, EventArgs e)
        {
            LVSearch.Visible = true;
            LVSearch.BringToFront();

            DataTable zTable = Access_Data.ListCustomer(SessionUser.UserLogin.PartnerNumber, txtCustomerName.Text.Trim());
            LoadData_LVSearch(LVSearch, zTable);
        }
        private void DesignLayout_LVSearch(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Họ tên";
            colHead.Width = 250;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số điện thoại";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Địa chỉ";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số CMND";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            TN_Tools.TNPaintControl.DrawLVStyle(ref LVSearch);
        }

        private void LoadData_LVSearch(ListView LV, DataTable zTable)
        {
            this.Cursor = Cursors.WaitCursor;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow zRow = zTable.Rows[i];
                lvi.Tag = zRow["CustomerKey"].ToString();
                lvi.BackColor = Color.White;

                string zName = "(" + zRow["Aliases"].ToString() + ")-" + zRow["FullName"].ToString();
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zName;
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["Phone"].ToString(); ;
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["Address"].ToString(); ;
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["PassportNumber"].ToString(); ;
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }
            this.Cursor = Cursors.Default;
        }

        private void LVSearch_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            txtCustomerName.Tag = LVSearch.Items[e.ItemIndex].Tag.ToString();
            txtCustomerName.Text = LVSearch.Items[e.ItemIndex].SubItems[1].Text.Trim();
            txtCustomerPhone.Text = LVSearch.Items[e.ItemIndex].SubItems[2].Text.Trim();
            txtCustomerAddress.Text = LVSearch.Items[e.ItemIndex].SubItems[3].Text.Trim();
            txtCustomerPassport.Text = LVSearch.Items[e.ItemIndex].SubItems[4].Text.Trim();
        }

        private void LVSearch_ItemActivate(object sender, EventArgs e)
        {
            if (LVSearch.SelectedItems.Count > 0)
            {
                txtCustomerName.Tag = LVSearch.SelectedItems[0].Tag.ToString();
                txtCustomerName.Text = LVSearch.SelectedItems[0].SubItems[1].Text.Trim();
                txtCustomerPhone.Text = LVSearch.SelectedItems[0].SubItems[2].Text.Trim();
                txtCustomerAddress.Text = LVSearch.SelectedItems[0].SubItems[3].Text.Trim();
                txtCustomerPassport.Text = LVSearch.SelectedItems[0].SubItems[4].Text.Trim();
                LVSearch.Visible = false;
            }
        }

        private void LVSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtCustomerName.Tag = LVSearch.SelectedItems[0].Tag.ToString();
                txtCustomerName.Text = LVSearch.SelectedItems[0].SubItems[1].Text.Trim();
                txtCustomerPhone.Text = LVSearch.SelectedItems[0].SubItems[2].Text.Trim();
                txtCustomerAddress.Text = LVSearch.SelectedItems[0].SubItems[3].Text.Trim();
                txtCustomerPassport.Text = LVSearch.SelectedItems[0].SubItems[4].Text.Trim();
                LVSearch.Visible = false;
            }
        }

        private void TxtCustomerName_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                LVSearch.Focus();
                if (LVSearch.Items.Count > 0)
                {
                    LVSearch.Items[0].Selected = true;
                }
            }
            else
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (txtCustomerName.Text.Length > 0)
                    {
                        LVSearch.Focus();
                        LVSearch.Visible = true;
                        LVSearch.BringToFront();

                        DataTable zTable = Access_Data.ListCustomer(SessionUser.UserLogin.PartnerNumber, txtCustomerName.Text);
                        LoadData_LVSearch(LVSearch, zTable);
                    }
                    else
                    {
                        LVSearch.Visible = false;
                    }
                }
            }
        }

        private void TxtCustomerName_Leave(object sender, EventArgs e)
        {
            if (!LVSearch.Focused)
            {
                LVSearch.Visible = false;
            }

            if (txtCustomerName.Text.Trim().Length == 0)
            {
                txtCustomerName.Tag = null;
                txtCustomerName.Clear();
            }
        }
        #endregion

        #region [Left Panel]
        private void DesignLayout_GVCombo(DataGridView GV)
        {
            TNPaintControl.DrawGVStyle(ref GV);

            GV.Columns.Add("No", "STT");

            Image img = new Bitmap(TN_WinApp.Properties.Resources.image_plus);
            DataGridViewImageColumn GVImageCol = new DataGridViewImageColumn();
            GVImageCol.HeaderText = ".";
            GVImageCol.ImageLayout = DataGridViewImageCellLayout.Zoom;
            GVImageCol.Image = img;
            GVImageCol.Width = 32;
            GV.Columns.Add(GVImageCol);

            //
            GV.Columns.Add("ComboKey", "ComboKey");
            GV.Columns.Add("ComboID", "Mã");
            GV.Columns.Add("ComboName", "Tên");
            GV.Columns.Add("ComboCost", "Giá gói");
            GV.Columns.Add("Description", "Ghi chú");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["ComboKey"].Visible = false;
            //

            GV.Columns["ComboID"].Width = 80;
            GV.Columns["ComboID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["ComboName"].Width = 120;
            GV.Columns["ComboName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["ComboName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["ComboCost"].Width = 100;
            GV.Columns["ComboCost"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["Description"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            GV.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["Description"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.ReadOnly = true;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = false;
            GV.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }
        private void LoadData_GVCombo(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();

            int i = 0;
            foreach (DataRow r in Table.Rows)
            {
                GV.Rows.Add();
                DataGridViewRow zGvRow = GV.Rows[i];
                zGvRow.Cells["No"].Value = (i + 1).ToString();
                //zGvRow.Cells["ProductKey"].Value = r["ProductKey"].ToString();
                zGvRow.Cells["ComboKey"].Value = r["ComboKey"].ToString();
                zGvRow.Cells["ComboID"].Value = r["ComboID"].ToString().Trim();
                zGvRow.Cells["ComboName"].Value = r["ComboName"].ToString().Trim();

                zGvRow.Cells["Description"].Value = r["Description"].ToString().Trim();

                double ComboCost = 0;
                if (r["ComboCost"] != DBNull.Value)
                {
                    ComboCost = r["ComboCost"].ToDouble();
                    zGvRow.Cells["ComboCost"].Value = ComboCost.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                }

                i++;
            }
        }
        private void GVCombo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //Column hình dấu +
            if (GVCombo.CurrentCell.ColumnIndex == 1)
            {
                _ListOrderDetails = new List<Order_Item_FED_Info>();

                DataGridViewRow zGVRow = GVCombo.CurrentRow;
                string ComboKey = zGVRow.Cells["ComboKey"].Value.ToString();

                Order_Item_FED_Info zItem = new Order_Item_FED_Info();
                zItem.ComboKey = ComboKey;
                zItem.ItemID = zGVRow.Cells["ComboID"].Value.ToString();
                zItem.ItemName = " > Gói dịch vụ: " + zGVRow.Cells["ComboName"].Value.ToString();
                double ComboCost = zGVRow.Cells["ComboCost"].Value.ToDouble();
                zItem.SubTotal = ComboCost;
                zItem.Quantity = 1;

                _ListOrderDetails.Add(zItem);

                DataTable zTable = Access_Data.ListComboDetail(ComboKey);
                foreach (DataRow r in zTable.Rows)
                {
                    zItem = new Order_Item_FED_Info();
                    zItem.ComboKey = ComboKey;
                    string ProductKey = r["ProductKey"].ToString();
                    zItem.ProductKey = ProductKey;

                    Product_FED_Info zInfo = new Product_FED_Info(ProductKey);
                    zItem.ItemCategoryKey = zInfo.CategoryKey;
                    zItem.ItemCategoryName = zInfo.CategoryName;

                    zItem.ItemID = r["ItemID"].ToString();
                    zItem.ItemName = " ----- " + r["ItemName"].ToString();
                    float Quantity = r["Quantity"].ToFloat();
                    zItem.Quantity = Quantity;
                    zItem.UnitKey = r["UnitKey"].ToInt();
                    zItem.UnitName = r["UnitName"].ToString();
                    double StandardCost = r["StandardCost"].ToDouble();
                    zItem.StandardCost = StandardCost;

                    zItem.SubTotal = StandardCost * Quantity;
                    _ListOrderDetails.Add(zItem);
                }

                LoadData_GVOrderDetail(GVOrderDetail);
            }
        }

        private void DesignLayout_GVProduct(DataGridView GV)
        {
            TNPaintControl.DrawGVStyle(ref GV);

            GV.Columns.Add("No", "STT");

            Image img = new Bitmap(TN_WinApp.Properties.Resources.image_plus);
            DataGridViewImageColumn GVImageCol = new DataGridViewImageColumn();
            GVImageCol.HeaderText = ".";
            GVImageCol.ImageLayout = DataGridViewImageCellLayout.Zoom;
            GVImageCol.Image = img;
            GVImageCol.Width = 32;
            GV.Columns.Add(GVImageCol); //

            GV.Columns.Add("ProductKey", "ProductKey");
            GV.Columns.Add("ProductID", "Mã");
            GV.Columns.Add("ProductName", "Tên");
            GV.Columns.Add("StandardUnitKey", "StandardUnitKey");
            GV.Columns.Add("StandardUnitName", "ĐVT");
            GV.Columns.Add("RentCost", "Giá");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["ProductKey"].Visible = false;

            GV.Columns["ProductID"].Width = 80;
            GV.Columns["ProductID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["ProductName"].Width = 250;
            GV.Columns["ProductName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["ProductName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["StandardUnitKey"].Visible = false;
            GV.Columns["StandardUnitName"].Width = 80;
            GV.Columns["StandardUnitName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["RentCost"].Width = 100;
            GV.Columns["RentCost"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.ReadOnly = true;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = false;
            GV.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }
        private void LoadData_GVProduct(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();

            int i = 0;
            foreach (DataRow r in Table.Rows)
            {
                GV.Rows.Add();
                DataGridViewRow zGvRow = GV.Rows[i];
                zGvRow.Cells["No"].Value = (i + 1).ToString();
                zGvRow.Cells["ProductKey"].Value = r["ProductKey"].ToString();
                zGvRow.Cells["ProductID"].Value = r["ProductID"].ToString().Trim();
                zGvRow.Cells["ProductName"].Value = r["ProductName"].ToString().Trim();

                zGvRow.Cells["StandardUnitKey"].Value = r["StandardUnitKey"].ToString().Trim();
                zGvRow.Cells["StandardUnitName"].Value = r["StandardUnitName"].ToString().Trim();

                double RentCost = 0;
                if (r["RentCost"] != DBNull.Value)
                {
                    RentCost = r["RentCost"].ToDouble();
                    zGvRow.Cells["RentCost"].Value = RentCost.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                }

                i++;
            }
        }
        private void GVProduct_CellContentClick(object sender, EventArgs e)
        {
            //Column hình dấu +
            if (GVProduct.CurrentCell.ColumnIndex == 1)
            {
                DataGridViewRow zGVRow = GVProduct.CurrentRow;
                Order_Item_FED_Info zItem = new Order_Item_FED_Info();

                string ProductKey = zGVRow.Cells["ProductKey"].Value.ToString();
                zItem.ProductKey = ProductKey;

                Product_FED_Info zInfo = new Product_FED_Info(ProductKey);
                zItem.ItemCategoryKey = zInfo.CategoryKey;
                zItem.ItemCategoryName = zInfo.CategoryName;

                zItem.ItemID = zGVRow.Cells["ProductID"].Value.ToString();
                zItem.ItemName = zGVRow.Cells["ProductName"].Value.ToString();
                zItem.Quantity = 1;

                if (zGVRow.Cells["RentCost"].Value != DBNull.Value)
                {
                    double Cost = double.Parse(zGVRow.Cells["RentCost"].Value.ToString(), CultureInfo.GetCultureInfo("vi-VN"));
                    zItem.StandardCost = Cost;
                }

                zItem.UnitKey = zGVRow.Cells["StandardUnitKey"].Value.ToInt();
                zItem.UnitName = zGVRow.Cells["StandardUnitName"].Value.ToString();

                AddOrderItem(zItem);
                LoadData_GVOrderDetail(GVOrderDetail);
            }
        }

        private void DesignLayout_GVService(DataGridView GV)
        {
            TNPaintControl.DrawGVStyle(ref GV);

            GV.Columns.Add("No", "STT");

            Image img = new Bitmap(TN_WinApp.Properties.Resources.image_plus);
            DataGridViewImageColumn GVImageCol = new DataGridViewImageColumn();
            GVImageCol.HeaderText = ".";
            GVImageCol.ImageLayout = DataGridViewImageCellLayout.Zoom;
            GVImageCol.Image = img;
            GVImageCol.Width = 32;
            GV.Columns.Add(GVImageCol); //

            GV.Columns.Add("ServiceKey", "ServiceKey");
            GV.Columns.Add("ServiceID", "Mã");
            GV.Columns.Add("ServiceName", "Tên");
            GV.Columns.Add("StandardUnitKey", "StandardUnitKey");
            GV.Columns.Add("StandardUnitName", "ĐVT");
            GV.Columns.Add("StandardCost", "Giá thuê");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["ServiceKey"].Visible = false;

            GV.Columns["ServiceID"].Width = 80;
            GV.Columns["ServiceID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["ServiceName"].Width = 250;
            GV.Columns["ServiceName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["ServiceName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["StandardUnitKey"].Visible = false;
            GV.Columns["StandardUnitName"].Width = 80;
            GV.Columns["StandardUnitName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["StandardCost"].Width = 120;
            GV.Columns["StandardCost"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.ReadOnly = true;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = false;
            GV.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }
        private void LoadData_GVService(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();

            int i = 0;
            foreach (DataRow r in Table.Rows)
            {
                GV.Rows.Add();
                DataGridViewRow zGvRow = GV.Rows[i];
                zGvRow.Cells["No"].Value = (i + 1).ToString();
                zGvRow.Cells["ServiceID"].Value = r["ServiceID"].ToString().Trim();
                zGvRow.Cells["ServiceName"].Value = r["ServiceName"].ToString().Trim();
                zGvRow.Cells["ServiceKey"].Value = r["ServiceKey"].ToString();
                zGvRow.Cells["StandardUnitKey"].Value = r["StandardUnitKey"].ToString().Trim();
                zGvRow.Cells["StandardUnitName"].Value = r["StandardUnitName"].ToString().Trim();

                double StandardCost = 0;
                if (r["StandardCost"] != DBNull.Value)
                {
                    StandardCost = r["StandardCost"].ToDouble();
                    zGvRow.Cells["StandardCost"].Value = StandardCost.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                }
                //zGvRow.Cells["Description"].Value = r["Description"].ToString().Trim();
                i++;
            }
        }
        private void GVService_CellContentClick(object sender, EventArgs e)
        {
            //Column hình dấu +
            if (GVService.CurrentCell.ColumnIndex == 1)
            {
                DataGridViewRow zGVRow = GVService.CurrentRow;
                Order_Item_FED_Info zItemFED = new Order_Item_FED_Info();

                zItemFED.ServiceKey = zGVRow.Cells["ServiceKey"].Value.ToString();
                zItemFED.ItemID = zGVRow.Cells["ServiceID"].Value.ToString();
                zItemFED.ItemName = zGVRow.Cells["ServiceName"].Value.ToString();
                zItemFED.Quantity = 1;

                if (zGVRow.Cells["StandardCost"].Value != DBNull.Value)
                {
                    double StandardCost = double.Parse(zGVRow.Cells["StandardCost"].Value.ToString(), CultureInfo.GetCultureInfo("vi-VN"));
                    zItemFED.StandardCost = StandardCost;
                }

                zItemFED.UnitKey = zGVRow.Cells["StandardUnitKey"].Value.ToInt();
                zItemFED.UnitName = zGVRow.Cells["StandardUnitName"].Value.ToString();

                AddOrderItem(zItemFED);
                LoadData_GVOrderDetail(GVOrderDetail);
            }
        }

        private void DesignLayout_GVEmployee(DataGridView GV)
        {
            TNPaintControl.DrawGVStyle(ref GV);

            GV.Columns.Add("No", "STT");

            Image img = new Bitmap(TN_WinApp.Properties.Resources.image_plus);
            DataGridViewImageColumn GVImageCol = new DataGridViewImageColumn();
            GVImageCol.HeaderText = ".";
            GVImageCol.ImageLayout = DataGridViewImageCellLayout.Zoom;
            GVImageCol.Image = img;
            GVImageCol.Width = 32;
            GV.Columns.Add(GVImageCol); //

            GV.Columns.Add("EmployeeKey", "EmployeeKey");
            GV.Columns.Add("EmployeeID", "Mã");
            GV.Columns.Add("EmployeeName", "Tên nhân viên");
            GV.Columns.Add("JobKey", "JobKey");
            GV.Columns.Add("Phone", "SĐT");
            GV.Columns.Add("JobName", "Công việc");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["EmployeeKey"].Visible = false;

            GV.Columns["EmployeeID"].Width = 80;
            GV.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["EmployeeName"].Width = 200;
            GV.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["JobKey"].Visible = false;

            GV.Columns["JobName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            GV.Columns["JobName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.ReadOnly = true;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = false;
            GV.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }
        private void LoadData_GVEmployee(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();

            int i = 0;
            foreach (DataRow r in Table.Rows)
            {
                GV.Rows.Add();
                DataGridViewRow zGvRow = GV.Rows[i];
                zGvRow.Cells["No"].Value = (i + 1).ToString();
                zGvRow.Cells["EmployeeKey"].Value = r["EmployeeKey"].ToString().Trim();
                zGvRow.Cells["EmployeeID"].Value = r["EmployeeID"].ToString().Trim();
                zGvRow.Cells["EmployeeName"].Value = r["EmployeeName"].ToString().Trim();
                zGvRow.Cells["JobKey"].Value = r["JobKey"].ToString().Trim();
                zGvRow.Cells["JobName"].Value = r["JobName"].ToString().Trim();
                zGvRow.Cells["Phone"].Value = r["Phone"].ToString().Trim();
                i++;
            }
        }
        private void GVEmployee_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //Column hình dấu +
            if (GVEmployee.CurrentCell.ColumnIndex == 1)
            {
                DataGridViewRow zGVRow = GVEmployee.CurrentRow;

                Order_Employee_FED_Info zItem = new Order_Employee_FED_Info();
                zItem.EmployeeKey = zGVRow.Cells["EmployeeKey"].Value.ToString();
                zItem.EmployeeID = zGVRow.Cells["EmployeeID"].Value.ToString();
                zItem.EmployeeName = zGVRow.Cells["EmployeeName"].Value.ToString();
                zItem.JobKey = zGVRow.Cells["JobKey"].Value.ToInt();
                zItem.JobName = zGVRow.Cells["JobName"].Value.ToString();
                zItem.Phone = zGVRow.Cells["Phone"].Value.ToString();
                _ListOrderEmployee.Add(zItem);
                LoadData_GVOrderEmployeeDetail(GVOrderEmployee);
            }
        }
        #endregion

        #region [Right Panel]

        List<Receipt_Info> _ListOrderReceipt = new List<Receipt_Info>();
        List<Order_Item_FED_Info> _ListOrderDetails = new List<Order_Item_FED_Info>();
        List<Order_Employee_FED_Info> _ListOrderEmployee = new List<Order_Employee_FED_Info>();

        private void AddOrderItem(Order_Item_FED_Info AddInItem)
        {
            bool isExist = false;
            for (int i = 0; i < _ListOrderDetails.Count; i++)
            {
                Order_Item_FED_Info zExistItem = _ListOrderDetails[i];
                if (zExistItem.ItemID == AddInItem.ItemID)
                {
                    zExistItem.Quantity = zExistItem.Quantity + 1;
                    zExistItem.ItemCost = zExistItem.StandardCost;
                    zExistItem.SubTotal = zExistItem.Quantity * zExistItem.StandardCost;
                    isExist = true;
                    break;
                }
            }

            if (isExist == false)
            {
                AddInItem.ItemCost = AddInItem.StandardCost;
                AddInItem.SubTotal = AddInItem.Quantity * AddInItem.StandardCost;
                _ListOrderDetails.Add(AddInItem);
            }
        }
        private void DeleteOrderItem(string ItemID)
        {
            for (int i = 0; i < _ListOrderDetails.Count; i++)
            {
                Order_Item_FED_Info zExistItem = _ListOrderDetails[i];
                if (zExistItem.ItemID == ItemID)
                {
                    if (zExistItem.Quantity == 1)
                    {
                        _ListOrderDetails.Remove(zExistItem);
                    }
                    else
                    {
                        zExistItem.Quantity = zExistItem.Quantity - 1;
                        zExistItem.SubTotal = zExistItem.Quantity * zExistItem.StandardCost;
                    }

                    break;
                }
            }
        }

        private void AddItemEmployee(Order_Employee_FED_Info AddInItem)
        {
            bool isExist = false;
            for (int i = 0; i < _ListOrderEmployee.Count; i++)
            {
                Order_Employee_FED_Info zExistItem = _ListOrderEmployee[i];
                if (zExistItem.EmployeeID == AddInItem.EmployeeID)
                {
                    isExist = true;
                    break;
                }
            }

            if (isExist == false)
            {
                _ListOrderEmployee.Add(AddInItem);
            }
        }
        private void DeleteEmployee(string EmployeeID)
        {
            for (int i = 0; i < _ListOrderEmployee.Count; i++)
            {
                Order_Employee_FED_Info zExistItem = _ListOrderEmployee[i];
                if (zExistItem.EmployeeID == EmployeeID)
                {
                    _ListOrderEmployee.Remove(zExistItem);
                    break;
                }
            }
        }

        //------------------------------GV Employee ORDER
        private void DesignLayout_GVOrderEmployeeDetail(DataGridView GV)
        {
            TNPaintControl.DrawGVStyle(ref GV);
            Image imgMinus = new Bitmap(TN_WinApp.Properties.Resources.image_minus);
            DataGridViewImageColumn GVImageCol = new DataGridViewImageColumn();
            GVImageCol.HeaderText = ".";
            GVImageCol.ImageLayout = DataGridViewImageCellLayout.Zoom;
            GVImageCol.Image = imgMinus;
            GVImageCol.Width = 32;
            GV.Columns.Add(GVImageCol); //

            GV.Columns.Add("No", "STT");
            GV.Columns.Add("EmployeeKey", "EmployeeKey");
            GV.Columns.Add("EmployeeID", "Mã số");
            GV.Columns.Add("EmployeeName", "Tên nhân sự");
            GV.Columns.Add("JobKey", "JobKey");
            GV.Columns.Add("Phone", "SĐT");
            GV.Columns.Add("JobName", "Công việc");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["EmployeeKey"].Visible = false;

            GV.Columns["EmployeeID"].Width = 80;
            GV.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["EmployeeName"].Width = 150;
            GV.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["JobKey"].Visible = false;

            GV.Columns["Phone"].Width = 120;
            GV.Columns["Phone"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["JobName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            GV.Columns["JobName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            //custom option style
            GV.ReadOnly = true;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = false;
            GV.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }
        private void LoadData_GVOrderEmployeeDetail(DataGridView GV)
        {
            GV.Rows.Clear();
            if (_ListOrderEmployee.Count > 0)
            {
                for (int i = 0; i < _ListOrderEmployee.Count; i++)
                {
                    Order_Employee_FED_Info zExistItem = _ListOrderEmployee[i];
                    GV.Rows.Add();

                    DataGridViewRow zGvRow = GV.Rows[i];
                    zGvRow.Cells["No"].Value = (i + 1).ToString();
                    zGvRow.Cells["EmployeeKey"].Value = zExistItem.ItemKey.Trim();
                    zGvRow.Cells["EmployeeID"].Value = zExistItem.EmployeeID.Trim();
                    zGvRow.Cells["EmployeeName"].Value = zExistItem.EmployeeName.ToString().Trim();
                    zGvRow.Cells["JobKey"].Value = zExistItem.JobKey;
                    zGvRow.Cells["JobName"].Value = zExistItem.JobName.ToString().Trim();
                    zGvRow.Cells["Phone"].Value = zExistItem.Phone.ToString().Trim();
                }
            }
        }
        private void GvOrderEmployee_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (GVOrderEmployee.CurrentCell.ColumnIndex == 0)
            {
                DataGridViewRow zGvRow = GVOrderEmployee.CurrentRow;
                string EmployeeID = zGvRow.Cells["EmployeeID"].Value.ToString();
                DeleteEmployee(EmployeeID);
                LoadData_GVOrderEmployeeDetail(GVOrderEmployee);
            }
        }

        //------------------------------GV Product & Service ORDER
        private void DesignLayout_GVOrderDetail(DataGridView GV)
        {
            TNPaintControl.DrawGVStyle(ref GV);

            Image imgMinus = new Bitmap(TN_WinApp.Properties.Resources.image_minus);
            DataGridViewImageColumn GVImageCol = new DataGridViewImageColumn();
            GVImageCol.HeaderText = ".";
            GVImageCol.ImageLayout = DataGridViewImageCellLayout.Zoom;
            GVImageCol.Image = imgMinus;
            GVImageCol.Width = 32;
            GV.Columns.Add(GVImageCol); //

            GV.Columns.Add("No", "STT");
            GV.Columns.Add("ItemKey", "ItemKey");
            GV.Columns.Add("ComboKey", "ComboKey");
            GV.Columns.Add("ItemID", "Mã");
            GV.Columns.Add("ItemName", "Tên");
            GV.Columns.Add("UnitKey", "UnitKey");
            GV.Columns.Add("UnitName", "ĐVT");
            GV.Columns.Add("Quantity", "SL");
            GV.Columns.Add("ItemCost", "Đơn giá");
            GV.Columns.Add("SubTotal", "Thành tiền");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["ComboKey"].Visible = false;
            GV.Columns["ItemKey"].Visible = false;


            GV.Columns["ItemID"].Width = 80;
            GV.Columns["ItemID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["ItemName"].Width = 250;
            GV.Columns["ItemName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["ItemName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["UnitKey"].Visible = false;

            GV.Columns["UnitName"].Width = 70;
            GV.Columns["UnitName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["Quantity"].Width = 70;
            GV.Columns["Quantity"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["ItemCost"].Width = 120;
            GV.Columns["ItemCost"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["SubTotal"].Width = 120;
            GV.Columns["SubTotal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.ReadOnly = true;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = false;
            GV.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }
        private void LoadData_GVOrderDetail(DataGridView GV)
        {
            GV.Rows.Clear();
            if (_ListOrderDetails.Count > 0)
            {
                double zTotal = 0;
                for (int i = 0; i < _ListOrderDetails.Count; i++)
                {
                    Order_Item_FED_Info zExistItem = _ListOrderDetails[i];
                    GV.Rows.Add();

                    DataGridViewRow zGvRow = GV.Rows[i];
                    zGvRow.Cells["No"].Value = (i + 1).ToString();
                    zGvRow.Cells["ItemID"].Value = zExistItem.ItemID.Trim();
                    zGvRow.Cells["ItemName"].Value = zExistItem.ItemName.ToString().Trim();
                    zGvRow.Cells["Quantity"].Value = zExistItem.Quantity;
                    zGvRow.Cells["UnitKey"].Value = zExistItem.UnitKey.ToString().Trim();
                    zGvRow.Cells["UnitName"].Value = zExistItem.UnitName.ToString().Trim();
                    zGvRow.Cells["ItemCost"].Value = zExistItem.StandardCost.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                    zGvRow.Cells["SubTotal"].Value = zExistItem.SubTotal.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                    zTotal += zExistItem.SubTotal;
                }

                txtTotalAmount.Text = zTotal.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
            }
            CalculatorOrder();
        }
        private void GVOrderDetail_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (GVOrderDetail.CurrentCell.ColumnIndex == 0)
            {
                DataGridViewRow zGvRow = GVOrderDetail.CurrentRow;
                string ItemID = zGvRow.Cells["ItemID"].Value.ToString();
                DeleteOrderItem(ItemID);
                LoadData_GVOrderDetail(GVOrderDetail);
            }
        }

        //------------------------------GV Receipt ORDER
        private void DesignLayout_GVOrderReceipt(DataGridView GV)
        {
            TNPaintControl.DrawGVStyle(ref GV);

            GV.Columns.Add("No", "STT");
            GV.Columns.Add("ReceiptKey", "ReceiptKey");
            GV.Columns.Add("ReceiptID", "Mã số");
            GV.Columns.Add("ReceiptDate", "Ngày thu");
            GV.Columns.Add("AmountCurrencyMain", "Số tiền");
            GV.Columns.Add("ReceiptDescription", "Ghi chú");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["ReceiptKey"].Visible = false;

            GV.Columns["ReceiptID"].Width = 120;
            GV.Columns["ReceiptID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["ReceiptDate"].Width = 120;
            GV.Columns["ReceiptDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["AmountCurrencyMain"].Width = 150;
            GV.Columns["AmountCurrencyMain"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["ReceiptDescription"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            //custom option style
            GV.ReadOnly = true;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = false;
            GV.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }
        private void LoadData_GVOrderReceipt(DataGridView GV)
        {
            GV.Rows.Clear();
            if (_ListOrderReceipt.Count > 0)
            {
                double zTotal = 0;
                for (int i = 0; i < _ListOrderReceipt.Count; i++)
                {
                    Receipt_Info zExistItem = _ListOrderReceipt[i];
                    GV.Rows.Add();

                    DataGridViewRow zGvRow = GV.Rows[i];
                    zGvRow.Cells["No"].Value = (i + 1).ToString();
                    zGvRow.Cells["ReceiptID"].Value = zExistItem.ReceiptID.Trim();
                    zGvRow.Cells["ReceiptKey"].Value = zExistItem.ReceiptKey.Trim();
                    zGvRow.Cells["ReceiptDescription"].Value = zExistItem.ReceiptDescription.Trim();

                    if (zExistItem.ReceiptDate != DateTime.MinValue)
                    {
                        zGvRow.Cells["ReceiptDate"].Value = zExistItem.ReceiptDate.ToString("dd/MM/yyyy");
                    }

                    zGvRow.Cells["AmountCurrencyMain"].Value = zExistItem.AmountCurrencyMain.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                    zTotal += zExistItem.AmountCurrencyMain;
                }

                txtReceiptAmount.Text = zTotal.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
            }
            CalculatorOrder();
        }

        //
        private void CalculatorOrder()
        {
            double TotalAmount = double.Parse(txtTotalAmount.Text, CultureInfo.GetCultureInfo("vi-VN"));
            double SaleAmount = double.Parse(txtSaleAmount.Text, CultureInfo.GetCultureInfo("vi-VN"));
            double ReceiptAmount = double.Parse(txtReceiptAmount.Text, CultureInfo.GetCultureInfo("vi-VN"));
            double OrderAmount = TotalAmount - SaleAmount - ReceiptAmount;

            txtTotalAmount.Text = TotalAmount.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
            txtSaleAmount.Text = SaleAmount.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
            txtReceiptAmount.Text = ReceiptAmount.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
            txtOrderAmount.Text = OrderAmount.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
        }
        #endregion

        #region [Dùng kéo rê form]
        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;



        private void Frm_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }
        private void Frm_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }
        private void Frm_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        #region [Auth]
        User_Role_Info _RoleForm = new User_Role_Info();
        private void Get_Auth()
        {
            List<User_Role_Info> zListRole = SessionUser.TableRole;
            if (zListRole.Count > 0)
            {
                try
                {
                    _RoleForm = zListRole.SingleOrDefault(r => r.RoleURL == _FormUrl);
                    if (_RoleForm == null)
                    {
                        _RoleForm = new User_Role_Info();
                        MessageBox.Show("Form này chưa được thêm quyền, vui lòng liên hệ IT !.");
                        this.Close();
                    }
                    else
                    {
                        if (!_RoleForm.RoleRead)
                        {
                            MessageBox.Show("Bạn không có quyền xem, vui lòng liên hệ IT !.");
                            this.Close();
                        }
                        if (!_RoleForm.RoleEdit)
                        {
                            btnSave.Visible = false;
                        }
                        if (!_RoleForm.RoleAdd)
                        {
                            btnSave.Visible = false;
                        }
                        if (!_RoleForm.RoleDel)
                        {
                            btnDelete.Visible = false;
                        }
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Bạn chưa được phân quyền, vui lòng liên hệ IT !.");
            }
        }
        #endregion

        #region [Access Info-Data]
        public class Access_Data
        {
            #region[Standard]
            public static DataTable ListCustomer(string PartnerNumber, string Name)
            {
                DataTable zTable = new DataTable();
                string zSQL = @"
                SELECT A.CustomerKey, A.CustomerID, A.FullName, A.Aliases, B.PassportNumber, B.LastName + ' ' + B.FirstName AS CustomerName, A.Phone, A.Address
                FROM [dbo].[CRM_Customer] A 
                LEFT JOIN [dbo].[CRM_Person] B ON A.CustomerKey = B.CustomerKey 
                WHERE A.RecordStatus <> 99 ";
                if (Name.Trim().Length > 0)
                {
                    zSQL += " AND A.FullName LIKE @Name";
                }

                zSQL += " AND A.PartnerNumber = @PartnerNumber ORDER BY A.CreatedOn DESC";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string Result = ex.ToString();
                }
                return zTable;
            }
            public static DataTable ListEmployee(string PartnerNumber, string Name)
            {
                DataTable zTable = new DataTable();
                string zSQL = @"
                SELECT EmployeeKey, EmployeeID, LastName, FirstName, LastName + ' ' + FirstName AS EmployeeName, JobKey, JobName, CompanyPhone AS Phone FROM [dbo].[HRM_Employee] 
                WHERE RecordStatus <> 99 ";
                if (Name.Trim().Length > 0)
                {
                    zSQL += " AND(LastName + ' ' + FirstName) LIKE @Name";
                }

                zSQL += " AND PartnerNumber= @PartnerNumber ORDER BY JobName";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string Result = ex.ToString();
                }
                return zTable;
            }
            #endregion

            public static bool CheckItemID(string ItemID)
            {
                bool zResult = false;  //  Khong co
                DataTable zTable = new DataTable();
                string zSQL = @"
SELECT Count(*) AS Amount
FROM FNC_Order_FED A
LEFT JOIN FNC_Order_Item_FED B ON A.OrderKey = B.OrderKey
WHERE A.RecordStatus <> 99
AND A.OrderStatusKey = 1
AND B.ItemID = @ItemID";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = ItemID;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string Result = ex.ToString();
                }
                if (zTable.Rows.Count > 0)
                {
                    int zAmount = 0;
                    DataRow zRow = zTable.Rows[0];
                    zAmount = int.Parse(zRow["Amount"].ToString());
                    if (zAmount == 0)
                    {
                        zResult = true;
                    }
                    else
                    {
                        zResult = false;
                    }
                }
                return zResult;
            }

            public static bool CheckOrderID(string PartnerNumber, string OrderID)
            {
                bool zResult = false;  //  Khong co
                DataTable zTable = new DataTable();
                string zSQL = @"SELECT Count(*) AS Amount FROM [dbo].[FNC_Order_FED] WHERE PartnerNumber = @PartnerNumber AND OrderID = @OrderID AND RecordStatus != 99";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = OrderID;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string Result = ex.ToString();
                }
                if (zTable.Rows.Count > 0)
                {
                    int zAmount = 0;
                    DataRow zRow = zTable.Rows[0];
                    zAmount = int.Parse(zRow["Amount"].ToString());
                    if (zAmount == 0)
                    {
                        zResult = true;
                    }
                    else
                    {
                        zResult = false;
                    }
                }
                return zResult;
            }
            public static string AutoOrderID(string Prefix, string PartnerNumber)
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "SELECT dbo.Auto_OrderFED_ID(@Prefix,@PartnerNumber)";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@Prefix", SqlDbType.NVarChar).Value = Prefix;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zResult = zCommand.ExecuteScalar().ToString();
                    zCommand.Dispose();
                }
                catch (Exception)
                {

                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }

            public static DataTable ListCombo(string PartnerNumber, string Name, DateTime? FromDate, DateTime? ToDate)
            {
                DataTable zTable = new DataTable();
                string zSQL = @"
SELECT * 
FROM [dbo].[PDT_Combo_FED] 
WHERE RecordStatus <> 99  
AND PartnerNumber = @PartnerNumber ";

                if (Name.Trim().Length > 0)
                {
                    zSQL += " AND (ComboID LIKE @Name OR ComboName LIKE @Name)";
                }

                if (FromDate != null && ToDate != null)
                {
                    zSQL += " AND FromDate >= @FromDate AND ToDate <= @ToDate";
                }

                zSQL += " ORDER BY CreatedOn DESC";
                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                    if (FromDate != null && ToDate != null)
                    {
                        zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                        zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                    }
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
            public static DataTable ListProduct(string PartnerNumber, string Name, int Category)
            {
                DataTable zTable = new DataTable();
                string zSQL = @"
SELECT A.*    --, B.ColorName, B.SizeName
FROM PDT_Product_FED A
--LEFT JOIN PDT_Product_Property_FED B ON A.ProductKey = B.ProductKey
WHERE 
A.RecordStatus <> 99 
AND A.StatusKey = 1
--AND B.RecordStatus <> 99  
AND A.PartnerNumber= @PartnerNumber ";

                if (Category != 0)
                {
                    zSQL += "AND A.CategoryKey = @CategoryKey";
                }
                if (Name.Trim().Length > 0)
                {
                    zSQL += " AND (A.ProductID LIKE @Name OR A.ProductName LIKE @Name)";
                }

                zSQL += " ORDER BY A.CreatedOn DESC";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Category;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
            public static DataTable ListService(string PartnerNumber, string Name, int Category)
            {
                DataTable zTable = new DataTable();
                string zSQL = @"
SELECT A.*
FROM PDT_Service A
WHERE A.RecordStatus <> 99  
AND A.PartnerNumber= @PartnerNumber ";

                if (Category != 0)
                {
                    zSQL = @"AND A.CategoryKey = @CategoryKey";
                }
                if (Name.Trim().Length > 0)
                {
                    zSQL += " AND (A.ServiceID LIKE @Name OR A.ServiceName LIKE @Name)";
                }

                zSQL += " ORDER BY A.CreatedOn DESC";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Category;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
            public static DataTable ListComboDetail(string ComboKey)
            {
                DataTable zTable = new DataTable();
                string zSQL = @"
SELECT A.*
FROM PDT_Combo_Item_FED A
WHERE A.RecordStatus <> 99  
AND A.ComboKey= @ComboKey ";

                zSQL += " ORDER BY A.CreatedOn DESC";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = new Guid(ComboKey);
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }

            public static DataTable ListOrderDetail(string PartnerNumber, string OrderKey)
            {
                DataTable zTable = new DataTable();
                string zSQL = "SELECT * FROM [dbo].[FNC_Order_Item_FED] WHERE PartnerNumber = @PartnerNumber AND OrderKey = @OrderKey AND RecordStatus != 99 ";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(OrderKey);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
            public static DataTable ListOrderEmployee(string PartnerNumber, string OrderKey)
            {
                DataTable zTable = new DataTable();
                string zSQL = "SELECT * FROM [dbo].[FNC_Order_Employee_FED] WHERE PartnerNumber = @PartnerNumber AND OrderKey = @OrderKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(OrderKey);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
            public static DataTable ListOrderReciept(string PartnerNumber, string OrderID)
            {
                DataTable zTable = new DataTable();
                string zSQL = "SELECT * FROM [dbo].[FNC_Receipt] WHERE PartnerNumber = @PartnerNumber AND DocumentID = @OrderID AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = OrderID;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
        }
        public class Receipt_Info
        {

            #region [ Field Name ]
            private string _ReceiptKey = "";
            private string _ReceiptID = "";
            private DateTime _ReceiptDate;
            private string _ReceiptDescription = "";
            private string _DocumentID = "";
            private int _CategoryKey = 0;
            private string _CategoryName = "";
            private int _StyleKey = 0;
            private string _CustomerKey = "";
            private string _CustomerName = "";
            private string _Depositor = "";
            private string _Address = "";
            private double _AmountCurrencyMain = 0;
            private double _AmountCurrencyForeign = 0;
            private string _CurrencyIDForeign = "";
            private double _CurrencyRate = 0;
            private string _BankName = "";
            private string _BankAccount = "";
            private double _BankVAT = 0;
            private double _BankFee = 0;
            private bool _IsFeeInside;
            private string _DebitNo = "";
            private string _CreditNo = "";
            private string _PartnerNumber = "";
            private int _RecordStatus = 0;
            private DateTime? _CreatedOn = null;
            private string _CreatedBy = "";
            private string _CreatedName = "";
            private DateTime? _ModifiedOn = null;
            private string _ModifiedBy = "";
            private string _ModifiedName = "";
            private string _Message = "";
            #endregion

            #region [ Constructor Get Information ]
            public Receipt_Info()
            {
                Guid zNewID = Guid.NewGuid();
                _ReceiptKey = zNewID.ToString();
            }
            public Receipt_Info(string ReceiptKey)
            {
                string zSQL = "SELECT * FROM [dbo].[FNC_Receipt] WHERE ReceiptKey = @ReceiptKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ReceiptKey", SqlDbType.UniqueIdentifier).Value = new Guid(ReceiptKey);
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _ReceiptKey = zReader["ReceiptKey"].ToString();
                        _ReceiptID = zReader["ReceiptID"].ToString();
                        if (zReader["ReceiptDate"] != DBNull.Value)
                        {
                            _ReceiptDate = (DateTime)zReader["ReceiptDate"];
                        }

                        _ReceiptDescription = zReader["ReceiptDescription"].ToString();
                        _DocumentID = zReader["DocumentID"].ToString();
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                        _CategoryName = zReader["CategoryName"].ToString();
                        _StyleKey = int.Parse(zReader["StyleKey"].ToString());
                        _CustomerKey = zReader["CustomerKey"].ToString();
                        _CustomerName = zReader["CustomerName"].ToString();
                        _Depositor = zReader["Depositor"].ToString();
                        _Address = zReader["Address"].ToString();
                        _AmountCurrencyMain = double.Parse(zReader["AmountCurrencyMain"].ToString());
                        _AmountCurrencyForeign = double.Parse(zReader["AmountCurrencyForeign"].ToString());
                        _CurrencyIDForeign = zReader["CurrencyIDForeign"].ToString();
                        _CurrencyRate = double.Parse(zReader["CurrencyRate"].ToString());
                        _BankName = zReader["BankName"].ToString();
                        _BankAccount = zReader["BankAccount"].ToString();
                        _BankVAT = double.Parse(zReader["BankVAT"].ToString());
                        _BankFee = double.Parse(zReader["BankFee"].ToString());
                        _IsFeeInside = (bool)zReader["IsFeeInside"];
                        _DebitNo = zReader["DebitNo"].ToString();
                        _CreditNo = zReader["CreditNo"].ToString();
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }

            #endregion

            #region [ Properties ]
            public string ReceiptKey
            {
                get { return _ReceiptKey; }
                set { _ReceiptKey = value; }
            }
            public string ReceiptID
            {
                get { return _ReceiptID; }
                set { _ReceiptID = value; }
            }
            public DateTime ReceiptDate
            {
                get { return _ReceiptDate; }
                set { _ReceiptDate = value; }
            }
            public string ReceiptDescription
            {
                get { return _ReceiptDescription; }
                set { _ReceiptDescription = value; }
            }
            public string DocumentID
            {
                get { return _DocumentID; }
                set { _DocumentID = value; }
            }
            public int CategoryKey
            {
                get { return _CategoryKey; }
                set { _CategoryKey = value; }
            }
            public string CategoryName
            {
                get { return _CategoryName; }
                set { _CategoryName = value; }
            }
            public int StyleKey
            {
                get { return _StyleKey; }
                set { _StyleKey = value; }
            }
            public string CustomerKey
            {
                get { return _CustomerKey; }
                set { _CustomerKey = value; }
            }
            public string CustomerName
            {
                get { return _CustomerName; }
                set { _CustomerName = value; }
            }
            public string Depositor
            {
                get { return _Depositor; }
                set { _Depositor = value; }
            }
            public string Address
            {
                get { return _Address; }
                set { _Address = value; }
            }
            public double AmountCurrencyMain
            {
                get { return _AmountCurrencyMain; }
                set { _AmountCurrencyMain = value; }
            }
            public double AmountCurrencyForeign
            {
                get { return _AmountCurrencyForeign; }
                set { _AmountCurrencyForeign = value; }
            }
            public string CurrencyIDForeign
            {
                get { return _CurrencyIDForeign; }
                set { _CurrencyIDForeign = value; }
            }
            public double CurrencyRate
            {
                get { return _CurrencyRate; }
                set { _CurrencyRate = value; }
            }
            public string BankName
            {
                get { return _BankName; }
                set { _BankName = value; }
            }
            public string BankAccount
            {
                get { return _BankAccount; }
                set { _BankAccount = value; }
            }
            public double BankVAT
            {
                get { return _BankVAT; }
                set { _BankVAT = value; }
            }
            public double BankFee
            {
                get { return _BankFee; }
                set { _BankFee = value; }
            }
            public bool IsFeeInside
            {
                get { return _IsFeeInside; }
                set { _IsFeeInside = value; }
            }
            public string DebitNo
            {
                get { return _DebitNo; }
                set { _DebitNo = value; }
            }
            public string CreditNo
            {
                get { return _CreditNo; }
                set { _CreditNo = value; }
            }
            public string PartnerNumber
            {
                get { return _PartnerNumber; }
                set { _PartnerNumber = value; }
            }
            public int RecordStatus
            {
                get { return _RecordStatus; }
                set { _RecordStatus = value; }
            }
            public DateTime? CreatedOn
            {
                get { return _CreatedOn; }
                set { _CreatedOn = value; }
            }
            public string CreatedBy
            {
                get { return _CreatedBy; }
                set { _CreatedBy = value; }
            }
            public string CreatedName
            {
                get { return _CreatedName; }
                set { _CreatedName = value; }
            }
            public DateTime? ModifiedOn
            {
                get { return _ModifiedOn; }
                set { _ModifiedOn = value; }
            }
            public string ModifiedBy
            {
                get { return _ModifiedBy; }
                set { _ModifiedBy = value; }
            }
            public string ModifiedName
            {
                get { return _ModifiedName; }
                set { _ModifiedName = value; }
            }
            public string Code
            {
                get
                {
                    if (_Message.Length >= 3)
                    {
                        return _Message.Substring(0, 3);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            public string Message
            {
                get { return _Message; }
                set { _Message = value; }
            }
            #endregion

            #region [ Constructor Update Information ]

            public string Create_ServerKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[FNC_Receipt] ("
            + " ReceiptID ,ReceiptDate ,ReceiptDescription ,DocumentID ,CategoryKey ,CategoryName ,StyleKey ,CustomerKey ,CustomerName ,Depositor ,Address ,AmountCurrencyMain ,AmountCurrencyForeign ,CurrencyIDForeign ,CurrencyRate ,BankName ,BankAccount ,BankVAT ,BankFee ,IsFeeInside ,DebitNo ,CreditNo ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@ReceiptID ,@ReceiptDate ,@ReceiptDescription ,@DocumentID ,@CategoryKey ,@CategoryName ,@StyleKey ,@CustomerKey ,@CustomerName ,@Depositor ,@Address ,@AmountCurrencyMain ,@AmountCurrencyForeign ,@CurrencyIDForeign ,@CurrencyRate ,@BankName ,@BankAccount ,@BankVAT ,@BankFee ,@IsFeeInside ,@DebitNo ,@CreditNo ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ReceiptID", SqlDbType.NVarChar).Value = _ReceiptID;
                    if (_ReceiptDate == DateTime.MinValue)
                    {
                        zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = _ReceiptDate;
                    }

                    zCommand.Parameters.Add("@ReceiptDescription", SqlDbType.NVarChar).Value = _ReceiptDescription;
                    zCommand.Parameters.Add("@DocumentID", SqlDbType.NVarChar).Value = _DocumentID;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@StyleKey", SqlDbType.Int).Value = _StyleKey;
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_CustomerKey);
                    zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = _CustomerName;
                    zCommand.Parameters.Add("@Depositor", SqlDbType.NVarChar).Value = _Depositor;
                    zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                    zCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = _AmountCurrencyMain;
                    zCommand.Parameters.Add("@AmountCurrencyForeign", SqlDbType.Money).Value = _AmountCurrencyForeign;
                    zCommand.Parameters.Add("@CurrencyIDForeign", SqlDbType.NChar).Value = _CurrencyIDForeign;
                    zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = _CurrencyRate;
                    zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = _BankName;
                    zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = _BankAccount;
                    zCommand.Parameters.Add("@BankVAT", SqlDbType.Money).Value = _BankVAT;
                    zCommand.Parameters.Add("@BankFee", SqlDbType.Money).Value = _BankFee;
                    zCommand.Parameters.Add("@IsFeeInside", SqlDbType.Bit).Value = _IsFeeInside;
                    zCommand.Parameters.Add("@DebitNo", SqlDbType.NVarChar).Value = _DebitNo;
                    zCommand.Parameters.Add("@CreditNo", SqlDbType.NVarChar).Value = _CreditNo;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Create_ClientKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[FNC_Receipt] ("
            + " ReceiptKey ,ReceiptID ,ReceiptDate ,ReceiptDescription ,DocumentID ,CategoryKey ,CategoryName ,StyleKey ,CustomerKey ,CustomerName ,Depositor ,Address ,AmountCurrencyMain ,AmountCurrencyForeign ,CurrencyIDForeign ,CurrencyRate ,BankName ,BankAccount ,BankVAT ,BankFee ,IsFeeInside ,DebitNo ,CreditNo ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@ReceiptKey ,@ReceiptID ,@ReceiptDate ,@ReceiptDescription ,@DocumentID ,@CategoryKey ,@CategoryName ,@StyleKey ,@CustomerKey ,@CustomerName ,@Depositor ,@Address ,@AmountCurrencyMain ,@AmountCurrencyForeign ,@CurrencyIDForeign ,@CurrencyRate ,@BankName ,@BankAccount ,@BankVAT ,@BankFee ,@IsFeeInside ,@DebitNo ,@CreditNo ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ReceiptKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ReceiptKey);
                    zCommand.Parameters.Add("@ReceiptID", SqlDbType.NVarChar).Value = _ReceiptID;
                    if (_ReceiptDate == DateTime.MinValue)
                    {
                        zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = _ReceiptDate;
                    }

                    zCommand.Parameters.Add("@ReceiptDescription", SqlDbType.NVarChar).Value = _ReceiptDescription;
                    zCommand.Parameters.Add("@DocumentID", SqlDbType.NVarChar).Value = _DocumentID;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@StyleKey", SqlDbType.Int).Value = _StyleKey;
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_CustomerKey);
                    zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = _CustomerName;
                    zCommand.Parameters.Add("@Depositor", SqlDbType.NVarChar).Value = _Depositor;
                    zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                    zCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = _AmountCurrencyMain;
                    zCommand.Parameters.Add("@AmountCurrencyForeign", SqlDbType.Money).Value = _AmountCurrencyForeign;
                    zCommand.Parameters.Add("@CurrencyIDForeign", SqlDbType.NChar).Value = _CurrencyIDForeign;
                    zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = _CurrencyRate;
                    zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = _BankName;
                    zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = _BankAccount;
                    zCommand.Parameters.Add("@BankVAT", SqlDbType.Money).Value = _BankVAT;
                    zCommand.Parameters.Add("@BankFee", SqlDbType.Money).Value = _BankFee;
                    zCommand.Parameters.Add("@IsFeeInside", SqlDbType.Bit).Value = _IsFeeInside;
                    zCommand.Parameters.Add("@DebitNo", SqlDbType.NVarChar).Value = _DebitNo;
                    zCommand.Parameters.Add("@CreditNo", SqlDbType.NVarChar).Value = _CreditNo;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Update()
            {
                string zSQL = "UPDATE [dbo].[FNC_Receipt] SET "
                            + " ReceiptID = @ReceiptID,"
                            + " ReceiptDate = @ReceiptDate,"
                            + " ReceiptDescription = @ReceiptDescription,"
                            + " DocumentID = @DocumentID,"
                            + " CategoryKey = @CategoryKey,"
                            + " CategoryName = @CategoryName,"
                            + " StyleKey = @StyleKey,"
                            + " CustomerKey = @CustomerKey,"
                            + " CustomerName = @CustomerName,"
                            + " Depositor = @Depositor,"
                            + " Address = @Address,"
                            + " AmountCurrencyMain = @AmountCurrencyMain,"
                            + " AmountCurrencyForeign = @AmountCurrencyForeign,"
                            + " CurrencyIDForeign = @CurrencyIDForeign,"
                            + " CurrencyRate = @CurrencyRate,"
                            + " BankName = @BankName,"
                            + " BankAccount = @BankAccount,"
                            + " BankVAT = @BankVAT,"
                            + " BankFee = @BankFee,"
                            + " IsFeeInside = @IsFeeInside,"
                            + " DebitNo = @DebitNo,"
                            + " CreditNo = @CreditNo,"
                            + " PartnerNumber = @PartnerNumber,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedOn = GetDate(),"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName"
                           + " WHERE ReceiptKey = @ReceiptKey";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ReceiptKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ReceiptKey);
                    zCommand.Parameters.Add("@ReceiptID", SqlDbType.NVarChar).Value = _ReceiptID;
                    if (_ReceiptDate == DateTime.MinValue)
                    {
                        zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = _ReceiptDate;
                    }

                    zCommand.Parameters.Add("@ReceiptDescription", SqlDbType.NVarChar).Value = _ReceiptDescription;
                    zCommand.Parameters.Add("@DocumentID", SqlDbType.NVarChar).Value = _DocumentID;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@StyleKey", SqlDbType.Int).Value = _StyleKey;
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_CustomerKey);
                    zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = _CustomerName;
                    zCommand.Parameters.Add("@Depositor", SqlDbType.NVarChar).Value = _Depositor;
                    zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                    zCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = _AmountCurrencyMain;
                    zCommand.Parameters.Add("@AmountCurrencyForeign", SqlDbType.Money).Value = _AmountCurrencyForeign;
                    zCommand.Parameters.Add("@CurrencyIDForeign", SqlDbType.NChar).Value = _CurrencyIDForeign;
                    zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = _CurrencyRate;
                    zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = _BankName;
                    zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = _BankAccount;
                    zCommand.Parameters.Add("@BankVAT", SqlDbType.Money).Value = _BankVAT;
                    zCommand.Parameters.Add("@BankFee", SqlDbType.Money).Value = _BankFee;
                    zCommand.Parameters.Add("@IsFeeInside", SqlDbType.Bit).Value = _IsFeeInside;
                    zCommand.Parameters.Add("@DebitNo", SqlDbType.NVarChar).Value = _DebitNo;
                    zCommand.Parameters.Add("@CreditNo", SqlDbType.NVarChar).Value = _CreditNo;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Delete()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[FNC_Receipt] Set RecordStatus = 99 WHERE ReceiptKey = @ReceiptKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ReceiptKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ReceiptKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[FNC_Receipt] WHERE ReceiptKey = @ReceiptKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ReceiptKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ReceiptKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            #endregion
        }
        public class Order_Item_FED_Info
        {
            #region [ Field Name ]
            private string _ItemKey = "";
            private string _OrderKey = "";
            private string _ProductKey = "";
            private string _ServiceKey = "";
            private int _ItemCategoryKey = 0;
            private string _ItemCategoryName = "";
            private string _ItemID = "";
            private string _ItemName = "";
            private int _UnitKey = 0;
            private string _UnitName = "";
            private float _Quantity;
            private double _ItemCost = 0;
            private double _StandardCost = 0;
            private double _SaleCost = 0;
            private string _ComboKey = "";
            private double _SubTotal = 0;
            private string _Style = "";
            private string _Class = "";
            private string _CodeLine = "";
            private string _PartnerNumber = "";
            private int _RecordStatus = 0;
            private DateTime? _CreatedOn = null;
            private string _CreatedBy = "";
            private string _CreatedName = "";
            private DateTime? _ModifiedOn = null;
            private string _ModifiedBy = "";
            private string _ModifiedName = "";
            private string _Message = "";
            #endregion

            #region [ Constructor Get Information ]
            public Order_Item_FED_Info()
            {
                Guid zNewID = Guid.NewGuid();
                _ItemKey = zNewID.ToString();
            }
            public Order_Item_FED_Info(string ItemKey)
            {
                string zSQL = "SELECT * FROM [dbo].[FNC_Order_Item_FED] WHERE ItemKey = @ItemKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ItemKey", SqlDbType.UniqueIdentifier).Value = new Guid(ItemKey);
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _ItemKey = zReader["ItemKey"].ToString();
                        _OrderKey = zReader["OrderKey"].ToString();
                        _ProductKey = zReader["ProductKey"].ToString();
                        _ItemID = zReader["ItemID"].ToString();
                        _ItemName = zReader["ItemName"].ToString();
                        _UnitKey = int.Parse(zReader["UnitKey"].ToString());
                        _UnitName = zReader["UnitName"].ToString();
                        _Quantity = float.Parse(zReader["Quantity"].ToString());
                        _ItemCost = double.Parse(zReader["ItemCost"].ToString());
                        _StandardCost = double.Parse(zReader["StandardCost"].ToString());
                        _SaleCost = double.Parse(zReader["SaleCost"].ToString());
                        _ComboKey = zReader["ComboKey"].ToString();
                        _SubTotal = double.Parse(zReader["SubTotal"].ToString());
                        _Style = zReader["Style"].ToString();
                        _Class = zReader["Class"].ToString();
                        _CodeLine = zReader["CodeLine"].ToString();
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }

            #endregion

            #region [ Properties ]
            public string ItemKey
            {
                get { return _ItemKey; }
                set { _ItemKey = value; }
            }
            public string OrderKey
            {
                get { return _OrderKey; }
                set { _OrderKey = value; }
            }
            public string ProductKey
            {
                get { return _ProductKey; }
                set { _ProductKey = value; }
            }
            public string ItemID
            {
                get { return _ItemID; }
                set { _ItemID = value; }
            }
            public string ItemName
            {
                get { return _ItemName; }
                set { _ItemName = value; }
            }
            public int UnitKey
            {
                get { return _UnitKey; }
                set { _UnitKey = value; }
            }
            public string UnitName
            {
                get { return _UnitName; }
                set { _UnitName = value; }
            }
            public float Quantity
            {
                get { return _Quantity; }
                set { _Quantity = value; }
            }
            public double ItemCost
            {
                get { return _ItemCost; }
                set { _ItemCost = value; }
            }
            public double StandardCost
            {
                get { return _StandardCost; }
                set { _StandardCost = value; }
            }
            public double SaleCost
            {
                get { return _SaleCost; }
                set { _SaleCost = value; }
            }
            public string ComboKey
            {
                get { return _ComboKey; }
                set { _ComboKey = value; }
            }
            public double SubTotal
            {
                get { return _SubTotal; }
                set { _SubTotal = value; }
            }
            public string Style
            {
                get { return _Style; }
                set { _Style = value; }
            }
            public string Class
            {
                get { return _Class; }
                set { _Class = value; }
            }
            public string CodeLine
            {
                get { return _CodeLine; }
                set { _CodeLine = value; }
            }
            public string PartnerNumber
            {
                get { return _PartnerNumber; }
                set { _PartnerNumber = value; }
            }
            public int RecordStatus
            {
                get { return _RecordStatus; }
                set { _RecordStatus = value; }
            }
            public DateTime? CreatedOn
            {
                get { return _CreatedOn; }
                set { _CreatedOn = value; }
            }
            public string CreatedBy
            {
                get { return _CreatedBy; }
                set { _CreatedBy = value; }
            }
            public string CreatedName
            {
                get { return _CreatedName; }
                set { _CreatedName = value; }
            }
            public DateTime? ModifiedOn
            {
                get { return _ModifiedOn; }
                set { _ModifiedOn = value; }
            }
            public string ModifiedBy
            {
                get { return _ModifiedBy; }
                set { _ModifiedBy = value; }
            }
            public string ModifiedName
            {
                get { return _ModifiedName; }
                set { _ModifiedName = value; }
            }
            public string Code
            {
                get
                {
                    if (_Message.Length >= 3)
                    {
                        return _Message.Substring(0, 3);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            public string Message
            {
                get { return _Message; }
                set { _Message = value; }
            }

            public int ItemCategoryKey
            {
                get
                {
                    return _ItemCategoryKey;
                }

                set
                {
                    _ItemCategoryKey = value;
                }
            }

            public string ItemCategoryName
            {
                get
                {
                    return _ItemCategoryName;
                }

                set
                {
                    _ItemCategoryName = value;
                }
            }

            public string ServiceKey
            {
                get
                {
                    return _ServiceKey;
                }

                set
                {
                    _ServiceKey = value;
                }
            }
            #endregion

            #region [ Constructor Update Information ]
            public string Create_ServerKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[FNC_Order_Item_FED] (" +
                    " OrderKey ,ProductKey , ItemCategoryKey, ItemCategoryName, ItemID ,ItemName ,UnitKey ,UnitName ,Quantity ,ItemCost ,StandardCost ,SaleCost , ComboKey , ServiceKey," +
                    " SubTotal ,Style ,Class ,CodeLine ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) " +
                    " VALUES ( " +
                    " @OrderKey ,@ProductKey ,@ItemCategoryKey, @ItemCategoryName, @ItemID ,@ItemName ,@UnitKey ,@UnitName ,@Quantity ,@ItemCost ,@StandardCost ,@SaleCost ,@ComboKey , @ServiceKey," +
                    " @SubTotal ,@Style ,@Class ,@CodeLine ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);

                    if (_ProductKey != string.Empty && _ProductKey.Length == 36)
                    {
                        zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ProductKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }

                    if (_ServiceKey != string.Empty && _ServiceKey.Length == 36)
                    {
                        zCommand.Parameters.Add("@ServiceKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ServiceKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ServiceKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }

                    if (_ComboKey != string.Empty && _ComboKey.Length == 36)
                    {
                        zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ComboKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }

                    zCommand.Parameters.Add("@ItemCategoryKey", SqlDbType.Int).Value = _ItemCategoryKey;
                    zCommand.Parameters.Add("@ItemCategoryName", SqlDbType.NVarChar).Value = _ItemCategoryName;

                    zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = _ItemID;
                    zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = _ItemName;
                    zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = _UnitKey;
                    zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = _UnitName;
                    zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = _Quantity;
                    zCommand.Parameters.Add("@ItemCost", SqlDbType.Money).Value = _ItemCost;
                    zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = _StandardCost;
                    zCommand.Parameters.Add("@SaleCost", SqlDbType.Money).Value = _SaleCost;
                    zCommand.Parameters.Add("@SubTotal", SqlDbType.Money).Value = _SubTotal;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = _CodeLine;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Create_ClientKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[FNC_Order_Item_FED] (" +
                    " ItemKey ,OrderKey ,ProductKey , ItemCategoryKey, ItemCategoryName, ItemID ,ItemName ,UnitKey ,UnitName ,Quantity ,ItemCost ,StandardCost ,SaleCost ,ComboKey , ServiceKey," +
                    " SubTotal ,Style ,Class ,CodeLine ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) " +
                    " VALUES ( " +
                    " @ItemKey ,@OrderKey ,@ProductKey , @ItemCategoryKey, @ItemCategoryName, @ItemID ,@ItemName ,@UnitKey ,@UnitName ,@Quantity ,@ItemCost ,@StandardCost ,@SaleCost ,@ComboKey , @ServiceKey," +
                    " @SubTotal ,@Style ,@Class ,@CodeLine ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ItemKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ItemKey);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);

                    if (_ProductKey != string.Empty && _ProductKey.Length == 36)
                    {
                        zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ProductKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }

                    if (_ServiceKey != string.Empty && _ServiceKey.Length == 36)
                    {
                        zCommand.Parameters.Add("@ServiceKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ServiceKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ServiceKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }

                    if (_ComboKey != string.Empty && _ComboKey.Length == 36)
                    {
                        zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ComboKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }

                    zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = _ItemID;
                    zCommand.Parameters.Add("@ItemCategoryKey", SqlDbType.Int).Value = _ItemCategoryKey;
                    zCommand.Parameters.Add("@ItemCategoryName", SqlDbType.NVarChar).Value = _ItemCategoryName;

                    zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = _ItemName;
                    zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = _UnitKey;
                    zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = _UnitName;
                    zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = _Quantity;
                    zCommand.Parameters.Add("@ItemCost", SqlDbType.Money).Value = _ItemCost;
                    zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = _StandardCost;
                    zCommand.Parameters.Add("@SaleCost", SqlDbType.Money).Value = _SaleCost;
                    zCommand.Parameters.Add("@SubTotal", SqlDbType.Money).Value = _SubTotal;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = _CodeLine;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Update()
            {
                string zSQL = "UPDATE [dbo].[FNC_Order_Item_FED] SET "
                            + " OrderKey = @OrderKey,"
                            + " ProductKey = @ProductKey,"
                            + " ItemID = @ItemID,"
                            + " ItemName = @ItemName,"
                            + " UnitKey = @UnitKey,"
                            + " UnitName = @UnitName,"
                            + " Quantity = @Quantity,"
                            + " ItemCost = @ItemCost,"
                            + " StandardCost = @StandardCost,"
                            + " SaleCost = @SaleCost,"
                            + " ComboKey = @ComboKey,"
                            + " SubTotal = @SubTotal,"
                            + " Style = @Style,"
                            + " Class = @Class,"
                            + " CodeLine = @CodeLine,"
                            + " PartnerNumber = @PartnerNumber,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedOn = GetDate(),"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName"
                           + " WHERE ItemKey = @ItemKey";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ItemKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ItemKey);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ProductKey);
                    zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = _ItemID;
                    zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = _ItemName;
                    zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = _UnitKey;
                    zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = _UnitName;
                    zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = _Quantity;
                    zCommand.Parameters.Add("@ItemCost", SqlDbType.Money).Value = _ItemCost;
                    zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = _StandardCost;
                    zCommand.Parameters.Add("@SaleCost", SqlDbType.Money).Value = _SaleCost;
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ComboKey);
                    zCommand.Parameters.Add("@SubTotal", SqlDbType.Money).Value = _SubTotal;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = _CodeLine;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Delete()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[FNC_Order_Item_FED] Set RecordStatus = 99 WHERE ItemKey = @ItemKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ItemKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ItemKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[FNC_Order_Item_FED] WHERE ItemKey = @ItemKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ItemKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ItemKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            #endregion
        }
        public class Order_Employee_FED_Info
        {
            #region [ Field Name ]
            private string _ItemKey = "";
            private string _OrderKey = "";
            private string _EmployeeKey = "";
            private string _EmployeeID = "";
            private string _EmployeeName = "";
            private int _JobKey = 0;
            private string _JobName = "";
            private string _Phone = "";
            private string _PartnerNumber = "";
            private int _RecordStatus = 0;
            private string _CreatedBy = "";
            private string _CreatedName = "";
            private DateTime? _CreatedOn = null;
            private string _ModifiedBy = "";
            private string _ModifiedName = "";
            private DateTime? _ModifiedOn = null;
            private string _Message = "";
            #endregion

            #region [ Constructor Get Information ]
            public Order_Employee_FED_Info()
            {
                Guid zNewID = Guid.NewGuid();
                _ItemKey = zNewID.ToString();
            }
            public Order_Employee_FED_Info(string ItemKey)
            {
                string zSQL = "SELECT * FROM [dbo].[FNC_Order_Employee_FED] WHERE ItemKey = @ItemKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ItemKey", SqlDbType.UniqueIdentifier).Value = new Guid(ItemKey);
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _ItemKey = zReader["ItemKey"].ToString();
                        _OrderKey = zReader["OrderKey"].ToString();
                        _EmployeeKey = zReader["EmployeeKey"].ToString();
                        _EmployeeID = zReader["EmployeeID"].ToString();
                        _EmployeeName = zReader["EmployeeName"].ToString();
                        _JobKey = int.Parse(zReader["JobKey"].ToString());
                        _JobName = zReader["JobName"].ToString();
                        _Phone = zReader["Phone"].ToString();
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }

            #endregion

            #region [ Properties ]
            public string ItemKey
            {
                get { return _ItemKey; }
                set { _ItemKey = value; }
            }
            public string OrderKey
            {
                get { return _OrderKey; }
                set { _OrderKey = value; }
            }
            public string EmployeeKey
            {
                get { return _EmployeeKey; }
                set { _EmployeeKey = value; }
            }
            public string EmployeeID
            {
                get { return _EmployeeID; }
                set { _EmployeeID = value; }
            }
            public string EmployeeName
            {
                get { return _EmployeeName; }
                set { _EmployeeName = value; }
            }
            public int JobKey
            {
                get { return _JobKey; }
                set { _JobKey = value; }
            }
            public string JobName
            {
                get { return _JobName; }
                set { _JobName = value; }
            }
            public string Phone
            {
                get { return _Phone; }
                set { _Phone = value; }
            }
            public string PartnerNumber
            {
                get { return _PartnerNumber; }
                set { _PartnerNumber = value; }
            }
            public int RecordStatus
            {
                get { return _RecordStatus; }
                set { _RecordStatus = value; }
            }
            public string CreatedBy
            {
                get { return _CreatedBy; }
                set { _CreatedBy = value; }
            }
            public string CreatedName
            {
                get { return _CreatedName; }
                set { _CreatedName = value; }
            }
            public DateTime? CreatedOn
            {
                get { return _CreatedOn; }
                set { _CreatedOn = value; }
            }
            public string ModifiedBy
            {
                get { return _ModifiedBy; }
                set { _ModifiedBy = value; }
            }
            public string ModifiedName
            {
                get { return _ModifiedName; }
                set { _ModifiedName = value; }
            }
            public DateTime? ModifiedOn
            {
                get { return _ModifiedOn; }
                set { _ModifiedOn = value; }
            }
            public string Code
            {
                get
                {
                    if (_Message.Length >= 3)
                    {
                        return _Message.Substring(0, 3);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            public string Message
            {
                get { return _Message; }
                set { _Message = value; }
            }
            #endregion

            #region [ Constructor Update Information ]

            public string Create_ServerKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[FNC_Order_Employee_FED] ("
                 + " OrderKey ,EmployeeKey ,EmployeeID ,EmployeeName ,JobKey ,JobName ,Phone ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName)"
                 + " VALUES ( "
                 + "@OrderKey ,@EmployeeKey ,@EmployeeID ,@EmployeeName ,@JobKey ,@JobName ,@Phone ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName)";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = new Guid(_EmployeeKey);
                    zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID;
                    zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName;
                    zCommand.Parameters.Add("@JobKey", SqlDbType.Int).Value = _JobKey;
                    zCommand.Parameters.Add("@JobName", SqlDbType.NVarChar).Value = _JobName;
                    zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = _Phone;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Create_ClientKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[FNC_Order_Employee_FED] ("
                + " ItemKey ,OrderKey ,EmployeeKey ,EmployeeID ,EmployeeName ,JobKey ,JobName ,Phone ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName)"
                + " VALUES ( "
                + "@ItemKey ,@OrderKey ,@EmployeeKey ,@EmployeeID ,@EmployeeName ,@JobKey ,@JobName ,@Phone ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName )";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ItemKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ItemKey);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = new Guid(_EmployeeKey);
                    zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID;
                    zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName;
                    zCommand.Parameters.Add("@JobKey", SqlDbType.Int).Value = _JobKey;
                    zCommand.Parameters.Add("@JobName", SqlDbType.NVarChar).Value = _JobName;
                    zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = _Phone;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Update()
            {
                string zSQL = "UPDATE [dbo].[FNC_Order_Employee_FED] SET "
                            + " OrderKey = @OrderKey,"
                            + " EmployeeKey = @EmployeeKey,"
                            + " EmployeeID = @EmployeeID,"
                            + " EmployeeName = @EmployeeName,"
                            + " JobKey = @JobKey,"
                            + " JobName = @JobName,"
                            + " Phone = @Phone,"
                            + " PartnerNumber = @PartnerNumber,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName,"
                            + " ModifiedOn = GetDate()"
                           + " WHERE ItemKey = @ItemKey";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ItemKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ItemKey);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = new Guid(_EmployeeKey);
                    zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID;
                    zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName;
                    zCommand.Parameters.Add("@JobKey", SqlDbType.Int).Value = _JobKey;
                    zCommand.Parameters.Add("@JobName", SqlDbType.NVarChar).Value = _JobName;
                    zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = _Phone;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Delete()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[FNC_Order_Employee_FED] Set RecordStatus = 99 WHERE ItemKey = @ItemKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ItemKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ItemKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[FNC_Order_Employee_FED] WHERE ItemKey = @ItemKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ItemKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ItemKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            #endregion
        }
        public class Order_FED_Info
        {
            #region [ Field Name ]
            private string _OrderKey = "";
            private string _OrderID = "";
            private DateTime? _OrderDate = null;
            private string _OrderDescription = "";
            private string _SellerKey = "";
            private string _SellerName = "";
            private string _BuyerKey = "";
            private string _BuyerName = "";
            private string _BuyerPhone = "";
            private string _BuyerAddress = "";
            private int _OrderStatusKey = 0;
            private string _OrderStatusName = "";
            private int _PaymentStatusKey = 0;
            private string _PaymentStatusName = "";
            private DateTime? _DeliverDate = null;
            private DateTime? _ReturnDate = null;
            private int _CategoryKey = 0;
            private string _CategoryName = "";
            private int _OrganizationKey = 0;
            private string _OrganizationID = "";
            private string _OrganizationName = "";
            private double _TotalAmount = 0;
            private double _SaleAmount = 0;
            private double _OrderAmount = 0;
            private double _ReceiptAmount = 0;
            private string _PartnerNumber = "";
            private int _RecordStatus = 0;
            private DateTime? _CreatedOn = null;
            private string _CreatedBy = "";
            private string _CreatedName = "";
            private DateTime? _ModifiedOn = null;
            private string _ModifiedBy = "";
            private string _ModifiedName = "";
            private string _Message = "";
            #endregion

            #region [ Constructor Get Information ]
            public Order_FED_Info()
            {
                Guid zNewID = Guid.NewGuid();
                _OrderKey = zNewID.ToString();
            }
            public Order_FED_Info(string OrderKey)
            {
                string zSQL = "SELECT * FROM [dbo].[FNC_Order_FED] WHERE OrderKey = @OrderKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(OrderKey);
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _OrderKey = zReader["OrderKey"].ToString();
                        _OrderID = zReader["OrderID"].ToString();
                        if (zReader["OrderDate"] != DBNull.Value)
                        {
                            _OrderDate = (DateTime)zReader["OrderDate"];
                        }

                        _OrderDescription = zReader["OrderDescription"].ToString();
                        _SellerKey = zReader["SellerKey"].ToString();
                        _SellerName = zReader["SellerName"].ToString();
                        _BuyerKey = zReader["BuyerKey"].ToString();
                        _BuyerName = zReader["BuyerName"].ToString();
                        _BuyerPhone = zReader["BuyerPhone"].ToString();
                        _BuyerAddress = zReader["BuyerAddress"].ToString();
                        _OrderStatusKey = int.Parse(zReader["OrderStatusKey"].ToString());
                        _OrderStatusName = zReader["OrderStatusName"].ToString();
                        _PaymentStatusKey = int.Parse(zReader["PaymentStatusKey"].ToString());
                        _PaymentStatusName = zReader["PaymentStatusName"].ToString();
                        if (zReader["DeliverDate"] != DBNull.Value)
                        {
                            _DeliverDate = (DateTime)zReader["DeliverDate"];
                        }

                        if (zReader["ReturnDate"] != DBNull.Value)
                        {
                            _ReturnDate = (DateTime)zReader["ReturnDate"];
                        }

                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                        _CategoryName = zReader["CategoryName"].ToString();
                        _OrganizationKey = int.Parse(zReader["OrganizationKey"].ToString());
                        _OrganizationID = zReader["OrganizationID"].ToString();
                        _OrganizationName = zReader["OrganizationName"].ToString();
                        _TotalAmount = double.Parse(zReader["TotalAmount"].ToString());
                        _SaleAmount = double.Parse(zReader["SaleAmount"].ToString());
                        _OrderAmount = double.Parse(zReader["OrderAmount"].ToString());
                        _ReceiptAmount = double.Parse(zReader["ReceiptAmount"].ToString());
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }

            #endregion

            #region [ Properties ]
            public string OrderKey
            {
                get { return _OrderKey; }
                set { _OrderKey = value; }
            }
            public string OrderID
            {
                get { return _OrderID; }
                set { _OrderID = value; }
            }
            public DateTime? OrderDate
            {
                get { return _OrderDate; }
                set { _OrderDate = value; }
            }
            public string OrderDescription
            {
                get { return _OrderDescription; }
                set { _OrderDescription = value; }
            }
            public string SellerKey
            {
                get { return _SellerKey; }
                set { _SellerKey = value; }
            }
            public string SellerName
            {
                get { return _SellerName; }
                set { _SellerName = value; }
            }
            public string BuyerKey
            {
                get { return _BuyerKey; }
                set { _BuyerKey = value; }
            }
            public string BuyerName
            {
                get { return _BuyerName; }
                set { _BuyerName = value; }
            }
            public string BuyerPhone
            {
                get { return _BuyerPhone; }
                set { _BuyerPhone = value; }
            }
            public string BuyerAddress
            {
                get { return _BuyerAddress; }
                set { _BuyerAddress = value; }
            }
            public int OrderStatusKey
            {
                get { return _OrderStatusKey; }
                set { _OrderStatusKey = value; }
            }
            public string OrderStatusName
            {
                get { return _OrderStatusName; }
                set { _OrderStatusName = value; }
            }
            public int PaymentStatusKey
            {
                get { return _PaymentStatusKey; }
                set { _PaymentStatusKey = value; }
            }
            public string PaymentStatusName
            {
                get { return _PaymentStatusName; }
                set { _PaymentStatusName = value; }
            }
            public DateTime? DeliverDate
            {
                get { return _DeliverDate; }
                set { _DeliverDate = value; }
            }
            public DateTime? ReturnDate
            {
                get { return _ReturnDate; }
                set { _ReturnDate = value; }
            }
            public int CategoryKey
            {
                get { return _CategoryKey; }
                set { _CategoryKey = value; }
            }
            public string CategoryName
            {
                get { return _CategoryName; }
                set { _CategoryName = value; }
            }
            public int OrganizationKey
            {
                get { return _OrganizationKey; }
                set { _OrganizationKey = value; }
            }
            public string OrganizationID
            {
                get { return _OrganizationID; }
                set { _OrganizationID = value; }
            }
            public string OrganizationName
            {
                get { return _OrganizationName; }
                set { _OrganizationName = value; }
            }
            public double TotalAmount
            {
                get { return _TotalAmount; }
                set { _TotalAmount = value; }
            }
            public double SaleAmount
            {
                get { return _SaleAmount; }
                set { _SaleAmount = value; }
            }
            public double OrderAmount
            {
                get { return _OrderAmount; }
                set { _OrderAmount = value; }
            }
            public double ReceiptAmount
            {
                get { return _ReceiptAmount; }
                set { _ReceiptAmount = value; }
            }
            public string PartnerNumber
            {
                get { return _PartnerNumber; }
                set { _PartnerNumber = value; }
            }
            public int RecordStatus
            {
                get { return _RecordStatus; }
                set { _RecordStatus = value; }
            }
            public DateTime? CreatedOn
            {
                get { return _CreatedOn; }
                set { _CreatedOn = value; }
            }
            public string CreatedBy
            {
                get { return _CreatedBy; }
                set { _CreatedBy = value; }
            }
            public string CreatedName
            {
                get { return _CreatedName; }
                set { _CreatedName = value; }
            }
            public DateTime? ModifiedOn
            {
                get { return _ModifiedOn; }
                set { _ModifiedOn = value; }
            }
            public string ModifiedBy
            {
                get { return _ModifiedBy; }
                set { _ModifiedBy = value; }
            }
            public string ModifiedName
            {
                get { return _ModifiedName; }
                set { _ModifiedName = value; }
            }
            public string Code
            {
                get
                {
                    if (_Message.Length >= 3)
                    {
                        return _Message.Substring(0, 3);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            public string Message
            {
                get { return _Message; }
                set { _Message = value; }
            }
            #endregion

            #region [ Constructor Update Information ]
            public string Create_ServerKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[FNC_Order_FED] ("
            + " OrderID ,OrderDate ,OrderDescription ,SellerKey ,SellerName ,BuyerKey ,BuyerName ,BuyerPhone ,BuyerAddress ,OrderStatusKey ,OrderStatusName ,PaymentStatusKey ,PaymentStatusName ,DeliverDate ,ReturnDate ,CategoryKey ,CategoryName ,OrganizationKey ,OrganizationID ,OrganizationName ,TotalAmount ,SaleAmount ,OrderAmount ,ReceiptAmount ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@OrderID ,@OrderDate ,@OrderDescription ,@SellerKey ,@SellerName ,@BuyerKey ,@BuyerName ,@BuyerPhone ,@BuyerAddress ,@OrderStatusKey ,@OrderStatusName ,@PaymentStatusKey ,@PaymentStatusName ,@DeliverDate ,@ReturnDate ,@CategoryKey ,@CategoryName ,@OrganizationKey ,@OrganizationID ,@OrganizationName ,@TotalAmount ,@SaleAmount ,@OrderAmount ,@ReceiptAmount ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID;
                    if (_OrderDate == null)
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                    }

                    zCommand.Parameters.Add("@OrderDescription", SqlDbType.NVarChar).Value = _OrderDescription;
                    if (_SellerKey != string.Empty && _SellerKey.Length == 36)
                    {
                        zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_SellerKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }
                    zCommand.Parameters.Add("@SellerName", SqlDbType.NVarChar).Value = _SellerName;
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_BuyerKey);
                    zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = _BuyerName;
                    zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = _BuyerPhone;
                    zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = _BuyerAddress;
                    zCommand.Parameters.Add("@OrderStatusKey", SqlDbType.Int).Value = _OrderStatusKey;
                    zCommand.Parameters.Add("@OrderStatusName", SqlDbType.NVarChar).Value = _OrderStatusName;
                    zCommand.Parameters.Add("@PaymentStatusKey", SqlDbType.Int).Value = _PaymentStatusKey;
                    zCommand.Parameters.Add("@PaymentStatusName", SqlDbType.NVarChar).Value = _PaymentStatusName;
                    if (_DeliverDate == null)
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = _DeliverDate;
                    }

                    if (_ReturnDate == null)
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = _ReturnDate;
                    }

                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = _OrganizationKey;
                    zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = _OrganizationID;
                    zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = _OrganizationName;
                    zCommand.Parameters.Add("@TotalAmount", SqlDbType.Money).Value = _TotalAmount;
                    zCommand.Parameters.Add("@SaleAmount", SqlDbType.Money).Value = _SaleAmount;
                    zCommand.Parameters.Add("@OrderAmount", SqlDbType.Money).Value = _OrderAmount;
                    zCommand.Parameters.Add("@ReceiptAmount", SqlDbType.Money).Value = _ReceiptAmount;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Create_ClientKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[FNC_Order_FED] ("
            + " OrderKey ,OrderID ,OrderDate ,OrderDescription ,SellerKey ,SellerName ,BuyerKey ,BuyerName ,BuyerPhone ,BuyerAddress ,OrderStatusKey ,OrderStatusName ,PaymentStatusKey ,PaymentStatusName ,DeliverDate ,ReturnDate ,CategoryKey ,CategoryName ,OrganizationKey ,OrganizationID ,OrganizationName ,TotalAmount ,SaleAmount ,OrderAmount ,ReceiptAmount ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@OrderKey ,@OrderID ,@OrderDate ,@OrderDescription ,@SellerKey ,@SellerName ,@BuyerKey ,@BuyerName ,@BuyerPhone ,@BuyerAddress ,@OrderStatusKey ,@OrderStatusName ,@PaymentStatusKey ,@PaymentStatusName ,@DeliverDate ,@ReturnDate ,@CategoryKey ,@CategoryName ,@OrganizationKey ,@OrganizationID ,@OrganizationName ,@TotalAmount ,@SaleAmount ,@OrderAmount ,@ReceiptAmount ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID;
                    if (_OrderDate == null)
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                    }

                    zCommand.Parameters.Add("@OrderDescription", SqlDbType.NVarChar).Value = _OrderDescription;
                    if (_SellerKey != string.Empty && _SellerKey.Length == 36)
                    {
                        zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_SellerKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }

                    zCommand.Parameters.Add("@SellerName", SqlDbType.NVarChar).Value = _SellerName;
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_BuyerKey);
                    zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = _BuyerName;
                    zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = _BuyerPhone;
                    zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = _BuyerAddress;
                    zCommand.Parameters.Add("@OrderStatusKey", SqlDbType.Int).Value = _OrderStatusKey;
                    zCommand.Parameters.Add("@OrderStatusName", SqlDbType.NVarChar).Value = _OrderStatusName;
                    zCommand.Parameters.Add("@PaymentStatusKey", SqlDbType.Int).Value = _PaymentStatusKey;
                    zCommand.Parameters.Add("@PaymentStatusName", SqlDbType.NVarChar).Value = _PaymentStatusName;
                    if (_DeliverDate == null)
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = _DeliverDate;
                    }

                    if (_ReturnDate == null)
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = _ReturnDate;
                    }

                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = _OrganizationKey;
                    zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = _OrganizationID;
                    zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = _OrganizationName;
                    zCommand.Parameters.Add("@TotalAmount", SqlDbType.Money).Value = _TotalAmount;
                    zCommand.Parameters.Add("@SaleAmount", SqlDbType.Money).Value = _SaleAmount;
                    zCommand.Parameters.Add("@OrderAmount", SqlDbType.Money).Value = _OrderAmount;
                    zCommand.Parameters.Add("@ReceiptAmount", SqlDbType.Money).Value = _ReceiptAmount;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Update()
            {
                string zSQL = "UPDATE [dbo].[FNC_Order_FED] SET "
                            + " OrderID = @OrderID,"
                            + " OrderDate = @OrderDate,"
                            + " OrderDescription = @OrderDescription,"
                            + " SellerKey = @SellerKey,"
                            + " SellerName = @SellerName,"
                            + " BuyerKey = @BuyerKey,"
                            + " BuyerName = @BuyerName,"
                            + " BuyerPhone = @BuyerPhone,"
                            + " BuyerAddress = @BuyerAddress,"
                            + " OrderStatusKey = @OrderStatusKey,"
                            + " OrderStatusName = @OrderStatusName,"
                            + " PaymentStatusKey = @PaymentStatusKey,"
                            + " PaymentStatusName = @PaymentStatusName,"
                            + " DeliverDate = @DeliverDate,"
                            + " ReturnDate = @ReturnDate,"
                            + " CategoryKey = @CategoryKey,"
                            + " CategoryName = @CategoryName,"
                            + " OrganizationKey = @OrganizationKey,"
                            + " OrganizationID = @OrganizationID,"
                            + " OrganizationName = @OrganizationName,"
                            + " TotalAmount = @TotalAmount,"
                            + " SaleAmount = @SaleAmount,"
                            + " OrderAmount = @OrderAmount,"
                            + " ReceiptAmount = @ReceiptAmount,"
                            + " PartnerNumber = @PartnerNumber,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedOn = GetDate(),"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName"
                           + " WHERE OrderKey = @OrderKey";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID;
                    if (_OrderDate == null)
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                    }

                    zCommand.Parameters.Add("@OrderDescription", SqlDbType.NVarChar).Value = _OrderDescription;
                    if (_SellerKey != string.Empty && _SellerKey.Length == 36)
                    {
                        zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_SellerKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }
                    zCommand.Parameters.Add("@SellerName", SqlDbType.NVarChar).Value = _SellerName;
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_BuyerKey);
                    zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = _BuyerName;
                    zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = _BuyerPhone;
                    zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = _BuyerAddress;
                    zCommand.Parameters.Add("@OrderStatusKey", SqlDbType.Int).Value = _OrderStatusKey;
                    zCommand.Parameters.Add("@OrderStatusName", SqlDbType.NVarChar).Value = _OrderStatusName;
                    zCommand.Parameters.Add("@PaymentStatusKey", SqlDbType.Int).Value = _PaymentStatusKey;
                    zCommand.Parameters.Add("@PaymentStatusName", SqlDbType.NVarChar).Value = _PaymentStatusName;
                    if (_DeliverDate == null)
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = _DeliverDate;
                    }

                    if (_ReturnDate == null)
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = _ReturnDate;
                    }

                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = _OrganizationKey;
                    zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = _OrganizationID;
                    zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = _OrganizationName;
                    zCommand.Parameters.Add("@TotalAmount", SqlDbType.Money).Value = _TotalAmount;
                    zCommand.Parameters.Add("@SaleAmount", SqlDbType.Money).Value = _SaleAmount;
                    zCommand.Parameters.Add("@OrderAmount", SqlDbType.Money).Value = _OrderAmount;
                    zCommand.Parameters.Add("@ReceiptAmount", SqlDbType.Money).Value = _ReceiptAmount;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Delete()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[FNC_Order_FED] Set RecordStatus = 99 WHERE OrderKey = @OrderKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Delete_Detail()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = @"
UPDATE [dbo].[FNC_Order_Employee_FED] Set RecordStatus = 99 WHERE OrderKey = @OrderKey
UPDATE [dbo].[FNC_Order_Item_FED] Set RecordStatus = 99 WHERE OrderKey = @OrderKey
";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[FNC_Order_FED] WHERE OrderKey = @OrderKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty_Detail()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = @"
DELETE FROM [dbo].[FNC_Order_Employee_FED] WHERE OrderKey = @OrderKey
DELETE FROM [dbo].[FNC_Order_Item_FED] WHERE OrderKey = @OrderKey
DELETE FROM [dbo].[FNC_Receipt] WHERE DocumentID = @OrderID
";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = OrderID;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            #endregion
        }
        public class Product_FED_Info
        {

            #region [ Field Name ]
            private string _ProductKey = "";
            private string _ProductID = "";
            private string _ProductName = "";
            private string _ProductNumber = "";
            private string _ProductSerial = "";
            private string _ProductModel = "";
            private double _StandardCost = 0;
            private double _RentCost = 0;
            private double _ProductPrice = 0;
            private int _StandardUnitKey = 0;
            private string _StandardUnitName = "";
            private DateTime? _DiscontinuedDate = null;
            private float _SafetyInStock;
            private string _BarCode = "";
            private string _QRCode = "";
            private string _Style = "";
            private string _Class = "";
            private string _ProductLine = "";
            private int _CategoryKey = 0;
            private string _CategoryName = "";
            private int _ProductModeKey = 0;
            private string _PhotoPath = "";
            private int _StatusKey = 0;
            private string _StatusName = "";
            private string _Description = "";
            private string _PartnerNumber = "";
            private int _RecordStatus = 0;
            private DateTime? _CreatedOn = null;
            private string _CreatedBy = "";
            private string _CreatedName = "";
            private DateTime? _ModifiedOn = null;
            private string _ModifiedBy = "";
            private string _ModifiedName = "";
            private string _Message = "";
            #endregion

            #region [ Constructor Get Information ]
            public Product_FED_Info()
            {
                Guid zNewID = Guid.NewGuid();
                _ProductKey = zNewID.ToString();
            }
            public Product_FED_Info(string ProductKey)
            {
                string zSQL = "SELECT * FROM [dbo].[PDT_Product_FED] WHERE ProductKey = @ProductKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(ProductKey);
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _ProductKey = zReader["ProductKey"].ToString();
                        _ProductID = zReader["ProductID"].ToString();
                        _ProductName = zReader["ProductName"].ToString();
                        _ProductNumber = zReader["ProductNumber"].ToString();
                        _ProductSerial = zReader["ProductSerial"].ToString();
                        _ProductModel = zReader["ProductModel"].ToString();
                        _StandardCost = double.Parse(zReader["StandardCost"].ToString());
                        _RentCost = double.Parse(zReader["RentCost"].ToString());
                        _ProductPrice = double.Parse(zReader["ProductPrice"].ToString());
                        _StandardUnitKey = int.Parse(zReader["StandardUnitKey"].ToString());
                        _StandardUnitName = zReader["StandardUnitName"].ToString();
                        if (zReader["DiscontinuedDate"] != DBNull.Value)
                        {
                            _DiscontinuedDate = (DateTime)zReader["DiscontinuedDate"];
                        }

                        _SafetyInStock = float.Parse(zReader["SafetyInStock"].ToString());
                        _BarCode = zReader["BarCode"].ToString();
                        _QRCode = zReader["QRCode"].ToString();
                        _Style = zReader["Style"].ToString();
                        _Class = zReader["Class"].ToString();
                        _ProductLine = zReader["ProductLine"].ToString();
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                        _CategoryName = zReader["CategoryName"].ToString();
                        _ProductModeKey = int.Parse(zReader["ProductModeKey"].ToString());
                        _PhotoPath = zReader["PhotoPath"].ToString();
                        _StatusKey = int.Parse(zReader["StatusKey"].ToString());
                        _StatusName = zReader["StatusName"].ToString();
                        _Description = zReader["Description"].ToString();
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }

            #endregion

            #region [ Properties ]
            public string ProductKey
            {
                get { return _ProductKey; }
                set { _ProductKey = value; }
            }
            public string ProductID
            {
                get { return _ProductID; }
                set { _ProductID = value; }
            }
            public string ProductName
            {
                get { return _ProductName; }
                set { _ProductName = value; }
            }
            public string ProductNumber
            {
                get { return _ProductNumber; }
                set { _ProductNumber = value; }
            }
            public string ProductSerial
            {
                get { return _ProductSerial; }
                set { _ProductSerial = value; }
            }
            public string ProductModel
            {
                get { return _ProductModel; }
                set { _ProductModel = value; }
            }
            public double StandardCost
            {
                get { return _StandardCost; }
                set { _StandardCost = value; }
            }
            public double RentCost
            {
                get { return _RentCost; }
                set { _RentCost = value; }
            }
            public double ProductPrice
            {
                get { return _ProductPrice; }
                set { _ProductPrice = value; }
            }
            public int StandardUnitKey
            {
                get { return _StandardUnitKey; }
                set { _StandardUnitKey = value; }
            }
            public string StandardUnitName
            {
                get { return _StandardUnitName; }
                set { _StandardUnitName = value; }
            }
            public DateTime? DiscontinuedDate
            {
                get { return _DiscontinuedDate; }
                set { _DiscontinuedDate = value; }
            }
            public float SafetyInStock
            {
                get { return _SafetyInStock; }
                set { _SafetyInStock = value; }
            }
            public string BarCode
            {
                get { return _BarCode; }
                set { _BarCode = value; }
            }
            public string QRCode
            {
                get { return _QRCode; }
                set { _QRCode = value; }
            }
            public string Style
            {
                get { return _Style; }
                set { _Style = value; }
            }
            public string Class
            {
                get { return _Class; }
                set { _Class = value; }
            }
            public string ProductLine
            {
                get { return _ProductLine; }
                set { _ProductLine = value; }
            }
            public int CategoryKey
            {
                get { return _CategoryKey; }
                set { _CategoryKey = value; }
            }
            public string CategoryName
            {
                get { return _CategoryName; }
                set { _CategoryName = value; }
            }
            public int ProductModeKey
            {
                get { return _ProductModeKey; }
                set { _ProductModeKey = value; }
            }
            public string PhotoPath
            {
                get { return _PhotoPath; }
                set { _PhotoPath = value; }
            }
            public int StatusKey
            {
                get { return _StatusKey; }
                set { _StatusKey = value; }
            }
            public string StatusName
            {
                get { return _StatusName; }
                set { _StatusName = value; }
            }
            public string Description
            {
                get { return _Description; }
                set { _Description = value; }
            }
            public string PartnerNumber
            {
                get { return _PartnerNumber; }
                set { _PartnerNumber = value; }
            }
            public int RecordStatus
            {
                get { return _RecordStatus; }
                set { _RecordStatus = value; }
            }
            public DateTime? CreatedOn
            {
                get { return _CreatedOn; }
                set { _CreatedOn = value; }
            }
            public string CreatedBy
            {
                get { return _CreatedBy; }
                set { _CreatedBy = value; }
            }
            public string CreatedName
            {
                get { return _CreatedName; }
                set { _CreatedName = value; }
            }
            public DateTime? ModifiedOn
            {
                get { return _ModifiedOn; }
                set { _ModifiedOn = value; }
            }
            public string ModifiedBy
            {
                get { return _ModifiedBy; }
                set { _ModifiedBy = value; }
            }
            public string ModifiedName
            {
                get { return _ModifiedName; }
                set { _ModifiedName = value; }
            }
            public string Code
            {
                get
                {
                    if (_Message.Length >= 3)
                    {
                        return _Message.Substring(0, 3);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            public string Message
            {
                get { return _Message; }
                set { _Message = value; }
            }
            #endregion

            #region [ Constructor Update Information ]

            public string Create_ServerKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[PDT_Product_FED] ("
            + " ProductID ,ProductName ,ProductNumber ,ProductSerial ,ProductModel ,StandardCost ,RentCost ,ProductPrice ,StandardUnitKey ,StandardUnitName ,DiscontinuedDate ,SafetyInStock ,BarCode ,QRCode ,Style ,Class ,ProductLine ,CategoryKey ,CategoryName ,ProductModeKey ,PhotoPath ,StatusKey ,StatusName ,Description ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@ProductID ,@ProductName ,@ProductNumber ,@ProductSerial ,@ProductModel ,@StandardCost ,@RentCost ,@ProductPrice ,@StandardUnitKey ,@StandardUnitName ,@DiscontinuedDate ,@SafetyInStock ,@BarCode ,@QRCode ,@Style ,@Class ,@ProductLine ,@CategoryKey ,@CategoryName ,@ProductModeKey ,@PhotoPath ,@StatusKey ,@StatusName ,@Description ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID;
                    zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = _ProductName;
                    zCommand.Parameters.Add("@ProductNumber", SqlDbType.NVarChar).Value = _ProductNumber;
                    zCommand.Parameters.Add("@ProductSerial", SqlDbType.NVarChar).Value = _ProductSerial;
                    zCommand.Parameters.Add("@ProductModel", SqlDbType.NVarChar).Value = _ProductModel;
                    zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = _StandardCost;
                    zCommand.Parameters.Add("@RentCost", SqlDbType.Money).Value = _RentCost;
                    zCommand.Parameters.Add("@ProductPrice", SqlDbType.Money).Value = _ProductPrice;
                    zCommand.Parameters.Add("@StandardUnitKey", SqlDbType.Int).Value = _StandardUnitKey;
                    zCommand.Parameters.Add("@StandardUnitName", SqlDbType.NVarChar).Value = _StandardUnitName;
                    if (_DiscontinuedDate == null)
                    {
                        zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = _DiscontinuedDate;
                    }

                    zCommand.Parameters.Add("@SafetyInStock", SqlDbType.Float).Value = _SafetyInStock;
                    zCommand.Parameters.Add("@BarCode", SqlDbType.NVarChar).Value = _BarCode;
                    zCommand.Parameters.Add("@QRCode", SqlDbType.NVarChar).Value = _QRCode;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@ProductLine", SqlDbType.NChar).Value = _ProductLine;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@ProductModeKey", SqlDbType.Int).Value = _ProductModeKey;
                    zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = _PhotoPath;
                    zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = _StatusKey;
                    zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = _StatusName;
                    zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Create_ClientKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[PDT_Product_FED] ("
            + " ProductKey ,ProductID ,ProductName ,ProductNumber ,ProductSerial ,ProductModel ,StandardCost ,RentCost ,ProductPrice ,StandardUnitKey ,StandardUnitName ,DiscontinuedDate ,SafetyInStock ,BarCode ,QRCode ,Style ,Class ,ProductLine ,CategoryKey ,CategoryName ,ProductModeKey ,PhotoPath ,StatusKey ,StatusName ,Description ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@ProductKey ,@ProductID ,@ProductName ,@ProductNumber ,@ProductSerial ,@ProductModel ,@StandardCost ,@RentCost ,@ProductPrice ,@StandardUnitKey ,@StandardUnitName ,@DiscontinuedDate ,@SafetyInStock ,@BarCode ,@QRCode ,@Style ,@Class ,@ProductLine ,@CategoryKey ,@CategoryName ,@ProductModeKey ,@PhotoPath ,@StatusKey ,@StatusName ,@Description ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ProductKey);
                    zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID;
                    zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = _ProductName;
                    zCommand.Parameters.Add("@ProductNumber", SqlDbType.NVarChar).Value = _ProductNumber;
                    zCommand.Parameters.Add("@ProductSerial", SqlDbType.NVarChar).Value = _ProductSerial;
                    zCommand.Parameters.Add("@ProductModel", SqlDbType.NVarChar).Value = _ProductModel;
                    zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = _StandardCost;
                    zCommand.Parameters.Add("@RentCost", SqlDbType.Money).Value = _RentCost;
                    zCommand.Parameters.Add("@ProductPrice", SqlDbType.Money).Value = _ProductPrice;
                    zCommand.Parameters.Add("@StandardUnitKey", SqlDbType.Int).Value = _StandardUnitKey;
                    zCommand.Parameters.Add("@StandardUnitName", SqlDbType.NVarChar).Value = _StandardUnitName;
                    if (_DiscontinuedDate == null)
                    {
                        zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = _DiscontinuedDate;
                    }

                    zCommand.Parameters.Add("@SafetyInStock", SqlDbType.Float).Value = _SafetyInStock;
                    zCommand.Parameters.Add("@BarCode", SqlDbType.NVarChar).Value = _BarCode;
                    zCommand.Parameters.Add("@QRCode", SqlDbType.NVarChar).Value = _QRCode;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@ProductLine", SqlDbType.NChar).Value = _ProductLine;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@ProductModeKey", SqlDbType.Int).Value = _ProductModeKey;
                    zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = _PhotoPath;
                    zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = _StatusKey;
                    zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = _StatusName;
                    zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Update()
            {
                string zSQL = "UPDATE [dbo].[PDT_Product_FED] SET "
                            + " ProductID = @ProductID,"
                            + " ProductName = @ProductName,"
                            + " ProductNumber = @ProductNumber,"
                            + " ProductSerial = @ProductSerial,"
                            + " ProductModel = @ProductModel,"
                            + " StandardCost = @StandardCost,"
                            + " RentCost = @RentCost,"
                            + " ProductPrice = @ProductPrice,"
                            + " StandardUnitKey = @StandardUnitKey,"
                            + " StandardUnitName = @StandardUnitName,"
                            + " DiscontinuedDate = @DiscontinuedDate,"
                            + " SafetyInStock = @SafetyInStock,"
                            + " BarCode = @BarCode,"
                            + " QRCode = @QRCode,"
                            + " Style = @Style,"
                            + " Class = @Class,"
                            + " ProductLine = @ProductLine,"
                            + " CategoryKey = @CategoryKey,"
                            + " CategoryName = @CategoryName,"
                            + " ProductModeKey = @ProductModeKey,"
                            + " PhotoPath = @PhotoPath,"
                            + " StatusKey = @StatusKey,"
                            + " StatusName = @StatusName,"
                            + " Description = @Description,"
                            + " PartnerNumber = @PartnerNumber,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedOn = GetDate(),"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName"
                           + " WHERE ProductKey = @ProductKey";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ProductKey);
                    zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID;
                    zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = _ProductName;
                    zCommand.Parameters.Add("@ProductNumber", SqlDbType.NVarChar).Value = _ProductNumber;
                    zCommand.Parameters.Add("@ProductSerial", SqlDbType.NVarChar).Value = _ProductSerial;
                    zCommand.Parameters.Add("@ProductModel", SqlDbType.NVarChar).Value = _ProductModel;
                    zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = _StandardCost;
                    zCommand.Parameters.Add("@RentCost", SqlDbType.Money).Value = _RentCost;
                    zCommand.Parameters.Add("@ProductPrice", SqlDbType.Money).Value = _ProductPrice;
                    zCommand.Parameters.Add("@StandardUnitKey", SqlDbType.Int).Value = _StandardUnitKey;
                    zCommand.Parameters.Add("@StandardUnitName", SqlDbType.NVarChar).Value = _StandardUnitName;
                    if (_DiscontinuedDate == null)
                    {
                        zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = _DiscontinuedDate;
                    }

                    zCommand.Parameters.Add("@SafetyInStock", SqlDbType.Float).Value = _SafetyInStock;
                    zCommand.Parameters.Add("@BarCode", SqlDbType.NVarChar).Value = _BarCode;
                    zCommand.Parameters.Add("@QRCode", SqlDbType.NVarChar).Value = _QRCode;
                    zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = _Style;
                    zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = _Class;
                    zCommand.Parameters.Add("@ProductLine", SqlDbType.NChar).Value = _ProductLine;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@ProductModeKey", SqlDbType.Int).Value = _ProductModeKey;
                    zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = _PhotoPath;
                    zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = _StatusKey;
                    zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = _StatusName;
                    zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Delete()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[PDT_Product_FED] Set RecordStatus = 99 WHERE ProductKey = @ProductKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ProductKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[PDT_Product_FED] WHERE ProductKey = @ProductKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ProductKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            #endregion
        }
        #endregion
    }
}