﻿namespace TN_WinApp.FNC
{
    partial class Frm_ReceiptEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_ReceiptEdit));
            this.TitleForm = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dteReceiptDate = new TN_Tools.TNDateTime();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCustomerName = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.btnSearchCustomer = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.txtCustomerPhone = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txtCustomerAddress = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDescription = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDocumentID = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.btnSearchDocument = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtReceiptID = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtOrderAmount = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txtReceiptAmount = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Panel_Info = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rdoService = new System.Windows.Forms.RadioButton();
            this.rdoProduct = new System.Windows.Forms.RadioButton();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnDelete = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btnPrint = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btnSave = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.LVSearch = new System.Windows.Forms.ListView();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.Panel_Info.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // TitleForm
            // 
            this.TitleForm.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.TitleForm.Dock = System.Windows.Forms.DockStyle.Top;
            this.TitleForm.Location = new System.Drawing.Point(0, 0);
            this.TitleForm.Name = "TitleForm";
            this.TitleForm.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.TitleForm.Size = new System.Drawing.Size(595, 42);
            this.TitleForm.TabIndex = 6;
            this.TitleForm.Values.Description = "";
            this.TitleForm.Values.Heading = "Lập phiếu thu";
            this.TitleForm.Values.Image = ((System.Drawing.Image)(resources.GetObject("TitleForm.Values.Image")));
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(20, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 16);
            this.label1.TabIndex = 230;
            this.label1.Text = "Mã phiếu thu";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(44, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 16);
            this.label2.TabIndex = 230;
            this.label2.Text = "Ngày thu";
            // 
            // dteReceiptDate
            // 
            this.dteReceiptDate.CustomFormat = "dd/MM/yyyy";
            this.dteReceiptDate.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.dteReceiptDate.Location = new System.Drawing.Point(104, 38);
            this.dteReceiptDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dteReceiptDate.Name = "dteReceiptDate";
            this.dteReceiptDate.Size = new System.Drawing.Size(120, 26);
            this.dteReceiptDate.TabIndex = 232;
            this.dteReceiptDate.Value = new System.DateTime(((long)(0)));
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(25, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 16);
            this.label3.TabIndex = 230;
            this.label3.Text = "Người nộp";
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCustomerName.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnSearchCustomer});
            this.txtCustomerName.Location = new System.Drawing.Point(93, 22);
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txtCustomerName.Size = new System.Drawing.Size(471, 26);
            this.txtCustomerName.StateCommon.Border.ColorAngle = 1F;
            this.txtCustomerName.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txtCustomerName.StateCommon.Border.Rounding = 4;
            this.txtCustomerName.StateCommon.Border.Width = 1;
            this.txtCustomerName.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerName.TabIndex = 238;
            // 
            // btnSearchCustomer
            // 
            this.btnSearchCustomer.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchCustomer.Image")));
            this.btnSearchCustomer.UniqueName = "FFABA6C89F274796F78435AB2589EE7D";
            // 
            // txtCustomerPhone
            // 
            this.txtCustomerPhone.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtCustomerPhone.Location = new System.Drawing.Point(93, 86);
            this.txtCustomerPhone.Name = "txtCustomerPhone";
            this.txtCustomerPhone.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txtCustomerPhone.Size = new System.Drawing.Size(151, 25);
            this.txtCustomerPhone.StateCommon.Border.ColorAngle = 1F;
            this.txtCustomerPhone.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txtCustomerPhone.StateCommon.Border.Rounding = 4;
            this.txtCustomerPhone.StateCommon.Border.Width = 1;
            this.txtCustomerPhone.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.txtCustomerPhone.TabIndex = 243;
            // 
            // txtCustomerAddress
            // 
            this.txtCustomerAddress.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtCustomerAddress.Location = new System.Drawing.Point(93, 54);
            this.txtCustomerAddress.Name = "txtCustomerAddress";
            this.txtCustomerAddress.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txtCustomerAddress.Size = new System.Drawing.Size(471, 25);
            this.txtCustomerAddress.StateCommon.Border.ColorAngle = 1F;
            this.txtCustomerAddress.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txtCustomerAddress.StateCommon.Border.Rounding = 4;
            this.txtCustomerAddress.StateCommon.Border.Width = 1;
            this.txtCustomerAddress.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.txtCustomerAddress.TabIndex = 244;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(8, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 16);
            this.label5.TabIndex = 241;
            this.label5.Text = "Số điện thoại";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(44, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 16);
            this.label8.TabIndex = 239;
            this.label8.Text = "Địa chỉ";
            // 
            // txtDescription
            // 
            this.txtDescription.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDescription.Location = new System.Drawing.Point(93, 55);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txtDescription.Size = new System.Drawing.Size(470, 47);
            this.txtDescription.StateCommon.Border.ColorAngle = 1F;
            this.txtDescription.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txtDescription.StateCommon.Border.Rounding = 4;
            this.txtDescription.StateCommon.Border.Width = 1;
            this.txtDescription.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.txtDescription.TabIndex = 244;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(41, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 16);
            this.label6.TabIndex = 241;
            this.label6.Text = "Ghi chú";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(6, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 16);
            this.label7.TabIndex = 240;
            this.label7.Text = "Mã biên nhận";
            // 
            // txtDocumentID
            // 
            this.txtDocumentID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDocumentID.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnSearchDocument});
            this.txtDocumentID.Location = new System.Drawing.Point(93, 22);
            this.txtDocumentID.Name = "txtDocumentID";
            this.txtDocumentID.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txtDocumentID.Size = new System.Drawing.Size(471, 26);
            this.txtDocumentID.StateCommon.Border.ColorAngle = 1F;
            this.txtDocumentID.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txtDocumentID.StateCommon.Border.Rounding = 4;
            this.txtDocumentID.StateCommon.Border.Width = 1;
            this.txtDocumentID.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDocumentID.TabIndex = 238;
            // 
            // btnSearchDocument
            // 
            this.btnSearchDocument.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchDocument.Image")));
            this.btnSearchDocument.UniqueName = "FFABA6C89F274796F78435AB2589EE7D";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtCustomerName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtCustomerPhone);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtCustomerAddress);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox1.ForeColor = System.Drawing.Color.Navy;
            this.groupBox1.Location = new System.Drawing.Point(11, 71);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(570, 120);
            this.groupBox1.TabIndex = 246;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin người nộp tiền";
            // 
            // txtReceiptID
            // 
            this.txtReceiptID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtReceiptID.Location = new System.Drawing.Point(104, 5);
            this.txtReceiptID.Multiline = true;
            this.txtReceiptID.Name = "txtReceiptID";
            this.txtReceiptID.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txtReceiptID.Size = new System.Drawing.Size(120, 26);
            this.txtReceiptID.StateCommon.Border.ColorAngle = 1F;
            this.txtReceiptID.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txtReceiptID.StateCommon.Border.Rounding = 4;
            this.txtReceiptID.StateCommon.Border.Width = 1;
            this.txtReceiptID.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.txtReceiptID.TabIndex = 243;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtDocumentID);
            this.groupBox2.Controls.Add(this.txtOrderAmount);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtReceiptAmount);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtDescription);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox2.ForeColor = System.Drawing.Color.Navy;
            this.groupBox2.Location = new System.Drawing.Point(11, 197);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(570, 174);
            this.groupBox2.TabIndex = 247;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Nội dung";
            // 
            // txtOrderAmount
            // 
            this.txtOrderAmount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtOrderAmount.Location = new System.Drawing.Point(402, 108);
            this.txtOrderAmount.Name = "txtOrderAmount";
            this.txtOrderAmount.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txtOrderAmount.Size = new System.Drawing.Size(161, 25);
            this.txtOrderAmount.StateCommon.Border.ColorAngle = 1F;
            this.txtOrderAmount.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txtOrderAmount.StateCommon.Border.Rounding = 4;
            this.txtOrderAmount.StateCommon.Border.Width = 1;
            this.txtOrderAmount.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.txtOrderAmount.TabIndex = 242;
            this.txtOrderAmount.Text = "0";
            this.txtOrderAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtReceiptAmount
            // 
            this.txtReceiptAmount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtReceiptAmount.Location = new System.Drawing.Point(402, 140);
            this.txtReceiptAmount.Name = "txtReceiptAmount";
            this.txtReceiptAmount.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txtReceiptAmount.Size = new System.Drawing.Size(161, 25);
            this.txtReceiptAmount.StateCommon.Border.ColorAngle = 1F;
            this.txtReceiptAmount.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txtReceiptAmount.StateCommon.Border.Rounding = 4;
            this.txtReceiptAmount.StateCommon.Border.Width = 1;
            this.txtReceiptAmount.StateCommon.Content.Color1 = System.Drawing.Color.Maroon;
            this.txtReceiptAmount.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtReceiptAmount.TabIndex = 243;
            this.txtReceiptAmount.Text = "0";
            this.txtReceiptAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(352, 113);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 16);
            this.label10.TabIndex = 240;
            this.label10.Text = "Số tiền";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.Maroon;
            this.label9.Location = new System.Drawing.Point(319, 145);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 16);
            this.label9.TabIndex = 230;
            this.label9.Text = "Số tiền nộp";
            // 
            // Panel_Info
            // 
            this.Panel_Info.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Info.Controls.Add(this.groupBox3);
            this.Panel_Info.Controls.Add(this.groupBox2);
            this.Panel_Info.Controls.Add(this.groupBox1);
            this.Panel_Info.Controls.Add(this.txtReceiptID);
            this.Panel_Info.Controls.Add(this.dteReceiptDate);
            this.Panel_Info.Controls.Add(this.label1);
            this.Panel_Info.Controls.Add(this.label2);
            this.Panel_Info.Controls.Add(this.panel6);
            this.Panel_Info.Controls.Add(this.LVSearch);
            this.Panel_Info.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Info.Location = new System.Drawing.Point(0, 42);
            this.Panel_Info.Name = "Panel_Info";
            this.Panel_Info.Size = new System.Drawing.Size(595, 442);
            this.Panel_Info.TabIndex = 248;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rdoService);
            this.groupBox3.Controls.Add(this.rdoProduct);
            this.groupBox3.Location = new System.Drawing.Point(242, -3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(169, 68);
            this.groupBox3.TabIndex = 245;
            this.groupBox3.TabStop = false;
            // 
            // rdoService
            // 
            this.rdoService.AutoSize = true;
            this.rdoService.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.rdoService.ForeColor = System.Drawing.Color.Navy;
            this.rdoService.Location = new System.Drawing.Point(26, 39);
            this.rdoService.Name = "rdoService";
            this.rdoService.Size = new System.Drawing.Size(117, 20);
            this.rdoService.TabIndex = 0;
            this.rdoService.TabStop = true;
            this.rdoService.Text = "Thu tiền dịch vụ";
            this.rdoService.UseVisualStyleBackColor = true;
            // 
            // rdoProduct
            // 
            this.rdoProduct.AutoSize = true;
            this.rdoProduct.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.rdoProduct.ForeColor = System.Drawing.Color.Navy;
            this.rdoProduct.Location = new System.Drawing.Point(26, 15);
            this.rdoProduct.Name = "rdoProduct";
            this.rdoProduct.Size = new System.Drawing.Size(91, 20);
            this.rdoProduct.TabIndex = 0;
            this.rdoProduct.TabStop = true;
            this.rdoProduct.Text = "Thu tiền áo";
            this.rdoProduct.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.btnDelete);
            this.panel6.Controls.Add(this.btnPrint);
            this.panel6.Controls.Add(this.btnSave);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 393);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(593, 47);
            this.panel6.TabIndex = 248;
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Location = new System.Drawing.Point(353, 2);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btnDelete.Size = new System.Drawing.Size(120, 40);
            this.btnDelete.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btnDelete.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btnDelete.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btnDelete.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btnDelete.TabIndex = 234;
            this.btnDelete.Values.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Values.Image")));
            this.btnDelete.Values.Text = "Xóa thông tin";
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.Location = new System.Drawing.Point(1, 2);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btnPrint.Size = new System.Drawing.Size(63, 40);
            this.btnPrint.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btnPrint.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btnPrint.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btnPrint.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btnPrint.TabIndex = 232;
            this.btnPrint.Values.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Values.Image")));
            this.btnPrint.Values.Text = "In";
            this.btnPrint.Visible = false;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(479, 2);
            this.btnSave.Name = "btnSave";
            this.btnSave.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btnSave.Size = new System.Drawing.Size(110, 40);
            this.btnSave.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btnSave.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btnSave.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btnSave.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btnSave.TabIndex = 14;
            this.btnSave.Values.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Values.Image")));
            this.btnSave.Values.Text = "Cập nhật";
            // 
            // LVSearch
            // 
            this.LVSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LVSearch.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.LVSearch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LVSearch.FullRowSelect = true;
            this.LVSearch.GridLines = true;
            this.LVSearch.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.LVSearch.HideSelection = false;
            this.LVSearch.Location = new System.Drawing.Point(104, 118);
            this.LVSearch.Name = "LVSearch";
            this.LVSearch.Size = new System.Drawing.Size(470, 193);
            this.LVSearch.TabIndex = 245;
            this.LVSearch.UseCompatibleStateImageBehavior = false;
            this.LVSearch.View = System.Windows.Forms.View.Details;
            this.LVSearch.Visible = false;
            // 
            // Frm_ReceiptEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(595, 484);
            this.ControlBox = false;
            this.Controls.Add(this.Panel_Info);
            this.Controls.Add(this.TitleForm);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Frm_ReceiptEdit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.Panel_Info.ResumeLayout(false);
            this.Panel_Info.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader TitleForm;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private TN_Tools.TNDateTime dteReceiptDate;
        private System.Windows.Forms.Label label3;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtCustomerName;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnSearchCustomer;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtCustomerPhone;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtCustomerAddress;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtDescription;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtDocumentID;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnSearchDocument;
        private System.Windows.Forms.GroupBox groupBox1;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtReceiptID;
        private System.Windows.Forms.GroupBox groupBox2;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtReceiptAmount;
        private System.Windows.Forms.Label label9;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtOrderAmount;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel Panel_Info;
        private System.Windows.Forms.ListView LVSearch;
        private System.Windows.Forms.Panel panel6;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnPrint;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnSave;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnDelete;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rdoService;
        private System.Windows.Forms.RadioButton rdoProduct;
    }
}