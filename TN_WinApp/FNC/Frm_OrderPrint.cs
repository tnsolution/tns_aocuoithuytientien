﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Windows.Forms;
using TN_Connection;

namespace TN_WinApp
{
    public partial class Frm_OrderPrint : Form
    {
        private string _OrderKey = "";
        public string OrderKey
        {
            get
            {
                return _OrderKey;
            }

            set
            {
                _OrderKey = value;
            }
        }

        public Frm_OrderPrint()
        {
            InitializeComponent();
            this.Load += Frm_OrderPrint_Load;
            btnClose.Click += (o, e) => { this.Close(); };
        }

        private void Frm_OrderPrint_Load(object sender, EventArgs e)
        {
            if (_OrderKey != string.Empty)
            {
                Order_FED_Info zInfo = new Order_FED_Info(_OrderKey);
                if (zInfo.Code != "404")
                {
                    DataSet Ds = new DataSet();
                    DataTable zTable = Ds.Tables["Order"];
                    DataTable zTableDetail = Ds.Tables["OrderDetail"];

                    DataRow r = zTable.NewRow();
                    r["OrderID"] = zInfo.OrderID;
                    r["OrderDate"] = zInfo.OrderDate.Value.ToString("dd/MM/yyyy");
                    r["BuyerName"] = zInfo.BuyerName;
                    r["BuyerPhone"] = zInfo.BuyerPhone;
                    r["BuyerAddress"] = zInfo.BuyerAddress;
                    r["BuyerIssuesID"] = "";
                    r["DeliverDate"] = zInfo.DeliverDate.Value.ToString("dd/MM/yyyy");
                    r["ReturnDate"] = zInfo.ReturnDate.Value.ToString("dd/MM/yyyy");
                    r["TotalAmount"] = zInfo.TotalAmount.ToString("n0", CultureInfo.GetCultureInfo("vi-VN"));
                    r["SaleAmount"] = zInfo.SaleAmount.ToString("n0", CultureInfo.GetCultureInfo("vi-VN"));
                    r["ReceiptAmount"] = zInfo.ReceiptAmount.ToString("n0", CultureInfo.GetCultureInfo("vi-VN"));
                    r["OrderAmount"] = zInfo.OrderAmount.ToString("n0", CultureInfo.GetCultureInfo("vi-VN"));
                    zTable.Rows.Add(r);

                    //
                    zTableDetail = Access_Data.ListOrderDetail(zInfo.PartnerNumber, zInfo.OrderKey);
                    for (int i = 0; i < 5; i++)
                    {
                        DataRow rDetail = zTableDetail.NewRow();
                        zTableDetail.Rows.Add(rDetail);
                    }

                    rptOrder rpt = new rptOrder();
                    rpt.Database.Tables["Order"].SetDataSource(zTable);
                    rpt.Database.Tables["OrderDetail"].SetDataSource(zTableDetail);
                    rptViewer.ReportSource = rpt;
                }
            }
        }

        #region MyRegion
        public class Order_FED_Info
        {
            #region [ Field Name ]
            private string _OrderKey = "";
            private string _OrderID = "";
            private DateTime? _OrderDate = null;
            private string _OrderDescription = "";
            private string _SellerKey = "";
            private string _SellerName = "";
            private string _BuyerKey = "";
            private string _BuyerName = "";
            private string _BuyerPhone = "";
            private string _BuyerAddress = "";
            private int _OrderStatusKey = 0;
            private string _OrderStatusName = "";
            private int _PaymentStatusKey = 0;
            private string _PaymentStatusName = "";
            private DateTime? _DeliverDate = null;
            private DateTime? _ReturnDate = null;
            private int _CategoryKey = 0;
            private string _CategoryName = "";
            private int _OrganizationKey = 0;
            private string _OrganizationID = "";
            private string _OrganizationName = "";
            private double _TotalAmount = 0;
            private double _SaleAmount = 0;
            private double _OrderAmount = 0;
            private double _ReceiptAmount = 0;
            private string _PartnerNumber = "";
            private int _RecordStatus = 0;
            private DateTime? _CreatedOn = null;
            private string _CreatedBy = "";
            private string _CreatedName = "";
            private DateTime? _ModifiedOn = null;
            private string _ModifiedBy = "";
            private string _ModifiedName = "";
            private string _Message = "";
            #endregion

            #region [ Constructor Get Information ]
            public Order_FED_Info()
            {
                Guid zNewID = Guid.NewGuid();
                _OrderKey = zNewID.ToString();
            }
            public Order_FED_Info(string OrderKey)
            {
                string zSQL = "SELECT * FROM [dbo].[FNC_Order_FED] WHERE OrderKey = @OrderKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(OrderKey);
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _OrderKey = zReader["OrderKey"].ToString();
                        _OrderID = zReader["OrderID"].ToString();
                        if (zReader["OrderDate"] != DBNull.Value)
                        {
                            _OrderDate = (DateTime)zReader["OrderDate"];
                        }

                        _OrderDescription = zReader["OrderDescription"].ToString();
                        _SellerKey = zReader["SellerKey"].ToString();
                        _SellerName = zReader["SellerName"].ToString();
                        _BuyerKey = zReader["BuyerKey"].ToString();
                        _BuyerName = zReader["BuyerName"].ToString();
                        _BuyerPhone = zReader["BuyerPhone"].ToString();
                        _BuyerAddress = zReader["BuyerAddress"].ToString();
                        _OrderStatusKey = int.Parse(zReader["OrderStatusKey"].ToString());
                        _OrderStatusName = zReader["OrderStatusName"].ToString();
                        _PaymentStatusKey = int.Parse(zReader["PaymentStatusKey"].ToString());
                        _PaymentStatusName = zReader["PaymentStatusName"].ToString();
                        if (zReader["DeliverDate"] != DBNull.Value)
                        {
                            _DeliverDate = (DateTime)zReader["DeliverDate"];
                        }

                        if (zReader["ReturnDate"] != DBNull.Value)
                        {
                            _ReturnDate = (DateTime)zReader["ReturnDate"];
                        }

                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                        _CategoryName = zReader["CategoryName"].ToString();
                        _OrganizationKey = int.Parse(zReader["OrganizationKey"].ToString());
                        _OrganizationID = zReader["OrganizationID"].ToString();
                        _OrganizationName = zReader["OrganizationName"].ToString();
                        _TotalAmount = double.Parse(zReader["TotalAmount"].ToString());
                        _SaleAmount = double.Parse(zReader["SaleAmount"].ToString());
                        _OrderAmount = double.Parse(zReader["OrderAmount"].ToString());
                        _ReceiptAmount = double.Parse(zReader["ReceiptAmount"].ToString());
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }

            #endregion

            #region [ Properties ]
            public string OrderKey
            {
                get { return _OrderKey; }
                set { _OrderKey = value; }
            }
            public string OrderID
            {
                get { return _OrderID; }
                set { _OrderID = value; }
            }
            public DateTime? OrderDate
            {
                get { return _OrderDate; }
                set { _OrderDate = value; }
            }
            public string OrderDescription
            {
                get { return _OrderDescription; }
                set { _OrderDescription = value; }
            }
            public string SellerKey
            {
                get { return _SellerKey; }
                set { _SellerKey = value; }
            }
            public string SellerName
            {
                get { return _SellerName; }
                set { _SellerName = value; }
            }
            public string BuyerKey
            {
                get { return _BuyerKey; }
                set { _BuyerKey = value; }
            }
            public string BuyerName
            {
                get { return _BuyerName; }
                set { _BuyerName = value; }
            }
            public string BuyerPhone
            {
                get { return _BuyerPhone; }
                set { _BuyerPhone = value; }
            }
            public string BuyerAddress
            {
                get { return _BuyerAddress; }
                set { _BuyerAddress = value; }
            }
            public int OrderStatusKey
            {
                get { return _OrderStatusKey; }
                set { _OrderStatusKey = value; }
            }
            public string OrderStatusName
            {
                get { return _OrderStatusName; }
                set { _OrderStatusName = value; }
            }
            public int PaymentStatusKey
            {
                get { return _PaymentStatusKey; }
                set { _PaymentStatusKey = value; }
            }
            public string PaymentStatusName
            {
                get { return _PaymentStatusName; }
                set { _PaymentStatusName = value; }
            }
            public DateTime? DeliverDate
            {
                get { return _DeliverDate; }
                set { _DeliverDate = value; }
            }
            public DateTime? ReturnDate
            {
                get { return _ReturnDate; }
                set { _ReturnDate = value; }
            }
            public int CategoryKey
            {
                get { return _CategoryKey; }
                set { _CategoryKey = value; }
            }
            public string CategoryName
            {
                get { return _CategoryName; }
                set { _CategoryName = value; }
            }
            public int OrganizationKey
            {
                get { return _OrganizationKey; }
                set { _OrganizationKey = value; }
            }
            public string OrganizationID
            {
                get { return _OrganizationID; }
                set { _OrganizationID = value; }
            }
            public string OrganizationName
            {
                get { return _OrganizationName; }
                set { _OrganizationName = value; }
            }
            public double TotalAmount
            {
                get { return _TotalAmount; }
                set { _TotalAmount = value; }
            }
            public double SaleAmount
            {
                get { return _SaleAmount; }
                set { _SaleAmount = value; }
            }
            public double OrderAmount
            {
                get { return _OrderAmount; }
                set { _OrderAmount = value; }
            }
            public double ReceiptAmount
            {
                get { return _ReceiptAmount; }
                set { _ReceiptAmount = value; }
            }
            public string PartnerNumber
            {
                get { return _PartnerNumber; }
                set { _PartnerNumber = value; }
            }
            public int RecordStatus
            {
                get { return _RecordStatus; }
                set { _RecordStatus = value; }
            }
            public DateTime? CreatedOn
            {
                get { return _CreatedOn; }
                set { _CreatedOn = value; }
            }
            public string CreatedBy
            {
                get { return _CreatedBy; }
                set { _CreatedBy = value; }
            }
            public string CreatedName
            {
                get { return _CreatedName; }
                set { _CreatedName = value; }
            }
            public DateTime? ModifiedOn
            {
                get { return _ModifiedOn; }
                set { _ModifiedOn = value; }
            }
            public string ModifiedBy
            {
                get { return _ModifiedBy; }
                set { _ModifiedBy = value; }
            }
            public string ModifiedName
            {
                get { return _ModifiedName; }
                set { _ModifiedName = value; }
            }
            public string Code
            {
                get
                {
                    if (_Message.Length >= 3)
                    {
                        return _Message.Substring(0, 3);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            public string Message
            {
                get { return _Message; }
                set { _Message = value; }
            }
            #endregion

            #region [ Constructor Update Information ]
            public string Create_ServerKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[FNC_Order_FED] ("
            + " OrderID ,OrderDate ,OrderDescription ,SellerKey ,SellerName ,BuyerKey ,BuyerName ,BuyerPhone ,BuyerAddress ,OrderStatusKey ,OrderStatusName ,PaymentStatusKey ,PaymentStatusName ,DeliverDate ,ReturnDate ,CategoryKey ,CategoryName ,OrganizationKey ,OrganizationID ,OrganizationName ,TotalAmount ,SaleAmount ,OrderAmount ,ReceiptAmount ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@OrderID ,@OrderDate ,@OrderDescription ,@SellerKey ,@SellerName ,@BuyerKey ,@BuyerName ,@BuyerPhone ,@BuyerAddress ,@OrderStatusKey ,@OrderStatusName ,@PaymentStatusKey ,@PaymentStatusName ,@DeliverDate ,@ReturnDate ,@CategoryKey ,@CategoryName ,@OrganizationKey ,@OrganizationID ,@OrganizationName ,@TotalAmount ,@SaleAmount ,@OrderAmount ,@ReceiptAmount ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID;
                    if (_OrderDate == null)
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                    }

                    zCommand.Parameters.Add("@OrderDescription", SqlDbType.NVarChar).Value = _OrderDescription;
                    if (_SellerKey != string.Empty && _SellerKey.Length == 36)
                    {
                        zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_SellerKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }
                    zCommand.Parameters.Add("@SellerName", SqlDbType.NVarChar).Value = _SellerName;
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_BuyerKey);
                    zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = _BuyerName;
                    zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = _BuyerPhone;
                    zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = _BuyerAddress;
                    zCommand.Parameters.Add("@OrderStatusKey", SqlDbType.Int).Value = _OrderStatusKey;
                    zCommand.Parameters.Add("@OrderStatusName", SqlDbType.NVarChar).Value = _OrderStatusName;
                    zCommand.Parameters.Add("@PaymentStatusKey", SqlDbType.Int).Value = _PaymentStatusKey;
                    zCommand.Parameters.Add("@PaymentStatusName", SqlDbType.NVarChar).Value = _PaymentStatusName;
                    if (_DeliverDate == null)
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = _DeliverDate;
                    }

                    if (_ReturnDate == null)
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = _ReturnDate;
                    }

                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = _OrganizationKey;
                    zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = _OrganizationID;
                    zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = _OrganizationName;
                    zCommand.Parameters.Add("@TotalAmount", SqlDbType.Money).Value = _TotalAmount;
                    zCommand.Parameters.Add("@SaleAmount", SqlDbType.Money).Value = _SaleAmount;
                    zCommand.Parameters.Add("@OrderAmount", SqlDbType.Money).Value = _OrderAmount;
                    zCommand.Parameters.Add("@ReceiptAmount", SqlDbType.Money).Value = _ReceiptAmount;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Create_ClientKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[FNC_Order_FED] ("
            + " OrderKey ,OrderID ,OrderDate ,OrderDescription ,SellerKey ,SellerName ,BuyerKey ,BuyerName ,BuyerPhone ,BuyerAddress ,OrderStatusKey ,OrderStatusName ,PaymentStatusKey ,PaymentStatusName ,DeliverDate ,ReturnDate ,CategoryKey ,CategoryName ,OrganizationKey ,OrganizationID ,OrganizationName ,TotalAmount ,SaleAmount ,OrderAmount ,ReceiptAmount ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@OrderKey ,@OrderID ,@OrderDate ,@OrderDescription ,@SellerKey ,@SellerName ,@BuyerKey ,@BuyerName ,@BuyerPhone ,@BuyerAddress ,@OrderStatusKey ,@OrderStatusName ,@PaymentStatusKey ,@PaymentStatusName ,@DeliverDate ,@ReturnDate ,@CategoryKey ,@CategoryName ,@OrganizationKey ,@OrganizationID ,@OrganizationName ,@TotalAmount ,@SaleAmount ,@OrderAmount ,@ReceiptAmount ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID;
                    if (_OrderDate == null)
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                    }

                    zCommand.Parameters.Add("@OrderDescription", SqlDbType.NVarChar).Value = _OrderDescription;
                    if (_SellerKey != string.Empty && _SellerKey.Length == 36)
                    {
                        zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_SellerKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }

                    zCommand.Parameters.Add("@SellerName", SqlDbType.NVarChar).Value = _SellerName;
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_BuyerKey);
                    zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = _BuyerName;
                    zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = _BuyerPhone;
                    zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = _BuyerAddress;
                    zCommand.Parameters.Add("@OrderStatusKey", SqlDbType.Int).Value = _OrderStatusKey;
                    zCommand.Parameters.Add("@OrderStatusName", SqlDbType.NVarChar).Value = _OrderStatusName;
                    zCommand.Parameters.Add("@PaymentStatusKey", SqlDbType.Int).Value = _PaymentStatusKey;
                    zCommand.Parameters.Add("@PaymentStatusName", SqlDbType.NVarChar).Value = _PaymentStatusName;
                    if (_DeliverDate == null)
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = _DeliverDate;
                    }

                    if (_ReturnDate == null)
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = _ReturnDate;
                    }

                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = _OrganizationKey;
                    zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = _OrganizationID;
                    zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = _OrganizationName;
                    zCommand.Parameters.Add("@TotalAmount", SqlDbType.Money).Value = _TotalAmount;
                    zCommand.Parameters.Add("@SaleAmount", SqlDbType.Money).Value = _SaleAmount;
                    zCommand.Parameters.Add("@OrderAmount", SqlDbType.Money).Value = _OrderAmount;
                    zCommand.Parameters.Add("@ReceiptAmount", SqlDbType.Money).Value = _ReceiptAmount;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Update()
            {
                string zSQL = "UPDATE [dbo].[FNC_Order_FED] SET "
                            + " OrderID = @OrderID,"
                            + " OrderDate = @OrderDate,"
                            + " OrderDescription = @OrderDescription,"
                            + " SellerKey = @SellerKey,"
                            + " SellerName = @SellerName,"
                            + " BuyerKey = @BuyerKey,"
                            + " BuyerName = @BuyerName,"
                            + " BuyerPhone = @BuyerPhone,"
                            + " BuyerAddress = @BuyerAddress,"
                            + " OrderStatusKey = @OrderStatusKey,"
                            + " OrderStatusName = @OrderStatusName,"
                            + " PaymentStatusKey = @PaymentStatusKey,"
                            + " PaymentStatusName = @PaymentStatusName,"
                            + " DeliverDate = @DeliverDate,"
                            + " ReturnDate = @ReturnDate,"
                            + " CategoryKey = @CategoryKey,"
                            + " CategoryName = @CategoryName,"
                            + " OrganizationKey = @OrganizationKey,"
                            + " OrganizationID = @OrganizationID,"
                            + " OrganizationName = @OrganizationName,"
                            + " TotalAmount = @TotalAmount,"
                            + " SaleAmount = @SaleAmount,"
                            + " OrderAmount = @OrderAmount,"
                            + " ReceiptAmount = @ReceiptAmount,"
                            + " PartnerNumber = @PartnerNumber,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedOn = GetDate(),"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName"
                           + " WHERE OrderKey = @OrderKey";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID;
                    if (_OrderDate == null)
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                    }

                    zCommand.Parameters.Add("@OrderDescription", SqlDbType.NVarChar).Value = _OrderDescription;
                    if (_SellerKey != string.Empty && _SellerKey.Length == 36)
                    {
                        zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_SellerKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }
                    zCommand.Parameters.Add("@SellerName", SqlDbType.NVarChar).Value = _SellerName;
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_BuyerKey);
                    zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = _BuyerName;
                    zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = _BuyerPhone;
                    zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = _BuyerAddress;
                    zCommand.Parameters.Add("@OrderStatusKey", SqlDbType.Int).Value = _OrderStatusKey;
                    zCommand.Parameters.Add("@OrderStatusName", SqlDbType.NVarChar).Value = _OrderStatusName;
                    zCommand.Parameters.Add("@PaymentStatusKey", SqlDbType.Int).Value = _PaymentStatusKey;
                    zCommand.Parameters.Add("@PaymentStatusName", SqlDbType.NVarChar).Value = _PaymentStatusName;
                    if (_DeliverDate == null)
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = _DeliverDate;
                    }

                    if (_ReturnDate == null)
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = _ReturnDate;
                    }

                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = _OrganizationKey;
                    zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = _OrganizationID;
                    zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = _OrganizationName;
                    zCommand.Parameters.Add("@TotalAmount", SqlDbType.Money).Value = _TotalAmount;
                    zCommand.Parameters.Add("@SaleAmount", SqlDbType.Money).Value = _SaleAmount;
                    zCommand.Parameters.Add("@OrderAmount", SqlDbType.Money).Value = _OrderAmount;
                    zCommand.Parameters.Add("@ReceiptAmount", SqlDbType.Money).Value = _ReceiptAmount;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Delete()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[FNC_Order_FED] Set RecordStatus = 99 WHERE OrderKey = @OrderKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Delete_Detail()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = @"
UPDATE [dbo].[FNC_Order_Employee_FED] Set RecordStatus = 99 WHERE OrderKey = @OrderKey
UPDATE [dbo].[FNC_Order_Item_FED] Set RecordStatus = 99 WHERE OrderKey = @OrderKey
";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[FNC_Order_FED] WHERE OrderKey = @OrderKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty_Detail()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = @"
DELETE FROM [dbo].[FNC_Order_Employee_FED] WHERE OrderKey = @OrderKey
DELETE FROM [dbo].[FNC_Order_Item_FED] WHERE OrderKey = @OrderKey
DELETE FROM [dbo].[FNC_Receipt] WHERE DocumentID = @OrderID
";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = OrderID;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            #endregion
        }
        public class Access_Data
        {
            public static DataTable ListOrderDetail(string PartnerNumber, string OrderKey)
            {
                DataTable zTable = new DataTable();
                string zSQL = "SELECT * FROM [dbo].[FNC_Order_Item_FED] WHERE PartnerNumber = @PartnerNumber AND OrderKey = @OrderKey AND RecordStatus != 99 ";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(OrderKey);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
        }
        #endregion
    }
}
