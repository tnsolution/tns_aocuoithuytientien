﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using TN_Connection;
using TN_Tools;
using TN_User;

namespace TN_WinApp.FNC
{
    public partial class Frm_ReceiptEdit : Form
    {
        private string _FormUrl = "/FNC/Frm_ReceiptEdit";
        private string _ReceiptKey = "";
        private string _OrderID;
        private string _LVSearchStyle = "CRM";
        public string OrderID
        {
            get
            {
                return _OrderID;
            }

            set
            {
                _OrderID = value;
            }
        }
        public string ReceiptKey
        {
            get
            {
                return _ReceiptKey;
            }

            set
            {
                _ReceiptKey = value;
            }
        }
        public string FormUrl
        {
            get
            {
                return _FormUrl;
            }

            set
            {
                _FormUrl = value;
            }
        }

        public Frm_ReceiptEdit()
        {
            InitializeComponent();

            TitleForm.MouseDown += Frm_MouseDown;
            TitleForm.MouseMove += Frm_MouseMove;
            TitleForm.MouseUp += Frm_MouseUp;

            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;

            btnSave.Click += BtnSave_Click;
            btnDelete.Click += BtnDelete_Click;
            btnSearchCustomer.Click += BtnSearchCustomer_Click;
            btnSearchDocument.Click += BtnSearchDocument_Click;

            txtDocumentID.KeyUp += (o, e) =>
            {

                if (e.KeyCode == Keys.Down)
                {
                    LVSearch.Focus();
                    if (LVSearch.Items.Count > 0)
                    {
                        LVSearch.Items[0].Selected = true;
                    }
                }
                else
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        if (txtDocumentID.Text.Length > 0)
                        {
                            _LVSearchStyle = "FNC";
                            DesignLayout_FIN(LVSearch);
                            LVSearch.Location = new Point(104, 244);
                            LVSearch.Focus();
                            LVSearch.Visible = true;
                            LVSearch.BringToFront();
                            LoadListView_FIN();
                        }
                        else
                        {
                            LVSearch.Visible = false;
                        }
                    }
                    if (e.KeyCode == Keys.Escape)
                    {
                        LVSearch.Visible = false;
                    }
                }
            };
            txtDocumentID.Leave += (o, e) =>
            {
                if (!LVSearch.Focused)
                {
                    LVSearch.Visible = false;
                }

                if (txtCustomerName.Text.Trim().Length == 0)
                {
                    txtCustomerName.Tag = null;
                    txtCustomerName.Clear();
                }
            };

            txtCustomerName.KeyUp += (o, e) =>
            {
                if (e.KeyCode == Keys.Down)
                {
                    LVSearch.Focus();
                    if (LVSearch.Items.Count > 0)
                    {
                        LVSearch.Items[0].Selected = true;
                    }
                }
                else
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        if (txtCustomerName.Text.Length > 0)
                        {
                            _LVSearchStyle = "CRM";
                            DesignLayout_CRM(LVSearch);
                            LVSearch.Location = new Point(104, 118);
                            LVSearch.Focus();
                            LVSearch.Visible = true;
                            LVSearch.BringToFront();
                            LoadListView_CRM();
                        }
                        else
                        {
                            LVSearch.Visible = false;
                        }
                    }
                    if (e.KeyCode == Keys.Escape)
                    {
                        LVSearch.Visible = false;
                    }
                }
            };
            txtCustomerName.Leave += (o, e) =>
            {
                if (!LVSearch.Focused)
                {
                    LVSearch.Visible = false;
                }

                if (txtCustomerName.Text.Trim().Length == 0)
                {
                    txtCustomerName.Tag = null;
                    txtCustomerName.Clear();
                }
            };

            txtOrderAmount.KeyPress += TextBoxNumber_KeyPress;
            txtOrderAmount.Leave += (o, e) =>
            {
                double money = double.Parse(txtOrderAmount.Text, CultureInfo.GetCultureInfo("vi-VN"));
                txtOrderAmount.Text = money.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
            };

            txtReceiptAmount.KeyPress += TextBoxNumber_KeyPress;
            txtReceiptAmount.Leave += (o, e) => {
                double money = double.Parse(txtReceiptAmount.Text, CultureInfo.GetCultureInfo("vi-VN"));
                txtReceiptAmount.Text = money.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
            };

            LVSearch.KeyDown += LVSearch_KeyDown;
            LVSearch.ItemActivate += LVSearch_ItemActivate;
            LVSearch.ItemSelectionChanged += LVSearch_ItemSelectionChanged;

            this.Text = "Thông tin phiếu thu";
            this.WindowState = FormWindowState.Normal;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.FormBorderStyle = FormBorderStyle.None;
            this.ShowInTaskbar = true;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.Load += Frm_ReceiptEdit_Load;
        }

        private void Frm_ReceiptEdit_Load(object sender, EventArgs e)
        {
            Get_Auth();
            if (_RoleForm.RoleRead)
            {
                LoadData();
            }
        }

        #region [ListView]
        private void DesignLayout_CRM(ListView LV)
        {
            LV.Clear();

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Khách hàng";
            colHead.Width = 450;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Địa chỉ";
            colHead.Width = 450;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "SĐT";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            TNPaintControl.DrawLVStyle(ref LVSearch);
        }
        private void DesignLayout_FIN(ListView LV)
        {
            LV.Clear();

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã biên nhận";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
            colHead = new ColumnHeader();
            colHead.Text = "Khách hàng";
            colHead.Width = 250;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "Số tiền";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ghi chú";
            colHead.Width = 250;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            TNPaintControl.DrawLVStyle(ref LVSearch);
        }
        private void LoadListView_FIN()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVSearch;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable zTable = Access_Data.ListOrder(SessionUser.UserLogin.PartnerNumber, txtDocumentID.Text.Trim());
            LV.Items.Clear();
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow zRow = zTable.Rows[i];
                lvi.Tag = zRow["OrderKey"].ToString();
                lvi.BackColor = Color.White;

                DateTime zDate = DateTime.Parse(zRow["OrderDate"].ToString());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zDate.ToString("dd/MM/yyyy");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["OrderID"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["BuyerName"].ToString();
                lvi.SubItems.Add(lvsi);

                double zAmount = zRow["OrderAmount"].ToDouble();
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zAmount.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["OrderDescription"].ToString();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;

        }
        private void LoadListView_CRM()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVSearch;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable zTable = Access_Data.ListCustomer(txtCustomerName.Text, SessionUser.UserLogin.PartnerNumber);
            LV.Items.Clear();
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow zRow = zTable.Rows[i];
                lvi.Tag = zRow["CustomerKey"].ToString();
                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["CustomerID"].ToString();
                lvi.SubItems.Add(lvsi);

                string zName = "(" + zRow["Aliases"].ToString() + ")-" + zRow["FullName"].ToString();
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zName;
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["Address"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["Phone"].ToString();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }
            this.Cursor = Cursors.Default;

        }
        private void LVSearch_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (_LVSearchStyle == "CRM")
            {
                txtCustomerName.Tag = LVSearch.Items[e.ItemIndex].Tag.ToString();
                txtCustomerName.Text = LVSearch.Items[e.ItemIndex].SubItems[2].Text.Trim();
                txtCustomerAddress.Text = LVSearch.Items[e.ItemIndex].SubItems[3].Text.Trim();
                txtCustomerPhone.Text = LVSearch.Items[e.ItemIndex].SubItems[4].Text.Trim();
            }
            if (_LVSearchStyle == "FNC")
            {
                txtDocumentID.Tag = LVSearch.Items[e.ItemIndex].Tag;
                txtDocumentID.Text = LVSearch.Items[e.ItemIndex].SubItems[2].Text.Trim();
                txtOrderAmount.Text = LVSearch.Items[e.ItemIndex].SubItems[4].Text.Trim();
            }
        }
        private void LVSearch_ItemActivate(object sender, EventArgs e)
        {
            if (_LVSearchStyle == "CRM")
            {
                txtCustomerName.Tag = LVSearch.SelectedItems[0].Tag;
                txtCustomerName.Text = LVSearch.SelectedItems[0].SubItems[2].Text.Trim();
                txtCustomerAddress.Text = LVSearch.SelectedItems[0].SubItems[3].Text.Trim();
                txtCustomerPhone.Text = LVSearch.SelectedItems[0].SubItems[4].Text.Trim();

                txtCustomerName.Focus();
                LVSearch.Visible = false;
            }
            if (_LVSearchStyle == "FNC")
            {
                txtDocumentID.Tag = LVSearch.SelectedItems[0].Tag;
                txtDocumentID.Text = LVSearch.SelectedItems[0].SubItems[2].Text.Trim();
                txtOrderAmount.Text = LVSearch.SelectedItems[0].SubItems[4].Text.Trim();

                txtDocumentID.Focus();
                LVSearch.Visible = false;
            }
        }
        private void LVSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (LVSearch.SelectedItems.Count > 0)
                {
                    if (_LVSearchStyle == "CRM")
                    {

                        txtCustomerName.Tag = LVSearch.SelectedItems[0].Tag;
                        txtCustomerName.Text = LVSearch.SelectedItems[0].SubItems[2].Text.Trim();
                        txtCustomerAddress.Text = LVSearch.SelectedItems[0].SubItems[3].Text.Trim();
                        txtCustomerPhone.Text = LVSearch.SelectedItems[0].SubItems[4].Text.Trim();
                        txtCustomerPhone.Focus();
                    }
                    if (_LVSearchStyle == "FNC")
                    {
                        txtDocumentID.Tag = LVSearch.SelectedItems[0].Tag;
                        txtDocumentID.Text = LVSearch.SelectedItems[0].SubItems[2].Text.Trim();
                        txtOrderAmount.Text = LVSearch.SelectedItems[0].SubItems[4].Text.Trim();
                        txtDocumentID.Focus();
                    }

                }
                LVSearch.Visible = false;
            }
        }
        #endregion

        #region [Event]
        private void TextBoxNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            if (_RoleForm.RoleDel)
            {
                DialogResult dlr = MessageBox.Show("Thao tác này sẽ xóa tất cả mọi thông tin liên quan đến doanh thu !.", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                string Message = "";
                if (dlr == DialogResult.Yes && _ReceiptKey.Length == 36)
                {
                    Message = Delete();

                    if (Message.Substring(0, 3) == "200" ||
                        Message.Substring(0, 3) == "201")
                    {
                        Message = UpdateOrderMoneyAfterDelete();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show(Message);
                    }
                }
            }
        }
        private void BtnSearchDocument_Click(object sender, EventArgs e)
        {
            _LVSearchStyle = "FNC";
            DesignLayout_FIN(LVSearch);
            LVSearch.Location = new Point(104, 244);
            LVSearch.Focus();
            LVSearch.Visible = true;
            LVSearch.BringToFront();
            LoadListView_FIN();
        }
        private void BtnSearchCustomer_Click(object sender, EventArgs e)
        {
            _LVSearchStyle = "CRM";
            DesignLayout_CRM(LVSearch);
            LVSearch.Location = new Point(104, 118);
            LVSearch.Focus();
            LVSearch.Visible = true;
            LVSearch.BringToFront();
            LoadListView_CRM();
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            string zMessage = CheckBeforSave();
            if (zMessage.Length == 0)
            {
                string zResult = Save();
                if (zResult.Substring(0, 3) == "200" ||
                    zResult.Substring(0, 3) == "201")
                {
                    MessageBox.Show("Cập nhật phiếu thành công!");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Lỗi.Vui lòng liên hệ IT.Chi tiết lỗi mã : \n " + zResult);
                }
            }
            else
            {
                MessageBox.Show(zMessage);
            }
        }
        #endregion

        #region [Process]
        private void LoadData()
        {
            if (_ReceiptKey != string.Empty)
            {
                Receipt_Info zInfo = new Receipt_Info(_ReceiptKey);
                if (zInfo.ReceiptDate != null)
                {
                    dteReceiptDate.Value = zInfo.ReceiptDate.Value;
                }
                else
                {
                    dteReceiptDate.Value = DateTime.Now;
                }

                txtReceiptID.Text = zInfo.ReceiptID;
                txtDocumentID.Text = zInfo.DocumentID;
                txtCustomerName.Text = zInfo.CustomerName;
                txtCustomerPhone.Text = zInfo.CustomerPhone;
                txtCustomerAddress.Text = zInfo.Address;
                txtReceiptAmount.Text = zInfo.AmountCurrencyMain.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                txtDescription.Text = zInfo.ReceiptDescription;
                if (zInfo.DocumentID != string.Empty && zInfo.Slug == 1)
                {
                    txtOrderAmount.Text = new Order_FED_Info(zInfo.DocumentID).OrderAmount.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                }

                if (zInfo.StyleKey == 1)
                {
                    rdoProduct.Checked = true;
                }
                else
                {
                    rdoService.Checked = true;
                }

                return;
            }

            if (_OrderID != string.Empty)
            {
                txtReceiptID.Text = Access_Data.AutoReceiptID("PT", SessionUser.UserLogin.PartnerNumber);

                Order_FED_Info zOrder = new Order_FED_Info(_OrderID, true);
                txtDocumentID.Text = zOrder.OrderID;
                txtCustomerName.Tag = zOrder.BuyerKey;
                txtCustomerName.Text = zOrder.BuyerName;
                txtCustomerAddress.Text = zOrder.BuyerAddress;
                txtCustomerPhone.Text = zOrder.BuyerPhone;
                dteReceiptDate.Value = DateTime.Now;

                txtOrderAmount.Text = zOrder.OrderAmount.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                txtReceiptAmount.Text = "0";
                return;
            }

            txtReceiptID.Text = Access_Data.AutoReceiptID("PT", SessionUser.UserLogin.PartnerNumber);
            dteReceiptDate.Value = DateTime.Now;
        }
        private string Save()
        {
            Receipt_Info zInfo;

            if (_ReceiptKey != "")
            {
                zInfo = new Receipt_Info(_ReceiptKey);
                zInfo.ReceiptID = txtReceiptID.Text.Trim();
            }
            else
            {
                zInfo = new Receipt_Info();
                zInfo.ReceiptID = Access_Data.AutoReceiptID("PT", SessionUser.UserLogin.PartnerNumber);
            }
            if (txtCustomerName.Tag != null)
            {
                zInfo.CustomerKey = txtCustomerName.Tag.ToString();
            }

            zInfo.PartnerNumber = SessionUser.UserLogin.PartnerNumber;
            zInfo.CustomerName = txtCustomerName.Text;
            zInfo.CustomerPhone = txtCustomerPhone.Text;

            zInfo.ReceiptDate = dteReceiptDate.Value;
            zInfo.DocumentID = txtDocumentID.Text.Trim();
            zInfo.ReceiptDescription = txtDescription.Text.Trim();

            if (txtReceiptAmount.Text != string.Empty)
            {
                zInfo.AmountCurrencyMain = double.Parse(txtReceiptAmount.Text, CultureInfo.GetCultureInfo("vi-VN"));
            }

            if (rdoProduct.Checked)
            {
                zInfo.StyleKey = 1;
                zInfo.StyleName = rdoProduct.Text;
            }

            if (rdoService.Checked)
            {
                zInfo.StyleKey = 2;
                zInfo.StyleName = rdoService.Text;
            }

            zInfo.CreatedBy = SessionUser.UserLogin.UserKey;
            zInfo.CreatedName = SessionUser.UserLogin.EmployeeName;
            zInfo.ModifiedBy = SessionUser.UserLogin.UserKey;
            zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;

            if (_ReceiptKey != "")
            {
                zInfo.Update();
            }
            else
            {
                zInfo.Create_ServerKey();
            }

            return zInfo.Message;
        }
        private string CheckBeforSave()
        {
            string zResult = "";
            if (txtReceiptID.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng điền mã phiếu thu ! \n";
                txtReceiptID.StateCommon.Back.Color1 = Color.FromArgb(255, 102, 102);
            }
            else
            {
                txtReceiptID.StateCommon.Back.Color1 = Color.White;
            }
            if (ReceiptKey == "")
            {
                bool Count = Access_Data.CheckReceiptID(SessionUser.UserLogin.PartnerNumber, txtReceiptID.Text.Trim());
                if (!Count)
                {
                    zResult += "Vui lòng chọn mã phiếu thu khác! \n";
                    txtReceiptID.StateCommon.Back.Color1 = Color.FromArgb(255, 102, 102);
                }
            }
            else
            {
                txtReceiptID.StateCommon.Back.Color1 = Color.White;
            }

            if (txtCustomerName.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng nhập thông tin khách hàng ! \n";
                txtCustomerName.StateCommon.Back.Color1 = Color.FromArgb(255, 102, 102);
            }
            else
            {
                txtCustomerName.StateCommon.Back.Color1 = Color.White;
            }

            if (txtReceiptAmount.Text.Trim().Length == 0)
            {
                zResult += "Vui lòng nhập số tiền thu ! \n";
                txtReceiptAmount.StateCommon.Back.Color1 = Color.FromArgb(255, 102, 102);
            }
            else
            {
                txtReceiptAmount.StateCommon.Back.Color1 = Color.White;
            }
            return zResult;
        }
        private string Delete()
        {
            Receipt_Info zInfo = new Receipt_Info();
            zInfo.ReceiptKey = _ReceiptKey;
            zInfo.Delete();
            return zInfo.Message;
        }
        private string UpdateOrderMoneyAfterDelete()
        {
            DataTable zTable = Access_Data.ListOrderReciept(SessionUser.UserLogin.PartnerNumber, txtDocumentID.Text.Trim());
            List<Receipt_Info> ListOrderReceipt = zTable.CopyToList<Receipt_Info>();

            double zTotal = 0;
            for (int i = 0; i < ListOrderReceipt.Count; i++)
            {
                Receipt_Info zExistItem = ListOrderReceipt[i];
                zTotal += zExistItem.AmountCurrencyMain;
            }

            Order_FED_Info zInfo = new Order_FED_Info(txtDocumentID.Text.Trim(), true);
            zInfo.ReceiptAmount = zTotal;
            zInfo.Update();
            if (zInfo.Code != "200" &&
                zInfo.Code != "201")
            {
                MessageBox.Show(zInfo.Message, "Lỗi xóa phiếu thu vui lòng liên hệ IT !.", MessageBoxButtons.OK);
            }

            return zInfo.Message;
        }
        #endregion

        #region [Dùng kéo rê form]
        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;



        private void Frm_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }
        private void Frm_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }
        private void Frm_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        #region [Auth]
        User_Role_Info _RoleForm = new User_Role_Info();
        private void Get_Auth()
        {
            List<User_Role_Info> zListRole = SessionUser.TableRole;
            if (zListRole.Count > 0)
            {
                try
                {
                    _RoleForm = zListRole.SingleOrDefault(r => r.RoleURL == FormUrl);
                    if (!_RoleForm.RoleRead)
                    {
                        _RoleForm = new User_Role_Info();
                        MessageBox.Show("Bạn không có quyền xem, vui lòng liên hệ IT !.");
                        this.Close();
                    }
                    else
                    {
                        if (!_RoleForm.RoleEdit)
                        {
                            btnSave.Visible = false;
                        }
                        if (!_RoleForm.RoleAdd)
                        {
                            btnSave.Visible = false;
                        }
                        if (!_RoleForm.RoleDel)
                        {
                            btnDelete.Visible = false;
                        }
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Bạn chưa được phân quyền, vui lòng liên hệ IT !.");
            }
        }
        #endregion       

        #region [Access Info-Data]
        public class Access_Data
        {
            public static DataTable ListOrderReciept(string PartnerNumber, string OrderID)
            {
                DataTable zTable = new DataTable();
                string zSQL = "SELECT * FROM [dbo].[FNC_Receipt] WHERE PartnerNumber = @PartnerNumber AND DocumentID = @OrderID AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = OrderID;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
            public static DataTable ListCustomer(string Name, string PartnerNumber)
            {
                DataTable zTable = new DataTable();
                string zSQL = @"
                SELECT CustomerKey, CustomerID, FullName, Aliases, Address, Phone FROM [dbo].[CRM_Customer] 
                WHERE RecordStatus <> 99 ";
                if (Name.Trim().Length > 0)
                {
                    zSQL += " AND FullName LIKE @Name";
                }

                zSQL += " AND PartnerNumber = @PartnerNumber ORDER BY CreatedOn DESC";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string Result = ex.ToString();
                }
                return zTable;
            }
            public static DataTable ListOrder(string PartnerNumber, string OrderID)
            {
                DataTable zTable = new DataTable();
                string zSQL = @"
SELECT * 
FROM FNC_Order_FED
WHERE RecordStatus <> 99  
AND PartnerNumber= @PartnerNumber";

                if (OrderID.Trim().Length > 0)
                {
                    zSQL += " AND OrderID LIKE @Name";
                }

                zSQL += " ORDER BY OrderDate DESC";
                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + OrderID + "%";
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
            public static string AutoReceiptID(string Prefix, string PartnerNumber)
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "SELECT dbo.Auto_Receipt_ID(@Prefix, @PartnerNumber)";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@Prefix", SqlDbType.NVarChar).Value = Prefix;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zResult = zCommand.ExecuteScalar().ToString();
                    zCommand.Dispose();
                }
                catch (Exception)
                {

                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public static bool CheckReceiptID(string PartnerNumber, string ReceiptID)
            {
                bool zResult = false;  //  Khong co
                DataTable zTable = new DataTable();
                string zSQL = @"SELECT Count(*) AS Amount FROM [dbo].[FNC_Receipt] WHERE PartnerNumber = @PartnerNumber AND ReceiptID = @ReceiptID AND RecordStatus != 99";

                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@ReceiptID", SqlDbType.NVarChar).Value = ReceiptID;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string Result = ex.ToString();
                }
                if (zTable.Rows.Count > 0)
                {
                    int zAmount = 0;
                    DataRow zRow = zTable.Rows[0];
                    zAmount = int.Parse(zRow["Amount"].ToString());
                    if (zAmount == 0)
                    {
                        zResult = true;
                    }
                    else
                    {
                        zResult = false;
                    }
                }
                return zResult;
            }
        }
        public class Receipt_Info
        {
            #region [ Field Name ]
            private string _ReceiptKey = "";
            private string _ReceiptID = "";
            private DateTime? _ReceiptDate = null;
            private string _ReceiptDescription = "";
            private string _DocumentID = "";
            private int _CategoryKey = 0;
            private string _CategoryName = "";
            private int _StyleKey = 0;
            private string _StyleName = "";
            private string _CustomerKey = "";
            private string _CustomerName = "";
            private string _CustomerPhone = "";
            private string _Depositor = "";
            private string _Address = "";
            private double _AmountCurrencyMain = 0;
            private double _AmountCurrencyForeign = 0;
            private string _CurrencyIDForeign = "";
            private double _CurrencyRate = 0;
            private string _BankName = "";
            private string _BankAccount = "";
            private double _BankVAT = 0;
            private double _BankFee = 0;
            private bool _IsFeeInside;
            private int _Slug = 0;
            private string _DebitNo = "";
            private string _CreditNo = "";
            private string _PartnerNumber = "";
            private int _RecordStatus = 0;
            private DateTime? _CreatedOn = null;
            private string _CreatedBy = "";
            private string _CreatedName = "";
            private DateTime? _ModifiedOn = null;
            private string _ModifiedBy = "";
            private string _ModifiedName = "";
            private string _Message = "";
            #endregion

            #region [ Constructor Get Information ]
            public Receipt_Info()
            {
                Guid zNewID = Guid.NewGuid();
                _ReceiptKey = zNewID.ToString();
            }
            public Receipt_Info(string ReceiptKey)
            {
                string zSQL = "SELECT * FROM [dbo].[FNC_Receipt] WHERE ReceiptKey = @ReceiptKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ReceiptKey", SqlDbType.UniqueIdentifier).Value = new Guid(ReceiptKey);
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _ReceiptKey = zReader["ReceiptKey"].ToString();
                        _ReceiptID = zReader["ReceiptID"].ToString();
                        if (zReader["ReceiptDate"] != DBNull.Value)
                        {
                            _ReceiptDate = (DateTime)zReader["ReceiptDate"];
                        }

                        _ReceiptDescription = zReader["ReceiptDescription"].ToString();
                        _DocumentID = zReader["DocumentID"].ToString();
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                        _CategoryName = zReader["CategoryName"].ToString();
                        _StyleKey = int.Parse(zReader["StyleKey"].ToString());
                        _StyleName = zReader["StyleName"].ToString();
                        _CustomerKey = zReader["CustomerKey"].ToString();
                        _CustomerName = zReader["CustomerName"].ToString();
                        _CustomerPhone = zReader["CustomerPhone"].ToString();
                        _Depositor = zReader["Depositor"].ToString();
                        _Address = zReader["Address"].ToString();
                        _AmountCurrencyMain = double.Parse(zReader["AmountCurrencyMain"].ToString());
                        _AmountCurrencyForeign = double.Parse(zReader["AmountCurrencyForeign"].ToString());
                        _CurrencyIDForeign = zReader["CurrencyIDForeign"].ToString();
                        _CurrencyRate = double.Parse(zReader["CurrencyRate"].ToString());
                        _BankName = zReader["BankName"].ToString();
                        _BankAccount = zReader["BankAccount"].ToString();
                        _BankVAT = double.Parse(zReader["BankVAT"].ToString());
                        _BankFee = double.Parse(zReader["BankFee"].ToString());
                        _IsFeeInside = (bool)zReader["IsFeeInside"];
                        _Slug = int.Parse(zReader["Slug"].ToString());
                        _DebitNo = zReader["DebitNo"].ToString();
                        _CreditNo = zReader["CreditNo"].ToString();
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }

            #endregion

            #region [ Properties ]
            public string ReceiptKey
            {
                get { return _ReceiptKey; }
                set { _ReceiptKey = value; }
            }
            public string ReceiptID
            {
                get { return _ReceiptID; }
                set { _ReceiptID = value; }
            }
            public DateTime? ReceiptDate
            {
                get { return _ReceiptDate; }
                set { _ReceiptDate = value; }
            }
            public string ReceiptDescription
            {
                get { return _ReceiptDescription; }
                set { _ReceiptDescription = value; }
            }
            public string DocumentID
            {
                get { return _DocumentID; }
                set { _DocumentID = value; }
            }
            public int CategoryKey
            {
                get { return _CategoryKey; }
                set { _CategoryKey = value; }
            }
            public string CategoryName
            {
                get { return _CategoryName; }
                set { _CategoryName = value; }
            }
            public int StyleKey
            {
                get { return _StyleKey; }
                set { _StyleKey = value; }
            }
            public string CustomerKey
            {
                get { return _CustomerKey; }
                set { _CustomerKey = value; }
            }
            public string CustomerName
            {
                get { return _CustomerName; }
                set { _CustomerName = value; }
            }
            public string CustomerPhone
            {
                get { return _CustomerPhone; }
                set { _CustomerPhone = value; }
            }
            public string Depositor
            {
                get { return _Depositor; }
                set { _Depositor = value; }
            }
            public string Address
            {
                get { return _Address; }
                set { _Address = value; }
            }
            public double AmountCurrencyMain
            {
                get { return _AmountCurrencyMain; }
                set { _AmountCurrencyMain = value; }
            }
            public double AmountCurrencyForeign
            {
                get { return _AmountCurrencyForeign; }
                set { _AmountCurrencyForeign = value; }
            }
            public string CurrencyIDForeign
            {
                get { return _CurrencyIDForeign; }
                set { _CurrencyIDForeign = value; }
            }
            public double CurrencyRate
            {
                get { return _CurrencyRate; }
                set { _CurrencyRate = value; }
            }
            public string BankName
            {
                get { return _BankName; }
                set { _BankName = value; }
            }
            public string BankAccount
            {
                get { return _BankAccount; }
                set { _BankAccount = value; }
            }
            public double BankVAT
            {
                get { return _BankVAT; }
                set { _BankVAT = value; }
            }
            public double BankFee
            {
                get { return _BankFee; }
                set { _BankFee = value; }
            }
            public bool IsFeeInside
            {
                get { return _IsFeeInside; }
                set { _IsFeeInside = value; }
            }
            public int Slug
            {
                get { return _Slug; }
                set { _Slug = value; }
            }
            public string DebitNo
            {
                get { return _DebitNo; }
                set { _DebitNo = value; }
            }
            public string CreditNo
            {
                get { return _CreditNo; }
                set { _CreditNo = value; }
            }
            public string PartnerNumber
            {
                get { return _PartnerNumber; }
                set { _PartnerNumber = value; }
            }
            public int RecordStatus
            {
                get { return _RecordStatus; }
                set { _RecordStatus = value; }
            }
            public DateTime? CreatedOn
            {
                get { return _CreatedOn; }
                set { _CreatedOn = value; }
            }
            public string CreatedBy
            {
                get { return _CreatedBy; }
                set { _CreatedBy = value; }
            }
            public string CreatedName
            {
                get { return _CreatedName; }
                set { _CreatedName = value; }
            }
            public DateTime? ModifiedOn
            {
                get { return _ModifiedOn; }
                set { _ModifiedOn = value; }
            }
            public string ModifiedBy
            {
                get { return _ModifiedBy; }
                set { _ModifiedBy = value; }
            }
            public string ModifiedName
            {
                get { return _ModifiedName; }
                set { _ModifiedName = value; }
            }
            public string Code
            {
                get
                {
                    if (_Message.Length >= 3)
                    {
                        return _Message.Substring(0, 3);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            public string Message
            {
                get { return _Message; }
                set { _Message = value; }
            }

            public string StyleName
            {
                get
                {
                    return _StyleName;
                }

                set
                {
                    _StyleName = value;
                }
            }
            #endregion

            #region [ Constructor Update Information ]
            public string Create_ServerKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[FNC_Receipt] ("
             + " ReceiptID ,ReceiptDate ,ReceiptDescription ,DocumentID ,CategoryKey ,CategoryName ,StyleKey, StyleName ,CustomerKey ,CustomerName ,CustomerPhone ,Depositor ,Address ,AmountCurrencyMain ,AmountCurrencyForeign ,CurrencyIDForeign ,CurrencyRate ,BankName ,BankAccount ,BankVAT ,BankFee ,IsFeeInside ,Slug ,DebitNo ,CreditNo ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@ReceiptID ,@ReceiptDate ,@ReceiptDescription ,@DocumentID ,@CategoryKey ,@CategoryName ,@StyleKey, @StyleName ,@CustomerKey ,@CustomerName ,@CustomerPhone ,@Depositor ,@Address ,@AmountCurrencyMain ,@AmountCurrencyForeign ,@CurrencyIDForeign ,@CurrencyRate ,@BankName ,@BankAccount ,@BankVAT ,@BankFee ,@IsFeeInside ,@Slug ,@DebitNo ,@CreditNo ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ReceiptID", SqlDbType.NVarChar).Value = _ReceiptID;
                    if (_ReceiptDate == null)
                    {
                        zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = _ReceiptDate;
                    }

                    zCommand.Parameters.Add("@ReceiptDescription", SqlDbType.NVarChar).Value = _ReceiptDescription;
                    zCommand.Parameters.Add("@DocumentID", SqlDbType.NVarChar).Value = _DocumentID;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@StyleKey", SqlDbType.Int).Value = _StyleKey;
                    zCommand.Parameters.Add("@StyleName", SqlDbType.NVarChar).Value = StyleName;
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_CustomerKey);
                    zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = _CustomerName;
                    zCommand.Parameters.Add("@CustomerPhone", SqlDbType.NVarChar).Value = _CustomerPhone;
                    zCommand.Parameters.Add("@Depositor", SqlDbType.NVarChar).Value = _Depositor;
                    zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                    zCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = _AmountCurrencyMain;
                    zCommand.Parameters.Add("@AmountCurrencyForeign", SqlDbType.Money).Value = _AmountCurrencyForeign;
                    zCommand.Parameters.Add("@CurrencyIDForeign", SqlDbType.NChar).Value = _CurrencyIDForeign;
                    zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = _CurrencyRate;
                    zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = _BankName;
                    zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = _BankAccount;
                    zCommand.Parameters.Add("@BankVAT", SqlDbType.Money).Value = _BankVAT;
                    zCommand.Parameters.Add("@BankFee", SqlDbType.Money).Value = _BankFee;
                    zCommand.Parameters.Add("@IsFeeInside", SqlDbType.Bit).Value = _IsFeeInside;
                    zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                    zCommand.Parameters.Add("@DebitNo", SqlDbType.NVarChar).Value = _DebitNo;
                    zCommand.Parameters.Add("@CreditNo", SqlDbType.NVarChar).Value = _CreditNo;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Create_ClientKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[FNC_Receipt] ("
            + " ReceiptKey ,ReceiptID ,ReceiptDate ,ReceiptDescription ,DocumentID ,CategoryKey ,CategoryName ,StyleKey, StyleName ,CustomerKey ,CustomerName ,CustomerPhone ,Depositor ,Address ,AmountCurrencyMain ,AmountCurrencyForeign ,CurrencyIDForeign ,CurrencyRate ,BankName ,BankAccount ,BankVAT ,BankFee ,IsFeeInside ,Slug ,DebitNo ,CreditNo ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@ReceiptKey ,@ReceiptID ,@ReceiptDate ,@ReceiptDescription ,@DocumentID ,@CategoryKey ,@CategoryName ,@StyleKey, @StyleName ,@CustomerKey ,@CustomerName ,@CustomerPhone ,@Depositor ,@Address ,@AmountCurrencyMain ,@AmountCurrencyForeign ,@CurrencyIDForeign ,@CurrencyRate ,@BankName ,@BankAccount ,@BankVAT ,@BankFee ,@IsFeeInside ,@Slug ,@DebitNo ,@CreditNo ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ReceiptKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ReceiptKey);
                    zCommand.Parameters.Add("@ReceiptID", SqlDbType.NVarChar).Value = _ReceiptID;
                    if (_ReceiptDate == null)
                    {
                        zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = _ReceiptDate;
                    }

                    zCommand.Parameters.Add("@ReceiptDescription", SqlDbType.NVarChar).Value = _ReceiptDescription;
                    zCommand.Parameters.Add("@DocumentID", SqlDbType.NVarChar).Value = _DocumentID;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@StyleKey", SqlDbType.Int).Value = _StyleKey;
                    zCommand.Parameters.Add("@StyleName", SqlDbType.NVarChar).Value = StyleName;
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_CustomerKey);
                    zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = _CustomerName;
                    zCommand.Parameters.Add("@CustomerPhone", SqlDbType.NVarChar).Value = _CustomerPhone;
                    zCommand.Parameters.Add("@Depositor", SqlDbType.NVarChar).Value = _Depositor;
                    zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                    zCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = _AmountCurrencyMain;
                    zCommand.Parameters.Add("@AmountCurrencyForeign", SqlDbType.Money).Value = _AmountCurrencyForeign;
                    zCommand.Parameters.Add("@CurrencyIDForeign", SqlDbType.NChar).Value = _CurrencyIDForeign;
                    zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = _CurrencyRate;
                    zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = _BankName;
                    zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = _BankAccount;
                    zCommand.Parameters.Add("@BankVAT", SqlDbType.Money).Value = _BankVAT;
                    zCommand.Parameters.Add("@BankFee", SqlDbType.Money).Value = _BankFee;
                    zCommand.Parameters.Add("@IsFeeInside", SqlDbType.Bit).Value = _IsFeeInside;
                    zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                    zCommand.Parameters.Add("@DebitNo", SqlDbType.NVarChar).Value = _DebitNo;
                    zCommand.Parameters.Add("@CreditNo", SqlDbType.NVarChar).Value = _CreditNo;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Update()
            {
                string zSQL = "UPDATE [dbo].[FNC_Receipt] SET "
                            + " ReceiptID = @ReceiptID,"
                            + " ReceiptDate = @ReceiptDate,"
                            + " ReceiptDescription = @ReceiptDescription,"
                            + " DocumentID = @DocumentID,"
                            + " CategoryKey = @CategoryKey,"
                            + " CategoryName = @CategoryName,"
                            + " StyleKey = @StyleKey, StyleName = @StyleName,"
                            + " CustomerKey = @CustomerKey,"
                            + " CustomerName = @CustomerName,"
                            + " CustomerPhone = @CustomerPhone,"
                            + " Depositor = @Depositor,"
                            + " Address = @Address,"
                            + " AmountCurrencyMain = @AmountCurrencyMain,"
                            + " AmountCurrencyForeign = @AmountCurrencyForeign,"
                            + " CurrencyIDForeign = @CurrencyIDForeign,"
                            + " CurrencyRate = @CurrencyRate,"
                            + " BankName = @BankName,"
                            + " BankAccount = @BankAccount,"
                            + " BankVAT = @BankVAT,"
                            + " BankFee = @BankFee,"
                            + " IsFeeInside = @IsFeeInside,"
                            + " Slug = @Slug,"
                            + " DebitNo = @DebitNo,"
                            + " CreditNo = @CreditNo,"
                            + " PartnerNumber = @PartnerNumber,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedOn = GetDate(),"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName"
                           + " WHERE ReceiptKey = @ReceiptKey";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@ReceiptKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ReceiptKey);
                    zCommand.Parameters.Add("@ReceiptID", SqlDbType.NVarChar).Value = _ReceiptID;
                    if (_ReceiptDate == null)
                    {
                        zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = _ReceiptDate;
                    }

                    zCommand.Parameters.Add("@ReceiptDescription", SqlDbType.NVarChar).Value = _ReceiptDescription;
                    zCommand.Parameters.Add("@DocumentID", SqlDbType.NVarChar).Value = _DocumentID;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@StyleKey", SqlDbType.Int).Value = _StyleKey;
                    zCommand.Parameters.Add("@StyleName", SqlDbType.NVarChar).Value = StyleName;
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_CustomerKey);
                    zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = _CustomerName;
                    zCommand.Parameters.Add("@CustomerPhone", SqlDbType.NVarChar).Value = _CustomerPhone;
                    zCommand.Parameters.Add("@Depositor", SqlDbType.NVarChar).Value = _Depositor;
                    zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                    zCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = _AmountCurrencyMain;
                    zCommand.Parameters.Add("@AmountCurrencyForeign", SqlDbType.Money).Value = _AmountCurrencyForeign;
                    zCommand.Parameters.Add("@CurrencyIDForeign", SqlDbType.NChar).Value = _CurrencyIDForeign;
                    zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = _CurrencyRate;
                    zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = _BankName;
                    zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = _BankAccount;
                    zCommand.Parameters.Add("@BankVAT", SqlDbType.Money).Value = _BankVAT;
                    zCommand.Parameters.Add("@BankFee", SqlDbType.Money).Value = _BankFee;
                    zCommand.Parameters.Add("@IsFeeInside", SqlDbType.Bit).Value = _IsFeeInside;
                    zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                    zCommand.Parameters.Add("@DebitNo", SqlDbType.NVarChar).Value = _DebitNo;
                    zCommand.Parameters.Add("@CreditNo", SqlDbType.NVarChar).Value = _CreditNo;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Delete()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[FNC_Receipt] Set RecordStatus = 99 WHERE ReceiptKey = @ReceiptKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ReceiptKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ReceiptKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[FNC_Receipt] WHERE ReceiptKey = @ReceiptKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@ReceiptKey", SqlDbType.UniqueIdentifier).Value = new Guid(_ReceiptKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            #endregion
        }
        public class Order_FED_Info
        {
            #region [ Field Name ]
            private string _OrderKey = "";
            private string _OrderID = "";
            private DateTime? _OrderDate = null;
            private string _OrderDescription = "";
            private string _SellerKey = "";
            private string _SellerName = "";
            private string _BuyerKey = "";
            private string _BuyerName = "";
            private string _BuyerPhone = "";
            private string _BuyerAddress = "";
            private int _OrderStatusKey = 0;
            private string _OrderStatusName = "";
            private int _PaymentStatusKey = 0;
            private string _PaymentStatusName = "";
            private DateTime? _DeliverDate = null;
            private DateTime? _ReturnDate = null;
            private int _CategoryKey = 0;
            private string _CategoryName = "";
            private int _OrganizationKey = 0;
            private string _OrganizationID = "";
            private string _OrganizationName = "";
            private double _TotalAmount = 0;
            private double _SaleAmount = 0;
            private double _OrderAmount = 0;
            private double _ReceiptAmount = 0;
            private string _PartnerNumber = "";
            private int _RecordStatus = 0;
            private DateTime? _CreatedOn = null;
            private string _CreatedBy = "";
            private string _CreatedName = "";
            private DateTime? _ModifiedOn = null;
            private string _ModifiedBy = "";
            private string _ModifiedName = "";
            private string _Message = "";
            #endregion

            #region [ Constructor Get Information ]
            public Order_FED_Info()
            {
                Guid zNewID = Guid.NewGuid();
                _OrderKey = zNewID.ToString();
            }
            public Order_FED_Info(string OrderKey)
            {
                string zSQL = "SELECT * FROM [dbo].[FNC_Order_FED] WHERE OrderKey = @OrderKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(OrderKey);
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _OrderKey = zReader["OrderKey"].ToString();
                        _OrderID = zReader["OrderID"].ToString();
                        if (zReader["OrderDate"] != DBNull.Value)
                        {
                            _OrderDate = (DateTime)zReader["OrderDate"];
                        }

                        _OrderDescription = zReader["OrderDescription"].ToString();
                        _SellerKey = zReader["SellerKey"].ToString();
                        _SellerName = zReader["SellerName"].ToString();
                        _BuyerKey = zReader["BuyerKey"].ToString();
                        _BuyerName = zReader["BuyerName"].ToString();
                        _BuyerPhone = zReader["BuyerPhone"].ToString();
                        _BuyerAddress = zReader["BuyerAddress"].ToString();
                        _OrderStatusKey = int.Parse(zReader["OrderStatusKey"].ToString());
                        _OrderStatusName = zReader["OrderStatusName"].ToString();
                        _PaymentStatusKey = int.Parse(zReader["PaymentStatusKey"].ToString());
                        _PaymentStatusName = zReader["PaymentStatusName"].ToString();
                        if (zReader["DeliverDate"] != DBNull.Value)
                        {
                            _DeliverDate = (DateTime)zReader["DeliverDate"];
                        }

                        if (zReader["ReturnDate"] != DBNull.Value)
                        {
                            _ReturnDate = (DateTime)zReader["ReturnDate"];
                        }

                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                        _CategoryName = zReader["CategoryName"].ToString();
                        _OrganizationKey = int.Parse(zReader["OrganizationKey"].ToString());
                        _OrganizationID = zReader["OrganizationID"].ToString();
                        _OrganizationName = zReader["OrganizationName"].ToString();
                        _TotalAmount = double.Parse(zReader["TotalAmount"].ToString());
                        _SaleAmount = double.Parse(zReader["SaleAmount"].ToString());
                        _OrderAmount = double.Parse(zReader["OrderAmount"].ToString());
                        _ReceiptAmount = double.Parse(zReader["ReceiptAmount"].ToString());
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }
            public Order_FED_Info(string OrderID, bool Receipt)
            {
                string zSQL = "SELECT * FROM [dbo].[FNC_Order_FED] WHERE OrderID = @OrderID AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = OrderID;
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _OrderKey = zReader["OrderKey"].ToString();
                        _OrderID = zReader["OrderID"].ToString();
                        if (zReader["OrderDate"] != DBNull.Value)
                        {
                            _OrderDate = (DateTime)zReader["OrderDate"];
                        }

                        _OrderDescription = zReader["OrderDescription"].ToString();
                        _SellerKey = zReader["SellerKey"].ToString();
                        _SellerName = zReader["SellerName"].ToString();
                        _BuyerKey = zReader["BuyerKey"].ToString();
                        _BuyerName = zReader["BuyerName"].ToString();
                        _BuyerPhone = zReader["BuyerPhone"].ToString();
                        _BuyerAddress = zReader["BuyerAddress"].ToString();
                        _OrderStatusKey = int.Parse(zReader["OrderStatusKey"].ToString());
                        _OrderStatusName = zReader["OrderStatusName"].ToString();
                        _PaymentStatusKey = int.Parse(zReader["PaymentStatusKey"].ToString());
                        _PaymentStatusName = zReader["PaymentStatusName"].ToString();
                        if (zReader["DeliverDate"] != DBNull.Value)
                        {
                            _DeliverDate = (DateTime)zReader["DeliverDate"];
                        }

                        if (zReader["ReturnDate"] != DBNull.Value)
                        {
                            _ReturnDate = (DateTime)zReader["ReturnDate"];
                        }

                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                        _CategoryName = zReader["CategoryName"].ToString();
                        _OrganizationKey = int.Parse(zReader["OrganizationKey"].ToString());
                        _OrganizationID = zReader["OrganizationID"].ToString();
                        _OrganizationName = zReader["OrganizationName"].ToString();
                        _TotalAmount = double.Parse(zReader["TotalAmount"].ToString());
                        _SaleAmount = double.Parse(zReader["SaleAmount"].ToString());
                        _OrderAmount = double.Parse(zReader["OrderAmount"].ToString());
                        _ReceiptAmount = double.Parse(zReader["ReceiptAmount"].ToString());
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }
            #endregion

            #region [ Properties ]
            public string OrderKey
            {
                get { return _OrderKey; }
                set { _OrderKey = value; }
            }
            public string OrderID
            {
                get { return _OrderID; }
                set { _OrderID = value; }
            }
            public DateTime? OrderDate
            {
                get { return _OrderDate; }
                set { _OrderDate = value; }
            }
            public string OrderDescription
            {
                get { return _OrderDescription; }
                set { _OrderDescription = value; }
            }
            public string SellerKey
            {
                get { return _SellerKey; }
                set { _SellerKey = value; }
            }
            public string SellerName
            {
                get { return _SellerName; }
                set { _SellerName = value; }
            }
            public string BuyerKey
            {
                get { return _BuyerKey; }
                set { _BuyerKey = value; }
            }
            public string BuyerName
            {
                get { return _BuyerName; }
                set { _BuyerName = value; }
            }
            public string BuyerPhone
            {
                get { return _BuyerPhone; }
                set { _BuyerPhone = value; }
            }
            public string BuyerAddress
            {
                get { return _BuyerAddress; }
                set { _BuyerAddress = value; }
            }
            public int OrderStatusKey
            {
                get { return _OrderStatusKey; }
                set { _OrderStatusKey = value; }
            }
            public string OrderStatusName
            {
                get { return _OrderStatusName; }
                set { _OrderStatusName = value; }
            }
            public int PaymentStatusKey
            {
                get { return _PaymentStatusKey; }
                set { _PaymentStatusKey = value; }
            }
            public string PaymentStatusName
            {
                get { return _PaymentStatusName; }
                set { _PaymentStatusName = value; }
            }
            public DateTime? DeliverDate
            {
                get { return _DeliverDate; }
                set { _DeliverDate = value; }
            }
            public DateTime? ReturnDate
            {
                get { return _ReturnDate; }
                set { _ReturnDate = value; }
            }
            public int CategoryKey
            {
                get { return _CategoryKey; }
                set { _CategoryKey = value; }
            }
            public string CategoryName
            {
                get { return _CategoryName; }
                set { _CategoryName = value; }
            }
            public int OrganizationKey
            {
                get { return _OrganizationKey; }
                set { _OrganizationKey = value; }
            }
            public string OrganizationID
            {
                get { return _OrganizationID; }
                set { _OrganizationID = value; }
            }
            public string OrganizationName
            {
                get { return _OrganizationName; }
                set { _OrganizationName = value; }
            }
            public double TotalAmount
            {
                get { return _TotalAmount; }
                set { _TotalAmount = value; }
            }
            public double SaleAmount
            {
                get { return _SaleAmount; }
                set { _SaleAmount = value; }
            }
            public double OrderAmount
            {
                get { return _OrderAmount; }
                set { _OrderAmount = value; }
            }
            public double ReceiptAmount
            {
                get { return _ReceiptAmount; }
                set { _ReceiptAmount = value; }
            }
            public string PartnerNumber
            {
                get { return _PartnerNumber; }
                set { _PartnerNumber = value; }
            }
            public int RecordStatus
            {
                get { return _RecordStatus; }
                set { _RecordStatus = value; }
            }
            public DateTime? CreatedOn
            {
                get { return _CreatedOn; }
                set { _CreatedOn = value; }
            }
            public string CreatedBy
            {
                get { return _CreatedBy; }
                set { _CreatedBy = value; }
            }
            public string CreatedName
            {
                get { return _CreatedName; }
                set { _CreatedName = value; }
            }
            public DateTime? ModifiedOn
            {
                get { return _ModifiedOn; }
                set { _ModifiedOn = value; }
            }
            public string ModifiedBy
            {
                get { return _ModifiedBy; }
                set { _ModifiedBy = value; }
            }
            public string ModifiedName
            {
                get { return _ModifiedName; }
                set { _ModifiedName = value; }
            }
            public string Code
            {
                get
                {
                    if (_Message.Length >= 3)
                    {
                        return _Message.Substring(0, 3);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            public string Message
            {
                get { return _Message; }
                set { _Message = value; }
            }
            #endregion

            #region [ Constructor Update Information ]

            public string Create_ServerKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[FNC_Order_FED] ("
            + " OrderID ,OrderDate ,OrderDescription ,SellerKey ,SellerName ,BuyerKey ,BuyerName ,BuyerPhone ,BuyerAddress ,OrderStatusKey ,OrderStatusName ,PaymentStatusKey ,PaymentStatusName ,DeliverDate ,ReturnDate ,CategoryKey ,CategoryName ,OrganizationKey ,OrganizationID ,OrganizationName ,TotalAmount ,SaleAmount ,OrderAmount ,ReceiptAmount ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@OrderID ,@OrderDate ,@OrderDescription ,@SellerKey ,@SellerName ,@BuyerKey ,@BuyerName ,@BuyerPhone ,@BuyerAddress ,@OrderStatusKey ,@OrderStatusName ,@PaymentStatusKey ,@PaymentStatusName ,@DeliverDate ,@ReturnDate ,@CategoryKey ,@CategoryName ,@OrganizationKey ,@OrganizationID ,@OrganizationName ,@TotalAmount ,@SaleAmount ,@OrderAmount ,@ReceiptAmount ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID;
                    if (_OrderDate == null)
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                    }

                    zCommand.Parameters.Add("@OrderDescription", SqlDbType.NVarChar).Value = _OrderDescription;
                    zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_SellerKey);
                    zCommand.Parameters.Add("@SellerName", SqlDbType.NVarChar).Value = _SellerName;
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_BuyerKey);
                    zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = _BuyerName;
                    zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = _BuyerPhone;
                    zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = _BuyerAddress;
                    zCommand.Parameters.Add("@OrderStatusKey", SqlDbType.Int).Value = _OrderStatusKey;
                    zCommand.Parameters.Add("@OrderStatusName", SqlDbType.NVarChar).Value = _OrderStatusName;
                    zCommand.Parameters.Add("@PaymentStatusKey", SqlDbType.Int).Value = _PaymentStatusKey;
                    zCommand.Parameters.Add("@PaymentStatusName", SqlDbType.NVarChar).Value = _PaymentStatusName;
                    if (_DeliverDate == null)
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = _DeliverDate;
                    }

                    if (_ReturnDate == null)
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = _ReturnDate;
                    }

                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = _OrganizationKey;
                    zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = _OrganizationID;
                    zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = _OrganizationName;
                    zCommand.Parameters.Add("@TotalAmount", SqlDbType.Money).Value = _TotalAmount;
                    zCommand.Parameters.Add("@SaleAmount", SqlDbType.Money).Value = _SaleAmount;
                    zCommand.Parameters.Add("@OrderAmount", SqlDbType.Money).Value = _OrderAmount;
                    zCommand.Parameters.Add("@ReceiptAmount", SqlDbType.Money).Value = _ReceiptAmount;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Create_ClientKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[FNC_Order_FED] ("
            + " OrderKey ,OrderID ,OrderDate ,OrderDescription ,SellerKey ,SellerName ,BuyerKey ,BuyerName ,BuyerPhone ,BuyerAddress ,OrderStatusKey ,OrderStatusName ,PaymentStatusKey ,PaymentStatusName ,DeliverDate ,ReturnDate ,CategoryKey ,CategoryName ,OrganizationKey ,OrganizationID ,OrganizationName ,TotalAmount ,SaleAmount ,OrderAmount ,ReceiptAmount ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@OrderKey ,@OrderID ,@OrderDate ,@OrderDescription ,@SellerKey ,@SellerName ,@BuyerKey ,@BuyerName ,@BuyerPhone ,@BuyerAddress ,@OrderStatusKey ,@OrderStatusName ,@PaymentStatusKey ,@PaymentStatusName ,@DeliverDate ,@ReturnDate ,@CategoryKey ,@CategoryName ,@OrganizationKey ,@OrganizationID ,@OrganizationName ,@TotalAmount ,@SaleAmount ,@OrderAmount ,@ReceiptAmount ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID;
                    if (_OrderDate == null)
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                    }

                    zCommand.Parameters.Add("@OrderDescription", SqlDbType.NVarChar).Value = _OrderDescription;
                    zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_SellerKey);
                    zCommand.Parameters.Add("@SellerName", SqlDbType.NVarChar).Value = _SellerName;
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_BuyerKey);
                    zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = _BuyerName;
                    zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = _BuyerPhone;
                    zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = _BuyerAddress;
                    zCommand.Parameters.Add("@OrderStatusKey", SqlDbType.Int).Value = _OrderStatusKey;
                    zCommand.Parameters.Add("@OrderStatusName", SqlDbType.NVarChar).Value = _OrderStatusName;
                    zCommand.Parameters.Add("@PaymentStatusKey", SqlDbType.Int).Value = _PaymentStatusKey;
                    zCommand.Parameters.Add("@PaymentStatusName", SqlDbType.NVarChar).Value = _PaymentStatusName;
                    if (_DeliverDate == null)
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = _DeliverDate;
                    }

                    if (_ReturnDate == null)
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = _ReturnDate;
                    }

                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = _OrganizationKey;
                    zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = _OrganizationID;
                    zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = _OrganizationName;
                    zCommand.Parameters.Add("@TotalAmount", SqlDbType.Money).Value = _TotalAmount;
                    zCommand.Parameters.Add("@SaleAmount", SqlDbType.Money).Value = _SaleAmount;
                    zCommand.Parameters.Add("@OrderAmount", SqlDbType.Money).Value = _OrderAmount;
                    zCommand.Parameters.Add("@ReceiptAmount", SqlDbType.Money).Value = _ReceiptAmount;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Update()
            {
                string zSQL = "UPDATE [dbo].[FNC_Order_FED] SET "
                            + " OrderID = @OrderID,"
                            + " OrderDate = @OrderDate,"
                            + " OrderDescription = @OrderDescription,"
                            + " SellerKey = @SellerKey,"
                            + " SellerName = @SellerName,"
                            + " BuyerKey = @BuyerKey,"
                            + " BuyerName = @BuyerName,"
                            + " BuyerPhone = @BuyerPhone,"
                            + " BuyerAddress = @BuyerAddress,"
                            + " OrderStatusKey = @OrderStatusKey,"
                            + " OrderStatusName = @OrderStatusName,"
                            + " PaymentStatusKey = @PaymentStatusKey,"
                            + " PaymentStatusName = @PaymentStatusName,"
                            + " DeliverDate = @DeliverDate,"
                            + " ReturnDate = @ReturnDate,"
                            + " CategoryKey = @CategoryKey,"
                            + " CategoryName = @CategoryName,"
                            + " OrganizationKey = @OrganizationKey,"
                            + " OrganizationID = @OrganizationID,"
                            + " OrganizationName = @OrganizationName,"
                            + " TotalAmount = @TotalAmount,"
                            + " SaleAmount = @SaleAmount,"
                            + " OrderAmount = @OrderAmount,"
                            + " ReceiptAmount = @ReceiptAmount,"
                            + " PartnerNumber = @PartnerNumber,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedOn = GetDate(),"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName"
                           + " WHERE OrderKey = @OrderKey";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID;
                    if (_OrderDate == null)
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                    }

                    zCommand.Parameters.Add("@OrderDescription", SqlDbType.NVarChar).Value = _OrderDescription;
                    zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_SellerKey);
                    zCommand.Parameters.Add("@SellerName", SqlDbType.NVarChar).Value = _SellerName;
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_BuyerKey);
                    zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = _BuyerName;
                    zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = _BuyerPhone;
                    zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = _BuyerAddress;
                    zCommand.Parameters.Add("@OrderStatusKey", SqlDbType.Int).Value = _OrderStatusKey;
                    zCommand.Parameters.Add("@OrderStatusName", SqlDbType.NVarChar).Value = _OrderStatusName;
                    zCommand.Parameters.Add("@PaymentStatusKey", SqlDbType.Int).Value = _PaymentStatusKey;
                    zCommand.Parameters.Add("@PaymentStatusName", SqlDbType.NVarChar).Value = _PaymentStatusName;
                    if (_DeliverDate == null)
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = _DeliverDate;
                    }

                    if (_ReturnDate == null)
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = _ReturnDate;
                    }

                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = _OrganizationKey;
                    zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = _OrganizationID;
                    zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = _OrganizationName;
                    zCommand.Parameters.Add("@TotalAmount", SqlDbType.Money).Value = _TotalAmount;
                    zCommand.Parameters.Add("@SaleAmount", SqlDbType.Money).Value = _SaleAmount;
                    zCommand.Parameters.Add("@OrderAmount", SqlDbType.Money).Value = _OrderAmount;
                    zCommand.Parameters.Add("@ReceiptAmount", SqlDbType.Money).Value = _ReceiptAmount;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }


            public string Delete()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[FNC_Order_FED] Set RecordStatus = 99 WHERE OrderKey = @OrderKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[FNC_Order_FED] WHERE OrderKey = @OrderKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            #endregion
        }
        #endregion
    }
}
