﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using TN_Connection;
using TN_Tools;
using TN_User;

namespace TN_WinApp.FNC
{
    public partial class Frm_OrderList : Form
    {
        private int _CategoryKey = 0;
        private string _FormUrl = "/FNC/Frm_OrderList";

        public int CategoryKey
        {
            get
            {
                return _CategoryKey;
            }

            set
            {
                _CategoryKey = value;
            }
        }
        public string FormUrl
        {
            get
            {
                return _FormUrl;
            }

            set
            {
                _FormUrl = value;
            }
        }

        public Frm_OrderList()
        {
            InitializeComponent();

            TitleForm.MouseDown += Frm_MouseDown;
            TitleForm.MouseMove += Frm_MouseMove;
            TitleForm.MouseUp += Frm_MouseUp;

            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;

            btnAddNew.Click += BtnAddNew_Click;
            btnSearch.Click += BtnSearch_Click;
            LVData.ItemActivate += LVData_ItemActivate;
            LVData.KeyDown += LVData_KeyDown;

            this.Text = "Thông tin đơn hàng/ biên nhận";
            this.WindowState = FormWindowState.Maximized;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.FormBorderStyle = FormBorderStyle.None;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.Load += Frm_OrderList_Load;
        }

        private void Frm_OrderList_Load(object sender, EventArgs e)
        {
            Get_Auth();
            if (_RoleForm.RoleRead)
            {
                DateTime zDate = DateTime.Now;
                DateTime FromDate = new DateTime(zDate.Year, zDate.Month, 1, 0, 0, 0);
                DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
                ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
                dteFromDate.Value = FromDate;
                dteToDate.Value = ToDate;
                DesignLayout(LVData);
                LoadListView();
            }
        }

        #region [Event]
        private void BtnAddNew_Click(object sender, EventArgs e)
        {
            Frm_OrderEdit Frm = new Frm_OrderEdit();
            Frm.OrderKey = string.Empty;
            Frm.ShowDialog();
            if (Frm.OrderKey != string.Empty)
            {
                LoadListView();
            }
        }



        private void BtnSearch_Click(object sender, EventArgs e)
        {
            LoadListView();
        }
        #endregion

        #region [Process]
        private string Delete(string OrderKey)
        {
            Order_FED_Info zInfo = new Order_FED_Info(OrderKey);
            zInfo.Empty();
            zInfo.Empty_Detail();
            return zInfo.Message;
        }
        #endregion

        #region [ListView]
        private void DesignLayout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã biên nhận";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
            colHead = new ColumnHeader();
            colHead.Text = "Khách hàng";
            colHead.Width = 250;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "Số tiền";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ghi chú";
            colHead.Width = 250;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            TNPaintControl.DrawLVStyle(ref LVData);
        }
        private void LoadListView()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable zTable = Access_Data.ListOrder(SessionUser.UserLogin.PartnerNumber, txtSearch.Text.Trim(), CategoryKey, dteFromDate.Value, dteToDate.Value);
            LV.Items.Clear();
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow zRow = zTable.Rows[i];
                lvi.Tag = zRow["OrderKey"].ToString();
                lvi.BackColor = Color.White;

                DateTime zDate = DateTime.Parse(zRow["OrderDate"].ToString());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zDate.ToString("dd/MM/yyyy");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["OrderID"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["BuyerName"].ToString();
                lvi.SubItems.Add(lvsi);

                double zAmount = zRow["OrderAmount"].ToDouble();
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zAmount.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["OrderDescription"].ToString();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;

        }
        private void LVData_ItemActivate(object sender, EventArgs e)
        {
            if (LVData.SelectedItems.Count > 0)
            {
                string Key = LVData.SelectedItems[0].Tag.ToString();
                Frm_OrderEdit Frm = new Frm_OrderEdit();
                Frm.OrderKey = Key;
                Frm.ShowDialog();
                LoadListView();
            }
        }
        private void LVData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (_RoleForm.RoleDel)
                {
                    DialogResult dlr = MessageBox.Show("Thao tác này sẽ xóa tất cả mọi thông tin liên quan đến đơn hàng này !.", "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                    string Message = "";
                    if (dlr == DialogResult.Yes && LVData.SelectedItems.Count > 0)
                    {
                        for (int i = 0; i < LVData.SelectedItems.Count; i++)
                        {
                            string Key = LVData.SelectedItems[i].Tag.ToString();
                            string zResult = Delete(Key);

                            if (zResult.Substring(0, 3) == "200" ||
                                zResult.Substring(0, 3) == "201")
                            {
                                Message = string.Empty;
                            }
                            else
                            {
                                Message = zResult;
                            }
                        }

                        if (Message == string.Empty)
                        {
                            LoadListView();
                        }
                        else
                        {
                            MessageBox.Show(Message);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Bạn không có quyền xóa !");
                }
            }
        }
        #endregion

        #region [Dùng kéo rê form]
        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;

        private void Frm_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }
        private void Frm_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }
        private void Frm_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        #region [Auth]
        User_Role_Info _RoleForm = new User_Role_Info();
        private void Get_Auth()
        {
            List<User_Role_Info> zListRole = SessionUser.TableRole;
            if (zListRole.Count > 0)
            {
                try
                {
                    _RoleForm = zListRole.SingleOrDefault(r => r.RoleURL == FormUrl);
                    if (_RoleForm == null)
                    {
                        _RoleForm = new User_Role_Info();
                        MessageBox.Show("Form này chưa được thêm quyền, vui lòng liên hệ IT !.");
                        this.Close();
                    }
                    else
                    {
                        if (!_RoleForm.RoleRead)
                        {
                            MessageBox.Show("Bạn không có quyền xem, vui lòng liên hệ IT !.");
                            this.Close();
                        }
                        if (!_RoleForm.RoleEdit)
                        {

                        }
                        if (!_RoleForm.RoleAdd)
                        {

                        }
                        if (!_RoleForm.RoleDel)
                        {

                        }
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Bạn chưa được phân quyền, vui lòng liên hệ IT !.");
            }
        }
        #endregion       

        #region[Access_Data]
        public class Access_Data
        {
            public static DataTable ListOrder(string PartnerNumber, string OrderID, int CategoryKey, DateTime FromDate, DateTime ToDate)
            {
                DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
                DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

                DataTable zTable = new DataTable();
                string zSQL = @"
SELECT * 
FROM FNC_Order_FED
WHERE RecordStatus <> 99  
AND PartnerNumber= @PartnerNumber
AND OrderDate BETWEEN @FromDate AND @ToDate";
                if (CategoryKey != 0)
                {
                    zSQL += " AND CategoryKey = @CategoryKey";
                }
                if (OrderID.Trim().Length > 0)
                {
                    zSQL += " AND OrderID LIKE @Name";
                }

                zSQL += " ORDER BY OrderDate DESC";
                string zConnectionString = ConnectDataBase.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                    zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + OrderID + "%";
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                }
                catch (Exception ex)
                {
                    string zstrMessage = ex.ToString();
                }
                return zTable;
            }
        }
        public class Order_FED_Info
        {
            #region [ Field Name ]
            private string _OrderKey = "";
            private string _OrderID = "";
            private DateTime? _OrderDate = null;
            private string _OrderDescription = "";
            private string _SellerKey = "";
            private string _SellerName = "";
            private string _BuyerKey = "";
            private string _BuyerName = "";
            private string _BuyerPhone = "";
            private string _BuyerAddress = "";
            private int _OrderStatusKey = 0;
            private string _OrderStatusName = "";
            private int _PaymentStatusKey = 0;
            private string _PaymentStatusName = "";
            private DateTime? _DeliverDate = null;
            private DateTime? _ReturnDate = null;
            private int _CategoryKey = 0;
            private string _CategoryName = "";
            private int _OrganizationKey = 0;
            private string _OrganizationID = "";
            private string _OrganizationName = "";
            private double _TotalAmount = 0;
            private double _SaleAmount = 0;
            private double _OrderAmount = 0;
            private double _ReceiptAmount = 0;
            private string _PartnerNumber = "";
            private int _RecordStatus = 0;
            private DateTime? _CreatedOn = null;
            private string _CreatedBy = "";
            private string _CreatedName = "";
            private DateTime? _ModifiedOn = null;
            private string _ModifiedBy = "";
            private string _ModifiedName = "";
            private string _Message = "";
            #endregion

            #region [ Constructor Get Information ]
            public Order_FED_Info()
            {
                Guid zNewID = Guid.NewGuid();
                _OrderKey = zNewID.ToString();
            }
            public Order_FED_Info(string OrderKey)
            {
                string zSQL = "SELECT * FROM [dbo].[FNC_Order_FED] WHERE OrderKey = @OrderKey AND RecordStatus != 99 ";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(OrderKey);
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        _OrderKey = zReader["OrderKey"].ToString();
                        _OrderID = zReader["OrderID"].ToString();
                        if (zReader["OrderDate"] != DBNull.Value)
                        {
                            _OrderDate = (DateTime)zReader["OrderDate"];
                        }

                        _OrderDescription = zReader["OrderDescription"].ToString();
                        _SellerKey = zReader["SellerKey"].ToString();
                        _SellerName = zReader["SellerName"].ToString();
                        _BuyerKey = zReader["BuyerKey"].ToString();
                        _BuyerName = zReader["BuyerName"].ToString();
                        _BuyerPhone = zReader["BuyerPhone"].ToString();
                        _BuyerAddress = zReader["BuyerAddress"].ToString();
                        _OrderStatusKey = int.Parse(zReader["OrderStatusKey"].ToString());
                        _OrderStatusName = zReader["OrderStatusName"].ToString();
                        _PaymentStatusKey = int.Parse(zReader["PaymentStatusKey"].ToString());
                        _PaymentStatusName = zReader["PaymentStatusName"].ToString();
                        if (zReader["DeliverDate"] != DBNull.Value)
                        {
                            _DeliverDate = (DateTime)zReader["DeliverDate"];
                        }

                        if (zReader["ReturnDate"] != DBNull.Value)
                        {
                            _ReturnDate = (DateTime)zReader["ReturnDate"];
                        }

                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                        _CategoryName = zReader["CategoryName"].ToString();
                        _OrganizationKey = int.Parse(zReader["OrganizationKey"].ToString());
                        _OrganizationID = zReader["OrganizationID"].ToString();
                        _OrganizationName = zReader["OrganizationName"].ToString();
                        _TotalAmount = double.Parse(zReader["TotalAmount"].ToString());
                        _SaleAmount = double.Parse(zReader["SaleAmount"].ToString());
                        _OrderAmount = double.Parse(zReader["OrderAmount"].ToString());
                        _ReceiptAmount = double.Parse(zReader["ReceiptAmount"].ToString());
                        _PartnerNumber = zReader["PartnerNumber"].ToString();
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        if (zReader["CreatedOn"] != DBNull.Value)
                        {
                            _CreatedOn = (DateTime)zReader["CreatedOn"];
                        }

                        _CreatedBy = zReader["CreatedBy"].ToString();
                        _CreatedName = zReader["CreatedName"].ToString();
                        if (zReader["ModifiedOn"] != DBNull.Value)
                        {
                            _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                        }

                        _ModifiedBy = zReader["ModifiedBy"].ToString();
                        _ModifiedName = zReader["ModifiedName"].ToString();
                        _Message = "200 OK";
                    }
                    else
                    {
                        _Message = "404 Not Found";
                    }
                    zReader.Close();
                    zCommand.Dispose();
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
            }

            #endregion

            #region [ Properties ]
            public string OrderKey
            {
                get { return _OrderKey; }
                set { _OrderKey = value; }
            }
            public string OrderID
            {
                get { return _OrderID; }
                set { _OrderID = value; }
            }
            public DateTime? OrderDate
            {
                get { return _OrderDate; }
                set { _OrderDate = value; }
            }
            public string OrderDescription
            {
                get { return _OrderDescription; }
                set { _OrderDescription = value; }
            }
            public string SellerKey
            {
                get { return _SellerKey; }
                set { _SellerKey = value; }
            }
            public string SellerName
            {
                get { return _SellerName; }
                set { _SellerName = value; }
            }
            public string BuyerKey
            {
                get { return _BuyerKey; }
                set { _BuyerKey = value; }
            }
            public string BuyerName
            {
                get { return _BuyerName; }
                set { _BuyerName = value; }
            }
            public string BuyerPhone
            {
                get { return _BuyerPhone; }
                set { _BuyerPhone = value; }
            }
            public string BuyerAddress
            {
                get { return _BuyerAddress; }
                set { _BuyerAddress = value; }
            }
            public int OrderStatusKey
            {
                get { return _OrderStatusKey; }
                set { _OrderStatusKey = value; }
            }
            public string OrderStatusName
            {
                get { return _OrderStatusName; }
                set { _OrderStatusName = value; }
            }
            public int PaymentStatusKey
            {
                get { return _PaymentStatusKey; }
                set { _PaymentStatusKey = value; }
            }
            public string PaymentStatusName
            {
                get { return _PaymentStatusName; }
                set { _PaymentStatusName = value; }
            }
            public DateTime? DeliverDate
            {
                get { return _DeliverDate; }
                set { _DeliverDate = value; }
            }
            public DateTime? ReturnDate
            {
                get { return _ReturnDate; }
                set { _ReturnDate = value; }
            }
            public int CategoryKey
            {
                get { return _CategoryKey; }
                set { _CategoryKey = value; }
            }
            public string CategoryName
            {
                get { return _CategoryName; }
                set { _CategoryName = value; }
            }
            public int OrganizationKey
            {
                get { return _OrganizationKey; }
                set { _OrganizationKey = value; }
            }
            public string OrganizationID
            {
                get { return _OrganizationID; }
                set { _OrganizationID = value; }
            }
            public string OrganizationName
            {
                get { return _OrganizationName; }
                set { _OrganizationName = value; }
            }
            public double TotalAmount
            {
                get { return _TotalAmount; }
                set { _TotalAmount = value; }
            }
            public double SaleAmount
            {
                get { return _SaleAmount; }
                set { _SaleAmount = value; }
            }
            public double OrderAmount
            {
                get { return _OrderAmount; }
                set { _OrderAmount = value; }
            }
            public double ReceiptAmount
            {
                get { return _ReceiptAmount; }
                set { _ReceiptAmount = value; }
            }
            public string PartnerNumber
            {
                get { return _PartnerNumber; }
                set { _PartnerNumber = value; }
            }
            public int RecordStatus
            {
                get { return _RecordStatus; }
                set { _RecordStatus = value; }
            }
            public DateTime? CreatedOn
            {
                get { return _CreatedOn; }
                set { _CreatedOn = value; }
            }
            public string CreatedBy
            {
                get { return _CreatedBy; }
                set { _CreatedBy = value; }
            }
            public string CreatedName
            {
                get { return _CreatedName; }
                set { _CreatedName = value; }
            }
            public DateTime? ModifiedOn
            {
                get { return _ModifiedOn; }
                set { _ModifiedOn = value; }
            }
            public string ModifiedBy
            {
                get { return _ModifiedBy; }
                set { _ModifiedBy = value; }
            }
            public string ModifiedName
            {
                get { return _ModifiedName; }
                set { _ModifiedName = value; }
            }
            public string Code
            {
                get
                {
                    if (_Message.Length >= 3)
                    {
                        return _Message.Substring(0, 3);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            public string Message
            {
                get { return _Message; }
                set { _Message = value; }
            }
            #endregion

            #region [ Constructor Update Information ]
            public string Create_ServerKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[FNC_Order_FED] ("
            + " OrderID ,OrderDate ,OrderDescription ,SellerKey ,SellerName ,BuyerKey ,BuyerName ,BuyerPhone ,BuyerAddress ,OrderStatusKey ,OrderStatusName ,PaymentStatusKey ,PaymentStatusName ,DeliverDate ,ReturnDate ,CategoryKey ,CategoryName ,OrganizationKey ,OrganizationID ,OrganizationName ,TotalAmount ,SaleAmount ,OrderAmount ,ReceiptAmount ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@OrderID ,@OrderDate ,@OrderDescription ,@SellerKey ,@SellerName ,@BuyerKey ,@BuyerName ,@BuyerPhone ,@BuyerAddress ,@OrderStatusKey ,@OrderStatusName ,@PaymentStatusKey ,@PaymentStatusName ,@DeliverDate ,@ReturnDate ,@CategoryKey ,@CategoryName ,@OrganizationKey ,@OrganizationID ,@OrganizationName ,@TotalAmount ,@SaleAmount ,@OrderAmount ,@ReceiptAmount ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID;
                    if (_OrderDate == null)
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                    }

                    zCommand.Parameters.Add("@OrderDescription", SqlDbType.NVarChar).Value = _OrderDescription;
                    if (_SellerKey != string.Empty && _SellerKey.Length == 36)
                    {
                        zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_SellerKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }
                    zCommand.Parameters.Add("@SellerName", SqlDbType.NVarChar).Value = _SellerName;
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_BuyerKey);
                    zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = _BuyerName;
                    zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = _BuyerPhone;
                    zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = _BuyerAddress;
                    zCommand.Parameters.Add("@OrderStatusKey", SqlDbType.Int).Value = _OrderStatusKey;
                    zCommand.Parameters.Add("@OrderStatusName", SqlDbType.NVarChar).Value = _OrderStatusName;
                    zCommand.Parameters.Add("@PaymentStatusKey", SqlDbType.Int).Value = _PaymentStatusKey;
                    zCommand.Parameters.Add("@PaymentStatusName", SqlDbType.NVarChar).Value = _PaymentStatusName;
                    if (_DeliverDate == null)
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = _DeliverDate;
                    }

                    if (_ReturnDate == null)
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = _ReturnDate;
                    }

                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = _OrganizationKey;
                    zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = _OrganizationID;
                    zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = _OrganizationName;
                    zCommand.Parameters.Add("@TotalAmount", SqlDbType.Money).Value = _TotalAmount;
                    zCommand.Parameters.Add("@SaleAmount", SqlDbType.Money).Value = _SaleAmount;
                    zCommand.Parameters.Add("@OrderAmount", SqlDbType.Money).Value = _OrderAmount;
                    zCommand.Parameters.Add("@ReceiptAmount", SqlDbType.Money).Value = _ReceiptAmount;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Create_ClientKey()
            {
                //---------- String SQL Access Database ---------------
                string zSQL = "INSERT INTO [dbo].[FNC_Order_FED] ("
            + " OrderKey ,OrderID ,OrderDate ,OrderDescription ,SellerKey ,SellerName ,BuyerKey ,BuyerName ,BuyerPhone ,BuyerAddress ,OrderStatusKey ,OrderStatusName ,PaymentStatusKey ,PaymentStatusName ,DeliverDate ,ReturnDate ,CategoryKey ,CategoryName ,OrganizationKey ,OrganizationID ,OrganizationName ,TotalAmount ,SaleAmount ,OrderAmount ,ReceiptAmount ,PartnerNumber ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
             + " VALUES ( "
             + "@OrderKey ,@OrderID ,@OrderDate ,@OrderDescription ,@SellerKey ,@SellerName ,@BuyerKey ,@BuyerName ,@BuyerPhone ,@BuyerAddress ,@OrderStatusKey ,@OrderStatusName ,@PaymentStatusKey ,@PaymentStatusName ,@DeliverDate ,@ReturnDate ,@CategoryKey ,@CategoryName ,@OrganizationKey ,@OrganizationID ,@OrganizationName ,@TotalAmount ,@SaleAmount ,@OrderAmount ,@ReceiptAmount ,@PartnerNumber ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID;
                    if (_OrderDate == null)
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                    }

                    zCommand.Parameters.Add("@OrderDescription", SqlDbType.NVarChar).Value = _OrderDescription;
                    if (_SellerKey != string.Empty && _SellerKey.Length == 36)
                    {
                        zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_SellerKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }

                    zCommand.Parameters.Add("@SellerName", SqlDbType.NVarChar).Value = _SellerName;
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_BuyerKey);
                    zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = _BuyerName;
                    zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = _BuyerPhone;
                    zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = _BuyerAddress;
                    zCommand.Parameters.Add("@OrderStatusKey", SqlDbType.Int).Value = _OrderStatusKey;
                    zCommand.Parameters.Add("@OrderStatusName", SqlDbType.NVarChar).Value = _OrderStatusName;
                    zCommand.Parameters.Add("@PaymentStatusKey", SqlDbType.Int).Value = _PaymentStatusKey;
                    zCommand.Parameters.Add("@PaymentStatusName", SqlDbType.NVarChar).Value = _PaymentStatusName;
                    if (_DeliverDate == null)
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = _DeliverDate;
                    }

                    if (_ReturnDate == null)
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = _ReturnDate;
                    }

                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = _OrganizationKey;
                    zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = _OrganizationID;
                    zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = _OrganizationName;
                    zCommand.Parameters.Add("@TotalAmount", SqlDbType.Money).Value = _TotalAmount;
                    zCommand.Parameters.Add("@SaleAmount", SqlDbType.Money).Value = _SaleAmount;
                    zCommand.Parameters.Add("@OrderAmount", SqlDbType.Money).Value = _OrderAmount;
                    zCommand.Parameters.Add("@ReceiptAmount", SqlDbType.Money).Value = _ReceiptAmount;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "201 Created";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Update()
            {
                string zSQL = "UPDATE [dbo].[FNC_Order_FED] SET "
                            + " OrderID = @OrderID,"
                            + " OrderDate = @OrderDate,"
                            + " OrderDescription = @OrderDescription,"
                            + " SellerKey = @SellerKey,"
                            + " SellerName = @SellerName,"
                            + " BuyerKey = @BuyerKey,"
                            + " BuyerName = @BuyerName,"
                            + " BuyerPhone = @BuyerPhone,"
                            + " BuyerAddress = @BuyerAddress,"
                            + " OrderStatusKey = @OrderStatusKey,"
                            + " OrderStatusName = @OrderStatusName,"
                            + " PaymentStatusKey = @PaymentStatusKey,"
                            + " PaymentStatusName = @PaymentStatusName,"
                            + " DeliverDate = @DeliverDate,"
                            + " ReturnDate = @ReturnDate,"
                            + " CategoryKey = @CategoryKey,"
                            + " CategoryName = @CategoryName,"
                            + " OrganizationKey = @OrganizationKey,"
                            + " OrganizationID = @OrganizationID,"
                            + " OrganizationName = @OrganizationName,"
                            + " TotalAmount = @TotalAmount,"
                            + " SaleAmount = @SaleAmount,"
                            + " OrderAmount = @OrderAmount,"
                            + " ReceiptAmount = @ReceiptAmount,"
                            + " PartnerNumber = @PartnerNumber,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedOn = GetDate(),"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName"
                           + " WHERE OrderKey = @OrderKey";
                string zResult = "";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID;
                    if (_OrderDate == null)
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                    }

                    zCommand.Parameters.Add("@OrderDescription", SqlDbType.NVarChar).Value = _OrderDescription;
                    if (_SellerKey != string.Empty && _SellerKey.Length == 36)
                    {
                        zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_SellerKey);
                    }
                    else
                    {
                        zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                    }
                    zCommand.Parameters.Add("@SellerName", SqlDbType.NVarChar).Value = _SellerName;
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_BuyerKey);
                    zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = _BuyerName;
                    zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = _BuyerPhone;
                    zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = _BuyerAddress;
                    zCommand.Parameters.Add("@OrderStatusKey", SqlDbType.Int).Value = _OrderStatusKey;
                    zCommand.Parameters.Add("@OrderStatusName", SqlDbType.NVarChar).Value = _OrderStatusName;
                    zCommand.Parameters.Add("@PaymentStatusKey", SqlDbType.Int).Value = _PaymentStatusKey;
                    zCommand.Parameters.Add("@PaymentStatusName", SqlDbType.NVarChar).Value = _PaymentStatusName;
                    if (_DeliverDate == null)
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@DeliverDate", SqlDbType.DateTime).Value = _DeliverDate;
                    }

                    if (_ReturnDate == null)
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                    else
                    {
                        zCommand.Parameters.Add("@ReturnDate", SqlDbType.DateTime).Value = _ReturnDate;
                    }

                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                    zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = _OrganizationKey;
                    zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = _OrganizationID;
                    zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = _OrganizationName;
                    zCommand.Parameters.Add("@TotalAmount", SqlDbType.Money).Value = _TotalAmount;
                    zCommand.Parameters.Add("@SaleAmount", SqlDbType.Money).Value = _SaleAmount;
                    zCommand.Parameters.Add("@OrderAmount", SqlDbType.Money).Value = _OrderAmount;
                    zCommand.Parameters.Add("@ReceiptAmount", SqlDbType.Money).Value = _ReceiptAmount;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(_PartnerNumber);
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Delete()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "UPDATE [dbo].[FNC_Order_FED] Set RecordStatus = 99 WHERE OrderKey = @OrderKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Delete_Detail()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = @"
UPDATE [dbo].[FNC_Order_Employee_FED] Set RecordStatus = 99 WHERE OrderKey = @OrderKey
UPDATE [dbo].[FNC_Order_Item_FED] Set RecordStatus = 99 WHERE OrderKey = @OrderKey
";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = "DELETE FROM [dbo].[FNC_Order_FED] WHERE OrderKey = @OrderKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            public string Empty_Detail()
            {
                string zResult = "";
                //---------- String SQL Access Database ---------------
                string zSQL = @"
DELETE FROM [dbo].[FNC_Order_Employee_FED] WHERE OrderKey = @OrderKey
DELETE FROM [dbo].[FNC_Order_Item_FED] WHERE OrderKey = @OrderKey
DELETE FROM [dbo].[FNC_Receipt] WHERE DocumentID = @OrderID
";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = OrderID;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    _Message = "200 OK";
                }
                catch (Exception Err)
                {
                    _Message = "501 " + Err.ToString();
                }
                finally
                {
                    zConnect.Close();
                }
                return zResult;
            }
            #endregion
        }
        #endregion]
    }
}
